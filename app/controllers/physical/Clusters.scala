package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Clusters controller.
 */
object Clusters extends Controller with Secured {

  val Home = Redirect(routes.Clusters.list(0, 2, ""))

  /**
   * Describe the cluster form (used in edit screen).
   */ 
  val clusterForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "name" -> nonEmptyText,
      "domain" -> nonEmptyText
    )(Cluster.apply)(Cluster.unapply)
  )

  /**
   * Display the paginated list of clusters.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on clusters names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(
      html.clusters.list(Cluster.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")), orderBy, filter)
    )
  }

  /**
   * Clusters view.
   */
  def view(id: Long) = IsAuthenticated { username => _ =>
    Cluster.find(id).map { cluster =>
      Ok(
	html.clusters.view("I am a cluster!", cluster)
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Clusters edit.
   */
  def edit(id: Long) = IsAuthenticated { username => _ =>
    Cluster.find(id).map { cluster =>
      Ok(
	html.clusters.edit(id, clusterForm.fill(cluster))
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Handle the 'edit cluster form' submission 
   *
   * @param id Id of the cluster to edit
   */
  def update(id: Long) = Action { implicit request =>
    clusterForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.clusters.edit(id, formWithErrors)),
      cluster => {
        Cluster.update(id, cluster)
        Home.flashing("success" -> "Cluster %s has been updated".format(cluster.name))
      }
    )
  }
  
  /**
   * Display the 'new cluster form'.
   */
  def create = Action {
    Ok(html.clusters.create(clusterForm))
  }

  /**
   * Handle the 'new cluster form' submission.
   */
  def save = Action { implicit request =>
    clusterForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.clusters.create(formWithErrors)),
      cluster => {
        Cluster.insert(cluster)
        Home.flashing("success" -> "Cluster %s has been created".format(cluster.name))
      }
    )
  }

  /**
   * Handle the 'destroy cluster' action.
   */
  def delete(id: Long) = Action { implicit request =>
    Cluster.delete(id)
    Home.flashing("success" -> "Cluster has been deleted")
  }

}

