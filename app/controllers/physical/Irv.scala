package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Manage data centre related operations.
 */
object Irv extends Controller with Secured {

  /**
   * Display the data centre index page.
   */
  def index = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.irv.index(
          "Data centre index - concurrentCOMMAND",
          user
        )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Display the data centre plan view page (index).
   */
  def rack_view = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.irv.rack_view(
          "Interactive rack view - concurrentCOMMAND",
          user
        )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Data centre configuration.
   */
  def configuration = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.irv.configuration()
      )
    }.getOrElse(Forbidden)
  }

}
