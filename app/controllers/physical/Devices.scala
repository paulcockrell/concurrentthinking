package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Devices controller.
 */
object Devices extends Controller with Secured {

  val Home = Redirect(routes.Devices.list(0, 2, ""))

  /**
   * Describe the device form (used in edit screen).
   */ 
  val deviceForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "manufacturer" -> nonEmptyText,
      "model" -> nonEmptyText,
      "rack" -> nonEmptyText
    )(Device.apply)(Device.unapply)
  )

  /**
   * Display the paginated list of devices.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on devices names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(
      html.physical.devices.list(Device.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")), orderBy, filter)
    )
  }

  /**
   * Device view.
   */
  def view(id: Long) = IsAuthenticated { username => _ =>
    Device.find(id).map { device =>
      Ok(
      	html.physical.devices.view("I am a device!", device)
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Device metrics.
   */
  def metrics(id: Long) = IsAuthenticated { username => _ =>
    Device.find(id).map { device =>
      Ok(
      	html.physical.devices.metrics("I am a device!", device)
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Device management.
   */
  def management(id: Long) = IsAuthenticated { username => _ =>
    Device.find(id).map { device =>
      Ok(
      	html.physical.devices.management("I am a device!", device)
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Device SNMP.
   */
  def snmp(id: Long) = IsAuthenticated { username => _ =>
    Device.find(id).map { device =>
      Ok(
      	html.physical.devices.snmp("I am a device!", device)
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Device edit.
   */
  def edit(id: Long) = IsAuthenticated { username => _ =>
    Device.find(id).map { device =>
      Ok(
	      html.physical.devices.edit(id, deviceForm.fill(device))
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Handle the 'edit device form' submission 
   *
   * @param id Id of the cluster to edit
   */
  def update(id: Long) = Action { implicit request =>
    deviceForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.physical.devices.edit(id, formWithErrors)),
      device => {
	      Device.update(id, device)
	      Home.flashing("success" -> "Device %s has been updated".format(device.name))
      }
    )
  }
  
  /**
   * Display the 'new device form'.
   */
  def create = Action {
    Ok(html.physical.devices.create(deviceForm))
  }

  /**
   * Handle the 'new device form' submission.
   */
  def save = Action { implicit request =>
    deviceForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.physical.devices.create(formWithErrors)),
      device => {
	      Device.insert(device)
	      Home.flashing("success" -> "Device %s has been created".format(device.name))
      }
    )
  }

  /**
   * Handle the 'destroy device' action.
   */
  def delete(id: Long) = Action { implicit request =>
    Device.delete(id)
    Home.flashing("success" -> "Device has been deleted")
  }

}


