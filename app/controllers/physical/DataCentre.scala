package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Manage data centre related operations.
 */
object DataCentre extends Controller with Secured {

  /**
   * Display the data centre index page.
   */
  def index = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.data_centre.index(
          "Data centre index - concurrentCOMMAND",
          user
        )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Display the data centre plan view page
   */
  def plan_view = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.data_centre.plan_view(
          "Data centre plan view - concurrentCOMMAND",
          user
        )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Display the data centre 3D page
   */
  def threedee = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.data_centre.threedee(
          "Data centre 3D - concurrentCOMMAND",
          user
        )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Data centre configuration.
   */
  def configuration = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.data_centre.configuration()
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Data centre rack api.
   */
  def rack_defenition = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.physical.data_centre.rack_defenition()
      )
    }.getOrElse(Forbidden)
  }

}
