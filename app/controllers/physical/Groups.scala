package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Groups controller.
 */
object Groups extends Controller with Secured {

  val Home = Redirect(routes.Groups.list(0, 2, ""))

  /**
   * Describe the group form (used in edit screen).
   */ 
  val groupForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "group_type" -> nonEmptyText,
      "feature_packs" -> nonEmptyText
    )(Group.apply)(Group.unapply)
  )

  /**
   * Display the paginated list of groups.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on groups names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(
      html.groups.list(Group.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")), orderBy, filter)
    )
  }

  /**
   * Group view.
   */
  def view(id: Long) = IsAuthenticated { username => _ =>
    Group.find(id).map { group =>
      Ok(
	html.groups.view("I am a group!", group)
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Group edit.
   */
  def edit(id: Long) = IsAuthenticated { username => _ =>
    Group.find(id).map { group =>
      Ok(
	html.groups.edit(id, groupForm.fill(group))
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Handle the 'edit group form' submission 
   *
   * @param id Id of the group to edit
   */
  def update(id: Long) = Action { implicit request =>
    groupForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.groups.edit(id, formWithErrors)),
      group => {
	Group.update(id, group)
	Home.flashing("success" -> "Group %s has been updated".format(group.name))
      }
    )
  }
  
  /**
   * Display the 'new group form'.
   */
  def create = Action {
    Ok(html.groups.create(groupForm))
  }

  /**
   * Handle the 'new group form' submission.
   */
  def save = Action { implicit request =>
    groupForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.groups.create(formWithErrors)),
      group => {
	Group.insert(group)
	Home.flashing("success" -> "Group %s has been created".format(group.name))
      }
    )
  }

  /**
   * Handle the 'destroy group' action.
   */
  def delete(id: Long) = Action { implicit request =>
    Group.delete(id)
    Home.flashing("success" -> "Group has been deleted")
  }

}



