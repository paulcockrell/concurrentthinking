package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import models._
import views._

/**
 * Bookmarks controller.
 */
object Bookmarks extends Controller with Secured {

  /**
   * Display the bookmarks index page.
   */
  def index = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.metadata.bookmarks.index(
          "Bookmarks - concurrentCOMMAND",
          user
        )
      )
    }.getOrElse(Forbidden)
  }

  /**
   * Display the snippet bookmarks index page.
   */
  def ajax_index = IsAuthenticated { username => _ =>
    User.findByEmail(username).map { user =>
      Ok(
        html.metadata.bookmarks.ajax_index.render("hello", user)
      )
    }.getOrElse(Forbidden)
  }
}

