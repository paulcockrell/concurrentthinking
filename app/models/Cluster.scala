package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class Cluster(id: Pk[Long] = NotAssigned, name: String, domain: String)

/**
 * Helper for pagination.
 */
case class Page[A](items: Seq[A], page: Int, offset: Long, total: Long) {
  lazy val prev = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
}

object Cluster {
  
  // -- Parsers
  
  /**
   * Parse a Cluster from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("cluster.id") ~
    get[String]("cluster.name") ~
    get[String]("cluster.domain") map {
      case id~name~domain => Cluster(id, name, domain)
    } 
  }
  
  // -- Queries
  
  /**
   * Retrieve a Cluster from name.
   */
  def find(id: Long): Option[Cluster] = {
    DB.withConnection { implicit connection =>
      SQL("select * from cluster where id = {id}")
	.on('id -> id)
	.as(Cluster.simple.singleOpt)
    }
  }

  /**
   * Retrieve first Cluster.
   */
  def first: Option[Cluster] = {
    DB.withConnection { implicit connection =>
      SQL("select * from cluster limit 1")
	.as(Cluster.simple.singleOpt)
    }
  }
  
  /**
   * Retrieve all clusters.
   */
  def findAll: Seq[Cluster] = {
    DB.withConnection { implicit connection =>
      SQL("select * from cluster").as(Cluster.simple *)
    }
  }
  
  /**
   * Create a Cluster.
   */
  def create(cluster: Cluster): Cluster = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          insert into cluster values (
            {name}, {domain}
          )
        """
      ).on(
        'name -> cluster.name,
        'domain -> cluster.domain
      ).executeUpdate()
      
      cluster
      
    }
  }

  /**
   * Update a cluster.
   *
   * @param id The cluster id
   * @param computer The cluster values.
   */
  def update(id: Long, cluster: Cluster) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          update cluster
          set name = {name}, domain = {domain}
          where id = {id}
        """
      ).on(
        'id -> id,
        'name -> cluster.name,
        'domain -> cluster.domain
      ).executeUpdate()
    }
  }
  
  /**
   * Insert a new cluster.
   *
   * @param cluster The cluster values.
   */
  def insert(cluster: Cluster) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          insert into cluster (name, domain) values (
            {name}, {domain}
          )
        """
      ).on(
        'name -> cluster.name,
        'domain -> cluster.domain
      ).executeUpdate()
    }
  }
  
  /**
   * Return a page of (Computer,Company).
   *
   * @param page Page to display
   * @param pageSize Number of computers per page
   * @param orderBy Computer property used for sorting
   * @param filter Filter applied on the name column
   */
  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[Cluster] = {
    
    val offest = pageSize * page
    
    DB.withConnection { implicit connection =>
      
      val clusters = SQL(
        """
          select * from cluster 
          where cluster.name like {filter}
          order by {orderBy} nulls last
          limit {pageSize} offset {offset}
        """
      ).on(
        'pageSize -> pageSize, 
        'offset -> offest,
        'filter -> filter,
        'orderBy -> orderBy
      ).as(Cluster.simple *)

      val totalRows = SQL(
        """
          select count(*) from cluster 
          where cluster.name like {filter}
        """
      ).on(
        'filter -> filter
      ).as(scalar[Long].single)

      Page(clusters, page, offest, totalRows)
      
    }
    
  }  

  /**
   * Delete a cluster.
   *
   * @param id Id of the cluster to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from cluster where id = {id}").on('id -> id).executeUpdate()
    }
  }

}
