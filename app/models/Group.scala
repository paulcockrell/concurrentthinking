package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class Group(id: Pk[Long] = NotAssigned, name: String, description: String, group_type: String, feature_packs: String)

object Group {
  
  // -- Parsers
  
  /**
   * Parse a Group from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("groups.id") ~
    get[String]("groups.name") ~
    get[String]("groups.description") ~
    get[String]("groups.group_type") ~
    get[String]("groups.feature_packs") map {
      case id~name~description~group_type~feature_packs => Group(id, name, description, group_type, feature_packs)
    } 
  }
  
  // -- Queries
  
  /**
   * Retrieve a Group from id.
   */
  def find(id: Long): Option[Group] = {
    DB.withConnection { implicit connection =>
      SQL("select * from groups where id = {id}")
	.on('id -> id)
	.as(Group.simple.singleOpt)
    }
  }

  /**
   * Retrieve first group.
   */
  def first: Option[Group] = {
    DB.withConnection { implicit connection =>
      SQL("select * from groups limit 1")
	.as(Group.simple.singleOpt)
    }
  }
  
  /**
   * Retrieve all groups.
   */
  def findAll: Seq[Group] = {
    DB.withConnection { implicit connection =>
      SQL("select * from groups").as(Group.simple *)
    }
  }
  
  /**
   * Create a Group.
   */
  def create(group: Group): Group = {
    DB.withConnection { implicit connection =>
      SQL(
	"""
	  insert into groups values (
	    {name}, {description}, {group_type}, {feature_packs}
	  )
	"""
      ).on(
	'name -> group.name,
	'description -> group.description,
	'group_type -> group.group_type,
	'feature_packs -> group.feature_packs
      ).executeUpdate()
      
      group
      
    }
  }

  /**
   * Update a group.
   *
   * @param id The group id
   * @param group The group values.
   */
  def update(id: Long, group: Group) = {
    DB.withConnection { implicit connection =>
      SQL(
	"""
	  update groups
	  set name = {name}, description = {description}, group_type = {group_type}, feature_packs = {feature_packs}
	  where id = {id}
	"""
      ).on(
	'id -> id,
	'name -> group.name,
	'description -> group.description,
	'group_type -> group.group_type,
	'feature_packs -> group.feature_packs
      ).executeUpdate()
    }
  }
  
  /**
   * Insert a new group.
   *
   * @param group The group values.
   */
  def insert(group: Group) = {
    DB.withConnection { implicit connection =>
      SQL(
	"""
	  insert into groups (name, description, group_type, feature_packs) values (
	    {name}, {description}, {group_type}, {feature_packs}
	  )
	"""
      ).on(
	'name -> group.name,
	'description -> group.description,
	'group_type -> group.group_type,
	'feature_packs -> group.feature_packs
      ).executeUpdate()
    }
  }
  
  /**
   * Return a page of groups
   *
   * @param page Page to display
   * @param pageSize Number of groups per page
   * @param orderBy group property used for sorting
   * @param filter Filter applied on the name column
   */
  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[Group] = {
    
    val offest = pageSize * page
    
    DB.withConnection { implicit connection =>
      
      val groups = SQL(
	"""
	  select * from groups 
	  where groups.name like {filter}
	  order by {orderBy} nulls last
	  limit {pageSize} offset {offset}
	"""
      ).on(
	'pageSize -> pageSize, 
	'offset -> offest,
	'filter -> filter,
	'orderBy -> orderBy
      ).as(Group.simple *)

      val totalRows = SQL(
	"""
	  select count(*) from groups 
	  where groups.name like {filter}
	"""
      ).on(
	'filter -> filter
      ).as(scalar[Long].single)

      Page(groups, page, offest, totalRows)
      
    }
    
  }  

  /**
   * Delete a group.
   *
   * @param id Id of the group to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from groups where id = {id}").on('id -> id).executeUpdate()
    }
  }

}


