package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class Device(id: Pk[Long] = NotAssigned, name: String, description: String, manufacturer: String, model: String, rack: String)

object Device {
  
  // -- Parsers
  
  /**
   * Parse a Cluster from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("device.id") ~
    get[String]("device.name") ~
    get[String]("device.description") ~
    get[String]("device.manufacturer") ~
    get[String]("device.model") ~
    get[String]("device.rack") map {
      case id~name~description~manufacturer~model~rack => Device(id, name, description, manufacturer, model, rack)
    } 
  }
  
  // -- Queries
  
  /**
   * Retrieve a Device from id.
   */
  def find(id: Long): Option[Device] = {
    DB.withConnection { implicit connection =>
      SQL("select * from device where id = {id}")
	.on('id -> id)
	.as(Device.simple.singleOpt)
    }
  }

  /**
   * Retrieve first device.
   */
  def first: Option[Device] = {
    DB.withConnection { implicit connection =>
      SQL("select * from device limit 1")
	.as(Device.simple.singleOpt)
    }
  }
  
  /**
   * Retrieve all devices.
   */
  def findAll: Seq[Device] = {
    DB.withConnection { implicit connection =>
      SQL("select * from device").as(Device.simple *)
    }
  }
  
  /**
   * Create a Device.
   */
  def create(device: Device): Device = {
    DB.withConnection { implicit connection =>
      SQL(
	"""
	  insert into device values (
	    {name}, {description}, {manufacturer}, {model}, {rack}
	  )
	"""
      ).on(
	'name -> device.name,
	'description -> device.description,
	'manufacturer -> device.manufacturer,
        'model -> device.model,
        'rack -> device.rack
      ).executeUpdate()
      
      device
      
    }
  }

  /**
   * Update a device.
   *
   * @param id The device id
   * @param device The device values.
   */
  def update(id: Long, device: Device) = {
    DB.withConnection { implicit connection =>
      SQL(
	"""
	  update device
	  set name = {name}, description = {description}, manufacturer = {manufacturer}, model = {model}, rack = {rack}
	  where id = {id}
	"""
      ).on(
	'id -> id,
	'name -> device.name,
	'description -> device.description,
	'manufacturer -> device.manufacturer,
        'model -> device.model,
        'rack -> device.rack
      ).executeUpdate()
    }
  }
  
  /**
   * Insert a new device.
   *
   * @param device The device values.
   */
  def insert(device: Device) = {
    DB.withConnection { implicit connection =>
      SQL(
	"""
	  insert into device (name, description, manufacturer, model, rack) values (
	    {name}, {description}, {manufacturer}, {model}, {rack}
	  )
	"""
      ).on(
	'name -> device.name,
	'description -> device.description,
	'manufacturer -> device.manufacturer,
        'model -> device.model,
        'rack -> device.rack
      ).executeUpdate()
    }
  }
  
  /**
   * Return a page of devices
   *
   * @param page Page to display
   * @param pageSize Number of devices per page
   * @param orderBy device property used for sorting
   * @param filter Filter applied on the name column
   */
  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[Device] = {
    
    val offest = pageSize * page
    
    DB.withConnection { implicit connection =>
      
      val devices = SQL(
	"""
	  select * from device 
	  where device.name like {filter}
	  order by {orderBy} nulls last
	  limit {pageSize} offset {offset}
	"""
      ).on(
	'pageSize -> pageSize, 
	'offset -> offest,
	'filter -> filter,
	'orderBy -> orderBy
      ).as(Device.simple *)

      val totalRows = SQL(
	"""
	  select count(*) from device 
	  where device.name like {filter}
	"""
      ).on(
	'filter -> filter
      ).as(scalar[Long].single)

      Page(devices, page, offest, totalRows)
      
    }
    
  }  

  /**
   * Delete a cluster.
   *
   * @param id Id of the cluster to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from device where id = {id}").on('id -> id).executeUpdate()
    }
  }

}

