define ['jquery', 'bootstrap', 'uniform', 'iButton', 'tipsy', 'collapsible', 'xBreadcrumbs'], ($) ->
  console.log "Common JS loaded"

  $(document).ready ->
    #===== iButton management =====
    #
    #
    $('.on_off :checkbox, .on_off :radio').iButton
      labelOn: "",
      labelOff: "",
      enableDrag: false
    
    
    $('.yes_no :checkbox, .yes_no :radio').iButton
    	labelOn: "On",
    	labelOff: "Off",
    	enableDrag: false

    
    $('.enabled_disabled :checkbox, .enabled_disabled :radio').iButton
    	labelOn: "Enabled",
    	labelOff: "Disabled",
    	enableDrag: false

    #===== Add classes for sub sidebar detection =====
    #
    #
    if $("div").hasClass "secNav"
      $("#sidebar").addClass "with"
    else
      $("#sidebar").addClass "without"
      $("#content").css "margin-left", "100px"
      $("#footer > .wrapper").addClass "fullOne"


    #===== Add class on #content resize. Needed for responsive grid =====
    #
    #
    $(window).resize(-> # Run resize on window load
      width = $("#content").width()
      if width < 769
        $("#content").addClass "under"
      else
        $("#content").removeClass "under"
    ).resize()
    

    #===== Button for showing up sidebar on iPad portrait mode. Appears on right top =====
    #
    #
    $("ul.userNav li a.sidebar").click ->
      $(".secNav").toggleClass "display"
    
    
    #===== Form elements styling =====
    #
    #
    $(".styled, input:radio, input:checkbox, .dataTables_length select").uniform()


    #===== Adding class to :last-child elements =====
    #
    #
    $(".subNav li:last-child a, .formRow:last-child, .userList li:last-child, table tbody tr:last-child td, .breadLinks ul li ul li:last-child, .fulldd li ul li:last-child, .niceList li:last-child").addClass "noBorderB"


    #===== Dropdown =====
    #
    #
    $('.dropdown-toggle').dropdown()

    #===== User nav dropdown =====
    #
    #
    $("a.leftUserDrop").click ->
      $(".leftUser").slideToggle 200
    
    $(document).bind "click", (e) ->
      $clicked = $(e.target)
      $(".leftUser").slideUp 200  unless $clicked.parents().hasClass("leftUserDrop")
    
    
    #===== User nav dropdown =====
    #
    #
    $(".mainUserNavDrop").click (evt) ->
      $(".mainUserNavDrop").each (idx, el) ->
        $(el).children(".mainUserNav").slideUp 100  if el isnt evt.currentTarget
    
      $(this).children(".mainUserNav").slideToggle 200  unless $(evt.srcElement).hasClass("exp")
    
    $(document).bind "click", (e) ->
      $clicked = $(e.target)
      $(".mainUserNav").slideUp 200  unless $clicked.parents().hasClass("mainNav")
    
    
    #===== Tooltips =====
    #
    #
    $(".tipN").tipsy
      gravity: "n"
      fade: true
      html: true
    
    $(".tipS").tipsy
      gravity: "s"
      fade: true
      html: true
    
    $(".tipW").tipsy
      gravity: "w"
      fade: true
      html: true
    
    $(".tipE").tipsy
      gravity: "e"
      fade: true
      html: true

    #===== Add classes for sub sidebar detection =====
    #
    #
    if $("div").hasClass("secNav")
      $("#sidebar").addClass "with"
    else
      $("#sidebar").addClass "without"
      $("#content").css "margin-left", "100px"
      $("#footer > .wrapper").addClass "fullOne"
    
    #===== Collapsible elements management =====
    #
    #
    $(".exp").click (el) ->
      $(".exp").each (idx, el_anon) ->
        $(el_anon).collapsible "close"  if el_anon isnt el.currentTarget
    
    
    $(".exp").collapsible
      defaultOpen: "current"
      cookieName: "navAct"
      cssOpen: "subOpened"
      cssClose: "subClosed"
      speed: 200
    
    $(".opened").collapsible
      defaultOpen: "opened,toggleOpened"
      cssOpen: "inactive"
      cssClose: "normal"
      speed: 200
    
    $(".closed").collapsible
      defaultOpen: ""
      cssOpen: "inactive"
      cssClose: "normal"
      speed: 200

    
    #===== Full width sidebar dropdown =====//
    $(".fulldd li").click ->
      $(this).children("ul").slideToggle 150
    
    $(document).bind "click", (e) ->
      $clicked = $(e.target)
      $(".fulldd li").children("ul").slideUp 150  unless $clicked.parents().hasClass("has")
    
    
    #===== Top panel search field =====//
    $(".userNav a.search").click ->
      $(".topSearch").fadeToggle 150
    
    
    #===== 2 responsive buttons (320px - 480px) =====//
    $(".iTop").click ->
      $("#sidebar").slideToggle 100
    
    $(".iButton").click ->
      $(".altMenu").slideToggle 100
    
    
    #===== Animated dropdown for the right links group on breadcrumbs line =====//
    $(".breadLinks ul li").click ->
      $(this).children("ul").slideToggle 150
    
    $(document).bind "click", (e) ->
      $clicked = $(e.target)
      $(".breadLinks ul li").children("ul").slideUp 150  unless $clicked.parents().hasClass("has")


    #== Adding class to :last-child elements ==//
    $(".subNav li:last-child a, .formRow:last-child, .userList li:last-child, table tbody tr:last-child td, .breadLinks ul li ul li:last-child, .fulldd li ul li:last-child, .niceList li:last-child").addClass "noBorderB"
    

    #===== Add classes for sub sidebar detection =====//
    if $("div").hasClass("secNav")
      $("#sidebar").addClass "with"
    
      #$('#content').addClass('withSide');
    else
      $("#sidebar").addClass "without"
      $("#content").css "margin-left", "100px" #.addClass('withoutSide');
      $("#footer > .wrapper").addClass "fullOne"


    #===== Breadcrumbs =====//
    $("#breadcrumbs").xBreadcrumbs()
