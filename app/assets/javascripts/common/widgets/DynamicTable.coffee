define [], () ->
  #===== Dynamic data table =====// 
  oTable = $(".dTable").dataTable(
    bJQueryUI: false
    bAutoWidth: false
    sPaginationType: "full_numbers"
    sDom: "<\"tablePars\"fl>t<\"tableFooter\"ip>"
    oLanguage:
      sLengthMenu: "<span class='showentries'>Show entries:</span> _MENU_"
  )
  
  #===== Dynamic table toolbars =====//     
  $("#dyn .tOptions").click ->
    $("#dyn .tablePars").slideToggle 200
  
  $("#dyn2 .tOptions").click ->
    $("#dyn2 .tablePars").slideToggle 200
  
  $(".tOptions").click ->
    $(this).toggleClass "act"
