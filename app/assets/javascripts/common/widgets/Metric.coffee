define ['common/util/Util', 'common/gfx/Easing'], (Util, Easing) ->

  class Metric

    # statics overwritten by config
    @ALPHA         : .5
    @ANIM_DURATION : 500
    @FADE_DURATION : 500

    @MODEL_DEPENDENCIES : { scaleMetrics: "scaleMetrics", metricData: "metricData", colourMaps: "colourMaps", colourScale: "colourScale" }

    # constants and run-time assigned statics
    @TYPE_ELLIPTICAL  : 0
    @TYPE_RECTANGULAR : 1


    constructor: (@group, @id, @gfx, @x, @y, @width, @height, @model, @type = Metric.TYPE_RECTANGULAR) ->
      @visible    = false
      @active     = false
      @horizontal = @width > @height
      @scale      = 0
      @assets     = []

      @subscriptions = []
      @subscriptions.push(@model[Metric.MODEL_DEPENDENCIES.scaleMetrics].subscribe(@toggleScale))
      @subscriptions.push(@model[Metric.MODEL_DEPENDENCIES.metricData].subscribe(@update))
      @subscriptions.push(@model[Metric.MODEL_DEPENDENCIES.colourMaps].subscribe(@update))
      @subscriptions.push(@model[Metric.MODEL_DEPENDENCIES.stat].subscribe(@update)) if Metric.MODEL_DEPENDENCIES.stat?

      @update()


    destroy: ->
      sub.dispose() for sub in @subscriptions


    setActive: (@active) ->
      if @active and @value? then @show() else @hide()

 
    update: =>
      metrics = @model[Metric.MODEL_DEPENDENCIES.metricData]()
      val     = metrics.values[@group][@id]

      @value = (if Metric.MODEL_DEPENDENCIES.stat? then val[@model[Metric.MODEL_DEPENDENCIES.stat]()] else null) ? val.value ? val if val?

      if @value?
        colour_scale = @model[Metric.MODEL_DEPENDENCIES.colourScale]()
        colour_map   = @model[Metric.MODEL_DEPENDENCIES.colourMaps]()[metrics.metricId]
        range        = colour_map.range
        @scale       = (@value - colour_map.low) / range
        @scale       = 0 if @scale < 0
        @scale       = 1 if @scale > 1

        if @active
          @show()
          @updateBar()
        else
          @hide()
      else
        @scale = null
        @hide()


    setCoords: (x, y) ->
      if @assets.length > 0 and (x isnt @x or y isnt @y)
        dx = x - @x
        dy = y - @y

        for asset in @assets
          @gfx.setAttributes(asset,
            x: dx + @gfx.getAttribute(asset, 'x')
            y: dy + @gfx.getAttribute(asset, 'y'))

      @x = x
      @y = y


    setSize: (width, height) ->
      return if width is @width and height is @height

      @width  = width
      @height = height

      #@draw() if @visible and @active


    updateBar: ->
      if @type is Metric.TYPE_RECTANGULAR then @updateRectBar() else @updateEllipseBar()


    updateRectBar: ->
      colour = '#' + @getColour().toString(16)
      unless @model[Metric.MODEL_DEPENDENCIES.scaleMetrics]()
        attrs =
          fill   : colour
          width  : @width
          height : @height
          y      : @y
      else

        if @horizontal
          attrs =
            fill  : colour
            width : @width * @scale

        else
          new_height = @height * @scale
          attrs =
            fill   : colour
            height : new_height
            y      : @y + @height - new_height

      @gfx.animate(@assets[0], attrs, Metric.ANIM_DURATION, Easing.Cubic.easeOut)


    updateEllipseBar: ->
      colour = '#' + @getColour().toString(16)
      @gfx.animate(@assets[0], { fill: colour, width: @width + Math.random(), height: @height }, Metric.ANIM_DURATION, Easing.Cubic.easeOut)

      if @model[Metric.MODEL_DEPENDENCIES.scaleMetrics]()
        if @horizontal
          bite_width = @width * (1 - @scale)
          @gfx.animate(@assets[1], { x: @x + @width - bite_width, width: bite_width }, Metric.ANIM_DURATION, Easing.Cubic.easeOut)
        else
          @gfx.animate(@assets[1], { y: @y, height: @height * (1 - @scale) }, Metric.ANIM_DURATION, Easing.Cubic.easeOut)
      else
        if @horizontal
          attrs = { width: 0, height: @height }
        else
          attrs = { width: @width, height: 0 }

        @gfx.animate(@assets[1], attrs, Metric.ANIM_DURATION, Easing.Cubic.easeOut)


    getColour: ->
      colours = @model[Metric.MODEL_DEPENDENCIES.colourScale]()

      return colours[0].col if @scale <= 0 or isNaN(@scale)
      return colours[colours.length - 1].col if @scale >= 1

      count = 0
      len   = colours.length
      while @scale > colours[count].pos
        ++count
      low  = colours[count - 1]
      high = colours[count]
      Util.blendColour(low.col, high.col, (@scale - low.pos) / (high.pos - low.pos))


    draw: ->
      @gfx.remove(asset) for asset in @assets
      @assets = []

      if @type is Metric.TYPE_RECTANGULAR
        @drawRect()
      else
        @drawEllipse()

      
    drawEllipse: ->
      colour = '#' + @getColour().toString(16)

      @assets.push(@gfx.addEllipse(
        x      : @x,
        y      : @y,
        width  : @width,
        height : @height,
        fill   : colour,
        alpha  : Metric.ALPHA
      ))

      if @horizontal
        bite_width = @width * (1 - @scale)

        @assets.push(@gfx.addRect(
          fx     : 'destination-out',
          x      : @x + @width - bite_width,
          y      : @y,
          width  : bite_width,
          height : @height,
          fill   : '#000000'
        ))
      else
        @assets.push(@gfx.addRect(
          fx     : 'destination-out',
          x      : @x,
          y      : @y,
          width  : @width,
          height : @height * (1 - @scale),
          fill   : '#000000'
        ))


    drawRect: ->
      colour = '#' + @getColour().toString(16)

      unless @model[Metric.MODEL_DEPENDENCIES.scaleMetrics]()
        @assets.push(@gfx.addRect({ x: @x, y: @y, width: @width, height: @height, alpha: Metric.ALPHA, fill: colour }))
      else

        if @horizontal
          @assets.push(@gfx.addRect({ x: @x, y: @y, width: @width * @scale, height: @height, alpha: Metric.ALPHA, fill: colour }))
        else
          new_height = @height * @scale
          @assets.push(@gfx.addRect({ x: @x, y: @y + @height - new_height, width: @width, height: new_height, alpha: Metric.ALPHA, fill: colour }))


    show: =>
      unless @visible
        @visible = true
        @draw()


    hide: =>
      if @visible
        @visible = false
        @gfx.remove(asset) for asset in @assets
        @assets = []


    fadeOut: ->
      if @assets.length > 0
        @visible = false
        @gfx.animate(@assets[0], { alpha: 0 }, Metric.FADE_DURATION, Easing.Quad.easeOut, @hide)


    fadeIn: ->
      unless @visible
        @show()
        @gfx.setAttributes(@assets[0], { alpha: 0 })
        @gfx.animate(@assets[0], { alpha: Metric.ALPHA }, Metric.FADE_DURATION, Easing.Quad.easeOut, @evAnimComplete)


    toggleScale: =>
      @updateBar() if @active and @value?

