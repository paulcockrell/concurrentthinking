define [], () ->
  $(".uSlider").slider() # Usual slider

  $(".uRange").slider # Range slider
    range: true
    min: 0
    max: 500
    values: [75, 300]
    slide: (event, ui) ->
      $("#rangeAmount").val "$" + ui.values[0] + " - $" + ui.values[1]
  
  $("#rangeAmount").val "$" + $(".uRange").slider("values", 0) + " - $" + $(".uRange").slider("values", 1)

  $(".uMin").slider # Slider with minimum
    range: "min"
    value: 37
    min: 100
    max: 200
    slide: (event, ui) ->
      $("#minRangeAmount").val ui.value + "%"
  
  $("#minRangeAmount").val $(".uMin").slider("value") + "%"

  $(".uMax").slider # Slider with maximum
    range: "max"
    min: 1
    max: 100
    value: 20
    slide: (event, ui) ->
      $("#maxRangeAmount").val ui.value
  
  $("#maxRangeAmount").val $(".uMax").slider("value")
