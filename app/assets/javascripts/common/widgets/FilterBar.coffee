# magical orientation agnostic colour slidertron. Here be dragons.
define ['common/util/Util', 'common/gfx/SimpleRenderer', 'common/util/Events'], (Util, SimpleRenderer, Events) ->

  class FilterBar

    # statics overwritten by config
    # length/thickness used here as they're more orientation agnostic
    # than width/height
    @THICKNESS     : 50
    @LENGTH        : .8
    @PADDING       : 20
    @DEFAULT_ALIGN : 0

    @DRAG_TAB_SHAPE          : [{ x: -5, y: -15 }, { x: 5, y: -15 }, { x: 5, y: -5 }, { x: 0, y: 0 }, { x: -5, y: -5 }]
    @DRAG_TAB_FILL           : '#bbbbbb'
    @DRAG_TAB_STROKE         : '#333333'
    @DRAG_TAB_STROKE_WIDTH   : 2
    @DRAG_TAB_DISABLED_ALPHA : .3
    @DRAG_UPDATE_DELAY       : 500

    @CUTOFF_LINE_STROKE       : '#000000'
    @CUTOFF_LINE_STROKE_WIDTH : 10
    @CUTOFF_LINE_ALPHA        : .2
    
    @INPUT_WIDTH        : 40
    @INPUT_SPACING      : 10
    @INPUT_UPDATE_DELAY : 1000

    @FONT      : 'Karla'
    @FONT_SIZE : 14
    @FONT_FILL : '#000000'

    @DRAG_BOX_STROKE       : '#ff00ff'
    @DRAG_BOX_STROKE_WIDTH : 20
    @DRAG_BOX_ALPHA        : .5
    
    @MODEL_DEPENDENCIES:
      colourScale    : "colourScale"
      colourMaps     : "colourMaps"
      filters        : "filters"
      activeFilter   : "activeFilter"
      selectedMetric : "selectedMetric"

    # run-time assigned statics
    @DRAG_TAB_SHAPE_V : null
    @NORMAL_COLOURS   : null
    @REVERSE_COLOURS  : null

    # constants
    @ALIGN_TOP    : 0
    @ALIGN_BOTTOM : 1
    @ALIGN_LEFT   : 2
    @ALIGN_RIGHT  : 3


    constructor: (@containerEl, @parentEl, @model) ->
      # create vertical drag-tab shape from a transformed horizontal drag tab
      unless FilterBar.DRAG_TAB_SHAPE_V?
        FilterBar.DRAG_TAB_SHAPE_V = []
        FilterBar.DRAG_TAB_SHAPE_V.push({ x: coord.y, y: coord.x }) for coord in FilterBar.DRAG_TAB_SHAPE

        FilterBar.NORMAL_COLOURS  = @model[FilterBar.MODEL_DEPENDENCIES.colourScale]()
        FilterBar.REVERSE_COLOURS = []

        count = 0
        len   = FilterBar.NORMAL_COLOURS.length
        while count < len
          FilterBar.REVERSE_COLOURS[len - count - 1] = { col: FilterBar.NORMAL_COLOURS[count].col, pos: 1 - FilterBar.NORMAL_COLOURS[count].pos }
          ++count

      @alignment  = FilterBar.DEFAULT_ALIGN
      @horizontal = @alignment is FilterBar.ALIGN_BOTTOM or @alignment is FilterBar.ALIGN_TOP

      # set input sizes and event listeners
      @inputMax = document.getElementById('input_max')
      @inputMin = document.getElementById('input_min')
      Util.setStyle(@inputMax, 'width', FilterBar.INPUT_WIDTH)
      Util.setStyle(@inputMin, 'width', FilterBar.INPUT_WIDTH)
      @inputHeight = Util.getStyleNumeric(@inputMax, 'height')
      Events.addEventListener(@inputMax, 'blur', @evBlurInput)
      Events.addEventListener(@inputMax, 'keyup', @evInputChanged)
      Events.addEventListener(@inputMin, 'blur', @evBlurInput)
      Events.addEventListener(@inputMin, 'keyup', @evInputChanged)

      # create gfx. Frame rate is zero, we'll force a redraw on demand
      @gfx = new SimpleRenderer(@containerEl, @width, @height, 1, 0)
      Util.setStyle(@gfx.cvs, 'position', 'absolute')
      Util.setStyle(@gfx.cvs, 'left', 0)
      Util.setStyle(@gfx.cvs, 'top', 0)
      @gfx.cvs.getContext('2d').rotate(Math.PI / 2)

      # layout offsets
      @layoutOffsetX = FilterBar.PADDING
      @layoutOffsetY = FilterBar.PADDING

      @allowDrag = true

      if @horizontal
        @layoutOffsetX = (FilterBar.INPUT_SPACING * 2) + FilterBar.INPUT_WIDTH
      else
        @layoutOffsetY = (FilterBar.INPUT_SPACING * 2) + FilterBar.INPUT_HEIGHT

      @updateLayout()
      # UI assets
      bar_thickness = FilterBar.THICKNESS - (FilterBar.PADDING * 2)
      @assets =
        bar     : @gfx.addImg({ img: @gradient, x: @layoutOffsetX, y: @layoutOffsetY })
        lineA   : @gfx.addPoly({ stroke: FilterBar.CUTOFF_LINE_STROKE, strokeWidth: FilterBar.CUTOFF_LINE_STROKE_WIDTH, alpha: FilterBar.CUTOFF_LINE_ALPHA, x: @layoutOffsetX, y: @layoutOffsetY, coords: [{ x: 0, y: 0 }, { x: 0, y: @gradient.height }] })
        lineB   : @gfx.addPoly({ stroke: FilterBar.CUTOFF_LINE_STROKE, strokeWidth: FilterBar.CUTOFF_LINE_STROKE_WIDTH, alpha: FilterBar.CUTOFF_LINE_ALPHA, x: @layoutOffsetX, y: @layoutOffsetY, coords: [{ x: 0, y: 0 }, { x: 0, y: @gradient.height }] })
        sliderA : @gfx.addPoly({ stroke: FilterBar.DRAG_TAB_STROKE, strokeWidth: FilterBar.DRAG_TAB_STROKE_WIDTH, fill: FilterBar.DRAG_TAB_FILL, coords: FilterBar.DRAG_TAB_SHAPE, x: @layoutOffsetX, y: @layoutOffsetY })
        sliderB : @gfx.addPoly({ stroke: FilterBar.DRAG_TAB_STROKE, strokeWidth: FilterBar.DRAG_TAB_STROKE_WIDTH, fill: FilterBar.DRAG_TAB_FILL, coords: FilterBar.DRAG_TAB_SHAPE, x: @layoutOffsetX, y: @layoutOffsetY })

      if @horizontal
        @assets.labelA = @gfx.addText({ font: FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT, fill: FilterBar.FONT_FILL, x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, align: 'center' })
        @assets.labelB = @gfx.addText({ font: FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT, fill: FilterBar.FONT_FILL, x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, align: 'center' })
      else
        @assets.labelA = @gfx.addImg({ img: @rotateLabel(' '), x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE})
        @assets.labelB = @gfx.addImg({ img: @rotateLabel(' '), x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE })
      
      @model[FilterBar.MODEL_DEPENDENCIES.selectedMetric].subscribe(@setMap)
      @filterSub = @model[FilterBar.MODEL_DEPENDENCIES.filters].subscribe(@setFilter)

      if @model[FilterBar.MODEL_DEPENDENCIES.selectedMetric]()?
        @update()
      else
        Util.setStyle(@containerEl, 'visibility', 'hidden')


    setFilter: =>
      return unless @map?

      model_filter = @model[FilterBar.MODEL_DEPENDENCIES.filters]()[@model[FilterBar.MODEL_DEPENDENCIES.selectedMetric]()]
      @filter      = {}
      @filter.max  = model_filter.max ? @map.high
      @filter.min  = model_filter.min ? @map.low

      @update()


    updateLayout: (orientation_changed = false) ->
      parent_x      = Util.getStyleNumeric(@parentEl, 'left')
      parent_y      = Util.getStyleNumeric(@parentEl, 'top')
      parent_width  = Util.getStyleNumeric(@parentEl, 'width')
      parent_height = Util.getStyleNumeric(@parentEl, 'height')

      @setAnchor(parent_x, parent_y, parent_width, parent_height)

      cvs = $('<canvas>')
      ctx = cvs[0].getContext('2d')

      @layoutOffsetX = FilterBar.PADDING
      @layoutOffsetY = FilterBar.PADDING

      if @horizontal
        new_width      = parent_width * FilterBar.LENGTH
        new_height     = FilterBar.THICKNESS
        @layoutOffsetX = (FilterBar.INPUT_SPACING * 2) + FilterBar.INPUT_WIDTH
        grd            = ctx.createLinearGradient(0, 0, new_width - (@layoutOffsetX * 2), 0)
      else
        new_width      = FilterBar.THICKNESS
        new_height     = parent_height * FilterBar.LENGTH
        @layoutOffsetY = (FilterBar.INPUT_SPACING * 2) + @inputHeight
        grd            = ctx.createLinearGradient(0, new_height - (@layoutOffsetY * 2), 0, 0)

      # redraw gradient only if the dimensions have changed
      if new_width isnt @width or new_height isnt @height
        @width     = new_width
        @height    = new_height
        cvs.width  = @width - (@layoutOffsetX * 2)
        cvs.height = @height - (@layoutOffsetY * 2)
        colours    = @model[FilterBar.MODEL_DEPENDENCIES.colourScale]()

        for colour in colours
          col = colour.col.toString(16)
          col = '0' + col while col.length < 6
          grd.addColorStop(colour.pos, '#' + col)

        ctx.fillStyle = grd
        ctx.fillRect(0, 0, cvs.width, cvs.height)

        @gradient = cvs
        @gfx.setDims(@width, @height)
        @gfx.setAttributes(@assets.bar, { img: cvs, width: cvs.width, height: cvs.height, sliceWidth: cvs.width, sliceHeight: cvs.height }) if @assets?

        if orientation_changed
          cutoff_line_shape = if @horizontal then [{ x: 0, y: 0 }, { x: 0, y: @gradient.height }] else [{ x: 0, y: 0 }, { x: @gradient.width, y: 0 }]
          slider_shape      = if @horizontal then FilterBar.DRAG_TAB_SHAPE else FilterBar.DRAG_TAB_SHAPE_V
          @gfx.remove(@assets.sliderA)
          @gfx.remove(@assets.sliderB)

          @gfx.setAttributes(@assets.bar, { x: @layoutOffsetX, y: @layoutOffsetY })
          @gfx.setAttributes(@assets.lineA, { coords: cutoff_line_shape, x: @layoutOffsetX, y: @layoutOffsetY })
          @gfx.setAttributes(@assets.lineB, { coords: cutoff_line_shape, x: @layoutOffsetX, y: @layoutOffsetY })
          #@gfx.setAttributes(@assets.labelA, { x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE })
          #@gfx.setAttributes(@assets.labelB, { x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE })
          #@gfx.setAttributes(@assets.sliderA, { coords: slider_shape, x: @layoutOffsetX, y: @layoutOffsetY })
          #@gfx.setAttributes(@assets.sliderB, { coords: slider_shape, x: @layoutOffsetX, y: @layoutOffsetY })
          @gfx.remove(@assets.labelA)
          @gfx.remove(@assets.labelB)

          if @horizontal

            @assets.labelA = @gfx.addText({ font: FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT, fill: FilterBar.FONT_FILL, x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, align: 'center' })
            @assets.labelB = @gfx.addText({ font: FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT, fill: FilterBar.FONT_FILL, x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, align: 'center' })
          else
            @assets.labelA = @gfx.addImg({ img: @rotateLabel(' '), x: FilterBar.THICKNESS - FilterBar.PADDING - FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE})
            @assets.labelB = @gfx.addImg({ img: @rotateLabel(' '), x: FilterBar.THICKNESS - FilterBar.PADDING - FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE })

          @gfx.setAttributes(@dragBox, { width: @width, height: @height }) if @dragBox?

          @assets.sliderA = @gfx.addPoly({ stroke: FilterBar.DRAG_TAB_STROKE, strokeWidth: FilterBar.DRAG_TAB_STROKE_WIDTH, fill: FilterBar.DRAG_TAB_FILL, coords: slider_shape, x: @layoutOffsetX, y: @layoutOffsetY })
          @assets.sliderB = @gfx.addPoly({ stroke: FilterBar.DRAG_TAB_STROKE, strokeWidth: FilterBar.DRAG_TAB_STROKE_WIDTH, fill: FilterBar.DRAG_TAB_FILL, coords: slider_shape, x: @layoutOffsetX, y: @layoutOffsetY })

        @update() if @map?

      rh_bound     = parent_x + parent_width - @width
      bottom_bound = parent_y + parent_height - @height
      container_x  = @anchor.x - (@width / 2)
      container_x  = rh_bound if container_x > rh_bound
      container_x  = parent_x if container_x < parent_x
      container_y  = @anchor.y - (@height / 2)
      container_y  = bottom_bound if container_y > bottom_bound
      container_y  = parent_y if container_y < parent_y

      Util.setStyle(@containerEl, 'left', container_x + 'px')
      Util.setStyle(@containerEl, 'top', container_y + 'px')
      Util.setStyle(@containerEl, 'width', @width)
      Util.setStyle(@containerEl, 'height', @height)

      if @horizontal
        y = (@height - @inputHeight) / 2
        Util.setStyle(@inputMin, 'top', y)
        Util.setStyle(@inputMin, 'left', FilterBar.INPUT_SPACING)
        Util.setStyle(@inputMax, 'top', y)
        Util.setStyle(@inputMax, 'left', @width - FilterBar.INPUT_SPACING - FilterBar.INPUT_WIDTH)
      else
        Util.setStyle(@inputMin, 'top', @height - FilterBar.INPUT_SPACING - @inputHeight)
        Util.setStyle(@inputMin, 'left', (@width - FilterBar.INPUT_WIDTH) / 2)
        Util.setStyle(@inputMax, 'top', FilterBar.INPUT_SPACING)
        Util.setStyle(@inputMax, 'left', (@width - FilterBar.INPUT_WIDTH) / 2)


    rotateLabel: (caption, font = FilterBar.FONT, size = FilterBar.FONT_SIZE, colour = FilterBar.FONT_FILL) ->
      cvs        = $('<canvas>')
      ctx        = cvs.getContext('2d')
      ctx.font   = FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT
      width      = ctx.measureText(caption).width
      cvs.width  = FilterBar.FONT_SIZE
      cvs.height = width

      # re-assign font, this gets reset after setting the canvas size
      ctx.font      = FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT
      ctx.translate(2 * FilterBar.FONT_SIZE, width)
      ctx.rotate(-Math.PI/ 2)
      ctx.fillStyle = FilterBar.FONT_FILL
      ctx.fillText(caption, 0, -FilterBar.FONT_SIZE)
      return cvs


    update: =>
      colours  = @model[FilterBar.MODEL_DEPENDENCIES.colourScale]()
      inverted = @map.low > @map.high
      if inverted
        low  = @map.high
        high = @map.low
      else
        low  = @map.low
        high = @map.high

      min_alpha = if @filter.min is low then FilterBar.DRAG_TAB_DISABLED_ALPHA else 1
      max_alpha = if @filter.max is high then FilterBar.DRAG_TAB_DISABLED_ALPHA else 1

      if @horizontal
        val_range = (@map.high - @map.low) / @gradient.width
        min_coord = ((@filter.min - @map.low) / val_range) + @layoutOffsetX
        max_coord = ((@filter.max - @map.low) / val_range) + @layoutOffsetX

        if isNaN(min_coord) or isNaN(max_coord)
          min_coord   = -9999999
          max_coord   = -9999999
          min_caption = ''
          max_caption = ''
        else
          min_caption = Util.formatValue(@filter.min)
          max_caption = Util.formatValue(@filter.max)

        bar_bounds   = @gfx.getBounds(@assets.bar)
        label_offset = @gfx.getAttribute(@assets.labelA, 'width') / 2
        min_label_x  = min_coord
        min_label_x  = bar_bounds.x + label_offset if min_label_x - label_offset < bar_bounds.x
        min_label_x  = bar_bounds.x + bar_bounds.width - label_offset if min_label_x + label_offset > bar_bounds.x + bar_bounds.width

        label_offset = @gfx.getAttribute(@assets.labelB, 'width') / 2
        max_label_x  = max_coord
        max_label_x  = bar_bounds.x + label_offset if max_label_x - label_offset < bar_bounds.x
        max_label_x  = bar_bounds.x + bar_bounds.width - label_offset if max_label_x + label_offset > bar_bounds.x + bar_bounds.width

        @gfx.setAttributes(@assets.lineA, { x: min_coord })
        @gfx.setAttributes(@assets.lineB, { x: max_coord })
        @gfx.setAttributes(@assets.labelA, { caption: min_caption, x: min_label_x})
        @gfx.setAttributes(@assets.labelB, { caption: max_caption, x: max_label_x})
        @gfx.setAttributes(@assets.sliderA, { x: min_coord, alpha: min_alpha })
        @gfx.setAttributes(@assets.sliderB, { x: max_coord, alpha: max_alpha })

        @gfx.redraw()
      else
        val_range = (@map.high - @map.low) / @gradient.height
        min_coord = ((@filter.min - @map.low) / val_range) + @layoutOffsetY
        max_coord = ((@filter.max - @map.low) / val_range) + @layoutOffsetY

        if isNaN(min_coord) or isNaN(max_coord)
          min_coord   = -9999999
          max_coord   = -9999999
          min_caption = ' '
          max_caption = ' '
        else
          min_caption = Util.formatValue(@filter.min)
          max_caption = Util.formatValue(@filter.max)

        @gfx.remove(@assets.labelA)
        @gfx.remove(@assets.labelB)

        @gfx.setAttributes(@assets.lineA, { y: min_coord })
        @gfx.setAttributes(@assets.lineB, { y: max_coord })
        @assets.labelA = @gfx.addImg({ img: @rotateLabel(min_caption), x: FilterBar.THICKNESS - FilterBar.PADDING, y: 0 })
        @assets.labelB = @gfx.addImg({ img: @rotateLabel(max_caption), x: FilterBar.THICKNESS - FilterBar.PADDING, y: 0 })
        @gfx.setAttributes(@assets.sliderA, { y: min_coord, alpha: min_alpha })
        @gfx.setAttributes(@assets.sliderB, { y: max_coord, alpha: max_alpha })

        bar_bounds   = @gfx.getBounds(@assets.bar)
        label_height = @gfx.getAttribute(@assets.labelA, 'height')
        min_label_y  = min_coord - (label_height / 2)
        min_label_y  = bar_bounds.y if min_label_y < bar_bounds.y
        min_label_y  = bar_bounds.y + bar_bounds.height - label_height if min_label_y + label_height > bar_bounds.y + bar_bounds.height
        label_height = @gfx.getAttribute(@assets.labelB, 'height')

        max_label_y  = max_coord - (label_height / 2)
        max_label_y  = bar_bounds.y if max_label_y < bar_bounds.y
        max_label_y  = bar_bounds.y + bar_bounds.height - label_height if max_label_y + label_height > bar_bounds.y + bar_bounds.height

        @gfx.setAttributes(@assets.labelA, { y: min_label_y })
        @gfx.setAttributes(@assets.labelB, { y: max_label_y })

        @gfx.redraw()


    getSliderAt: (x, y) ->
      bounds = @gfx.getBounds(@assets.sliderA)
      if x >= bounds.x and x <= bounds.x + bounds.width and y >= bounds.y and y <= bounds.y + bounds.height
        @fixedFlt = @filter.max
        return @assets.sliderA

      bounds = @gfx.getBounds(@assets.sliderB)
      if x >= bounds.x and x <= bounds.x + bounds.width and y >= bounds.y and y <= bounds.y + bounds.height
        @fixedFlt = @filter.min
        return @assets.sliderB

      return null


    dragSlider: (slider_id, x, y) ->
      return unless @allowDrag

      coords =
        x: x
        y: y

      if @horizontal
        coord  = 'x'
        dim    = 'width'
        offset = @layoutOffsetX
      else
        coord  = 'y'
        dim    = 'height'
        offset = @layoutOffsetY

      a_pos   = @gfx.getAttribute(@assets.sliderA, coord)
      b_pos   = @gfx.getAttribute(@assets.sliderB, coord)
      new_val = ((coords[coord] - offset) / @gradient[dim] * (@map.high - @map.low)) + @map.low

      if @map.high > @map.low
        high = @map.high
        low  = @map.low
      else
        high = @map.low
        low  = @map.high

      if new_val < low
        new_val       = low
        coords[coord] = offset
      else if new_val > high
        new_val       = high
        coords[coord] = @width - offset

      # convert new value to the coordinate space
      new_coord  = (new_val - low) / ((high - low) / @gradient[dim])
      new_coord += offset

      # update filter values
      if new_val > @fixedFlt
        @filter.min = @fixedFlt
        @filter.max = new_val
      else
        @filter.min = new_val
        @filter.max = @fixedFlt

      clearTimeout(@syncDelay)
      @syncDelay = setTimeout(@updateFilter, FilterBar.DRAG_UPDATE_DELAY)

      @update()


    startDrag: ->
      parent_x      = Util.getStyleNumeric(@parentEl, 'left')
      parent_y      = Util.getStyleNumeric(@parentEl, 'top')
      parent_width  = Util.getStyleNumeric(@parentEl, 'width')
      parent_height = Util.getStyleNumeric(@parentEl, 'height')
      mid_x         = parent_x + (parent_width / 2)
      mid_y         = parent_y + (parent_height / 2)

      @anchorPoints = []
      @anchorPoints.push({ align: FilterBar.ALIGN_LEFT, x: parent_x, y: mid_y })
      @anchorPoints.push({ align: FilterBar.ALIGN_RIGHT, x: parent_x + parent_width, y: mid_y })
      @anchorPoints.push({ align: FilterBar.ALIGN_TOP, x: mid_x, y: parent_y })
      @anchorPoints.push({ align: FilterBar.ALIGN_BOTTOM, x: mid_x, y: parent_y + parent_height })

      @dragBox = @gfx.addRect({ stroke: FilterBar.DRAG_BOX_STROKE, strokeWidth: FilterBar.DRAG_BOX_STROKE_WIDTH, alpha: FilterBar.DRAG_BOX_ALPHA, x: 0, y: 0, width: @width, height: @height })
      @gfx.redraw()


    stopDrag: ->
      if @dragBox?
        @gfx.remove(@dragBox)
        @gfx.redraw()
        @dragBox = null
        Events.dispatchEvent(@containerEl, 'filterBarSetAnchor')
      else
        clearTimeout(@syncDelay)
        @updateFilter()
        @allowDrag = true


    dragBar: (x, y) ->
      dist = []
      dist.push({ anchor: anchor, dist: Math.sqrt(Math.pow(anchor.x - x, 2) + Math.pow(anchor.y - y, 2)) }) for anchor in @anchorPoints
      dist    = Util.sortByProperty(dist, 'dist', true)
      nearest = dist[0]
      if nearest.anchor.align isnt @alignment
        @alignment     = nearest.anchor.align
        #old_horizontal = @horizontal
        @horizontal    = @alignment is FilterBar.ALIGN_BOTTOM or @alignment is FilterBar.ALIGN_TOP
        #if @horizontal isnt old_horizontal
        #  @gfx.remove(@assets.labelA)
        #  @gfx.remove(@assets.labelB)
        #
        #  if @horizontal
        #    @assets.labelA = @gfx.addText({ font: FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT, fill: FilterBar.FONT_FILL, x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, align: 'center' })
        #    @assets.labelB = @gfx.addText({ font: FilterBar.FONT_SIZE + 'px ' + FilterBar.FONT, fill: FilterBar.FONT_FILL, x: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE, align: 'center' })
        #  else
        #    @assets.labelA = @gfx.addImg({ img: @rotateLabel(' '), x: FilterBar.THICKNESS - FilterBar.PADDING - FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE})
        #    @assets.labelB = @gfx.addImg({ img: @rotateLabel(' '), x: FilterBar.THICKNESS - FilterBar.PADDING - FilterBar.FONT_SIZE, y: FilterBar.THICKNESS - FilterBar.PADDING + FilterBar.FONT_SIZE })

        @updateLayout(true)


    setMap: =>
      Util.setStyle(@containerEl, 'visibility', 'visible')

      if @mapSub?
        @mapSub.dispose()
        @mapSub = null

      selected_metric = @model[FilterBar.MODEL_DEPENDENCIES.selectedMetric]()

      unless @model[FilterBar.MODEL_DEPENDENCIES.colourMaps]()[selected_metric]?
        @mapSub = @model[FilterBar.MODEL_DEPENDENCIES.colourMaps].subscribe(@setMap)
        return

      @map = @model[FilterBar.MODEL_DEPENDENCIES.colourMaps]()[selected_metric]

      filter = @model[FilterBar.MODEL_DEPENDENCIES.filters]()[selected_metric]

      low  = @map.low
      high = @map.high

      if @map.high < @map.low
        tmp       = @map.high
        @map.high = @map.low
        @map.low  = tmp
        low       = @map.high
        high      = @map.low

      # careful to create a copy of the model filter object here otherwise we'll be 
      # quietly modifying the model while we play with the local reference of filter
      @filter =
        min : filter.min ? low
        max : filter.max ? high

      @inputMin.value = @map.low
      @inputMax.value = @map.high
      
      @update()
        

    updateFilter: =>
      @filterSub.dispose()

      selected_metric = @model[FilterBar.MODEL_DEPENDENCIES.selectedMetric]()

      min_active = @filter.min isnt @map.low and @filter.min isnt @map.high
      max_active = @filter.max isnt @map.high and @filter.max isnt @map.low

      filters = @model[FilterBar.MODEL_DEPENDENCIES.filters]()

      new_filter = {}
      new_filter.min = @filter.min if min_active
      new_filter.max = @filter.max if max_active

      filters[@model[FilterBar.MODEL_DEPENDENCIES.selectedMetric]()] = new_filter
      @model[FilterBar.MODEL_DEPENDENCIES.activeFilter](min_active or max_active)
      @model[FilterBar.MODEL_DEPENDENCIES.filters](filters)

      @filterSub = @model[FilterBar.MODEL_DEPENDENCIES.filters].subscribe(@setFilter)


    evBlurInput: (ev) =>
      clearTimeout(@changeTmr)
      @updateMap()


    evInputChanged: (ev) =>
      clearTimeout(@changeTmr)
      @changeTmr = setTimeout(@updateMap, FilterBar.INPUT_UPDATE_DELAY)


    updateMap: =>
      changed = false

      val = Number(@inputMax.value)
      unless isNaN(val) or @map.high is val
        @filter.max = val if @filter.max > val or @filter.max is @map.high
        @filter.min = val if @filter.min > val
        #@map.low    = val - 1 if @map.low >= val
        @map.high   = val
        changed     = true

      val = Number(@inputMin.value)
      unless isNaN(val) or @map.low is val
        @filter.max = val if @filter.max < val
        @filter.min = val if @filter.min < val or @filter.min is @map.low
        #@map.high   = val + 1 if @map.high <= val
        @map.low    = val
        changed     = true

      if changed
        @inputMax.setAttribute('value', @map.high)
        @inputMin.setAttribute('value', @map.low)
        @update()
        @updateFilter()

        range = if @map.high is @map.low then 1e-100 else Math.abs(@map.high - @map.low)

        if @map.high < @map.low
          @model[FilterBar.MODEL_DEPENDENCIES.colourScale](FilterBar.REVERSE_COLOURS)
          map = { high: @map.low, low: @map.high, inverted: true, range: range }
        else
          @model[FilterBar.MODEL_DEPENDENCIES.colourScale](FilterBar.NORMAL_COLOURS)
          map          = @map
          map.inverted = false
          map.range    = range

        maps = @model[FilterBar.MODEL_DEPENDENCIES.colourMaps]()
        maps[@model[FilterBar.MODEL_DEPENDENCIES.selectedMetric]()] = map
        @model[FilterBar.MODEL_DEPENDENCIES.colourMaps](maps)


    setAnchor: (parent_x, parent_y, parent_width, parent_height) ->
      switch @alignment
        when FilterBar.ALIGN_BOTTOM
          @anchor = { x: parent_x + (parent_width / 2), y: parent_y + parent_height }
        when FilterBar.ALIGN_TOP
          @anchor = { x: parent_x + (parent_width / 2), y: parent_y }
        when FilterBar.ALIGN_LEFT
          @anchor = { x: parent_x, y: parent_y + (parent_height / 2) }
        else
          @anchor = { x: parent_x + parent_width, y: parent_y + (parent_height / 2) }

