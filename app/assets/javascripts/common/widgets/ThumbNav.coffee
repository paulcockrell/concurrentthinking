define ['common/gfx/SimpleRenderer', 'common/util/Events', 'common/util/Util', 'irv/view/ThumbHint'], (SimpleRenderer, Events, Util, ThumbHint) ->


  class ThumbNav

    # statics overwritten by config
    @SHADE_FILL      : '#000000'
    @SHADE_ALPHA     : .5
    @MASK_FILL       : '#333377'
    @MASK_FILL_ALPHA : .5
    @BREACH_FILL     : '#ff0000'
    @BREACH_ALPHA    : 1

    @MODEL_DEPENDENCIES : { image: 'dcImage', breachZones: 'breachZones', scale: 'scale', groups: 'groups' }


    constructor: (@containerEl, @maxWidth, @maxHeight, @model) ->
      # assign a zero frame rate, we'll force the renderer to redraw on demand
      @gfx = new SimpleRenderer(@containerEl, @width, @height, 1, 0)
      Util.setStyle(@gfx.cvs, 'position', 'absolute')
      Util.setStyle(@gfx.cvs, 'left', '0px')
      Util.setStyle(@gfx.cvs, 'top', '0px')

      @breachGfx = new SimpleRenderer(@containerEl, @width, @height, 1, 0)
      Util.setStyle(@breachGfx.cvs, 'position', 'absolute')
      Util.setStyle(@breachGfx.cvs, 'left', '0px')
      Util.setStyle(@breachGfx.cvs, 'top', '0px')

      @hint = new ThumbHint(@containerEl, @model)

      @area =
        left   : 0
        top    : 0
        width  : @maxWidth
        height : @maxHeight

      @area.right  = @area.left + @area.width
      @area.bottom = @area.top + @area.height
      @model[ThumbNav.MODEL_DEPENDENCIES.image].subscribe(@setImg)
      @model[ThumbNav.MODEL_DEPENDENCIES.breachZones].subscribe(@setBreaches)


    showHint: (device, x, y) ->
      @hint.show(device, x, y)


    hideHint: ->
      @hint.hide()


    setImg: (img) =>
      reurn unless img?

      # fit model image to available space
      scale_x = @maxWidth / img.width
      scale_y = @maxHeight / img.height
      @scale  = if scale_x < scale_y then scale_x else scale_y

      @width  = img.width * @scale
      @height = img.height * @scale

      # fit containing div to exact dimensions of new thumbnail
      Util.setStyle(@containerEl, 'width', @width + 'px')
      Util.setStyle(@containerEl, 'height', @height + 'px')

      left = Util.getStyle(@containerEl, 'left')
      top  = Util.getStyle(@containerEl, 'top')

      # align canvas to containing div
      Util.setStyle(@gfx.cvs, 'left', left)
      Util.setStyle(@gfx.cvs, 'top', top)
      Util.setStyle(@breachGfx.cvs, 'left', left)
      Util.setStyle(@breachGfx.cvs, 'top', top)

      # resize and update breaches
      @gfx.setDims(@width, @height)
      @breachGfx.setDims(@width, @height)
      @setBreaches()

      # resize model image to fit
      cvs        = document.createElement('canvas')
      cvs.width  = @width
      cvs.height = @height
      ctx        = cvs.getContext('2d')
      ctx.drawImage(img, 0, 0, @width, @height)
      @img = cvs

      @update()


    setBreaches: =>
      # we're likely to get mutliple breach updates at once (when metric
      # data is pushed). Use of setTimeout here pools these together to
      # prevent multiple redundant updates
      clearTimeout(@breachTmr)
      @breachTmr = setTimeout(@updateBreaches, 100)


    updateBreaches: =>
      @breachGfx.removeAll()
      combined_scale = @scale * @model[ThumbNav.MODEL_DEPENDENCIES.scale]()

      zones  = @model[ThumbNav.MODEL_DEPENDENCIES.breachZones]()
      groups = @model[ThumbNav.MODEL_DEPENDENCIES.groups]()
      for group in groups
        for id of zones[group]
          zone = zones[group][id]
          @breachGfx.addRect(
            fill   : ThumbNav.BREACH_FILL
            alpha  : ThumbNav.BREACH_ALPHA
            x      : zone.x * combined_scale
            y      : zone.y * combined_scale
            width  : zone.width * combined_scale
            height : zone.height * combined_scale
          )

      # force immediate draw
      @breachGfx.redraw()


    update: (view_width, view_height, total_width, total_height, scroll_x, scroll_y) =>
      # update can be called internally (setImg) with no params
      # or via the controller on scroll with new view params
      if arguments.length > 0
        @scale = @width / total_width
        
        @area =
          left   : scroll_x  / total_width * @width
          top    : scroll_y / total_height * @height
          width  : view_width / total_width * @width
          height : view_height / total_height * @height

        @area.right  = @area.left + @area.width
        @area.bottom = @area.top + @area.height

      # define a rectangle with another rectangle cut out of the middle
      coords = [{ x: 0, y: 0 }, { x: @width, y: 0 }, { x: @width, y: @height }, { x: 0, y: @height }, { x: 0, y: @area.top }, { x: @area.left, y: @area.top }, { x: @area.left, y: @area.bottom }, { x: @area.right, y: @area.bottom }, { x: @area.right, y: @area.top }, { x: 0, y: @area.top }]

      @gfx.removeAll()

      @gfx.addImg({ img: @img, x: 0, y: 0 })
      @gfx.addPoly({ fill: ThumbNav.MASK_FILL, alpha: ThumbNav.MASK_FILL_ALPHA, fx: 'source-atop', coords: coords })
      @mask = @gfx.addPoly({ fill: ThumbNav.SHADE_FILL, alpha: ThumbNav.SHADE_ALPHA, coords: coords })
      @gfx.redraw()


    jumpTo: (x, y) ->
      x -= @area.width / 2
      y -= @area.height / 2

      x = 0 if x < 0
      x = @width - @area.width if x + @area.width > @width
      y = 0 if y < 0
      y = @height - @area.height if y + @area.height > @height

      @area.left   = x
      @area.right  = x + @area.width
      @area.top    = y
      @area.bottom = y + @area.height

      @update()

