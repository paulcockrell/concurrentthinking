define ->

  class CrossAppSettings

    # statics overwritten by config
    @LIFESPAN_MINS : 20000

    # constants and run-time assigned statics
    @COOKIE_NAME : 'shared_settings'


    @set: (settings_obj) ->
      settings        = JSON.stringify(settings_obj)
      expiration      = new Date((new Date()).getTime() + (CrossAppSettings.LIFESPAN_MINS * 60 * 1000))
      cookie_str      = escape(settings) + '; expires=' + expiration.toUTCString()
      document.cookie = CrossAppSettings.COOKIE_NAME + "=" + cookie_str
      console.log settings, expiration


    @get: ->
      cookie_str = document.cookie
      start_idx  = cookie_str.indexOf(' ' + CrossAppSettings.COOKIE_NAME + '=')
      start_idx  = cookie_str.indexOf(CrossAppSettings.COOKIE_NAME + '=') if start_idx is -1

      return {} if start_idx is -1

      start_idx = cookie_str.indexOf('=', start_idx) + 1
      end_idx   = cookie_str.indexOf(';',start_idx)
      end_idx   = cookie_str.length if end_idx is -1

      console.log JSON.parse(unescape(cookie_str.substring(start_idx, end_idx)))
      return JSON.parse(unescape(cookie_str.substring(start_idx, end_idx)))


    @clear: ->
      cookie_str = document.cookie
      document.cookie = CrossAppSettings.COOKIE_NAME + "=deleted; expires=" + new Date(0).toUTCString()
