define ->

  # Cross browser event handling. Each method is re-written during initial invokation
  # to avoid re-evaluating feature detection
  class Events

    @addEventListener: (element, event, listener, use_capture = false) ->
      console.log "event element", element
      if element.addEventListener
        @addEventListener = (element, event, listener, use_capture = false) ->
          element.addEventListener(event, listener, use_capture)
      else
        @addEventListener = (element, event, listener, use_capture = false) ->
          #element.addEventListener('on' + event, listener)

      @addEventListener(element, event, listener, use_capture)


    @removeEventListener: (element, event, listener, use_capture = false) ->
      if element.removeEventListener
        @removeEventListener = (element, event, listener, use_capture = false) ->
          element.removeEventListener(event, listener, use_capture)
      else
        @removeEventListener = (element, event, listener, use_capture = false) ->
          element.detachEvent('on' + event, listener)

      @removeEventListener(element, event, listener, use_capture)


    @dispatchEvent: (element, event) ->
      if element.addEventListener
        @dispatchEvent = (element, event) ->
          ev = document.createEvent('HTMLEvents')
          ev.initEvent(event, true, true)
          element.dispatchEvent(ev)
      else
        @dispatchEvent = (element, event) ->
          ev = document.createEventObject()
          element.fireEvent('on' + event, ev)

      @dispatchEvent(element, event)

