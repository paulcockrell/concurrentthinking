define ['common/util/Util', 'common/util/Events'], (Util, Events) ->

  class StaticGroupManager

    # statics overwritten by config
    @PATH   : 'path/to/groups/api'
    @GET    : 'get_req'
    @NEW    : 'set_req'
    @UPDATE : 'update_req'
    
    @MODEL_DEPENDENCIES : { selectedGroup: "selectedGroup", groupsById: "groupsById", deviceLookup: "deviceLookup" }
    @DOM_DEPENDENCIES   : { saveDialogue: "group_save_dialogue", nameInput: 'group_save_input', createNewBtn: "create_group", updateBtn: "update_group", confirmGroupSave: "confirm_group_save", cancelGroupSave: "cancel_group_save" }

    @ERR_INVALID_NAME      : 'Choose another name'
    @ERR_DUPLICATE_NAME    : 'Choose another'
    @ERR_CAPTION           : 'Failed: [[error_message]]'
    @ERR_WHITE_NAME        : 'Failed: [[error_message]]'
    @MSG_CONFIRM_UPDATE    : 'Aaaaw, really?'

    # constants and run-time assigned statics
    @GROUP_ID_MAP: { chassis: 'chassisIds', devices: 'deviceIds' }


    constructor: (@model) ->
      #XXX Once we are ready to collect static groups reinstate this
      #
      # console.log 'new StaticGroupManager', StaticGroupManager.PATH, StaticGroupManager.GET

      # # store references to relevant model attributes for convenience
      # @modelRefs       = {}
      # @modelRefs[attr] = @model[StaticGroupManager.MODEL_DEPENDENCIES[attr]] for attr of StaticGroupManager.MODEL_DEPENDENCIES

      # new Request.JSON(
      #   headers   : {'X-CSRF-Token': $$('meta[name="csrf-token"]')[0].getAttribute('content')}
      #   url       : StaticGroupManager.PATH + StaticGroupManager.GET + '?' + (new Date()).getTime()
      #   onSuccess : @evReceivedGroups
      #   onFail    : @loadFail
      #   onError   : @loadError
      # ).get()

      # @saveDialogueEl = $(StaticGroupManager.DOM_DEPENDENCIES.saveDialogue)
      # @selSub         = @modelRefs.selectedGroup.subscribe(@switchGroup)

      # Util.setStyle(@saveDialogueEl, 'display', 'none') if @saveDialogueEl?

      # el = $(StaticGroupManager.DOM_DEPENDENCIES.createNewBtn)
      # Events.addEventListener(el, 'click', @evShowGroupSaveDialogue) if el?
      # el = $(StaticGroupManager.DOM_DEPENDENCIES.updateBtn)
      # Events.addEventListener(el, 'click', @evUpdateGroup) if el?
      # el = $(StaticGroupManager.DOM_DEPENDENCIES.confirmGroupSave)
      # Events.addEventListener(el, 'click', @evConfirmGroupSave) if el?
      # el = $(StaticGroupManager.DOM_DEPENDENCIES.cancelGroupSave)
      # Events.addEventListener(el, 'click', @evCancelGroupSave) if el?

      # document.GROUPS = @


    updateGroup: =>
      @sendGroup()


    showSaveDialogue: =>
      Util.setStyle(@saveDialogueEl, 'display', 'block')
      input       = $(StaticGroupManager.DOM_DEPENDENCIES.nameInput)
      input.value = ''
      input.focus()


    showSaveDialogue: =>
      Util.setStyle(@saveDialogueEl, 'display', 'block')
      input       = $(StaticGroupManager.DOM_DEPENDENCIES.nameInput)
      input.value = ''
      input.focus()


    confirmSave: (name) =>
      groups = @modelRefs.groupsById()
      for id of groups
        if groups[id].name is name
          @showError(StaticGroupManager.ERR_DUPLICATE_NAME)
          return

      name = name.trim()

      if name.length is 0
        @showError(StaticGroupManager.ERR_WHITE_NAME)
        return

      @sendGroup(name)
      Util.setStyle(@saveDialogueEl, 'display', 'none')


    cancelSave: =>
      Util.setStyle(@saveDialogueEl, 'display', 'none')


    evReceivedGroups: (groups) =>
      groups_by_id = {}
      groups_by_id[group.id] = group for group in groups

      @modelRefs.groupsById(groups_by_id)


    switchGroup: =>
      groups   = @modelRefs.groupsById()
      selected = @modelRefs.selectedGroup()

      for i of groups
        if groups[i].name is selected
          selection = groups[i]
          break

      @displayGroup(selection) if selection?


    displayGroup: (static_group) ->
      @selectedGroup = static_group
      groups         = @modelRefs.groups()
      new_sel        = {}
      new_sel[group] = {} for group in groups

      for group of StaticGroupManager.GROUP_ID_MAP
        accessor = StaticGroupManager.GROUP_ID_MAP[group]
        new_sel[group][id] = true for id in static_group.memberIds[accessor]

      @modelRefs.activeSelection(true)
      @modelRefs.selectedDevices(new_sel)


    evShowGroupSaveDialogue: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @showSaveDialogue()


    evUpdateGroup: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      selected_group = @modelRefs.selectedGroup()
      return if selected_group is undefined
      confirm_dialog(Util.substitutePhrase(StaticGroupManager.MSG_CONFIRM_UPDATE, 'selected_group', selected_group), 'document.GROUPS.updateGroup()', 'void(0);', 'Please confirm', true, true)


    evConfirmGroupSave: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @confirmSave($(StaticGroupManager.DOM_DEPENDENCIES.nameInput).value)


    evCancelGroupSave: (ev) =>
      ev.preventDefault()
      ev.stopPropagation
      @cancelSave()


    showError: (msg) ->
      alert_dialog(Util.substitutePhrase(StaticGroupManager.ERR_CAPTION, 'error_message', msg))

    
    sendGroup: (name) ->
      # if a name is supplied, assume we're creating a new group
      @isNewGroup      = name?
      active_filter    = @modelRefs.activeFilter()
      active_selection = @modelRefs.activeSelection()
      device_lookup    = @modelRefs.deviceLookup()
      static_group     = {}
      additions        = {}
      deletions        = {}

      for group of StaticGroupManager.GROUP_ID_MAP
        static_group[group] = []
        additions[group]    = []
        deletions[group]    = []
      
      # find 
      if active_filter and active_selection
        filter    = @modelRefs.filteredDevices()
        selection = @modelRefs.selectedDevices()

        for group of static_group
          for id of filter[group]
            # provide only the device id in the case of a simple chassis
            continue if group is 'chassis' and not device_lookup.chassis[id].instance.complex
            static_group[group].push(id) if filter[group][id] and selection[group][id]

      else if active_filter
        filter = @modelRefs.filteredDevices()

        for group of static_group
          for id of filter[group]
            # provide only the device id in the case of a simple chassis
            continue if group is 'chassis' and not device_lookup.chassis[id].instance.complex
            static_group[group].push(id) if filter[group][id]

      else if active_selection
        selection = @modelRefs.selectedDevices()

        for group of static_group
          for id of selection[group]
            # provide only the device id in the case of a simple chassis
            continue if group is 'chassis' and not device_lookup.chassis[id].instance.complex
            static_group[group].push(id) if selection[group][id]

      else
        return

      @newGroup = static_group

      payload = { group_type: 'static', member_ids_to_add: additions, member_ids_to_remove: deletions }

      if name?
        payload.name = name
        payload.member_ids_to_add = static_group
        @selectedGroup = { name: name, breachGroup: false }
      else
        payload.name = @selectedGroup.name
        for group of static_group
          accessor  = StaticGroupManager.GROUP_ID_MAP[group]
          old_group = @selectedGroup.memberIds[accessor]

          # search for additions
          for id in static_group[group]
            found = false
            for id2 in old_group
              if id is id2
                found = true
                break

            unless found
              payload.member_ids_to_add[group].push(id)

          # search for deletions
          for id in old_group
            found = false
            for id2 in static_group[group]
              if id is Number(id2)
                found = true
                break

            unless found
              payload.member_ids_to_remove[group].push(id)
              
      # serialize arrays for ruby to interpret them correctly
      payload.member_ids_to_remove[group] = JSON.stringify(payload.member_ids_to_remove[group]) for group of payload.member_ids_to_remove
      payload.member_ids_to_add[group]    = JSON.stringify(payload.member_ids_to_add[group]) for group of payload.member_ids_to_add
      console.log payload

      new Request.JSON(
        headers    : {'X-CSRF-Token': $$('meta[name="csrf-token"]')[0].getAttribute('content')}
        url        : StaticGroupManager.PATH + Util.substitutePhrase((if name? then StaticGroupManager.NEW else StaticGroupManager.UPDATE), 'group_id', (if name? then null else @selectedGroup.id)) + '?' + (new Date()).getTime()
        method     : 'post'
        onComplete : @evGroupSent
        data       : payload
      ).send()

      # deserialize arrays, they'll be used again on successful response
      payload.member_ids_to_remove[group] = JSON.parse(payload.member_ids_to_remove[group]) for group of payload.member_ids_to_remove
      payload.member_ids_to_add[group]    = JSON.parse(payload.member_ids_to_add[group]) for group of payload.member_ids_to_add


    evGroupSent: (response) =>
      console.log 'send response', response, response.success is true
      if response.success
        groups = @modelRefs.groupsById()

        for group of @newGroup
          if StaticGroupManager.GROUP_ID_MAP[group]?
            @newGroup[StaticGroupManager.GROUP_ID_MAP[group]] = @newGroup[group]
            delete @newGroup[group]

        @selectedGroup.id         = response.id if @isNewGroup
        @selectedGroup.memberIds  = @newGroup
        groups[@selectedGroup.id] = @selectedGroup
        @modelRefs.groupsById(groups)
        @modelRefs.selectedGroup(@selectedGroup.name) if @isNewGroup
