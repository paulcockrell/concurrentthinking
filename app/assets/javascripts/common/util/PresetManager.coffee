define ['common/util/Util', 'common/util/Events'], (Util, Events) ->

  class PresetManager

    # statics overwritten by config
    @PATH   : 'path/to/preset/api'
    @GET    : 'get_req'
    @NEW    : 'set_req'
    @UPDATE : 'update_req'
    @VALUES : [{ type: 'simple', name: 'viewMode' }, { type: 'simple', name: 'scaleMetrics' }, { type: 'simple', name: 'face' }]
    
    @MODEL_DEPENDENCIES : { selectedPreset: "selectedPreset", presetsById: "presetsById" }
    @DOM_DEPENDENCIES   : { saveDialogue: "save_dialogue", nameInput: 'save_input', defaultAccessor: { element: 'rack_view', property: 'data-preset' } }

    @MSG_CONFIRM_UPDATE    : 'Are you sure you wish to overwrite the preset [[selected_preset]] with the current display settings?'
    @ERR_INVALID_NAME      : 'Choose another name'
    @ERR_DUPLICATE_NAME    : 'Choose another'
    @ERR_CAPTION           : 'Failed: [[error_message]]'
    @ERR_WHITE_NAME        : 'Failed: [[error_message]]'
    @WARN_THRESHOLD        : 'No threshold has been associated with your chosen preset.'
    @MESSAGE_HOLD_DURATION : 1

    # constants and run-time assigned statics
    @VALUES_BY_NAME: {}


    constructor: (@model, @ignoreDefault) ->
      #XXX Reinstate this when we are ready for presets
      #
      # new Request.JSON(
      #   headers   : {'X-CSRF-Token': $$('meta[name="csrf-token"]')[0].getAttribute('content')}
      #   url       : PresetManager.PATH + PresetManager.GET + '?' + (new Date()).getTime()
      #   onSuccess : @presetsReceived
      #   onFail    : @loadFail
      #   onError   : @loadError
      # ).get()

      # PresetManager.VALUES_BY_NAME[val.name] = val for val in PresetManager.VALUES

      # @saveDialogueEl = $(PresetManager.DOM_DEPENDENCIES.saveDialogue)
      # @selSub         = @model[PresetManager.MODEL_DEPENDENCIES.selectedPreset].subscribe(@switchPreset)

      # Util.setStyle(@saveDialogueEl, 'display', 'none')

      # Events.addEventListener($(PresetManager.DOM_DEPENDENCIES.createNewBtn), 'click', @evShowPresetSaveDialogue)
      # Events.addEventListener($(PresetManager.DOM_DEPENDENCIES.updateBtn), 'click', @evUpdatePreset)
      # Events.addEventListener($(PresetManager.DOM_DEPENDENCIES.saveBtn), 'click', @evConfirmPresetSave)
      # Events.addEventListener($(PresetManager.DOM_DEPENDENCIES.cancelBtn), 'click', @evCancelPresetSave)

      # document.PRESETS = @


    updatePreset: =>
      @sendPreset()


    showSaveDialogue: =>
      Util.setStyle(@saveDialogueEl, 'display', 'block')
      input       = $(PresetManager.DOM_DEPENDENCIES.nameInput)
      input.value = ''
      input.focus()


    confirmSave: (name) =>
      presets = @model[PresetManager.MODEL_DEPENDENCIES.presetsById]()
      for id of presets
        if presets[id].name is name
          @showError(PresetManager.ERR_DUPLICATE_NAME)
          return

      name = name.trim()

      if name.length is 0
        @showError(PresetManager.ERR_WHITE_NAME)
        return

      @sendPreset(name)
      Util.setStyle(@saveDialogueEl, 'display', 'none')


    cancelSave: =>
      Util.setStyle(@saveDialogueEl, 'display', 'none')


    parseBoolean: (val) ->
      return val is 'true'


    pagePresetDefault: ->
      $(PresetManager.DOM_DEPENDENCIES.defaultAccessor.element)?.get(PresetManager.DOM_DEPENDENCIES.defaultAccessor.property)


    presetExists: (preset_id_to_find, preset_ids) ->
      preset_ids.some (el, idx) ->
        preset_id_to_find is el


    presetsReceived: (presets) =>
      presets_by_id = {}
      console.log 'PresetManager.presetsReceived', @ignoreDefault
      page_preset_default = if @ignoreDefault then null else @pagePresetDefault()

      for preset in presets
        presets_by_id[preset.id] = preset
        default_preset           = preset.name if not @ignoreDefault and preset.default is true

      # set model
      @model[PresetManager.MODEL_DEPENDENCIES.presetsById](presets_by_id)

      # check to see if the page default preset exists, if it does overwrite the selected default preset
      default_preset = presets_by_id[page_preset_default].name if @presetExists(page_preset_default, Object.keys(presets_by_id))

      # select the default preset, if defined
      @model[PresetManager.MODEL_DEPENDENCIES.selectedPreset](default_preset) if default_preset?


    switchPreset: =>
      presets  = @model[PresetManager.MODEL_DEPENDENCIES.presetsById]()
      selected = @model[PresetManager.MODEL_DEPENDENCIES.selectedPreset]()
      for i of presets
        if presets[i].name is selected
          selection = presets[i]
          break

      @displayPreset(selection)


    displayPreset: (preset) ->
      @selected = preset
      
      return unless @selected?

      for val_def in PresetManager.VALUES
        val_name = val_def.name
        #continue unless preset.values[val_name]?
        try
          # only update the model if current value is different from preset value
          # this is only effective for simple data types
          #val_def = PresetManager.VALUES_BY_NAME[val_name]
          key     = if val_def.key? then JSON.parse(preset.values[val_def.key]) else null
          current = if key? then @model[val_name]()[key] else @model[val_name]()
          new_val = JSON.parse(preset.values[val_name]) if preset.values[val_name]?
          new_val = true if new_val is 'true'
          new_val = false if new_val is 'false'
          #new_val = JSON.parse(preset.values[val_name]) if preset.values[val_name]?
          if val_name is 'selectedThresholdId' and (not new_val? or not @model.thresholdsById()[new_val]?)
            MessageSlider.instance.display(PresetManager.WARN_THRESHOLD, PresetManager.WARN_THRESHOLD, PresetManager.MESSAGE_HOLD_DURATION, new Date())
          else if current isnt new_val
            if key?
              obj      = @model[val_name]()
              obj[key] = new_val
              @model[val_name](obj)
              console.log val_name, key, new_val
            else
              @model[val_name](new_val)
        catch err

    
    sendPreset: (name) ->
      create_new = name?

      return unless @selected? or create_new

      # @change stores the latest change to a preset from an update or new request
      # this is used to maintain synchonisation locally without re-requesting the presets
      # from the server
      @change = { values: {} }
      payload = { 'preset[values]': {} }

      if create_new
        @change.name               = name
        @change.default            = false
        payload['preset[name]']    = name
        payload['preset[default]'] = false
      else
        @change.default            = @selected.default
        payload['preset[id]']      = @selected.id
        payload['preset[name]']    = @selected.name
        payload['preset[default]'] = @selected.default

      for val in PresetManager.VALUES
        if val.key?
          str_val = JSON.stringify(@model[val.name]()[@model[val.key]()])
        else
          str_val = JSON.stringify(@model[val.name]())

        @change.values[val.name] = str_val if str_val?
        payload['preset[values]'][val.name] = str_val
      new Request.JSON(
        headers    : {'X-CSRF-Token': $$('meta[name="csrf-token"]')[0].getAttribute('content')}
        url        : PresetManager.PATH + Util.substitutePhrase((if create_new then PresetManager.NEW else PresetManager.UPDATE), 'preset_id', if @selected? then @selected.id else null) + '?' + (new Date()).getTime()
        method     : 'post'
        onComplete : @response
        data       : payload
      ).send()


    response: (response) =>
      if response.success is "true"
        switch_to            = @change.name?
        presets              = @model[PresetManager.MODEL_DEPENDENCIES.presetsById]()
        @change.id           = response.id
        @change.name         = presets[response.id].name unless switch_to
        presets[response.id] = @change

        @model[PresetManager.MODEL_DEPENDENCIES.presetsById](presets)
        if switch_to
          @selSub.dispose()
          @model[PresetManager.MODEL_DEPENDENCIES.selectedPreset](@change.name) if switch_to
          @selSub = @model[PresetManager.MODEL_DEPENDENCIES.selectedPreset].subscribe(@switchPreset)
      else
        @showError(response)


    showError: (msg) ->
      alert_dialog(Util.substitutePhrase(PresetManager.ERR_CAPTION, 'error_message', msg))


    evShowPresetSaveDialogue: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @showSaveDialogue()


    evUpdatePreset: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      selected_preset = @model[PresetManager.MODEL_DEPENDENCIES.selectedPreset]()
      return if selected_preset is undefined
      confirm_dialog(Util.substitutePhrase(PresetManager.MSG_CONFIRM_UPDATE, 'selected_preset', selected_preset), 'document.PRESETS.updatePreset()', 'void(0);', 'Please confirm', true, true)


    evConfirmPresetSave: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @confirmSave($(PresetManager.DOM_DEPENDENCIES.nameInput).value)


    evCancelPresetSave: (ev) =>
      ev.preventDefault()
      ev.stopPropagation
      @cancelSave()


