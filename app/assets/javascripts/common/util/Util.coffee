define ->


  class Util
  

    @SIG_FIG: 3


    @formatValue: (num) ->
      str_val = String(num)
      if str_val.split('.')[0].length >= @SIG_FIG
        return Math.round(num)
      return num.toPrecision(@SIG_FIG)
  
  
    # return a css property. This fn rewrites itself on first invocation 
    # to avoid re-evaluating feature detection
    @getStyle: (element, prop) ->
      if element.currentStyle
        @getStyle = (element, prop) ->
          element.currentStyle[prop]
      if document.defaultView and document.defaultView.getComputedStyle
        @getStyle = (element, prop) ->
          document.defaultView.getComputedStyle(element, '')[prop]
      else
        @getStyle = (element, prop) ->
          element.style[prop]
  
      @getStyle(element, prop)
  
  
    @getStyleNumeric: (element, prop) ->
      val = @getStyle(element, prop)
      return Number(val.replace(/[^-\d\.]/g, ''))
  
  
    # set a css property
    @setStyle: (element, prop, value) ->
      console.log element, "#{prop} #{value}"
      $(element).attr(prop, value)
      #console.log element.currentStyle[prop] = value
      #element.style[prop] = value
      #return
      #if element.currentStyle
      #  return element.currentStyle[prop] = value
      #element.style[prop] = value
  
  
    # takes two numeric colour values and blends one into the other by 'amount'
    @blendColour: (low, high, amount) ->
      low_amount = 1 - amount
    
      low_r  = (low >> 16) & 0xff
      low_g  = (low >>  8) & 0xff
      low_b  = low & 0xff
      high_r = (high >> 16) & 0xff
      high_g = (high >>  8) & 0xff
      high_b = high & 0xff
    
      new_r = (low_r * low_amount + high_r * amount) & 0xff
      new_g = (low_g * low_amount + high_g * amount) & 0xff
      new_b = (low_b * low_amount + high_b * amount) & 0xff
    
      0x0 | (new_r << 16 ) | (new_g << 8) | new_b
  
  
    # sort a list of objects according to an object property value
    @sortByProperty: (list, field, ascending) =>
      sortAsc = (a, b) ->
        if a[field] > b[field] then return 1
        if b[field] > a[field] then return -1
        return 0
  
      sortDesc = (a, b) ->
        if a[field] > b[field] then return -1
        if b[field] > a[field] then return 1
        return 0
  
      if ascending then list.sort(sortAsc) else list.sort(sortDesc)
  
  
    @sortCaseInsensitive: (list) ->
      caseEval = (a, b) ->
        diff = a.toLowerCase().localeCompare(b.toLowerCase())
        if diff is 0
          if a > b then return 1 else return -1
        else
          return diff
  
      list.sort(caseEval)
  
  
    # return an object with a DOM element's dimensions as numeric values
    @getElementDimensions: (element) ->
      dims = element.getSize()
      return { width: dims.x, height: dims.y }
      width  = @getStyle(element, 'width')
      width  = Number(width.substr(0, width.length - 2))
      height = @getStyle(element, 'height')
      height = Number(height.substr(0, height.length - 2))
      width  = @getStyleNumeric(element, 'width') + @getStyleNumeric(element, 'paddingLeft') + @getStyleNumeric(element, 'paddingRight')
      height = @getStyleNumeric(element, 'height') + @getStyleNumeric(element, 'paddingTop') + @getStyleNumeric(element, 'paddingBottom')
      { width: width, height: height }
  
  
    # assumes vertical scrollbars have the same thickness as horizontal
    @getScrollbarThickness: ->
      inner_el = document.createElement('p')
      @setStyle(inner_el, 'width', '100%')
      @setStyle(inner_el, 'height', '200px')
  
      outer_el = document.createElement('div')
      @setStyle(outer_el, 'position', 'absolute')
      @setStyle(outer_el, 'top', '0')
      @setStyle(outer_el, 'left', '0')
      @setStyle(outer_el, 'visibility', 'hidden')
      @setStyle(outer_el, 'width', '200px')
      @setStyle(outer_el, 'height', '150px')
      @setStyle(outer_el, 'overflow', 'hidden')
      outer_el.insertBefore(inner_el)
  
      $().insertBefore(outer_el)
      natural_width  = $(inner_el).offset().left
      natural_height = $(inner_el).offset().top
      @setStyle(outer_el, 'overflow', 'scroll')
      scroll_width = $(inner_el).offset().left
      
      if natural_width is scroll_width
        scroll_width = $(outer_el).width()
      
      $().remove(outer_el)
      
      return natural_width - scroll_width
  
  
    @substitutePhrase: (text, key, value) ->
      if value is undefined or value is null or value.length is 0 or (typeof(value) is 'string' and value.replace(/^\s+|\s+$/g, '').length is 0)
        phrase_match = new RegExp('\\({2}[^\(]*?\\[\\[' + key + '\\]\\].*?\\){2}', 'g')
        text         = text.replace(phrase_match, '')
        phrase_match = new RegExp('\\[\\[' + key + '\\]\\]', 'g')
        return text.replace(phrase_match, '')
      else
        phrase_match = new RegExp('\\[\\[' + key + '\\]\\]', 'g')
        return text.replace(phrase_match, value)
  
  
    @cleanUpSubstitutions: (text) ->
      return text.replace(/\({2}|\){2}/g, '')
  
  
    @addLeadingZeros: (num, len = 2) ->
      num      = String(num)
      parts    = num.split('.')
      parts[0] = 0 + parts[0] while parts[0].length < len
      return parts.join('.')
  
  
    # gets the mouse coords relative to the div
    @resolveMouseCoords: (div, ev) ->
      if ev.pageX?
        x = ev.pageX + div.scrollLeft
        y = ev.pageY + div.scrollTop
      else
        x = ev.clientX + document.body.scrollLeft + document.documentElement.scrollLeft + div.scrollLeft
        y = ev.clientY + document.body.scrollTop + document.documentElement.scrollTop + div.scrollTop
  
      offset = div.getPosition()
  
      { x: x - offset.x, y: y - offset.y }
  
  
    @forceImmediateRedraw: (el) ->
      console.log el
      tmp = document.createTextNode(' ')
      dsp = Util.getStyle(el, 'display')
      
      el.appendChild(tmp)
      Util.setStyle(el, 'display', 'none')
  
      setTimeout(->
        console.log 'ha', el, dsp, tmp
        #Util.setStyle(el, 'display', dsp)
        #el.removeChild(tmp)
      , 20)
    
    @printHtmlInNewPage: (html) ->
      new_window = window.open()
      node = document.doctype
      doctype = "<!DOCTYPE " + node.name + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '') + (!node.publicId && node.systemId ? ' SYSTEM' : '')  + (node.systemId ? ' "' + node.systemId + '"' : '') + '>'
      html_with_header = "#{doctype}<html>#{document.head.innerHTML}<body>#{html}</body></html>"
      new_window.document.write( html_with_header.replace(/absolute/g, 'fixed') )
      new_window.document.getElementsByTagName('svg')[0].style.position = 'static'
      
      canvas_list = document.getElementsByTagName('canvas')
      for canvas in canvas_list
        new_canvas = new_window.document.getElementById(canvas.id)
        if new_canvas
          context = new_canvas.getContext('2d')
          context.drawImage(canvas, 0, 0)
      
      new_window.print()
