define ['common/gfx/Easing', 'common/gfx/Primitives', 'common/gfx/Validator'], (Easing, Primitives, Validator) ->

  # optimise not organise
  # A simple canvas rendering engine. LIMITATIONS: doesn't support layers, it assumes no objects will overlap each other
  # for this reason multiple SimpleRenderers should be overlayed to give the effect. Animations will always override
  # any animation currently playing on the given asset.
  class SimpleRenderer

    @UNSUPPORTED        = 'Erk! Your browser doesn\'t support canvas :('
    @DEFAULT_FRAME_RATE = 24

    @CVS_IDX = 0


    constructor: (@containerEl, @width, @height, @scale = 1, frame_rate = SimpleRenderer.DEFAULT_FRAME_RATE) ->
      @cvs = document.createElement('canvas')
      #  @cvs = $('<canvas>')
      unless (@cvs.getContext)
        console.log SimpleRenderer.UNSUPPORTED
        return SimpleRenderer.UNSUPPORTED
      console.log "@containerEl", @containerEl
      console.log "@width", @width*@scale
      console.log "@height", @height*@scale
      console.log "@scale", @scale
      @cvs.id = 'SR' + SimpleRenderer.CVS_IDX
      @cvs.name = 'SR' + SimpleRenderer.CVS_IDX
      @cvs.width = @width * @scale
      @cvs.height = @height * @scale
  
      console.log "so this is @cvs now", @cvs
      document.lala = @cvs
      ++SimpleRenderer.CVS_IDX

      @ctx           = @cvs.getContext('2d')
      @frameInterval = 1000 / frame_rate
      @assetDir      = {}
      @assetId       = 0
      @anims         = []
      @clearList     = []
      @drawList      = []
      @animsById     = {}
      @scaling       = false
      @paused        = false
      $(@containerEl).append(@cvs)

      if frame_rate > 0
        # requestAnimation frame (where supported) tells the browser that animation is occurring
        # this allows for optimisation, e.g. rendering is ignored when viewing a different tab
        window.requestAnimationFrame = @setAnimFrame()
        # queue initial draw
        window.requestAnimationFrame(@drawFrame)
        # requestAnimationFrame runs at 60fps, the setInterval overrides this
        @drawId = setInterval(@queueFrame, @frameInterval)
        @


    setAnimFrame: ->
      # return browser specific requestAnimationFrame routine with fallback where unsupported
      fallback = (callback) ->
        window.setTimeout(callback, 1000 / 60)
      window.requestAnimationFrame ? window.webkitRequestAnimationFrame ? window.mozRequestAnimationFrame ? window.oRequestAnimationFrame ? window.msRequestAnimationFrame ? fallback


    queueFrame: =>
      window.requestAnimationFrame(@drawFrame)


    destroy: () ->
      clearInterval(@drawId) if @drawId?
      clearInterval(@scaleId) if @scaleId?
      @containerEl.removeChild(@cvs)


    clearCVS: () ->
      @ctx.clearRect(0, 0, @cvs.width, @cvs.height)


    removeAll: () ->
      @clearCVS()
      @anims     = []
      @animsById = {}
      @assetDir  = {}


    setDims: (@width, @height) ->
      console.log ">>> setDims", @width, @height, @scale
      @cvs.width  = @width * @scale
      @cvs.height = @height * @scale
      @redraw()


    # redraw one or all assets
    redraw: (asset_id) ->
      if asset_id?
        @drawList.push(asset_id)
      else
        @clearCVS()
        @assetDir[asset].draw(@scale) for asset of @assetDir
        @clearList = []
        @drawList  = []


    setScale: (scale, duration, ease, on_complete) ->
      console.log "SimpleGraphics#setScale", scale
      if duration?
        ease       = Easing.Linear.easeIn unless ease?
        num_frames = Math.ceil(duration / @frameInterval)
        @cvsAnim =
          frameIdx: 0
          sequence: @getSequence(@scale, scale, num_frames, ease)
          onComplete: on_complete

        unless @scaling
          @scaleId = setInterval(@animateCVS, @frameInterval)
          @scaling = true
      else
        @scaleCVS(scale)


    scaleCVS: (scale) ->
      document.fucker = @cvs
      console.log "scale", scale
      @cvs.width  = @width * scale
      @cvs.height = @height * scale
      @scale      = scale
      @redraw()


    animateCVS: () =>
      @scaleCVS(@cvsAnim.sequence[@cvsAnim.frameIdx])
      ++@cvsAnim.frameIdx
      if(@cvsAnim.frameIdx == @cvsAnim.sequence.length)
        @cvsAnim.onComplete() if @cvsAnim.onComplete?
        clearInterval(@scaleId)
        @scaling = false


    pauseAnims: () ->
      #clearInterval(@drawId)
      @paused = true


    resumeAnims: () ->
      @paused = false
      #if @anims.length > 0
      #  @drawId = setInterval(@runAnims, @frameInterval)


    # append asset region to clear list
    remove: (asset_id) ->
      asset = @assetDir[asset_id]
      if asset?
        @clearList.push(asset.getBoundaries(@scale))
        @stopAnim(asset_id)
        delete @assetDir[asset_id]


    # clear from the canvas
    clear: (asset) ->
      bounds = asset.getBoundaries(@scale)
      @ctx.clearRect(bounds.x - 1, bounds.y - 1, bounds.width + 2, bounds.height + 2)


    # tests if the bounding boxes of two assets overlap
    hitTest: (asset_a, asset_b) ->
      asset = @assetDir[asset_a]

      return false unless asset?

      bounds_a        = asset.getBoundaries(@scale)
      bounds_a.right  = bounds_a.x + bounds_a.width
      bounds_a.bottom = bounds_a.y + bounds_a.height

      asset = @assetDir[asset_b]

      return false unless asset?

      bounds_b = asset.getBoundaries(@scale)
      bounds_b.right  = bounds_b.x + bounds_b.width
      bounds_b.bottom = bounds_b.y + bounds_b.height

      # ugly! but a little more performant than splitting this up
      # into more legible pieces.
      return ((bounds_b.x >= bounds_a.x and bounds_b.x <= bounds_a.right) or (bounds_b.right >= bounds_a.x and bounds_b.right <= bounds_a.right) or (bounds_b.x < bounds_a.x and bounds_b.right > bounds_a.right)) and ((bounds_b.y >= bounds_a.y and bounds_b.y <= bounds_a.bottom) or (bounds_b.bottom > bounds_a.y and bounds_b.bottom < bounds_a.bottom) or (bounds_b.y < bounds_a.y and bounds_b.bottom > bounds_a.bottom))



    # add rectangle to canvas, returns asset id
    addRect: (def) ->
      if def.fill? and def.stroke?
        Validator.FullRect(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.FullRect(def, @scale, @ctx)
      else if def.fill?
        Validator.FilledRect(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.FilledRect(def, @scale, @ctx)
      else
        Validator.LineRect(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.LineRect(def, @scale, @ctx)

      @assetDir[asset_id] = asset
      @drawList.push(asset_id)
      asset_id


    # add polygon to canvas, returns asset id
    addPoly: (def) ->
      if def.fill? and def.stroke?
        Validator.FullPoly(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.FullPoly(def, @scale, @ctx)
      else if def.fill?
        Validator.FilledPoly(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.FilledPoly(def, @scale, @ctx)
      else
        Validator.LinePoly(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.LinePoly(def, @scale, @ctx)

      @assetDir[asset_id] = asset
      @drawList.push(asset_id)
      asset_id


    # add ellipse to canvas, returns asset id
    addEllipse: (def) ->
      if def.fill? and def.stroke?
        Validator.FullEllipse(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.FullEllipse(def, @scale, @ctx)
      else if def.fill?
        Validator.FilledEllipse(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.FilledEllipse(def, @scale, @ctx)
      else
        Validator.LineEllipse(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.LineEllipse(def, @scale, @ctx)

      @assetDir[asset_id] = asset
      @drawList.push(asset_id)
      asset_id


    # add image to canvas, returns asset id
    addImg: (def) ->
      Validator.Image(def)
      asset_id = ++@assetId
      def.id   = asset_id
      console.log "SimpleRenderer#addImg", @scale
      asset    = new Primitives.Image(def, @scale, @ctx)

      @assetDir[asset_id] = asset
      @drawList.push(asset_id)
      asset_id


    # add text to canvas, returns asset id
    addText: (def) ->
      if def.bgFill or def.bgStroke
        Validator.LabelText(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.LabelText(def, @scale, @ctx)
      else if(def.shadowColour?)
        Validator.ShadowText(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.ShadowText(def, @scale, @ctx)
      else
        Validator.Text(def)
        asset_id = ++@assetId
        def.id   = asset_id
        asset    = new Primitives.Text(def, @scale, @ctx)

      @assetDir[asset_id] = asset
      @drawList.push(asset_id)
      asset_id


    # animate an existing object. on_complete callbacks can occur immediately if duration
    # and delay are < 0.5 frames or the animation is invalid
    animate: (asset_id, attributes, duration, ease = Easing.Linear.easeIn, on_complete, params, redraw_list) ->
      num_frames = Math.ceil(duration / @frameInterval)

      # convert delay in ms to frames
      if attributes.delay?
        delay = Math.round(attributes.delay / @frameInterval)
        delete attributes.delay
      else
        delay = 0

      # if animation is zero length (or < 0.5 frames) set attributes and trigger any callback
      if num_frames <= 0 and delay <= 0
        on_complete(asset_id, params) if on_complete?
        return @setAttributes(asset_id, attributes)

      asset = @assetDir[asset_id]
      
      # prepare anim objet
      anim_obj =
        id         : asset_id
        frameIdx   : 0
        attributes : []
        onComplete : on_complete
        paramsi    : params
        redrawList : redraw_list

      valid = false

      # validate the supplied attributes, i.e. they exist, are of the same type as the existing
      # attributes and are a different value to the current attributes. valid = false if none
      # meet these requirements
      for i of attributes
        if asset[i]? and asset[i] isnt attributes[i] and typeof asset[i] is typeof attributes[i]
          anim_obj.attributes.push(
            attribute: i
            sequence: @getSequence(asset[i], attributes[i], num_frames, ease, delay))

          valid = true

      # if requested animation is valid, delete any existing anim for the asset and add anim object
      # to anim queue. Otherwise trigger any supplied callback
      if valid
        @anims.splice(@anims.indexOf(@animsById[asset_id]), 1) if @animsById[asset_id]?
        @anims.push(anim_obj)
        @animsById[asset_id] = anim_obj
      else if on_complete?
        on_complete(asset_id, params)

      valid


    # sets attributes of an asset and redraws. Encourages all attributes to be set in one go to avoid multiple redraws
    setAttributes: (asset_id, attributes) ->
      asset = @assetDir[asset_id]
      @clearList.push(asset.getBoundaries(@scale))
      asset[i] = attributes[i] for i of attributes
      @drawList.push(asset_id)


    # returns an asset attribute
    getAttribute: (asset_id, attribute) ->
      @assetDir[asset_id][attribute]


    getBounds: (asset_id) ->
      @assetDir[asset_id].getBoundaries(@scale)


    runAnims: =>
      return if @paused

      count = 0
      len   = @anims.length
      while count < len
        anim_obj = @anims[count]
        id       = anim_obj.id
        asset    = @assetDir[id]
        count2   = 0
        len2     = anim_obj.attributes.length

        # designate clearance region before attributes are updated
        @clearList.push(asset.getBoundaries(@scale))

        while count2 < len2
          attr_obj = anim_obj.attributes[count2]
          asset[attr_obj.attribute] = attr_obj.sequence[anim_obj.frameIdx]
          ++count2

        ++anim_obj.frameIdx
        if anim_obj.frameIdx is attr_obj.sequence.length
          on_complete = anim_obj.onComplete
          @anims.splice(count, 1)
          delete @animsById[id]
          on_complete(id, anim_obj.params) if on_complete?
          --len
        else
          ++count

        # push to draw list
        @drawList.push(id)
        if anim_obj.redrawList?
          for id in anim_obj.redrawList
            @clearList.push(@assetDir[id].getBoundaries(@scale))
            @drawList.push(id)


    drawFrame: =>
      # @runAnims()
      # @ctx.clearRect(region.x - 1, region.y - 1, region.width + 2, region.height + 2) for region in @clearList
      # @clearList = []

      # drawn = []
      # for asset_id in @drawList
      #   # prevent duplicate draws
      #   asset = @assetDir[asset_id]
      #   if asset? and not drawn[asset_id]
      #     @assetDir[asset_id].draw(@scale)
      #     drawn[asset_id] = true
      # @drawList = []

      # @drawComplete() if @drawComplete?


    # called on initialising an animation, generates an array of steps to animate from 'start' to 'end'
    getSequence: (start, end, num_frames, ease, delay) ->
      sequence = []

      count = 0
      while count < delay
        sequence.push(start)
        ++count

      count = 1

      if typeof(start) is 'string'
        start = parseInt(start.substr(1), 16)
        end   = parseInt(end.substr(1), 16)

        start_r = (start >> 16) & 0xff
        start_g = (start >> 8) & 0xff
        start_b = start & 0xff

        end_r = (end >> 16) & 0xff
        end_g = (end >> 8) & 0xff
        end_b = end & 0xff

        delta_r = end_r - start_r
        delta_g = end_g - start_g
        delta_b = end_b - start_b

        while(count <= num_frames)
          progress = ease(count, num_frames)
          r = start_r + delta_r * progress
          g = start_g + delta_g * progress
          b = start_b + delta_b * progress
          blended = ((r << 16) | (g << 8) | b).toString(16)
          while(blended.length < 6)
            blended = '0' + blended
          sequence.push('#' + blended)
          ++count
      else
        delta = end - start
        while count <= num_frames
          sequence.push(start + delta * ease(count, num_frames))
          ++count
      sequence


    stopAnim: (asset_id) ->
      # maintaining animsById allows us to check if the asset is animating 
      # without iterating through the whole anims array
      if @animsById[asset_id]?
        delete @animsById[asset_id]
        count = 0
        len = @anims.length
        while count < len
          if @anims[count].id is asset_id
            @anims.splice(count, 1)
            break
          ++count

