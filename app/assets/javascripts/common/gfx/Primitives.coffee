define ->

  Primitives = {}

  # base class
  Primitives.Asset = class

    constructor: (def) ->
      @id    = def.id
      @x     = def.x
      @y     = def.y
      @alpha = def.alpha
      @fx    = def.fx


    adjustStrokeBoundaries: (bounds, scale) ->
      scaled_stroke  = @strokeWidth * scale
      half_stroke    = scaled_stroke / 2

      bounds.x      -= half_stroke
      bounds.y      -= half_stroke
      bounds.width  += scaled_stroke
      bounds.height += scaled_stroke

      bounds


  Primitives.Rect = class extends Primitives.Asset

    constructor: (def) ->
      super(def)
      @width  = def.width
      @height = def.height


    draw: (scale) ->
      @ctx.beginPath()
      @ctx.globalAlpha              = @alpha
      @ctx.globalCompositeOperation = @fx
      @ctx.rect(@x * scale, @y * scale, @width * scale, @height * scale)


    getBoundaries: (scale) ->
      { x: @x * scale, y: @y * scale, width: @width * scale, height: @height * scale }


  Primitives.FilledRect = class extends Primitives.Rect

    constructor: (def, scale, @ctx) ->
      super(def)
      @fill = def.fill


    draw: (scale) ->
      super
      @ctx.fillStyle = @fill
      @ctx.fill()


  Primitives.LineRect = class extends Primitives.Rect

    constructor: (def, scale, @ctx) ->
      super(def)
      @stroke      = def.stroke
      @strokeWidth = def.strokeWidth


    draw: (scale) ->
      super
      @ctx.strokeStyle = @stroke
      @ctx.lineWidth   = @strokeWidth * scale
      @ctx.stroke()


    getBoundaries: (scale) ->
      @adjustStrokeBoundaries(super, scale)


  Primitives.FullRect = class extends Primitives.Rect

    constructor: (def, scale, @ctx) ->
      super(def)
      @fill        = def.fill
      @stroke      = def.stroke
      @strokeWidth = def.strokeWidth


    draw: (scale) ->
      super
      @ctx.fillStyle = @fill
      @ctx.fill()
      @ctx.strokeStyle = @stroke
      @ctx.lineWidth   = @strokeWidth * scale
      @ctx.stroke()


    getBoundaries: (scale) ->
      @adjustStrokeBoundaries(super, scale)


  Primitives.Poly = class extends Primitives.Asset

    constructor: (def) ->
      super(def)

      min_x = Number.MAX_VALUE
      min_y = Number.MAX_VALUE
      max_x = -Number.MAX_VALUE
      max_y = -Number.MAX_VALUE

      @coords = def.coords

      for coords in def.coords
        min_x = coords.x if coords.x < min_x
        min_y = coords.y if coords.y < min_y
        max_x = coords.x if coords.x > max_x
        max_y = coords.y if coords.y > max_y

      @offsetX = min_x
      @offsetY = min_y
      @width   = max_x - min_x
      @height  = max_y - min_y


    draw: (scale) ->
      @ctx.globalAlpha              = @alpha
      @ctx.globalCompositeOperation = @fx
      @ctx.beginPath()
      @ctx.moveTo((@x + @coords[0].x) * scale, (@y + @coords[0].y) * scale)

      count = 1
      len   = @coords.length
      while count < len
        @ctx.lineTo((@x + @coords[count].x) * scale, (@y + @coords[count].y) * scale)
        ++count

      @ctx.closePath()


    getBoundaries: (scale) ->
      { x: (@x + @offsetX) * scale, y: (@y + @offsetY) * scale, width: @width * scale, height: @height * scale }


  Primitives.FilledPoly = class extends Primitives.Poly

    constructor: (def, scale, @ctx) ->
      super(def)
      @fill = def.fill


    draw: (scale) ->
      super
      @ctx.fillStyle = @fill
      @ctx.fill()


  Primitives.LinePoly = class extends Primitives.Poly

    constructor: (def, scale, @ctx) ->
      super(def)
      @stroke      = def.stroke
      @strokeWidth = def.strokeWidth


    draw: (scale) ->
      super
      @ctx.strokeStyle = @stroke
      @ctx.lineWidth   = @strokeWidth * scale
      @ctx.stroke()


    getBoundaries: (scale) ->
      @adjustStrokeBoundaries(super, scale)


  Primitives.FullPoly = class extends Primitives.Poly

    constructor: (def, scale, @ctx) ->
      super(def)
      @fill        = def.fill
      @stroke      = def.stroke
      @strokeWidth = def.strokeWidth


    draw: (scale) ->
      super
      @ctx.strokeStyle = @stroke
      @ctx.lineWidth   = @strokeWidth * scale
      @ctx.stroke()
      @ctx.fillStyle = @fill
      @ctx.fill()


    getBoundaries: (scale) ->
      @adjustStrokeBoundaries(super, scale)


  Primitives.Ellipse = class extends Primitives.Asset

    constructor: (def) ->
      super(def)
      @width  = def.width
      @height = def.height


    draw: (scale) ->
      @ctx.beginPath()
      @ctx.globalAlpha              = @alpha
      @ctx.globalCompositeOperation = @fx
      @ctx.save()
      v_scale = @height / @width
      @ctx.scale(1, v_scale)
      radius = @width * scale / 2
      @ctx.beginPath()
      @ctx.arc((@x * scale) + radius, (@y * scale / v_scale) + radius, radius, 0, Math.PI * 2)
      @ctx.restore()


    getBoundaries: (scale) ->
      { x: @x * scale, y: @y * scale, width: @width * scale, height: @height * scale }


  Primitives.FilledEllipse = class extends Primitives.Ellipse

    constructor: (def, scale, @ctx) ->
      super(def)
      @fill = def.fill


    draw: (scale) ->
      super
      @ctx.fillStyle = @fill
      @ctx.fill()


  Primitives.LineEllipse = class extends Primitives.Ellipse

    constructor: (def, scale, @ctx) ->
      super(def, scale, @ctx)
      @stroke      = def.stroke
      @strokeWidth = def.strokeWidth


    draw: (scale) ->
      super
      @ctx.strokeStyle = @stroke
      @ctx.lineWidth   = @strokeWidth * scale
      @ctx.stroke()


    getBoundaries: (scale) ->
      @adjustStrokeBoundaries(super, scale)


  Primitives.FullEllipse = class extends Primitives.Ellipse

    constructor: (def, scale, @ctx) ->
      super(def, scale, @ctx)
      @stroke      = def.stroke
      @strokeWidth = def.strokeWidth
      @fill        = def.fill


    draw: (scale) ->
      super
      @ctx.strokeStyle = @stroke
      @ctx.lineWidth   = @strokeWidth * scale
      @ctx.stroke()
      @ctx.fillStyle = @fill
      @ctx.fill()


    getBoundaries: (scale) ->
      @adjustStrokeBoundaries(super, scale)


  Primitives.Image = class extends Primitives.Asset

    constructor: (def, scale, @ctx) ->
      super(def)
      @img         = def.img
      @width       = def.width
      @height      = def.height
      @sliceX      = def.sliceX
      @sliceY      = def.sliceY
      @sliceWidth  = def.sliceWidth
      @sliceHeight = def.sliceHeight


    draw: (scale) ->
      console.log "image", @img
      console.log "ctx", @ctx
      console.log "scale", scale
      document.poopoo = @ctx
      console.log "sliceX", @sliceX
      console.log "sliceY", @sliceY
      console.log "sliceWidth", @sliceWidth
      console.log "sliceHeight", @sliceHeight
      console.log "x * scale", @x * scale
      console.log "y * scale", @y * scale
      console.log "width * scale", @width * scale
      console.log "height * scale", @height * scale
      @ctx.globalAlpha              = @alpha
      @ctx.globalCompositeOperation = @fx
      @ctx.drawImage(@img, @sliceX, @sliceY, @sliceWidth, @sliceHeight, @x * scale, @y * scale, @width * scale, @height * scale)


    getBoundaries: (scale) ->
      { x: @x * scale, y: @y * scale, width: @width * scale, height: @height * scale }


  Primitives.Text = class extends Primitives.Asset

    @TRUNCATION_SUFFIX: '. . .'


    constructor: (def, scale, @ctx) ->
      super(def)
      @font     = def.font
      @fill     = def.fill
      @align    = def.align
      @caption  = def.caption
      @maxWidth = def.maxWidth
      @ctx.font = @font
      @width    = @ctx.measureText(@caption).width
      @height   = Number(@font.substring(0, @font.indexOf('px')))


    draw: (scale) ->
      @ctx.globalAlpha              = @alpha
      @ctx.globalCompositeOperation = @fx
      @ctx.font                     = @font
      @ctx.textAlign                = @align
      @ctx.fillStyle                = @fill

      caption = if @maxWidth then @truncate(scale) else @caption

      @text = caption
      @ctx.fillText(caption, @x * scale, @y * scale)
      @width = @ctx.measureText(caption).width


    getBoundaries: (scale) ->
      switch @align
        when 'left'
          { x: @x * scale, y: (@y * scale) - @height - @offsetHeight(), width: @width, height: @height }
        when 'right'
          { x: (@x * scale) - @width, y: (@y * scale) - @height - @offsetHeight(), width: @width, height: @height }
        when 'center'
          { x: (@x * scale) - (@width / 2), y: (@y * scale) - @height - @offsetHeight(), width: @width, height: @height }


    truncate: (scale) ->
      caption            = @caption
      width              = @ctx.measureText(caption).width
      truncated          = false
      relative_max_width = @maxWidth * scale

      while width > relative_max_width && caption.length > 0
        truncated = true
        caption   = caption.substr(0, caption.length - 1)
        width     = @ctx.measureText(caption + Primitives.Text.TRUNCATION_SUFFIX).width

      if truncated then caption + Primitives.Text.TRUNCATION_SUFFIX else caption


    offsetHeight: () ->
      # hack: height of text isn't reported accurately so we need to offset a proportional amount
      -@height / 5


  Primitives.LabelText = class extends Primitives.Text

    constructor: (def, scale, @ctx) ->
      @bgFill        = def.bgFill
      @bgStroke      = def.bgStroke
      @bgStrokeWidth = def.bgStrokeWidth
      @bgPadding     = def.bgPadding
      @bgAlpha       = def.bgAlpha
      super(def, scale, @ctx)


    draw: (scale) ->
      super

      @ctx.beginPath()
      double_pad = @bgPadding * 2
      switch @align
        when 'left'
          x = (@x * scale) - @bgPadding
        when 'right'
          x = (@x * scale) - @width - @bgPadding
        else
          x = (@x * scale) - (@width / 2) - @bgPadding
      
      @ctx.rect(x, (@y * scale) - @height - @bgPadding - @offsetHeight(), @width + double_pad, @height + double_pad)

      @ctx.globalAlpha = @bgAlpha

      if @bgFill
        @ctx.fillStyle = @bgFill
        @ctx.fill()

      if @bgStroke
        @ctx.strokeStyle = @bgStroke
        @ctx.lineWidth = @bgStrokeWidth
        @ctx.stroke()

      super


    getBoundaries: () ->
      bounds = super

      double_pad = @bgPadding * 2

      bounds.x      -= @bgPadding
      bounds.y      -= @bgPadding
      bounds.width  += double_pad
      bounds.height += double_pad
      bounds


  Primitives.ShadowText = class extends Primitives.Text

    constructor: (def, scale, @ctx) ->
      @shadowColour  = def.shadowColour
      @shadowOffsetX = def.shadowOffsetX
      @shadowOffsetY = def.shadowOffsetY
      @shadowBlur    = def.shadowBlur
      super(def, scale, @ctx)


    draw: (scale) ->
      @ctx.shadowColor   = @shadowColour
      @ctx.shadowOffsetX = @shadowOffsetX
      @ctx.shadowOffsetY = @shadowOffsetY
      @ctx.shadowBlur    = @shadowBlur

      super

      @ctx.shadowBlur    = 0
      @ctx.shadowOffsetX = 0
      @ctx.shadowOffsetY = 0


  Primitives
  
