define ->

  Easing = {}
  # super class provides generic ease routines
  Easing.Easer =

    easeIn: (current, total, factor) ->
      Math.pow(current / total, factor)

    easeOut: (current, total, factor) ->
      1 - Math.pow(1 - (current / total), factor)


  Easing.Linear =

    easeIn: (current, total) ->
      current / total

    easeOut: (current, total) ->
      current / total


  Easing.Quad =

    easeIn: (current, total) ->
      Easing.Easer.easeIn(current, total, 2)

    easeOut: (current, total) ->
      Easing.Easer.easeOut(current, total, 2)


  Easing.Cubic =

    easeIn: (current, total) ->
      Easing.Easer.easeIn(current, total, 3)

    easeOut: (current, total) ->
      Easing.Easer.easeOut(current, total, 3)


  Easing.Quart =

    easeIn: (current, total) ->
      Easing.Easer.easeIn(current, total, 4)

    easeOut: (current, total) ->
      Easing.Easer.easeOut(current, total, 4)


  Easing.Quint =

    easeIn: (current, total) ->
      Easing.Easer.easeIn(current, total, 5)

    easeOut: (current, total) ->
      Easing.Easer.easeOut(current, total, 5)

  return Easing
  
  
