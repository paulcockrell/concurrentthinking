# sets up default values and throws errors if required properties are missing for each primative graphic type

define ->

  Validator = {}

  Validator.Asset = (data) ->
    data.x     = 0 unless data.x?
    data.y     = 0 unless data.y?
    data.alpha = 1 unless data.alpha?
    data.fx    = 'source-over' unless data.fx?


  Validator.Shape = (data) ->
    throw "Missing property 'width'" unless data.width?
    throw "Missing property 'height'" unless data.height?


  Validator.FilledRect = (data) ->
    Validator.Asset(data)
    Validator.Shape(data)
    throw "Missing property 'fill'" unless data.fill?


  Validator.LineRect = (data) ->
    Validator.Asset(data)
    Validator.Shape(data)
    throw "Missing property 'stroke'" unless data.stroke?
    data.strokeWidth = 1 unless data.strokeWidth?


  Validator.FullRect = (data) ->
    Validator.Asset(data)
    Validator.Shape(data)
    throw "Missing property 'fill'" unless data.fill?
    throw "Missing property 'stroke'" unless data.stroke?
    data.strokeWidth = 1 unless data.strokeWidth?


  Validator.Poly = (data) ->
    throw "Missing property 'coords'" unless data.coords?
    throw "No coordinates defined in 'coords'" if data.coords.length is 0
    throw "Invalid coordinate supplied. 'coords' must be an array of objects { x: <x>, y: <y> }" unless data.coords instanceof Array
    for coord in data.coords
      throw "Invalid coordinate supplied. 'coords' must be an array of objects { x: <x>, y: <y> }" if not coord.x? or not coord.y?


  Validator.FilledPoly = (data) ->
    Validator.Asset(data)
    Validator.Poly(data)
    throw "Missing property 'fill'" unless data.fill?


  Validator.LinePoly = (data) ->
    Validator.Asset(data)
    Validator.Poly(data)
    throw "Missing property 'stroke'" unless data.stroke?


  Validator.FullPoly = (data) ->
    Validator.Asset(data)
    Validator.Poly(data)
    throw "Missing property 'fill'" unless data.fill?
    throw "Missing property 'stroke'" unless data.stroke?


  Validator.FilledEllipse = (data) ->
    Validator.Asset(data)
    Validator.Shape(data)
    throw "Missing property 'fill'" unless data.fill?


  Validator.LineEllipse = (data) ->
    Validator.Asset(data)
    Validator.Shape(data)
    throw "Missing property 'stroke'" unless data.stroke?
    data.strokeWidth = 1 unless data.strokeWidth?


  Validator.FullEllipse = (data) ->
    Validator.Asset(data)
    Validator.Shape(data)
    throw "Missing property 'fill'" unless data.fill?
    throw "Missing property 'stroke'" unless data.stroke?
    data.strokeWidth = 1 unless data.strokeWidth?


  Validator.Image = (data) ->
    Validator.Asset(data)
    throw "Missing property 'img'" unless data.img
    data.alpha       = 1 unless data.alpha?
    data.sliceX      = 0 unless data.sliceX?
    data.sliceY      = 0 unless data.sliceY?
    data.sliceWidth  = data.img.width unless data.sliceWidth?
    data.sliceHeight = data.img.height unless data.sliceHeight?
    data.width       = data.sliceWidth unless data.width?
    data.height      = data.sliceHeight unless data.height?


  Validator.Text = (data) ->
    Validator.Asset(data)
    data.align   = 'left' unless data.align?
    data.caption = '' unless data.caption?
    throw "Missing property 'fill'" unless data.fill?
    throw "Missing property 'font'" unless data.font?


  Validator.ShadowText = (data) ->
    Validator.Asset(data)
    Validator.Text(data)
    data.shadowOffsetX = 2 unless data.shadowOffsetX?
    data.shadowOffsetY = 2 unless data.shadowOffsetY?
    data.shadowBlur    = 0 unless data.shadowBlur?
    data.shadowColour  = 'black' unless data.shadowColour?


  Validator.LabelText = (data) ->
    Validator.Asset(data)
    Validator.Text(data)
    throw "Missing property 'bgFill' or 'bgStroke'" unless data.bgFill? or data.bgStroke?
    data.bgAlpha       = 1 unless data.bgAlpha
    data.bgPadding     = 2 unless data.bgPadding
    data.bgStrokeWidth = 1 unless data.bgStrokeWidth


  Validator

