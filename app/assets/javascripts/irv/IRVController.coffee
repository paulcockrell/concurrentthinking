# ╔═════╦════════════════════════════════════╗
# ║ IRV ║ ~ ©Concurrent Thinking Ltd. 2012 ~ ║
# ╚═════╩════════════════════════════════════╝

define ['common/util/CrossAppSettings', 'irv/view/UpdateMsg', 'irv/util/Configurator', 'common/util/Events', 'common/util/Util', 'irv/util/AssetManager', 'irv/view/ThumbHint', 'irv/view/RackSpace', 'irv/view/Rack', 'common/widgets/ThumbNav', 'common/widgets/FilterBar', 'irv/ViewModel', 'irv/util/Parser', 'common/util/PresetManager', 'common/util/StaticGroupManager', 'util/ComboBox', 'irv/view/Tooltip'], (CrossAppSettings, UpdateMsg, Configurator, Events, Util, AssetManager, ThumbHint, RackSpace, Rack, ThumbNav, FilterBar, ViewModel, Parser, PresetManager, StaticGroupManager, ComboBox, Tooltip) ->

  class IRVController

    # statics overwritten by config
    @DOUBLE_CLICK_TIMEOUT         : 250
    @DRAG_ACTIVATION_DIST         : 8
    @RACK_HINT_HOVER_DELAY        : 2000
    @NUM_RESOURCES                : 3
    @METRIC_POLL_RATE             : 15000
    @LIVE                         : false
    @LIVE_RESOURCES               : {}
    @OFFLINE_RESOURCES            : {}
    @DEFAULT_METRIC_ID            : 'cpu_user'
    @RACK_PAGE_HEIGHT_PROPORTION  : .7
    @ZOOM_KEY                     : 32
    @STEP_ZOOM_AMOUNT             : .01
    @THUMB_WIDTH                  : 200
    @THUMB_HEIGHT                 : 130
    @RESOURCE_LOAD_CAPTION        : 'Loading Resources<br>[[progress]]'
    @PRIMARTY_IMAGE_PATH          : '../'
    @SECONDARY_IMAGE_PATH         : '../'
    @THUMB_HINT_HOVER_DELAY       : 500
    @API_RETRY_DELAY              : 5000
    @SCREENSHOT_FILENAME          : 'rarararar.jpg'
    @EXPORT_FILENAME              : 'haaai.txt'
    @EXPORT_HEADER                : ''
    @EXPORT_RECORD                : ''
    @EXPORT_MESSAGE               : 'Saving IRV image, please wait...'
    @NAV_HIDE_LAYOUT_UPDATE_DELAY : 1000
    @EXPORT_IMAGE_URL             : '/-/api/v1/irv/racks/export_image'
    @THUMB_HINT_OFFSET_X          : 10
    @THUMB_HINT_OFFSET_Y          : -60
    @BREACH_POLL_RATE             : 60000
  
    # constants and run-time assigned statics
    @CONFIG : '/-/irv/configuration'


    constructor: (options) ->
      require.ready(@getConfig)
  
  
    getConfig: =>
      $('dialogue').innerHTML = 'Loading config'
      new Request.JSON(url: IRVController.CONFIG + '?' + (new Date()).getTime(), onSuccess: @configReceived, onFail: @loadFail, onError: @loadError).get()
  
  
    configReceived: (config) =>
      Configurator.setup(IRVController, config)

      # grab url params and overwrite view model statics
      params = String(window.location)
      params = params.substr(params.indexOf('?') + 1)
      if params.length > 0
        params = params.split('&')
        for param in params
          parts = param.split('=')
          # parse strings to booleans
          parts[1] = true if parts[1] is 'true'
          parts[1] = false if parts[1] is 'false'
          ViewModel[parts[0]] = parts[1]

      @dcpvSettings = CrossAppSettings.get()
  
      @model = new ViewModel()
      ko.applyBindings(@model)
  
      groups       = @model.groups()
      breach_zones = {}
      breach_zones[group] = {} for group in groups
      @model.breachZones(breach_zones)
  
      @assetSub = @model.assetList.subscribe(@evLoadRackAssets)
      @parser   = new Parser(@model)
      @getRackData()


    connectMetricCombos: ->
      ComboBox.connect_all('cbox')
      for id, cb of ComboBox.boxes
        cb.add_change_callback =>
          @model.selectedMetric cb.value

    getRackData: ->
      @resourceCount = 0
      @resources     = if IRVController.LIVE then IRVController.LIVE_RESOURCES else IRVController.OFFLINE_RESOURCES
      # load init data, this will need to be pushed from the server at some point
      @getRackDefs()
      @getMetricTemplates()
      @getThresholds()
      @testLoadProgress()
  
  
    getRackDefs: =>
      if IRVController.LIVE
        new Request.JSON(url: @resources.path + @resources.rackDefinitions + '?' + (new Date()).getTime(), onComplete: @receivedRackDefs, onTimeout: @retryRackDefs).get()
      else
        new Request.JSON(url: @resources.path + @resources.rackDefinitions + '?' + (new Date()).getTime(), onSuccess: @receivedRackDefs, onFail: @loadFail, onError: @loadError).get()
  
  
    getMetricTemplates: =>
      if IRVController.LIVE
        new Request.JSON(url: @resources.path + @resources.metricTemplates + '?' + (new Date()).getTime(), onComplete: @receivedMetricTemplates, onTimeout: @retryMetricTemplates).get()
      else
        new Request.JSON(url: @resources.path + @resources.metricTemplates + '?' + (new Date()).getTime(), onSuccess: @receivedMetricTemplates, onFail: @loadFail, onError: @loadError).get()


    getThresholds: =>
      if IRVController.LIVE
        new Request.JSON(url: @resources.path + @resources.thresholds + '?' + (new Date()).getTime(), onComplete: @receivedThresholds, onTimeout: @retryMetricTemplates).get()
      else
        new Request.JSON(url: @resources.path + @resources.thresholds + '?' + (new Date()).getTime(), onSuccess: @receivedThresholds, onFail: @loadFail, onError: @loadError).get()


    receivedThresholds: (thresholds) =>
      parsed = @parser.parseThresholds(thresholds)
      @model.thresholdsByMetric(parsed.byMetric)
      @model.thresholdsById(parsed.byId)
      ++@resourceCount
      @testLoadProgress()
 

    refreshMetricTemplates: =>
      if IRVController.LIVE
        new Request.JSON(url: @resources.path + @resources.metricTemplates + '?' + (new Date()).getTime(), onComplete: @refreshedMetricTemplates, onTimeout: @retryMetricTemplates).get()
      else
        new Request.JSON(url: @resources.path + @resources.metricTemplates + '?' + (new Date()).getTime(), onSuccess: @refreshedMetricTemplates, onFail: @loadFail, onError: @loadError).get()
  
  
    retryMetricTemplates: =>
      setTimeout(@getMetricTemplates, IRVController.API_RETRY_DELAY)
  
  
    retryRackDefs: =>
      setTimeout(@getRackDefs, IRVController.API_RETRY_DELAY)
  
  
    init: ->
      # Hide loader
      Util.setStyle($('loader'), 'visibility', 'hidden')
      @clickAssigned    = true
      @dragging         = false
      @hintTmr          = 0
      @clickTmr         = 0
      @ev               = {}
      @scrollAdjust     = Util.getScrollbarThickness()
      @keysPressed      = {}
      @autoSelectMetric = true

      # Store global reference to controller
      document.IRV = @
  
      @chartEl     = $('graph_container')
      @rackEl      = $('rack_container')
      @rackParent  = $('rack_view')
      @thumbEl     = $('thumb_nav')
      @filterBarEl = $('colour_map')

      # nav links
      Events.addEventListener($('reset_filters'), 'click', @evResetFilters)
      Events.addEventListener($('zoom_in_btn'), 'click', @evZoomIn)
      Events.addEventListener($('zoom_out_btn'), 'click', @evZoomOut)
      Events.addEventListener($('reset_zoom_btn'), 'click', @evResetZoom)
      Events.addEventListener($('hideSideContent'), 'click', @evHideNav)
      Events.addEventListener($('metrics'), 'mouseup', @evMouseUpMetricSelect)
      Events.addEventListener($('metrics'), 'blur', @evBlurSelect)
  
      Events.addEventListener(@filterBarEl, 'filterBarSetAnchor', @evDropFilterBar)
      Events.addEventListener(@rackEl, 'rackSpaceZoomComplete', @evZoomComplete)
      Events.addEventListener(@rackEl, 'rackSpaceFlipComplete', @evFlipComplete)
      Events.addEventListener(@rackEl, 'rackSpaceReset', @evReset)
      Events.addEventListener(@rackEl, 'scroll', @evScrollRacks)
      Events.addEventListener(window, 'keydown', @evKeyDown)
      Events.addEventListener(window, 'keyup', @evKeyUp)
      Events.addEventListener(@rackEl, 'mouseout', @evMouseOutRacks)
      Events.addEventListener(@thumbEl, 'mouseout', @evMouseOutThumb)
      Events.addEventListener(document.window, 'resize', @evResize)
      Events.addEventListener(@filterBarEl, 'mousedown', @evMouseDownFilter)
      Events.addEventListener(@filterBarEl, 'mouseup', @evMouseUpFilter)
      Events.addEventListener(@filterBarEl, 'mouseout', @evMouseOutFilter)
      Events.addEventListener(window, 'getHintInfo', @evGetHintInfo)
  
      @updateLayout()
  
      # Rack Space
      @rackSpace = new RackSpace(@rackEl, @chartEl, @model)
      @enableMouse()
  
      # thumb navigation
      @thumb = new ThumbNav(@thumbEl, IRVController.THUMB_WIDTH, IRVController.THUMB_HEIGHT, @model)
  
      # colour map
      @filterBar = new FilterBar(@filterBarEl, @rackParent, @model)

      # preset manager
      @presets = new PresetManager(@model, @dcpvSettings.selectedMetric?)
      
      @groups = new StaticGroupManager(@model)

      @updateMsg = new UpdateMsg(@rackParent, [$('side_bar')])
  
      # set up subscriptions
      @model.showChart.subscribe(@updateLayout)
      @model.selectedMetric.subscribe(@switchMetric)
      @model.face.subscribe(@switchFace)
      @model.filters.subscribe(@applyFilter)
  
      device_lookup = @model.deviceLookup()
      # resolve a list of included chassis and devices. This will be
      # used to filter metrics when sending the request
      @apiFilter = { device_ids: [], chassis_ids: [] }
      groups = ['chassis', 'devices']
      for id of device_lookup.chassis
        @apiFilter.chassis_ids.push(id) if device_lookup.chassis[id].instance.complex
      @apiFilter.device_ids.push(id) for id of device_lookup.devices

      @apiFilter.chassis_ids = JSON.stringify(@apiFilter.chassis_ids)
      @apiFilter.device_ids  = JSON.stringify(@apiFilter.device_ids)

      # request initial metric data and start poller
      @loadMetrics() if @model.selectedMetric()?
      @metricTmr = setInterval(@loadMetrics, IRVController.METRIC_POLL_RATE)
      @connectMetricCombos()
      @tooltip = new Tooltip()

      @loadBreaches()

      # since the metrics and breaches poll at the same rate (default), put
      # the requests out of phase with each other
      setTimeout(=>
        @breachTmr = setInterval(@loadBreaches, IRVController.BREACH_POLL_RATE)
      , IRVController.BREACH_POLL_RATE / 2)

      @applyCrossAppSettings()
      CrossAppSettings.clear()


    loadBreaches: =>
      new Request.JSON(url: @resources.path + @resources.breaches + '?' + (new Date()).getTime(), onSuccess: @evReceivedBreaches).get()


    evReceivedBreaches: (breaches) =>
      device_lookup    = @model.deviceLookup()
      groups           = @model.groups()
      breaching        = {}
      breaching[group] = {} for group in groups

      for group of breaches
        continue unless breaching[group]?
        breaching[group][id] = true for id in breaches[group]

      @model.breaches(breaching)


    applyCrossAppSettings: ->
      selected_metric = @dcpvSettings.selectedMetric

      if selected_metric?
        @model.selectedMetric(selected_metric)
        @model.filters()[selected_metric] = @dcpvSettings.filters[selected_metric]
        @model.colourMaps()[selected_metric] = @dcpvSettings.colourMaps[selected_metric]
        @model.filters(@model.filters())
        @model.colourMaps(@model.colourMaps())

    
    showUpdateMsg: ->
      @updateMsg.show()


    hideUpdateMsg: ->
      @updateMsg.hide()

  
    # direction should be 1 or -1, indicating wether to zoom in or out
    stepZoom: (direction, x, y) ->
      @showUpdateMsg()

      x = @rackEl.scrollLeft + (@rackElDims.width / 2) unless x?
      y = @rackEl.scrollTop + (@rackElDims.height / 2) unless y?
  
      @zooming = true
      clearTimeout(@hintTmr)
      @disableMouse()
      @rackSpace.quickZoom(x, y, @rackSpace.scale + (@rackSpace.scale * IRVController.STEP_ZOOM_AMOUNT * direction))
  
  
    zoomToPreset: (direction, x, y, cyclical = true) ->
      @showUpdateMsg()

      x = @rackEl.scrollLeft + (@rackElDims.width / 2) unless x?
      y = @rackEl.scrollTop + (@rackElDims.height / 2) unless y?
  
      @zooming = true
      clearTimeout(@hintTmr)
      @disableMouse()
      @rackSpace.zoomToPreset(direction, x, y, cyclical)


    evReset: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @resetFilters()
      @resetZoom()


    evResetZoom: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @resetZoom()


    resetZoom: ->
      @updateMsg.show()
      @rackSpace.resetZoom()


    evResetFilters: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @resetFilters()


    resetFilters: ->
      groups       = @model.groups()
      blank        = {}
      blank[group] = {} for group in groups
  
      @model.activeFilter(false)
      @model.filteredDevices(blank)
      @model.activeSelection(false)
      @model.selectedDevices(blank)
  
      selected_metric          = @model.selectedMetric()
      filters                  = @model.filters()
      filters[selected_metric] = {}
      @model.filters(filters)
  

    evZoomIn: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @zoomToPreset(1, null, null, false)


    evZoomOut: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @zoomToPreset(-1, null, null, false)

  
    saveScreen: =>
      alert_dialog IRVController.EXPORT_MESSAGE
      ts       = new Date()
      filename = IRVController.SCREENSHOT_FILENAME
      filename = Util.substitutePhrase(filename, 'day', Util.addLeadingZeros(ts.getUTCDate()))
      filename = Util.substitutePhrase(filename, 'month', Util.addLeadingZeros(ts.getUTCMonth() + 1))
      filename = Util.substitutePhrase(filename, 'year', Util.addLeadingZeros(ts.getUTCFullYear()))
      filename = Util.substitutePhrase(filename, 'hours', Util.addLeadingZeros(ts.getUTCHours()))
      filename = Util.substitutePhrase(filename, 'minutes', Util.addLeadingZeros(ts.getUTCMinutes()))
      filename = Util.substitutePhrase(filename, 'seconds', Util.addLeadingZeros(ts.getUTCSeconds()))
      filename = Util.cleanUpSubstitutions(filename)
  
      b64_img     = @grabScreen()
      dl          = document.createElement('a')
      dl.href     = b64_img.replace('image/png', 'image/octet-stream')
      dl.download = filename
  
      #document.body.appendChild(dl)
      #dl.click()
      #document.body.removeChild(dl)

      #output =
      #  filename : filename
      #  images   : [ { type: "png", data: b64_img.split(',')[1] }, { type: "svg", data: document.getElementsByTagName('svg')[0].parentElement.innerHTML }]

      #console.log(JSON.stringify(output))

      addFrmVal = (name, value) ->
        val       = document.createElement('input')
        val.name  = name
        val.type  = 'hidden'
        val.value = value
        frm.appendChild(val)

      svg = document.getElementsByTagName('svg')[0]

      # IE doesn't report the parent of our SVG graph. If you're sitting there thinking
      # hmmmm this is a particularly hooky way of extracting the SVG string, you're 
      # probably right
      if svg.parentElement?
        dim_ref = svg.parentElement
        svg_str = svg.parentElement.innerHTML
      else
        dim_ref = $('graph_container')
        svg_str = '<svg' + $('graph_container').innerHTML.split('<svg')[1].split('</svg>')[0] + '</svg>'
        console.log svg_str

      svg_dims = dim_ref.getCoordinates()

      frm         = document.createElement('form')
      frm.name    = 'imagePost-a-tron'
      frm.method  = 'post'
      frm.enctype = 'multipart/form-data'
      addFrmVal('filename', filename)
      addFrmVal('type[]', 'text')
      addFrmVal('data[]', b64_img.split(',')[1])
      addFrmVal('size[]', String(@rackSpace.coordReferenceEl.width) + 'x' + String(@rackSpace.coordReferenceEl.height))
      addFrmVal('type[]', 'svg')
      addFrmVal('data[]', svg_str)
      addFrmVal('size[]', String(Math.ceil(svg_dims.width)) + 'x' + String(Math.ceil(svg_dims.height)))
      addFrmVal('authenticity_token', $$('meta[name="csrf-token"]')[0].getAttribute('content'))

      document.body.appendChild(frm)
      frm.action = IRVController.EXPORT_IMAGE_URL
      frm.submit()
      #new Request(url: IRVController.EXPORT_IMAGE_URL).post(frm)
  
  
    printScreen: ->
      html = '<div style="height: 650px;">' + document.getElementById('rack_container').innerHTML + '</div><div></div><div>' + document.getElementById('graph_container').innerHTML + '</div>'
      Util.printHtmlInNewPage( html )
  
    exportData: ->
      selected_metric = @model.selectedMetric()
      return unless selected_metric?
  
      data          = @model.metricData()
      metric        = @model.metricTemplates()[selected_metric]
      groups        = @model.groups()
      device_lookup = @model.deviceLookup()
  
      output = IRVController.EXPORT_HEADER
      output = Util.substitutePhrase(output, 'metric_name', metric.name)
      output = Util.substitutePhrase(output, 'metric_units', metric.units)
      output = Util.substitutePhrase(output, 'metric_name', metric.units)
  
      for group in groups
        group_lookup = device_lookup[group]
        values       = data.values[group]
  
        for id of values
          device = group_lookup[id]
  
          if device?
            record = IRVController.EXPORT_RECORD
            record = Util.substitutePhrase(record, 'device_name', device.name)
            record = Util.substitutePhrase(record, 'device_id', device.id)
            record = Util.substitutePhrase(record, 'value', values[id])
  
            output += record + String.fromCharCode(10)
  
      ts       = new Date()
      filename = IRVController.EXPORT_FILENAME
      filename = Util.substitutePhrase(filename, 'metric_name', metric.name)
      filename = Util.substitutePhrase(filename, 'day', Util.addLeadingZeros(ts.getUTCDate()))
      filename = Util.substitutePhrase(filename, 'month', Util.addLeadingZeros(ts.getUTCMonth() + 1))
      filename = Util.substitutePhrase(filename, 'year', Util.addLeadingZeros(ts.getUTCFullYear()))
      filename = Util.substitutePhrase(filename, 'hours', Util.addLeadingZeros(ts.getUTCHours()))
      filename = Util.substitutePhrase(filename, 'minutes', Util.addLeadingZeros(ts.getUTCMinutes()))
      filename = Util.substitutePhrase(filename, 'seconds', Util.addLeadingZeros(ts.getUTCSeconds()))
      filename = Util.cleanUpSubstitutions(filename)
  
      dl          = document.createElement('a')
      dl.href     = 'data:text/octet-stream;charset=utf-8,' + encodeURIComponent(output)
      dl.download = filename
  
      document.body.appendChild(dl)
      dl.click()
      document.body.removeChild(dl)
  
  
    grabScreen: ->
      width  = @rackSpace.rackGfx.cvs.width
      height = @rackSpace.rackGfx.cvs.height# + Util.getStyleNumeric(@chartEl, 'height')
  
      cvs           = document.createElement('canvas')
      cvs.width     = width
      cvs.height    = height
      ctx           = cvs.getContext('2d')
      ctx.fillStyle = '#ffffff'
  
      ctx.fillRect(0, 0, width, height)
  
      ctx.drawImage(@rackSpace.rackGfx.cvs, 0, 0)
      ctx.drawImage(@rackSpace.infoGfx.cvs, 0, 0)
      ctx.drawImage(@rackSpace.alertGfx.cvs, 0, 0)

      #try
        #ctx.drawSvg(@rackSpace.chart.chart.getSVG().div.innerHTML, 0, @rackSpace.rackGfx.cvs.height)
        #ctx.drawSvg(document.getElementsByTagName('svg')[0], 0, @rackSpace.rackGfx.cvs.height)
      #catch e
        # ignore canvg error
      cvs.toDataURL()
  
  
    evFocusMetricSelect: (ev) ->
      return if @refreshingMetrics
      @refreshMetricTemplates()


    evMouseUpMetricSelect: (ev) =>
      return unless @autoSelectMetric
      @autoSelectMetric = false
      ev.target.select()


    evBlurSelect: (ev) =>
      @autoSelectMetric = true


    outputModel: ->
      for i of @model
        try
        catch e
        

    updatePreset: ->
      @presets.updatePreset()


    evHideNav: (ev) =>
      setTimeout(@updateLayout, IRVController.NAV_HIDE_LAYOUT_UPDATE_DELAY)
  
  
    evResize: (ev) =>
      @updateLayout()
  
  
    updateLayout: =>
      rack_height_proportion = if @model.showChart() then IRVController.RACK_PAGE_HEIGHT_PROPORTION else 1

      Util.setStyle(@rackParent, 'height', rack_height_proportion * 100 + '%')
      Util.setStyle(@chartEl, 'top', rack_height_proportion * 100 + '%')
      Util.setStyle(@chartEl, 'height', (1 - rack_height_proportion) * 100 + '%')

      dims     = @rackParent.getCoordinates()
      fb_dims  = @filterBarEl.getCoordinates()
      fb_align = if @filterBar? then @filterBar.alignment else FilterBar.DEFAULT_ALIGN

      switch fb_align
        when FilterBar.ALIGN_TOP
          Util.setStyle(@rackEl, 'top', FilterBar.THICKNESS)
          Util.setStyle(@rackEl, 'left', 0)
          Util.setStyle(@rackEl, 'width', '100%')
          Util.setStyle(@rackEl, 'height', (dims.height - FilterBar.THICKNESS) + 'px')
        when FilterBar.ALIGN_BOTTOM
          Util.setStyle(@rackEl, 'top', 0)
          Util.setStyle(@rackEl, 'left', 0)
          Util.setStyle(@rackEl, 'width', '100%')
          Util.setStyle(@rackEl, 'height', (dims.height - FilterBar.THICKNESS) + 'px')
        when FilterBar.ALIGN_LEFT
          Util.setStyle(@rackEl, 'top', 0)
          Util.setStyle(@rackEl, 'left', FilterBar.THICKNESS)
          Util.setStyle(@rackEl, 'width', (dims.width - FilterBar.THICKNESS) + 'px')
          Util.setStyle(@rackEl, 'height', '100%')
        when FilterBar.ALIGN_RIGHT
          Util.setStyle(@rackEl, 'top', 0)
          Util.setStyle(@rackEl, 'left', 0)
          Util.setStyle(@rackEl, 'width', (dims.width - FilterBar.THICKNESS) + 'px')
          Util.setStyle(@rackEl, 'height', '100%')

      @rackElDims  = @rackEl.getCoordinates()
      @chartElDims = @chartEl.getCoordinates()

      if @rackSpace?
        @filterBar.updateLayout()
        @rackSpace.updateLayout()
  
  
    switchMetric: (metric) =>
      # clear the preset selection if the metric we're switching to is not
      # the one stored agains the current preset
      selected_preset = @model.selectedPreset()
      if selected_preset?
        presets = @model.presetsById()
        for id of presets
          if presets[id].name is selected_preset
            associated_metric = presets[id].values.selectedMetric
            associated_metric = associated_metric.substr(1, associated_metric.length - 2)
            break

        @model.selectedPreset(null) unless associated_metric is metric

      @loadMetrics()
      clearInterval(@metricTmr)
      @metricTmr = setInterval(@loadMetrics, IRVController.METRIC_POLL_RATE)

      # reset filter
      if @model.activeFilter() and (not filter? or (filter.max is colour_map.high and filter.min is colour_map.low))
        groups       = @model.groups()
        blank        = {}
        blank[group] = {} for group in groups
        @model.activeFilter(false)
        @model.filteredDevices(blank)
  
  
    receivedMachineTemplates: (machine_templates) =>
      @model.templates(machine_templates)
      ++@resourceCount
      @testLoadProgress()
  
  
    receivedMetricTemplates: (metric_templates) =>
      templates = @parser.parseMetricTemplates(metric_templates)
      @model.metricTemplates(templates)

      metrics_available = false
      for i of templates
        metrics_available = true
        break

      $('metrics').value = 'Select a metric' if metrics_available
      ++@resourceCount
      @testLoadProgress()
  
  
    refreshedMetricTemplates: (metric_templates) =>
      templates = @parser.parseMetricTemplates(metric_templates)
  
      # look for changes
      old     = @model.metricTemplates()
      changed = false
      # check for additions
      for id of templates
        unless old[id]?
          changed = true
          break
  
      # check for deletions (only if we haven't already found changes)
      unless changed
        for id of old
          unless templates[id]?
            changed = true
            break
  
      # update model only if there are changes
      @model.metricTemplates(templates) if changed
  
  
    receivedRackDefs: (rack_defs) =>
      defs = @parser.parseRackDefs(rack_defs, @dcpvSettings.selectedRacks)
      @model.assetList(defs.assetList)
      @model.racks(defs.racks)
      @model.deviceLookup(defs.deviceLookup)

      ++@resourceCount
      @testLoadProgress()
  
  
    testLoadProgress: ->
      assets = @model.assetList()
  
      if assets? and assets.length > 0
        num_assets = assets.length
        progress   = ((@assetCount + @resourceCount) / (IRVController.NUM_RESOURCES + num_assets) * 100).toFixed(1)
        @init() if @resourceCount is IRVController.NUM_RESOURCES and @assetCount is num_assets
      else
        progress = 0
  
      $('dialogue').innerHTML = IRVController.RESOURCE_LOAD_CAPTION.replace(/\[\[progress\]\]/g, progress + '%')
  
  
    loadError: (err_str, err) ->
  
  
    loadFail: (failee) ->
  
  
    loadMetrics: =>
      selected_metric = @model.selectedMetric()
      return unless selected_metric?

      if IRVController.LIVE
        new Request.JSON(
          url        : @resources.path + @resources.metricData.replace(/\[\[metric_id\]\]/g, selected_metric) + '?' + (new Date()).getTime()
          onComplete : @receivedMetrics
          headers    : {'X-CSRF-Token': $$('meta[name="csrf-token"]')[0].getAttribute('content')}
          data       : @apiFilter
        ).send()
      else
        new Request.JSON(
          url       : @resources.path + @resources.metricData.replace(/\[\[metric_id\]\]/g, selected_metric) + '?' + (new Date()).getTime()
          onSuccess : @receivedMetrics
          onError   : @loadError
          onFail    : @loadFail
          data      : @apiFilter
        ).get()
  
  
    receivedMetrics: (metrics) =>
      # display update message and display metrics a short while after
      # this allows the screen to redraw once the update message has been
      # added, otherwise the process will be tied up in the processing of
      # the metrics and the update message will not be displayed. There
      # us a Util function forceImmediateRedraw which might negate the need
      # for a timeout here but I can't test if this actually works with
      # my small datacentre
      @showUpdateMsg()
      clearTimeout(@dispTmr)
      @dispTmr = setTimeout(@displayMetrics, 50, metrics)


    displayMetrics: (metrics) =>
      metrics = @parser.parseMetrics(metrics)

      filters  = @model.filters()
      filter   = filters[metrics.metricId]
      col_maps = @model.colourMaps()
      col_map  = col_maps[metrics.metricId]

      if col_map?
        @model.metricData(metrics)
        @applyFilter() if filter.max isnt col_map.high or filter.min isnt col_map.low
      else
        # set default maps/filters if not already set
        unless filters[metrics.metricId]?
          filters[metrics.metricId] = {}
          @model.filters(filters)

        # determin min max
        group_vals = metrics.values[@model.metricLevel()]
        min        = Number.MAX_VALUE
        max        = -Number.MAX_VALUE
        for id of group_vals
          val = Number(group_vals[id])
          min = val if val < min
          max = val if val > max

        if min is Number.MAX_VALUE
          range = 1e-100
          min   = 0
          max   = 0
        else
          range = max - min

        col_maps[metrics.metricId] = { low: min, high: max, range: range, inverted: false }

        @model.colourMaps(col_maps)
        @model.metricData(metrics)

      @hideUpdateMsg()
  
  
    evMouseWheelRack: (ev) =>
      if @keysPressed[IRVController.ZOOM_KEY]
        ev.preventDefault()
        ev.stopPropagation()
        coords = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
        delta = ev.wheelDelta ? -ev.detail
        @stepZoom(delta / Math.abs(delta), coords.x, coords.y)
  
  
    evMouseWheelThumb: (ev) =>
      if @keysPressed[IRVController.ZOOM_KEY]
        ev.preventDefault()
        ev.stopPropagation()
        coords = Util.resolveMouseCoords(@thumbEl, ev)
        @stepZoom(ev.wheelDelta / Math.abs(ev.wheelDelta), coords.x / @thumb.width * @rackSpace.coordReferenceEl.width, coords.y / @thumb.height * @rackSpace.coordReferenceEl.height)
  
  
    evKeyDown: (ev) =>
      @keysPressed[ev.keyCode] = true
  
  
    evKeyUp: (ev) =>
      @keysPressed[ev.keyCode] = false
  
  
    evRightClickRack: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      @clickAssigned = true
      coords         = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
      pos            = @rackEl.getPosition()
      # if the view decides to display a custom context menu then prevent the default menu from showing
      @rackSpace.showContextMenu({ x: ev.clientX - pos.x, y: ev.clientY - pos.y}, coords)
  
  
    evMouseDownRack: (ev) =>
      # ignore anything other than left-clicks
      return if (ev.which? and ev.which isnt 1) or (ev.button? and ev.button isnt 0)
      coords      = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
      rack_coords = Util.resolveMouseCoords(@rackEl, ev)

      # ensure mouse down hasn't originated from the scrollbar region
      if rack_coords.x - @rackEl.scrollLeft < @rackElDims.width - @scrollAdjust and rack_coords.y - @rackEl.scrollTop < @rackElDims.height - @scrollAdjust
        @downCoords = coords
        Events.addEventListener(@rackEl, 'mousemove', @evDrag)
  
  
    evMouseUpRack: (ev) =>
      # ignore anything other than left-clicks
      return if (ev.which? and ev.which isnt 1) or (ev.button? and ev.button isnt 0)

      @upCoords = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
      clearTimeout(@clickTmr)
      Events.removeEventListener(@rackEl, 'mousemove', @evDrag)
  
      # decide if this is a single or double-click
      if @dragging
        coords = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
        @rackSpace.stopDrag(coords.x, coords.y)
        @clickAssigned = true
        @dragging      = false
      else if @clickAssigned
        @clickAssigned = false
        @clickTmr      = setTimeout(@evClick, IRVController.DOUBLE_CLICK_TIMEOUT, ev)
      else
        @clickAssigned = true
        @evDoubleClick(ev)
  
  
    evClick: (ev) =>
      unless @clickAssigned
        @clickAssigned = true
        coords         = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
        @dragging      = false
  
        @rackSpace.click(coords.x, coords.y)
        Events.removeEventListener(@rackEl, 'mousemove', @evDrag)
  
  
    evDoubleClick: (ev) =>
      # clear selection
      if document.selection && document.selection.empty
          document.selection.empty()
      else if window.getSelection
          sel = window.getSelection()
          sel.removeAllRanges()
  
      ev.preventDefault()
      ev.stopPropagation()
      @zoomToPreset(1, @downCoords.x, @downCoords.y)
  
  
    evDrag: (ev) =>
      coords = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)
      if not @dragging
        @dragging = Math.sqrt(Math.pow(coords.x - @downCoords.x, 2) + Math.pow(coords.y - @downCoords.y, 2)) > IRVController.DRAG_ACTIVATION_DIST
        if @dragging
          @clickAssigned = true
          @rackSpace.startDrag(@downCoords.x, @downCoords.y)
      else
        @rackSpace.drag(coords.x, coords.y)
  
  
    evMouseMoveRack: (ev) =>
      # side step annoying false move events
      return if ev.clientX is @ev.clientX and ev.clientY is @ev.clientY
      clearTimeout(@hintTmr)
      div_coords    = Util.resolveMouseCoords(@rackEl, ev)
      div_coords.x -= @rackEl.scrollLeft
      div_coords.y -= @rackEl.scrollTop
      coords        = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev)

      # exit if moving over scrollbar area
      return if div_coords.x > @rackElDims.width - @scrollAdjust or div_coords.y > @rackElDims.height - @scrollAdjust

      @hintTmr = setTimeout(@showRackHint, IRVController.RACK_HINT_HOVER_DELAY)
      @ev      = ev
  
      @rackSpace.hideHint()
      unless @dragging
        coords = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, ev, true)
        @rackSpace.highlightAt(coords.x, coords.y)
  
  
    evMouseMoveThumb: (ev) =>
      # side step annoying false move events
      return if ev.clientX is @ev.clientX and ev.clientY is @ev.clientY
  
      clearTimeout(@hintTmr)
      @hintTmr = setTimeout(@showThumbHint, IRVController.THUMB_HINT_HOVER_DELAY)
      @ev = ev
      @thumb.hideHint()
  
  
    evMouseOutRacks: (ev) =>
      clearTimeout(@hintTmr)
      @rackSpace.evMouseOut(ev)
  
  
    evMouseOutThumb: (ev) =>
      clearTimeout(@hintTmr)
  
  
    evZoomComplete: (ev) =>
      @hideUpdateMsg()
      @zooming = false
      @updateThumb()
      @enableMouse()
  
  
    evFlipComplete: (ev) =>
      @hideUpdateMsg()
      @flipping = false
      @enableMouse()
  
  
    evMouseDownChart: (ev) =>
      coords      = Util.resolveMouseCoords(@chartEl, ev)
      @downCoords = coords
      Events.addEventListener(@chartEl, 'mousemove', @evDragChart)
  
  
    evMouseUpChart: (ev) =>
      @upCoords = Util.resolveMouseCoords(@chartEl, ev)
      Events.removeEventListener(@chartEl, 'mousemove', @evDragChart)
  
      if @dragging
        coords = Util.resolveMouseCoords(@chartEl, ev)
        @rackSpace.stopDragChart(coords.x, coords.y)
        @dragging = false
  
  
    evDragChart: (ev) =>
      coords = Util.resolveMouseCoords(@chartEl, ev)
      if not @dragging
        @dragging = Math.sqrt(Math.pow(coords.x - @downCoords.x, 2) + Math.pow(coords.y - @downCoords.y, 2)) > IRVController.DRAG_ACTIVATION_DIST
        if @dragging
          @rackSpace.startDragChart(@downCoords.x, @downCoords.y)
      else
        @rackSpace.dragChart(coords.x, coords.y)
  
  
    showRackHint: =>
      coords = Util.resolveMouseCoords(@rackSpace.coordReferenceEl, @ev)
      pos    = @rackEl.getPosition()
      @rackSpace.showHint({ x: @ev.clientX - pos.x, y: @ev.clientY - pos.y }, coords)
  
  
    showThumbHint: =>
      coords    = Util.resolveMouseCoords(@thumbEl, @ev)
      coords.x /= @thumb.scale * @rackSpace.scale
      coords.y /= @thumb.scale * @rackSpace.scale
  
      device = @rackSpace.getDeviceAt(coords.x, coords.y)
      @thumb.showHint(device, @ev.clientX + IRVController.THUMB_HINT_OFFSET_X, @ev.clientY + IRVController.THUMB_HINT_OFFSET_Y) if device?
  
  
    disableMouse: ->
      Events.removeEventListener(@chartEl, 'mousedown', @evMouseDownChart)
      Events.removeEventListener(@chartEl, 'mouseup', @evMouseUpChart)
      Events.removeEventListener(@chartEl, 'mousemove', @evMouseMoveChart)
  
      Events.removeEventListener(@rackEl, 'mousedown', @evMouseDownRack)
      Events.removeEventListener(@rackEl, 'mouseup', @evMouseUpRack)
      Events.removeEventListener(@rackEl, 'mousemove', @evMouseMoveRack)
      Events.removeEventListener(@rackEl, 'contextmenu', @evRightClickRack)
      Events.removeEventListener(@rackEl, 'mousewheel', @evMouseWheelRack)
      Events.removeEventListener(@rackEl, 'DOMMouseScroll', @evMouseWheelRack)
  
      Events.removeEventListener(@thumbEl, 'mousedown', @evMouseDownThumb)
      Events.removeEventListener(@thumbEl, 'mouseup', @evMouseUpThumb)
      Events.removeEventListener(@thumbEl, 'mousewheel', @evMouseWheelThumb)
      Events.removeEventListener(@thumbEl, 'DOMMouseScroll', @evMouseWheelThumb)
      Events.removeEventListener(@thumbEl, 'dblclick', @evDoubleClickThumb)
      Events.removeEventListener(@thumbEl, 'mousemove', @evMouseMoveThumb)
  
  
    enableMouse: ->
      Events.addEventListener(@chartEl, 'mousedown', @evMouseDownChart)
      Events.addEventListener(@chartEl, 'mouseup', @evMouseUpChart)
      Events.addEventListener(@chartEl, 'mousemove', @evMouseMoveChart)
  
      Events.addEventListener(@rackEl, 'mousedown', @evMouseDownRack)
      Events.addEventListener(@rackEl, 'mouseup', @evMouseUpRack)
      Events.addEventListener(@rackEl, 'mousemove', @evMouseMoveRack)
      Events.addEventListener(@rackEl, 'contextmenu', @evRightClickRack)
      Events.addEventListener(@rackEl, 'DOMMouseScroll', @evMouseWheelRack)
      Events.addEventListener(@rackEl, 'mousewheel', @evMouseWheelRack)
  
      Events.addEventListener(@thumbEl, 'mousedown', @evMouseDownThumb)
      Events.addEventListener(@thumbEl, 'mouseup', @evMouseUpThumb)
      Events.addEventListener(@thumbEl, 'mousewheel', @evMouseWheelThumb)
      Events.addEventListener(@thumbEl, 'DOMMouseScroll', @evMouseWheelThumb)
      Events.addEventListener(@thumbEl, 'dblclick', @evDoubleClickThumb)
      Events.addEventListener(@thumbEl, 'mousemove', @evMouseMoveThumb)
  
  
    updateThumb: ->
      @thumb.update(@rackElDims.width, @rackElDims.height, @rackSpace.coordReferenceEl.width, @rackSpace.coordReferenceEl.height, @rackEl.scrollLeft, @rackEl.scrollTop)
  
  
    # applies above/below/between filter and stores subset in the view model
    applyFilter: =>
      filters         = @model.filters()
      selected_metric = @model.selectedMetric()
      metrics         = @model.metricData()

      min = filters[selected_metric].min
      max = filters[selected_metric].max

      filtered_devices = {}
      groups           = @model.groups()
      filtered_devices[group] = {} for group in groups
  
      gt = (val) =>
        return val > min
  
      lt = (val) =>
        return val < max
  
      between = (val) =>
        return val > min and val < max
  
      if min? and max?
        filter = between
      else if min?
        filter = gt
      else if max?
        filter = lt
      else
        @model.activeFilter(false)
        @model.filteredDevices(filtered_devices)
        return

      is_valid = false
      # apply filter to each device
      for group in groups
        for id of metrics.values[group]
          is_valid = true
          filtered_devices[group][id] = filter(Number(metrics.values[group][id]))

      # it's possible to be applying a filter before any data has been received
      # (when settings are carried over from the DCPV) in these cases we should
      # set activeFilter to false
      unless is_valid
        @model.activeFilter(false)
        @model.filteredDevices(filtered_devices)
        return
  
      @model.activeFilter(true)
      @model.filteredDevices(filtered_devices)
  
  
    evScrollRacks: =>
      @updateThumb() unless @zooming or @flipping
  
  
    evLoadRackAssets: =>
      @assetCount = 0
      assets      = @model.assetList()
      assets.push(Rack.IMG_FRONT_BTM, Rack.IMG_FRONT_TOP, Rack.IMG_FRONT_REPEAT, Rack.IMG_REAR_BTM, Rack.IMG_REAR_TOP, Rack.IMG_REAR_REPEAT)
      AssetManager.get(IRVController.PRIMARY_IMAGE_PATH + asset, @evAssetLoaded, @evAssetFailed) for asset in assets
  
      @assetSub.dispose()
      @model.assetList(assets)
  
  
    evAssetLoaded: =>
      ++@assetCount
      @testLoadProgress()


    evAssetFailed: (path) =>
      image = path.substr(IRVController.PRIMARY_IMAGE_PATH.length)
      AssetManager.get(IRVController.SECONDARY_IMAGE_PATH + path.substr(IRVController.PRIMARY_IMAGE_PATH.length), @evAssetLoaded, @evAssetDoubleFailed)


    # image has failed to load from both the primary and secondary locations, it doesn't exist so scrap if out of the queue
    evAssetDoubleFailed: (path) =>
      image  = path.substr(IRVController.SECONDARY_IMAGE_PATH.length)
      assets = @model.assetList()
      idx    = assets.indexOf(image)
      assets.splice(idx, 1) unless idx is -1

  
    evMouseDownThumb: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
  
      @thumbScroll(ev)
      Events.addEventListener(@thumbEl, 'mousemove', @thumbScroll)
  
  
    evMouseUpThumb: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
  
      @thumbScroll(ev)
      Events.removeEventListener(@thumbEl, 'mousemove', @thumbScroll)
  
  
    evDoubleClickThumb: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
  
      coords = Util.resolveMouseCoords(@thumbEl, ev)
      @zoomToPreset(1, coords.x / @thumb.width * @rackSpace.coordReferenceEl.width, coords.y / @thumb.height * @rackSpace.coordReferenceEl.height)
  
  
    thumbScroll: (ev) =>
      coords    = Util.resolveMouseCoords(@thumbEl, ev)
  
      @rackEl.scrollLeft = (coords.x / @thumb.width * @rackSpace.coordReferenceEl.width) - (@rackElDims.width / 2)
      @rackEl.scrollTop  = (coords.y / @thumb.height * @rackSpace.coordReferenceEl.height) - (@rackElDims.height / 2)
  
  
    switchFace: =>
      @showUpdateMsg()
      @flipping = true
  
  
    evMouseDownFilter: (ev) =>
      return if ev.target instanceof HTMLInputElement
  
      ev.preventDefault()
      ev.stopPropagation()
  
      coords      = Util.resolveMouseCoords(@filterBarEl, ev)
      @slider     = @filterBar.getSliderAt(coords.x, coords.y)
      @dragging   = false
      @downCoords = coords
  
      if @slider?
        Events.addEventListener(@filterBarEl, 'mousemove', @evMouseMoveFilter)
      else
        Events.addEventListener(document.window, 'mousemove', @evMouseMoveFilter)
  
  
    evMouseOutFilter: (ev) =>
      Events.removeEventListener(@filterBarEl, 'mousemove', @evMouseMoveFilter)
  
  
    evMouseUpFilter: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
  
      @filterBar.stopDrag()
  
      Events.removeEventListener(@filterBarEl, 'mousemove', @evMouseMoveFilter)
      Events.removeEventListener(document.window, 'mousemove', @evMouseMoveFilter)
  
  
    evMouseMoveFilter: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
  
      coords = Util.resolveMouseCoords(@filterBarEl, ev)
  
      if @slider?
        @filterBar.dragSlider(@slider, coords.x, coords.y)
      else
        if @dragging
          @filterBar.dragBar(ev.pageX, ev.pageY)
        else
          # commence dragging only if the user has moved the mouse a certain distance
          @dragging = Math.sqrt(Math.pow(coords.x - @downCoords.x, 2) + Math.pow(coords.y - @downCoords.y, 2)) > IRVController.DRAG_ACTIVATION_DIST
          if @dragging
            @filterBar.startDrag()
            Events.addEventListener(document.window, 'mouseup', @evFilterStopDrag)
  
  
    evFilterStopDrag: (ev) =>
      @filterBar.stopDrag()
      Events.removeEventListener(document.window, 'mousemove', @evMouseMoveFilter)
  
  
    evGetHintInfo: (ev) =>
      url = @resources.path + @resources.hintData.replace(/\[\[device_id\]\]/g, @rackSpace.hint.device.id) + '?' + (new Date()).getTime()
      url = url.replace(/\[\[group\]\]/g, @rackSpace.hint.device.group)
  
      if IRVController.LIVE
        new Request.JSON(url: url, onComplete: @hintInfoReceived).get()
      else
        new Request.JSON(url: url, onSuccess: @hintInfoReceived, onError: @loadError, onFail: @loadFail).get()


    evDropFilterBar: (ev) =>
      @updateLayout()
  
  
    hintInfoReceived: (hint_info) =>
      @rackSpace.hint.appendData(hint_info)
