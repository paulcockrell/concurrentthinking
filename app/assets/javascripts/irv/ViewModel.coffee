define ['common/util/Util', 'knockout'], (Util, ko) ->

  class ViewModel
    
    # startup properties, can be overwritten by url parameters
    @INIT_VIEW_MODE     : 'Images and bars'
    @INIT_FACE          : 'front'
    @INIT_METRIC_LEVEL  : 'machine'
    @INIT_GRAPH_ORDER   : 'ascending'
    @INIT_SCALE_METRICS : true
    @INIT_SHOW_CHART    : true
    @INIT_METRIC        : null

    @VIEW_MODE_BOTH    : 'Images and bars'
    @VIEW_MODE_IMAGES  : 'Images only'
    @VIEW_MODE_METRICS : 'Bars only'

    @SELECT_THRESHOLD_CAPTION : 'Select a threshold to display'
    @NO_THRESHOLDS_CAPTION    : 'No thresholds available'

    # statics overwritten by config
    @COLOUR_SCALE: [{ pos: 0, col: '#000000' }, { pos: 1, col: '#ffffff' }]


    constructor: () ->
      # display mode settings
      @viewMode       = ko.observable(ViewModel.INIT_VIEW_MODE)
      @viewModes      = ko.observable([ViewModel.VIEW_MODE_IMAGES, ViewModel.VIEW_MODE_METRICS, ViewModel.VIEW_MODE_BOTH])
      @face           = ko.observable(ViewModel.INIT_FACE)
      @metricLevel    = ko.observable(ViewModel.INIT_METRIC_LEVEL)
      @graphOrder     = ko.observable(ViewModel.INIT_GRAPH_ORDER)
      @graphOrders    = ko.observable(['ascending', 'descending', 'physical position', 'name'])
      @scaleMetrics   = ko.observable(ViewModel.INIT_SCALE_METRICS)
      @showChart      = ko.observable(ViewModel.INIT_SHOW_CHART)
      @selectedMetric = ko.observable(ViewModel.INIT_METRIC)

      # used to determin when to show the update message
      @updating = ko.observable(false)

      # the currently highlighted device
      @highlighted = ko.observable()

      # id groups, both group and id are required to identify an individual device
      @groups = ko.observable(['racks', 'chassis', 'devices'])

      # list of images to preload
      @assetList = ko.observable()

      # arrays of thresholds grouped by their associated metric. Metric id is the key 
      @thresholdsByMetric = ko.observable()
      # object containing all thresholds using id as the key
      @thresholdsById = ko.observable({})
      @availableThresholds = ko.dependentObservable(->
        thresholds = []
        t_by_m     = @thresholdsByMetric()
        metric     = @selectedMetric()
 
        return [] unless t_by_m?
 
        list = t_by_m[metric]
 
        return [] unless list?
 
        thresholds.push(threshold.name) for threshold in list
        thresholds
      , @)

      @enableThresholdSelection = ko.dependentObservable(=>
        thresholds = @availableThresholds()
        thresholds? and thresholds.length > 0
      , @)

      @thresholdSelectCaption = ko.dependentObservable(=>
        if @enableThresholdSelection() then ViewModel.SELECT_THRESHOLD_CAPTION else ViewModel.NO_THRESHOLDS_CAPTION
      , @)

      # name of the selected threshold, set when clicked in the drop down 
      @selectedThresholdName = ko.observable()
      @selectedThresholdId   = ko.dependentObservable(
        read: =>
          t_by_m = @thresholdsByMetric()
          metric = @selectedMetric()
          t_name = @selectedThresholdName()
   
          return unless t_by_m?
   
          list = t_by_m[metric]
   
          return unless list?
   
          for threshold in list
            return threshold.id if threshold.name is t_name

        write: (val) =>
          thold = @thresholdsById()[val]
          if thold?
            @selectedThresholdName(thold.name)
            thold.id
          else
            null
      , @)

      # holds the selected threshold definition 
      @selectedThreshold = ko.dependentObservable(=>
        @thresholdsById()[@selectedThresholdId()]
      , @)

      @presetsById    = ko.observable([])
      @selectedPreset = ko.observable()
      @presetNames    = ko.dependentObservable(->
        presets      = @presetsById()
        preset_names = []
        preset_names.push(presets[i].name) for i of presets
        Util.sortCaseInsensitive(preset_names)
      , @)
      @enablePresetSelection = ko.dependentObservable(->
        presets = @presetNames()
        presets? and presets.length > 0
      , @)

      @groupsById    = ko.observable([])
      @selectedGroup = ko.observable()
      @groupNames    = ko.dependentObservable(->
        presets      = @groupsById()
        preset_names = []
        preset_names.push(presets[i].name) for i of presets
        Util.sortCaseInsensitive(preset_names)
      , @)
      @enableGroupSelection = ko.dependentObservable(->
        groups = @groupNames()
        groups? and groups.length > 0
      , @)

      # active selection is true when at least one chassis/machine is selected (elastic band)
      # used to decide if to show all metrics or use selectedDevices
      @activeSelection = ko.observable(false)
      @selectedDevices = ko.observable({})

      # metrics which satisfy above/below/between filters if any is active
      @activeFilter = ko.observable(false)
      @filteredDevices = ko.observable({})

      # metric templates
      @metricTemplates = ko.observable([])

      # metric data pushed from server. Values are contained in 'values' object
      @metricData = ko.observable({ values: { devices: {}, chassis: {} } })

      # stores the rack definition JSON after being parsed
      @racks = ko.observable([])

      # thumb image (unscaled)
      @rackImage = ko.observable()

      # stores each device JSON object with an additional property 'instance'
      # which is a reference to the class instance. Arranged by group then id
      @deviceLookup = ko.observable({})

      #@preset = ko.observable()

      # the physical locations of all breaching nodes, used by the thumbnail to
      # draw breaches
      @breachZones = ko.observable({})
      @breaches = ko.observable({})

      # the current zoom level of the rack view
      @scale = ko.observable()

      @colourMaps = ko.observable({})

      # range based filters, uses metricId as the key 
      @filters = ko.observable({})

      # used by metric bars, the graph and colour map to show hi/lo colours
      @colourScale = ko.observable(ViewModel.COLOUR_SCALE)

      @metricIds = ko.dependentObservable(->
        metric_ids = []
        metrics    = @metricTemplates()
        metric_ids.push(metrics[metric].id) for metric of metrics
        metric_ids
      , @)
      @enableMetricSelection = ko.dependentObservable(->
        metrics = @metricIds()
        metrics? and metrics.length > 0
      , @)

