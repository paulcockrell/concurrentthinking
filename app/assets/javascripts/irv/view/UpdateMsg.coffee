define ['common/util/Events'], (Events) ->

  class UpdateMsg

    # statics overwritten by config
    @MESSAGE : 'Updating...'


    # msg_element is masked off and has the message added, mask_elements
    # is an array of other elements to maks off (without message)
    constructor: (@msgElement, @maskElements) ->
      @maskList = []


    show: ->
      return if @visible

      @visible  = true

      el            = @addMask(@msgElement)
      msg           = document.createElement('div')
      msg.innerHTML = UpdateMsg.MESSAGE
      msg.setAttribute('id', 'updater_msg')
      el.appendChild(msg)

      @addMask(el) for el in @maskElements
      

    hide: ->
      return unless @visible

      @visible = false
      el.parentElement.removeChild(el) for el in @maskList
      @maskList = []


    addMask: (container_el) ->
      el = document.createElement('div')
      el.setAttribute('class', 'updater_mask')
      Events.addEventListener(el, 'click', @muteEvent)
      Events.addEventListener(el, 'contextmenu', @muteEvent)
      Events.addEventListener(el, 'dblclick', @muteEvent)
      container_el.appendChild(el)
      @maskList.push(el)
      el


    muteEvent: (ev) =>
      ev.stopPropagation()
      ev.preventDefault()
