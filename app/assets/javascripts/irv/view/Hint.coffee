define ['common/util/Util'], (Util) ->

  class Hint

    constructor: (@containerEl, @model) ->
      @hintEl = $('tooltip')
      @visible = false


    show: (content, x, y) ->
      @visible          = true
      @hintEl.innerHTML = content
      # adjust when near to edges of the screen
      container_dims = Util.getElementDimensions(@containerEl)
      hint_dims      = Util.getElementDimensions(@hintEl)

      x = if x + hint_dims.width > container_dims.width then x - hint_dims.width else x
      y = if y + hint_dims.height > container_dims.height then y - hint_dims.height else y

      Util.setStyle(@hintEl, 'left', x + 'px')
      Util.setStyle(@hintEl, 'top', y + 'px')
      Util.setStyle(@hintEl, 'visibility', 'visible')


    hide: () ->
      if @visible
        Util.setStyle(@hintEl, 'visibility', 'hidden')
        @hintEl.innerHTML = ' '
        @visible = false

