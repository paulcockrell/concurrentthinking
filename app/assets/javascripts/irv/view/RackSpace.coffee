# the RackSpace instanciates the racks and coordinates global operations and
# animations such as zooming and flipping between front and rear views
define ['common/util/Util', 'common/util/Events', 'common/gfx/SimpleRenderer', 'common/gfx/Easing', 'irv/view/RackObject', 'irv/view/Rack', 'irv/view/Chassis', 'irv/view/Machine', 'irv/view/Chart', 'irv/view/RackHint', 'irv/view/ContextMenu'], (Util, Events, SimpleRenderer, Easing, RackObject, Rack, Chassis, Machine, Chart, RackHint, ContextMenu) ->

  class RackSpace

    # statics overwritten by config
    @PADDING        : 100
    @RACK_H_SPACING : 50
    @RACK_V_SPACING : 100

    @DRAG_FADE_FILL  : '#0'
    @DRAG_FADE_ALPHA : .3
    @DRAG_SNAP_RANGE : 10
    @DRAG_ITEM_ALPHA : .5

    @METRIC_FADE_FILL  : '#000000'
    @METRIC_FADE_ALPHA : .7

    @SELECT_BOX_STROKE       : '#000000'
    @SELECT_BOX_ALPHA        : 1
    @SELECT_BOX_STROKE_WIDTH : 1

    @CHART_SELECTION_COUNT_FILL     : '#ffffff'
    @CHART_SELECTION_COUNT_FONT     : '14px Karla'
    @CHART_SELECTION_COUNT_BG_FILL  : '#000000'
    @CHART_SELECTION_COUNT_BG_ALPHA : 0.4
    @CHART_SELECTION_COUNT_CAPTION  : '[[selection_count]] metrics'
    @CHART_SELECTION_COUNT_OFFSET_X : 0
    @CHART_SELECTION_COUNT_OFFSET_Y : -10

    @ADDITIONAL_ROW_TOLERANCE : 3
    @LAYOUT_UPDATE_DELAY      : 1500
    @U_LBL_SCALE_CUTOFF       : .06
    @NAME_LBL_SCALE_CUTOFF    : .01
    @ZOOM_DURATION            : 500
    @FPS                      : 24
    @INFO_FADE_DURATION       : 200
    @FLIP_DURATION            : 500
    @FLIP_DELAY               : 200
    @CANVAS_MAX_DIMENSION     : 1000

    # hardcoded and run-time assigned statics
    @MIN_ZOOM : null
    @MAX_ZOOM : 1


    constructor: (@rackEl, @chartEl, @model) ->
      Rack.initialise()
      @zooming  = false
      @flipping = false

      @currentFace  = @model.face()
      @scrollAdjust = Util.getScrollbarThickness()

      @createGfx()

      RackObject.MODEL     = @model
      RackObject.RACK_GFX  = @rackGfx
      RackObject.INFO_GFX  = @infoGfx
      RackObject.ALERT_GFX = @alertGfx

      # makes position and size information accessible to the controller
      @coordReferenceEl = @alertGfx.cvs

      @setUpRacks()

      # init scale
      @setZoomPresets()
      @zoomIdx = 0
      @scale   = @zoomPresets[@zoomIdx]
      @model.scale(@scale)
      @rackGfx.setScale(@scale)
      @infoGfx.setScale(@scale)
      @alertGfx.setScale(@scale)
      @draw()
      @centreRacks()

      @chart       = new Chart(@chartEl, @model)
      @hint        = new RackHint(@rackEl, @model)
      @contextMenu = new ContextMenu(@rackEl, @model, @evContextClick)

      @model.face.subscribe(@setView)
      @model.viewMode.subscribe(@switchView)
      @model.highlighted.subscribe(@highlightDevice)
      @model.filteredDevices.subscribe(@showSelection)
      @model.selectedDevices.subscribe(@showSelection)
      @model.metricLevel.subscribe(@setMetricLevel)


    createGfx: ->
      # rackGfx canvas for rack and machine images
      @rackGfx = new SimpleRenderer(@rackEl, 1, 1, 0, RackSpace.FPS)
      Util.setStyle(@rackGfx.cvs, 'position', 'absolute')
      Util.setStyle(@rackGfx.cvs, 'left', '0px')
      Util.setStyle(@rackGfx.cvs, 'top', '0px')

      # infoGfx canvas for metrics and labels
      @infoGfx = new SimpleRenderer(@rackEl, 1, 1, 0, RackSpace.FPS)
      Util.setStyle(@infoGfx.cvs, 'position', 'absolute')
      Util.setStyle(@infoGfx.cvs, 'left', '0px')
      Util.setStyle(@infoGfx.cvs, 'top', '0px')

      # alertGfx canvas for selection and breaching highlights
      @alertGfx = new SimpleRenderer(@rackEl, 1, 1, 0, RackSpace.FPS)
      Util.setStyle(@alertGfx.cvs, 'position', 'absolute')
      Util.setStyle(@alertGfx.cvs, 'left', '0px')
      Util.setStyle(@alertGfx.cvs, 'top', '0px')


    setUpRacks: ->
      @racks = []
      max_u  = 0
      racks  = @model.racks()
      for rack in racks
        max_u = rack.uHeight if rack.uHeight > max_u
        @racks.push(new Rack(rack))
        
      @tallestRack = Rack.IMAGES.front.top.height + Rack.IMAGES.front.btm.height + (RackObject.U_PX_HEIGHT * max_u)
      @rowHeight   = (RackSpace.PADDING * 2) + @tallestRack

      @arrangeRacks()
      @synchroniseZoomIdx()


    updateLayout: ->
      # throttle the number of updates being executed using a timeout
      clearTimeout(@layoutTmr)
      @layoutTmr = setTimeout(@setLayout, RackSpace.LAYOUT_UPDATE_DELAY)


    setLayout: =>
      showing_all = @scale is RackSpace.MIN_ZOOM

      @arrangeRacks()
      @setZoomPresets()
      @synchroniseZoomIdx()
      if @scale <= RackSpace.MIN_ZOOM or showing_all
        @zoomIdx = 0
        @scale   = @zoomPresets[0]
        @model.scale(@scale)
        @rackGfx.setScale(@scale)
        @infoGfx.setScale(@scale)
        @alertGfx.setScale(@scale)
        @draw()
      @centreRacks()


    setZoomPresets: ->
      @zoomPresets = []
      dims         = @rackEl.getCoordinates()#Util.getElementDimensions(@rackEl)

      scale_x = (dims.width - @scrollAdjust) / @racksWidth
      scale_y = (dims.height - @scrollAdjust) / @racksHeight

      show_all     = if scale_x > scale_y then scale_y else scale_x
      scale_to_row = dims.height / @rowHeight

      max_dim            = if @racksWidth > @racksHeight then @racksWidth else @racksHeight
      RackSpace.MAX_ZOOM = if max_dim > RackSpace.CANVAS_MAX_DIMENSION then RackSpace.CANVAS_MAX_DIMENSION / max_dim else 1

      # show all and scale to row could be the same
      @zoomPresets.push(show_all) if show_all isnt scale_to_row
      # max zoom could be less than scale-to-row when restricting to max canvas size
      @zoomPresets.push(scale_to_row) if RackSpace.MAX_ZOOM > scale_to_row
      @zoomPresets.push(RackSpace.MAX_ZOOM)

      RackSpace.MIN_ZOOM = @zoomPresets[0]


    highlightAt: (x, y) ->
      return if @contextMenu.visible

      x /= @scale
      y /= @scale

      device = @getDeviceAt(x, y)
      device = device.parent if device? and device.pluggable and @metricLevel is 'chassis'

      @model.highlighted(device)


    highlightDevice: () =>
      new_highlight = @model.highlighted()

      if new_highlight instanceof Rack
        @removeHighlight()
      else if new_highlight isnt @highlighted
        @highlighted.deselect() if @highlighted?
        new_highlight.select() if new_highlight?
        # store a local reference to the current highlight so that it may be deselected
        # when the model is updated with a new highlight
        @highlighted = new_highlight


    removeHighlight: ->
      if @highlighted?
        @highlighted.deselect()
        @highlighted = null


    showSelection: =>
      rack.setIncluded() for rack in @racks
      @draw()


    draw: =>
      show_u_labels = @scale >= RackSpace.U_LBL_SCALE_CUTOFF
      show_name_label = @scale >= RackSpace.NAME_LBL_SCALE_CUTOFF
      rack.draw(show_u_labels, show_name_label) for rack in @racks
      @updateRackImage()


    setView: =>
      face = @model.face()
      if face is 'both'
        @showSplitView()
      else if @currentFace is 'both'
        @revertFromSplitView()
      else
        @flip()

      @currentFace = face


    switchRackSet: (new_racks) ->
      # kill off old rack set
      rack.destroy() for rack in @racks
      @rackGfx.removeAll()
      @infoGfx.removeAll()
      @alertGfx.removeAll()

      # update model and show new rack set
      @model.racks(new_racks)
      @setUpRacks()
      @setZoomPresets()
      @draw()
      @centreRacks()


    showSplitView: ->
      # create rack defs with rear facing duplicates
      racks     = @model.racks()
      new_racks = []
      for rack in racks
        rear          = {}
        rear[i]       = rack[i] for i of rack
        rack.bothView = 'front'
        rear.bothView = 'rear'
        new_racks.push(rack)
        new_racks.push(rear)

      @switchRackSet(new_racks)


    revertFromSplitView: ->
      racks     = @model.racks()
      new_racks = []
      count     = 0
      len       = racks.length
      while count < len
        new_racks.push(racks[count])
        count += 2

      @switchRackSet(new_racks)


    flip: =>
      @flipping = true
      @flipCount = 0

      @rackGfx.pauseAnims()
      @infoGfx.pauseAnims()
      @alertGfx.pauseAnims()

      # store current scroll
      @scrollOffset =
        x: @rackEl.scrollLeft
        y: @rackEl.scrollTop

      # store canvas offset (for centring)
      @cvsOffset =
        x: Util.getStyleNumeric(@rackGfx.cvs, 'left')
        y: Util.getStyleNumeric(@rackGfx.cvs, 'top')

      @rackEl.removeChild(@rackGfx.cvs)
      @rackEl.removeChild(@infoGfx.cvs)
      @rackEl.removeChild(@alertGfx.cvs)

      dims         = @rackEl.getCoordinates()#Util.getElementDimensions(@rackEl)
      dims.width  -= @scrollAdjust
      dims.height -= @scrollAdjust

      @fx  = @createFXLayer(@rackEl, 0, 0, dims.width, dims.height)
      @fx.addImg({ img: @rackGfx.cvs, x: @cvsOffset.x - @scrollOffset.x, y: @cvsOffset.y - @scrollOffset.y })
      @fx2     = @createFXLayer(@rackEl, 0, 0, dims.width, dims.height)
      info_img = @fx2.addImg({ img: @infoGfx.cvs, x: @cvsOffset.x - @scrollOffset.x, y: @cvsOffset.y - @scrollOffset.y })
      @fx2.animate(info_img, { alpha: 0 }, RackSpace.INFO_FADE_DURATION, Easing.Quad.easeOut, @evFlipReady)


    evFlipReady: =>
      @fx.removeAll()

      @rackLookup = {}
      count = 0
      len   = @racks.length
      mid   = len / 2 - .5
      while count < len
        rack = @racks[count]
        img  = @fx.addImg(
          img: @rackGfx.cvs
          x: @cvsOffset.x + (rack.x * @scale) - @scrollOffset.x
          y: @cvsOffset.y + (rack.y * @scale) - @scrollOffset.y
          sliceX: rack.x * @scale
          sliceY: rack.y * @scale
          sliceWidth: rack.width * @scale
          sliceHeight: rack.height * @scale)

        # rackLookup associates the rack with it's image, used when flipped half way
        @rackLookup[img] = rack
        @fx.animate(img, { delay: RackSpace.FLIP_DELAY * Math.abs(count - mid), x: @cvsOffset.x + ((rack.x + (rack.width / 2)) * @scale) - @scrollOffset.x, width: 0 }, RackSpace.FLIP_DURATION, Easing.Quad.easeIn, @evHalfFlipped)
        ++count

      # force immediate draw or we'll have a blank image for one frame
      @fx.redraw()


    evHalfFlipped: (img) =>
      show_u_labels   = @scale >= RackSpace.U_LBL_SCALE_CUTOFF
      show_name_label = @scale >= RackSpace.NAME_LBL_SCALE_CUTOFF
      @rackLookup[img].draw(show_u_labels, show_name_label)
      x     = @fx.getAttribute(img, 'x')
      width = @fx.getAttribute(img, 'sliceWidth')
      @fx.animate(img, { x: x - (width / 2), width: width }, RackSpace.FLIP_DURATION, Easing.Quad.easeOut, @evFlipped)


    evFlipped: (img) =>
      ++@flipCount
      if @flipCount is @racks.length
        @fx2.removeAll()
        info_img = @fx2.addImg({ img: @infoGfx.cvs, alpha: 0, x: @cvsOffset.x - @scrollOffset.x, y: @cvsOffset.y - @scrollOffset.y })
        @fx2.animate(info_img, { alpha: 1 }, RackSpace.INFO_FADE_DURATION, Easing.Quad.easeOut, @evFlipComplete)
        

    evFlipComplete: =>
      @flipping = false

      @fx.destroy()
      @fx2.destroy()

      @rackEl.appendChild(@rackGfx.cvs)
      @rackEl.appendChild(@infoGfx.cvs)
      @rackEl.appendChild(@alertGfx.cvs)

      @rackEl.scrollLeft = @scrollOffset.x
      @rackEl.scrollTop = @scrollOffset.y

      @rackGfx.resumeAnims()
      @infoGfx.resumeAnims()
      @alertGfx.resumeAnims()

      Events.dispatchEvent(@rackEl, 'rackSpaceFlipComplete')
      @updateRackImage()


    click: (x, y) ->
      if @contextMenu.visible
        @contextMenu.hide()
        @highlightAt(x, y)
      else
        x /= @scale
        y /= @scale

        clicked = @getDeviceAt(x, y)

        # whitespace has been clicked so clear existing selection
        unless clicked?
          if @model.activeSelection()
            groups = @model.groups()
            blank  = {}
            blank[group] = {} for group in groups

            @model.activeSelection(false)
            @model.selectedDevices(blank)


    startDrag: (x, y) ->
      x /= @scale
      y /= @scale
      @selection = @getDeviceAt(x, y)

      if @selection? and (@selection instanceof Chassis)# or @selection instanceof Machine)
        # drag a device/chassis
        @fx = @createFXLayer(@rackEl, Util.getStyleNumeric(@rackGfx.cvs, 'left'), Util.getStyleNumeric(@rackGfx.cvs, 'top'), @rackGfx.width, @rackGfx.height, @scale)

        @dragImgOffset = { x: @selection.x - x, y: @selection.y - y }
        dims           = Util.getElementDimensions(@rackEl)
        @dragImg       = @fx.addImg({ img: @selection.img, x: x + @dragImgOffset.x, y: y + @dragImgOffset.y, alpha: RackSpace.DRAG_ITEM_ALPHA })

        @fader = @rackGfx.addRect({ fx: 'source-atop', width: @rackGfx.width, height: @rackGfx.height, fill: RackSpace.DRAG_FADE_FILL, alpha: RackSpace.DRAG_FADE_ALPHA })

        rack.showSpaces(@selection.uHeight) for rack in @racks
        @selection.showDrag()
      else
        @selection = null
        # drag a selection box
        @fx        = @createFXLayer(@rackEl, Util.getStyleNumeric(@rackGfx.cvs, 'left'), Util.getStyleNumeric(@rackGfx.cvs, 'top'), @rackGfx.width, @rackGfx.height, @scale)
        @boxAnchor = { x: x, y: y }
        @box       = @fx.addRect({ x: x, y: y, stroke: RackSpace.SELECT_BOX_STROKE, strokeWidth: RackSpace.SELECT_BOX_STROKE_WIDTH / @scale, alpha: RackSpace.SELECT_BOX_ALPHA, width: 1, height: 1 })


    startDragChart: (x, y) ->
      # drag a selection box
      dims       = Util.getElementDimensions(@rackEl)
      @fx        = @createFXLayer(@chartEl, 0, 0, dims.width, dims.height)
      @boxAnchor = { x: x, y: y }

      @box = @fx.addRect(
        x           : x
        y           : y
        stroke      : RackSpace.SELECT_BOX_STROKE
        strokeWidth : RackSpace.SELECT_BOX_STROKE_WIDTH
        alpha       : RackSpace.SELECT_BOX_ALPHA
        width       : 1
        height      : 1)

      @selCount = @fx.addText(
        x       : x + RackSpace.CHART_SELECTION_COUNT_OFFSET_X
        y       : y + RackSpace.CHART_SELECTION_COUNT_OFFSET_Y
        fill    : RackSpace.CHART_SELECTION_COUNT_FILL
        bgFill  : RackSpace.CHART_SELECTION_COUNT_BG_FILL
        bgAlpha : RackSpace.CHART_SELECTION_COUNT_BG_ALPHA
        font    : RackSpace.CHART_SELECTION_COUNT_FONT
        padding : RackSpace.CHART_SELECTION_COUNT_PADDING
        caption : '')


    dragChart: (x, y) ->
      @dragBox(x, y)
      box =
        x      : @fx.getAttribute(@box, 'x')
        y      : @fx.getAttribute(@box, 'y')
        width  : @fx.getAttribute(@box, 'width')
        height : @fx.getAttribute(@box, 'height')

      box.left   = box.x
      box.top    = box.y
      box.right  = box.left + box.width
      box.bottom = box.top + box.height

      # update selection count
      @fx.setAttributes(@selCount,
        caption : RackSpace.CHART_SELECTION_COUNT_CAPTION.replace(/\[\[selection_count\]\]/g, @chart.getSelection(box).count)
        x       : box.x + RackSpace.CHART_SELECTION_COUNT_OFFSET_X
        y       : box.y + RackSpace.CHART_SELECTION_COUNT_OFFSET_Y)


    stopDragChart: (x, y) ->
      @fx.destroy()
      
      box = {}
      if x > @boxAnchor.x
        box.x     = @boxAnchor.x
        box.width = x - @boxAnchor.x
      else
        box.x     = x
        box.width = @boxAnchor.x - x

      if y > @boxAnchor.y
        box.y      = @boxAnchor.y
        box.height = y - @boxAnchor.y
      else
        box.y      = y
        box.height = @boxAnchor.y - y

      box.left   = box.x
      box.right  = box.x + box.width
      box.top    = box.y
      box.bottom = box.y + box.height
      @chart.selectWithinBox(box)


    drag: (x, y) ->
      x /= @scale
      y /= @scale

      if @selection?

        slots = []
        slots.push(rack.getNearestSpace(x, y)) for rack in @racks
        Util.sortByProperty(slots, 'dist', true)
        nearest = slots[0]

        if nearest? and (nearest.dist * @scale <= Math.pow(RackSpace.DRAG_SNAP_RANGE / @scale, 2) or (x > nearest.left and x < nearest.right and y > nearest.top and y < nearest.bottom))
          @fx.setAttributes(@dragImg, { alpha: 1 })
          @fx.setAttributes(@dragImg, {x: nearest.left + ((nearest.right - nearest.left) / 2) - (@fx.getAttribute(@dragImg, 'width') / 2), y: nearest.top })
          @fx.addRect({ x: @fx.getAttribute(@dragImg, 'x'), y: @fx.getAttribute(@dragImg, 'y'), width: @fx.getAttribute(@dragImg, 'width'), height: @fx.getAttribute(@dragImg, 'height'), stroke: '#ff00ff'})
        else
          @fx.setAttributes(@dragImg, { alpha: RackSpace.DRAG_ITEM_ALPHA })
          @fx.setAttributes(@dragImg, { x: x + @dragImgOffset.x, y: y + @dragImgOffset.y })
      else if @box
        @dragBox(x, y)


    dragBox: (x, y) ->
      attrs = {}

      if x > @boxAnchor.x
        attrs.x     = @boxAnchor.x
        attrs.width = x - @boxAnchor.x
      else
        attrs.x     = x
        attrs.width = @boxAnchor.x - x

      if y > @boxAnchor.y
        attrs.y      = @boxAnchor.y
        attrs.height = y - @boxAnchor.y
      else
        attrs.y      = y
        attrs.height = @boxAnchor.y - y

      @fx.setAttributes(@box, attrs)


    stopDrag: (x, y) ->
      if @selection?
        @rackGfx.remove(@fader)
        @rackGfx.redraw()
        rack.hideSpaces() for rack in @racks
        @fx.destroy()
        @fader = null
        @selection.hideDrag()
      else if @box?
        @fx.destroy()

        x /= @scale
        y /= @scale

        box = {}
        if x > @boxAnchor.x
          box.x     = @boxAnchor.x
          box.width = x - @boxAnchor.x
        else
          box.x     = x
          box.width = @boxAnchor.x - x

        if y > @boxAnchor.y
          box.y      = @boxAnchor.y
          box.height = y - @boxAnchor.y
        else
          box.y      = y
          box.height = @boxAnchor.y - y

        box.left   = box.x
        box.right  = box.x + box.width
        box.top    = box.y
        box.bottom = box.y + box.height

        selection = @selectWithin(box, true)
        @model.activeSelection(selection?)
        @model.selectedDevices(selection)


    resetZoom: ->
      if @scale is @zoomPresets[0]
        Events.dispatchEvent(@rackEl, 'rackSpaceZoomComplete')
        return

      @zoomIdx = 0
      centre_x = @coordReferenceEl.width / 2
      centre_y = @coordReferenceEl.height / 2
      @quickZoom(centre_x, centre_y, @zoomPresets[@zoomIdx])


    # direction indicates wether to zoom in or out, should be 1 or -1
    zoomToPreset: (direction = 1, centre_x, centre_y, cyclical = true) ->
      return if @zooming

      if not cyclical and (@zoomIdx + direction < 0 or @zoomIdx + direction >= @zoomPresets.length)
        Events.dispatchEvent(@rackEl, 'rackSpaceZoomComplete')
        return

      centre_x  = @coordReferenceEl.width / 2 unless centre_x?
      centre_y  = @coordReferenceEl.height / 2 unless centre_y?
      @zoomIdx += direction
      # cycle through presets
      @zoomIdx = 0 if @zoomIdx >= @zoomPresets.length
      @zoomIdx = @zoomPresets.length - 1 if @zoomIdx < 0
      @quickZoom(centre_x, centre_y, @zoomPresets[@zoomIdx])


    # calculate suitable zoom idx when an operation effects the zoom level
    # aside from zoomToPreset
    synchroniseZoomIdx: ->
      return unless @zoomPresets?

      idx = 1
      len = @zoomPresets.length
      ++idx while idx < len and @scale >= @zoomPresets[idx]
      @zoomIdx = idx - 1


    # down render everything to a single image and scale that rather than each individual asset
    quickZoom: (centre_x, centre_y, new_scale) ->
      return if @zooming

      dims         = @rackEl.getCoordinates()
      dims.width  -= @scrollAdjust
      dims.height -= @scrollAdjust

      # restrict requested scale to max and min values
      if new_scale > RackSpace.MAX_ZOOM
        @zoomIdx  = 0
        new_scale = RackSpace.MAX_ZOOM
      if new_scale < RackSpace.MIN_ZOOM
        @zoomIdx  = @zoomPresets.length - 1
        new_scale = RackSpace.MIN_ZOOM

      # calculate centre coords according to target scale
      centre_x = centre_x / (@scale / new_scale)
      centre_y = centre_y / (@scale / new_scale)

      # store current scroll
      @scrollOffset =
        x: @rackEl.scrollLeft
        y: @rackEl.scrollTop

      # calculate target coords according to top-left
      target_x = centre_x - (dims.width / 2)
      target_y = centre_y - (dims.height / 2)

      target_width  = @rackGfx.width * new_scale
      target_height = @rackGfx.height * new_scale

      # determin rack centreing offset at target scale
      offset_x = (dims.width - target_width) / 2
      offset_y = (dims.height - target_height) / 2
      offset_x = 0 if offset_x < 0
      offset_y = 0 if offset_y < 0

      # calculate boundaries of zoomed canvas
      lh_bound  = -offset_x
      rh_bound  = target_width - dims.width + offset_x
      top_bound = -offset_y
      btm_bound = target_height - dims.height + offset_y

      # retrict target coords to zoomed boundaries
      if target_x > rh_bound then target_x = rh_bound
      if target_x < lh_bound then target_x = lh_bound
      if target_y > btm_bound then target_y = btm_bound
      if target_y < top_bound then target_y = top_bound

      # store target offset
      @targetOffset =
        x: target_x
        y: target_y

      current_offset_x = Util.getStyleNumeric(@rackGfx.cvs, 'left')
      current_offset_y = Util.getStyleNumeric(@rackGfx.cvs, 'top')
      lazy_factor      = 2
      
      # only zoom if new scale is different to current and target scroll
      # is a significant change (greater than lazy factor)
      if new_scale is @scale and (Math.abs(@targetOffset.x - @scrollOffset.x - current_offset_x) < lazy_factor and Math.abs(@targetOffset.y - @scrollOffset.y - current_offset_y) < lazy_factor)
        Events.dispatchEvent(@rackEl, 'rackSpaceZoomComplete')
        return

      # commence zoom
      @hint.hide()
      @contextMenu.hide()
      @removeHighlight()
      @zooming     = true
      @targetScale = new_scale

      current_offset_x = Util.getStyleNumeric(@rackGfx.cvs, 'left')
      current_offset_y = Util.getStyleNumeric(@rackGfx.cvs, 'top')

      @fx      = @createFXLayer(@rackEl, 0, 0, dims.width, dims.height)
      @rackImg = @fx.addImg({ img: @rackGfx.cvs, x: current_offset_x - @scrollOffset.x, y: current_offset_y - @scrollOffset.y })
      @fx2     = @createFXLayer(@rackEl, 0, 0, dims.width, dims.height)
      info_img = @fx2.addImg({ img: @infoGfx.cvs, x: current_offset_x - @scrollOffset.x, y: current_offset_y - @scrollOffset.y })
      @fx2.animate(info_img, { alpha: 0 }, RackSpace.INFO_FADE_DURATION, Easing.Quad.easeOut, @evZoomReady)

      @rackGfx.pauseAnims()
      @infoGfx.pauseAnims()
      @alertGfx.pauseAnims()
      @rackEl.removeChild(@rackGfx.cvs)
      @rackEl.removeChild(@infoGfx.cvs)
      @rackEl.removeChild(@alertGfx.cvs)

      # force immediate draw or we'll have a blank image for one frame
      @fx.redraw()
      @fx2.redraw()


    evZoomReady: =>
      @fx2.removeAll()

      relative_scale = @targetScale / @rackGfx.scale

      @infoGfx.setScale(@targetScale)
      @alertGfx.setScale(@targetScale)

      # decide wether to show rack labels
      show_name_label = @targetScale >= RackSpace.NAME_LBL_SCALE_CUTOFF
      show_u_labels   = @targetScale >= RackSpace.U_LBL_SCALE_CUTOFF

      for rack in @racks
        rack.showNameLabel(show_name_label)
        rack.showULabels(show_u_labels)
      
      @fx.animate(@rackImg, { x: -@targetOffset.x, y: -@targetOffset.y, width: @rackGfx.cvs.width * relative_scale, height: @rackGfx.cvs.height * relative_scale }, RackSpace.ZOOM_DURATION, Easing.Cubic.easeOut, @evRackZoomComplete)


    evRackZoomComplete: =>
      # fade in info layer
      info_img = @fx2.addImg({ img: @infoGfx.cvs, x: -@targetOffset.x, y: -@targetOffset.y, alpha: 0 })
      @fx2.animate(info_img, { alpha: 1 }, RackSpace.INFO_FADE_DURATION, Easing.Quad.easeOut, @evZoomComplete)


    evZoomComplete: =>
      @fx.destroy()
      @fx2.destroy()

      @rackGfx.setScale(@targetScale)
      @rackEl.appendChild(@rackGfx.cvs)
      @rackEl.appendChild(@infoGfx.cvs)
      @rackEl.appendChild(@alertGfx.cvs)

      @rackEl.scrollLeft = @targetOffset.x
      @rackEl.scrollTop  = @targetOffset.y

      @rackGfx.resumeAnims()
      @infoGfx.resumeAnims()
      @alertGfx.resumeAnims()
      @scale = @targetScale
      @model.scale(@scale)
      @zooming = false
      @synchroniseZoomIdx()
      @centreRacks()

      Events.dispatchEvent(@rackEl, 'rackSpaceZoomComplete')


    createFXLayer: (container, x, y, width, height, scale = 1) ->
      fx = new SimpleRenderer(container, width, height, scale, RackSpace.FPS)
      Util.setStyle(fx.cvs, 'position', 'absolute')
      Util.setStyle(fx.cvs, 'left', x + 'px')
      Util.setStyle(fx.cvs, 'top', y + 'px')
      fx


    centreRacks: ->
      rack_dims = @rackEl.getCoordinates()#Util.getElementDimensions(@rackEl)
      cvs_dims  = @rackGfx.cvs.getCoordinates()#Util.getElementDimensions(@rackGfx.cvs)
      offset    = { x: rack_dims.width - @scrollAdjust - cvs_dims.width, y: rack_dims.height - @scrollAdjust - cvs_dims.height }
      
      offset.x = 0 if offset.x < 0
      offset.y = 0 if offset.y < 0

      centre_x = offset.x / 2 + 'px'
      centre_y = offset.y / 2 + 'px'

      Util.setStyle(@rackGfx.cvs, 'left', centre_x)
      Util.setStyle(@infoGfx.cvs, 'left', centre_x)
      Util.setStyle(@alertGfx.cvs, 'left', centre_x)

      Util.setStyle(@rackGfx.cvs, 'top', centre_y)
      Util.setStyle(@infoGfx.cvs, 'top', centre_y)
      Util.setStyle(@alertGfx.cvs, 'top', centre_y)


    getRackOffset: ->


    getDeviceAt: (x, y) ->
      for rack in @racks
        if x > rack.x and x < rack.x + rack.width and y > rack.y and y < rack.y + rack.height
          device = rack.getDeviceAt(x, y)
          if device? then return device else return rack

      return null


    showHint: (abs_coords, rel_coords) =>
      return if @contextMenu.visible

      rel_coords.x /= @scale
      rel_coords.y /= @scale

      device = @getDeviceAt(rel_coords.x, rel_coords.y)
      device = device.parent if device? and device.pluggable and @metricLevel is 'chassis'
      @hint.show(device, abs_coords.x, abs_coords.y) if device?


    hideHint: ->
      @hint.hide()


    showContextMenu: (abs_coords, rel_coords) ->
      @contextMenu.hide()
      @highlightAt(rel_coords.x, rel_coords.y)

      rel_coords.x /= @scale
      rel_coords.y /= @scale

      selection = @getDeviceAt(rel_coords.x, rel_coords.y)
      selection = selection.parent if selection? and selection.pluggable and @metricLevel is 'chassis'

      @hint.hide()

      # chassis only functionality, to include a context menu option 'Add blade' for an empty slot
      if selection? and (selection instanceof Chassis or selection instanceof Rack)
        available_slot = selection.getSlot(rel_coords.x, rel_coords.y)
        if available_slot? and available_slot.device?
          available_slot = null
        else if available_slot?
          available_slot.type = if selection instanceof Chassis then 'chassis' else 'rack'

      @contextMenu.show(selection, abs_coords.x, abs_coords.y, available_slot)


    evMouseOut: ->
      @model.highlighted(null) if not @contextMenu.visible


    evContextClick: (param_str) =>
      return unless param_str?

      params = param_str.split(',')

      switch params[0]
        when 'focusOn'
          @focusOn(params[1], params[2])
        when 'reset'
          Events.dispatchEvent(@rackEl, 'rackSpaceReset')


    focusOn: (group, id) ->
      target = @model.deviceLookup()[group][id]
      return unless target?

      target = target.instance

      centre_x = (target.x + (target.width / 2)) * @scale
      centre_y = (target.y + (target.height / 2)) * @scale

      dims    = Util.getElementDimensions(@rackEl)
      scale_x = (dims.width - @scrollAdjust) / target.width
      scale_y = (dims.height - @scrollAdjust) / target.height
      scale   = if scale_x > scale_y then scale_y else scale_x

      @quickZoom(centre_x, centre_y, scale)
      selection = @selectWithin({ left: target.x, right: target.x + target.width, top: target.y, bottom: target.y + target.height }, false)
      @model.activeSelection(selection?)
      @model.selectedDevices(selection)


    selectWithin: (box, inclusive) ->
      active_selection = false
      groups           = @model.groups()
      selected         = {}
      selected[group]  = {} for group in groups

      for rack in @racks
        subselection = rack.selectWithin(box, inclusive)
        for group in groups
          for i of subselection[group]
            # we only want to display a subset of metrics when at least one device or chassis is selected i.e. ignore rack only selections
            # additionally ignore native object properties, hence isNaN/parseInt stuff
            if not isNaN(Number(subselection[group][i]))
              active_selection   = true
              selected[group][i] = true
                
      if active_selection
        return selected
      else
        return


    # determin how the racks are layed out. Attempts to fill the containing div with minimal white space.
    # assumes fixed rack width
    arrangeRacks: () =>
      dims           = Util.getElementDimensions(@rackEl)
      dims_ratio     = dims.width / dims.height
      num_racks      = @racks.length
      rack_width     = @racks[0].width
      best_fit_ratio = 0
      count          = num_racks
      # calculate row width which produces dimensions that most closely match the ratio of container width and height
      # aka best fit row width
      while count > 0
        num_rows     = Math.ceil(num_racks / count)
        total_width  = ((rack_width + RackSpace.RACK_H_SPACING) * count) - RackSpace.RACK_H_SPACING + (RackSpace.PADDING * 2)
        total_height = ((@tallestRack + RackSpace.RACK_V_SPACING) * num_rows) - RackSpace.RACK_V_SPACING + (RackSpace.PADDING * 2)
        ratio        = total_width / total_height

        if Math.abs(ratio - dims_ratio) < Math.abs(best_fit_ratio - dims_ratio)
          best_fit       = count
          best_fit_ratio = ratio
        --count

      max_row_width = best_fit
      
      len = @racks.length
      # calculate how many rows of racks to display
      if len > max_row_width
        closest_width   = Number.MAX_VALUE
        least_spaces    = Number.MAX_VALUE
        count           = max_row_width
        target_num_rows = Math.ceil(len / max_row_width)
        while count > 0
          break if Math.ceil(len / count) > target_num_rows + RackSpace.ADDITIONAL_ROW_TOLERANCE
          remainder = len % count
          short_by  = if remainder is 0 then 0 else count - remainder
          if short_by < least_spaces
            closest_width = count
            least_spaces  = short_by
          
          --count

        row_width = closest_width
        num_rows  = Math.ceil(len / row_width)
      else
        row_width = len
        num_rows  = 1

      prev_width  = @racksWidth
      prev_height = @racksHeight

      @racksWidth  = (RackSpace.PADDING * 2) + (row_width * (rack_width + RackSpace.RACK_H_SPACING)) - RackSpace.RACK_H_SPACING
      @racksHeight = (RackSpace.PADDING * 2) + (num_rows * (@tallestRack + RackSpace.RACK_V_SPACING)) - RackSpace.RACK_V_SPACING

      # rearrange only if necessary
      if prev_width isnt @racksWidth or prev_height isnt @racksHeight
        # set rack coordinates using @tallestRack to offset shorter racks, assumes fixed rack width
        count = 0
        while count < len
          row_num = Math.floor(count / row_width)
          col_num = count - (row_num * row_width)
          rack    = @racks[count]
          rack.setCoords(RackSpace.PADDING + ((rack.width + RackSpace.RACK_H_SPACING) * col_num), RackSpace.PADDING + ((@tallestRack + RackSpace.RACK_V_SPACING) * row_num) + @tallestRack - rack.height)
          ++count

        @rackGfx.setDims(@racksWidth, @racksHeight)
        @infoGfx.setDims(@racksWidth, @racksHeight)
        @alertGfx.setDims(@racksWidth, @racksHeight)

      # always redraw, even if the layout hasn't changed the new scale may mean
      # that name or u labels may need to switch between hidden and visible 
      @draw()


    updateRackImage: ->
      # delay setting the image for one frame to give the renderer time to redraw
      #setTimeout(=>
      #  @model.rackImage(@rackGfx.cvs)
      #, @rackGfx.frameInterval)
      @rackGfx.drawComplete = @horor


    horor: =>
      @rackGfx.drawComplete = null
      @model.rackImage(@rackGfx.cvs)


    switchView: =>
      @draw()


    # redundant?
    evGrabbedRacks: (img) =>
      img_id = @fx.addImg({ img: img })
      relative_scale = @targetScale / @rackGfx.scale
      @fx.animate(img_id, { width: img.width * relative_scale, height: img.height * relative_scale }, RackSpace.ZOOM_DURATION, Easing.Quint.easeOut, @evZoomComplete)

      Util.setStyle(@rackGfx.cvs, 'visibility', 'hidden')


    # redundant?
    testGrab: (grab, callback) =>
      if grab.width > 0
        @grabGfx.destroy()
        callback(grab)
      else
        setTimeout(@testGrab, 10, grab, callback)


    # flatten layers to a single image, image takes a while to generate hence the delay and callback
    makeGrab: (gfx_layers, callback) ->
      @grabGfx = @createFXLayer(@rackEl, gfx_layers[0].width, gfx_layers[0].height)
      @grabGfx.addImg({ img: gfx.cvs }) for gfx in gfx_layers

      grab = new Image()
      grab.src = @grabGfx.cvs.toDataURL()

      @testGrab(grab, callback)


    setMetricLevel: (metric_level) =>
      @metricLevel = metric_level
      groups       = @model.groups()
      selection    = {}
      selection[group] = {} for group in groups

      if metric_level is 'chassis'
        device_lookup = @model.deviceLookup()
        for id of device_lookup[metric_level]
          device = device_lookup[metric_level][id]
          if device.instance.complex
            console.log group, id
            selection[device.instance.group][device.instance.id] = true
            selection[child.group][child.id] = true for child in device.instance.children

        @model.activeSelection(true)
        @model.selectedDevices(selection)
      else
        @model.activeSelection(false)
        @model.selectedDevices(selection)
