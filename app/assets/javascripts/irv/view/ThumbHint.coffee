define ['irv/view/Hint', 'common/util/Util'], (Hint, Util) ->

  class ThumbHint extends Hint


    @CAPTION : ''


    constructor: (container_el, model) ->
      super(container_el, model)


    show: (device, x, y) ->
      # find the containing rack
      device = device.parent while device.parent?

      caption = Util.substitutePhrase(ThumbHint.CAPTION, 'device_name', device.name)
      caption = Util.cleanUpSubstitutions(caption)

      super(caption, x, y)

