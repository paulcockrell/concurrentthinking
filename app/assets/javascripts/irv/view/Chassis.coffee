define ['irv/view/RackObject', 'irv/util/AssetManager', 'irv/view/Machine', 'irv/view/Metric', 'irv/view/Highlight', 'irv/view/Breacher', 'irv/ViewModel'], (RackObject, AssetManager, Machine, Metric, Highlight, Breacher, ViewModel) ->

  class Chassis extends RackObject

    # statics overwritten by config
    @DEPTH_SHADE_FILL      : '#0'
    @DEPTH_SHADE_MAX_ALPHA : .8
    @UNKNOWN_FILL          : '#ff00ff'
    @DEFAULT_WIDTH         : 370


    constructor: (def, parent) ->
      super(def, 'chassis', parent)
      @uHeight      = def.template.height
      @manufacturer = def.template.manufacturer
      @model        = def.template.model

      # max image dimensions are used to define the metric dims
      max_width  = 0
      max_height = 0

      @images = {}
      if def.template.images.front?
        face          = if @frontFacing then 'front' else 'rear'
        @images[face] = AssetManager.CACHE[RackObject.IMAGE_PATH + def.template.images.front]
        max_width     = @images[face].width if @images[face].width > max_width
        max_height    = @images[face].height if @images[face].height > max_height

      if def.template.images.rear?
        face          = if @frontFacing then 'rear' else 'front'
        @images[face] = AssetManager.CACHE[RackObject.IMAGE_PATH + def.template.images.rear]
        max_width     = @images[face].width if @images[face].width > max_width
        max_height    = @images[face].height if @images[face].height > max_height

      if not @images.front and not @images.rear
        @images.front = @getBlanker(Chassis.DEFAULT_WIDTH, RackObject.U_PX_HEIGHT * @uHeight)
        @images.rear  = @images.front
        max_width     = @images.front.width
        max_height    = @images.front.height
      else if not @frontFacing
        # swap images if rear mounted
        tmp           = @images.front
        @images.front = @images.rear
        @images.rear  = @images.front
      #console.log(@frontFacing)
      @images.both = @images.front

      face_img = @images[RackObject.MODEL.face()]

      @x      = 0
      @y      = 0
      @width  = if face_img? then face_img.width else 0
      @height = if face_img? then face_img.height else 0

      @complex       = def.template.rows > 1 or def.template.columns > 1
      @visible       = false
      @uStart        = def.uStart
      @slotsOccupied = []
      @assets        = []
      if @complex
        @slotWidth  = (@images.front.width - @template.padding.left - @template.padding.right) / @template.columns
        @slotHeight = (@images.front.height - @template.padding.top - @template.padding.bottom) / @template.rows
      else
        @slotWidth  = @images.front.width / def.Slots.length
        @slotHeight = @images.front.height

      @slotIds = {}
      for slot in def.Slots

        @slotIds[slot.column] = {} unless @slotIds[slot.column]?
        @slotIds[slot.column][slot.row] = slot.id

        continue unless slot.Machine?
        machine      = new Machine(slot.Machine, @)
        machine.type = def.template.deviceType

        @children.push(machine)
        if machine.row?
          @slotsOccupied[machine.row] = [] unless @slotsOccupied[machine.row]?
          @slotsOccupied[machine.row][machine.column] = true

      @subscriptions.push(RackObject.MODEL.metricLevel.subscribe(@setMetricVisibility))
      @subscriptions.push(RackObject.MODEL.breaches.subscribe(@setBreaching))

      @metric = new Metric(@group, @id, RackObject.INFO_GFX, 0, 0, max_width, max_height, RackObject.MODEL)


    destroy: ->
      @breach.destroy() if @breach?
      @highlight.destroy() if @highlight?
      @metric.destroy()
      super()


    setCoords: (@x, @y) ->
      @x -= @width / 2
      @alignMachines()
      @metric.setCoords(@x, @y) if @metric?


    draw: ->
      # clear
      RackObject.RACK_GFX.remove(asset) for asset in @assets
      @assets = []

      if @breach?
        @breach.destroy()
        @breach = null

      face = RackObject.MODEL.face()
      face = @bothView if face is 'both'

      @setMetricVisibility()
      @img = @images[face]
      if @img
        @visible = true
        # centre align
        @x = (@x + (@width / 2)) - (@img.width / 2)
        # chassis front/rear images may have different sizes
        @width  = @img.width
        @height = @img.height

        @assets.push(RackObject.RACK_GFX.addImg({ img: @img, x: @x, y: @y, alpha: if @included then 1 else RackObject.EXCLUDED_ALPHA }))

        # apply a fade for non full depth chassis in rear view
        if @template.depth is 'h' and ((face is 'rear' and @frontFacing) or (face is 'front' and not @frontFacing))
          @assets.push(RackObject.RACK_GFX.addRect({ x: @x, y: @y, width: @width, height: @height, fill: Chassis.DEPTH_SHADE_FILL, alpha: Chassis.DEPTH_SHADE_MAX_ALPHA * .5 }))
        # add a fade if in metric view mode
        if RackObject.MODEL.viewMode() is ViewModel.VIEW_MODE_METRICS
          @assets.push(RackObject.RACK_GFX.addRect({ fx: 'source-atop', x: @x, y: @y, width: @width, height: @height, fill: RackObject.METRIC_FADE_FILL, alpha: RackObject.METRIC_FADE_ALPHA }))
      else
        @visible = false

      @breach = new Breacher(@group, @id, RackObject.ALERT_GFX, @x, @y, @width, @height, RackObject.MODEL) if @breaching

      @alignMachines()
      super()


    select: ->
      unless @highlight?
        if @breaching
          offset = Breacher.STROKE_WIDTH
          double_off = offset * 2
          @highlight = new Highlight(Highlight.MODE_SELECT, @x + offset, @y + offset, @width - double_off, @height - double_off, RackObject.ALERT_GFX)
        else
          @highlight = new Highlight(Highlight.MODE_SELECT, @x, @y, @width, @height, RackObject.ALERT_GFX)


    deselect: ->
      if @highlight?
        @highlight.destroy()
        @highlight = null


    showDrag: ->
      if @highlight?
        @highlight.destroy()
        delete @highlight

      @highlight = new Highlight(Highlight.MODE_DRAG, @x, @y, @width, @height, RackObject.INFO_GFX)


    hideDrag: ->
      @highlight.destroy()
      delete @highlight


    alignMachines: ->
      if @images[RackObject.MODEL.face()]?
        if @template?
          for child in @children
            child.setCoords(@x + @template.padding.left + (child.column * @slotWidth), @y + @template.padding.top + ((@template.rows - child.row) * @slotHeight))
        else
          child.setCoords(@x, @y) for child in @children


    fadeOutMetrics: ->
      if @showMetric()
        #@metric.fadeOut()
      else
        child.fadeOutMetric() for child in @children


    fadeInMetrics: ->
      if @showMetric()
        #@metric.fadeIn()
      else
        child.fadeInMetric() for child in @children


    selectWithinOld: (box, inclusive) ->
      groups = RackObject.MODEL.groups()
      selected = {}
      selected[group] = {} for group in groups

      if inclusive
        for child in @children
          test_left = box.left >= child.x and box.left <= child.x + child.width
          test_right = box.right >= child.x and box.right <= child.x + child.width
          test_contained_h = box.left < child.x and box.right > child.x + child.width
          test_top = box.top >= child.y and box.top <= child.y + child.height
          test_bottom = box.bottom >= child.y and box.bottom <= child.y + child.height
          test_contained_v = box.top < child.y and box.bottom > child.y + child.height

          if (test_left or test_right or test_contained_h) and (test_top or test_bottom or test_contained_v)
            selected[child.group][child.id] = true
      else
       for child in @children
          test_contained_h = box.left <= child.x and box.right >= child.x + child.width
          test_contained_v = box.top <= child.y and box.bottom >= child.y + child.height
          
          if test_contained_h and test_contained_v
            selected[child.group][child.id] = true

      selected


    showMetric: ->
      face             = RackObject.MODEL.face()
      face             = @bothView if face is 'both'
      selected         = RackObject.MODEL.selectedDevices()
      metric_level     = RackObject.MODEL.metricLevel()
      view_mode        = RackObject.MODEL.viewMode()
      active_selection = RackObject.MODEL.activeSelection()
      active_filter    = RackObject.MODEL.activeFilter()
      filtered         = RackObject.MODEL.filteredDevices()

      return ((@frontFacing and face is 'front') or (not @frontFacing and face is 'rear')) and (metric_level is @group or @children.length is 0) and view_mode isnt ViewModel.VIEW_MODE_IMAGES and (not active_selection or selected[@group][@id]) and (not active_filter or filtered[@group][@id])


    setMetricVisibility: =>
      @metric.setActive(@showMetric())


    getSlot: (x, y) ->
      x -= @x + @template.padding.left
      y -= @y + @template.padding.top
      
      col = Math.floor(x / @slotWidth)
      row = @template.rows - Math.floor(y / @slotHeight) - 1
      if row >= 0 and row < @template.rows and col >= 0 and col < @template.columns

        device = null
        for child in @children
          if child.row is row and child.column is col
            device = child
            break

        return { id: @slotIds[col][row], row: row, column: col, device: device }

      return null


    setBreaching: (breaches) =>
      if breaches[@group]? and breaches[@group][@id]
        unless @breaching
          @breaching = true
          @breach = new Breacher(@group, @id, RackObject.ALERT_GFX, @x, @y, @width, @height, RackObject.MODEL)
      else
        @breaching = false
        if @breach?
          @breach.destroy()
          @breach = null
