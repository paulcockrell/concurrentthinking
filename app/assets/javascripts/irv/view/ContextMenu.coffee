define ['irv/view/Rack', 'irv/view/Chassis', 'irv/view/Machine', 'common/util/Util', 'common/util/Events'], (Rack, Chassis, Machine, Util, Events) ->

  class ContextMenu

    # statics overwritten by config
    @OPTIONS             : {}
    @URL_INTERNAL_PREFIX : 'internal::'
    @VERBOSE             : true
    @SPACER              : '<br>'
    @ASPECT_MAP          : { front: 'f', rear: 'r' }
    @DEVICE_TYPE_URL_MAP : { managedDevice: 'managed' }
    @COMMAND_MODEL       : 'concurrentCOMMAND'


    constructor: (@containerEl, @model, @internalCallback) ->
      @visible = false
      @menuEl  = $('context_menu')
      Events.addEventListener(@menuEl, 'click', @evClick)


    show: (device, x, y, available_slot) ->
      @visible    = true
      option_keys = ['common']

      if device?
        device_id   = device.id
        device_name = device.name
        device_type = if device.type? then ContextMenu.DEVICE_TYPE_URL_MAP[device.type] else undefined
        child       = device
      else
        option_keys.push('global')

      # iterate through device-parent hierarchy of selected item and collate a list of options to include
      while child?
        switch child.group
          when 'devices'
            device_id   = child.id
            device_name = child.name ? child.id
          when 'chassis'
            chassis_id   = child.id
            chassis_name = child.name ? child.id
          when 'racks'
            rack_id   = child.id
            rack_name = child.name ? child.id
            aspect    = @model.face()
            aspect    = child.bothView if aspect is 'both'
            aspect    = ContextMenu.ASPECT_MAP[aspect]
          else

        option_keys.push(child.group)
        if ContextMenu.VERBOSE
          child = child.parent
        else
          child = null
      
      if available_slot?
        if available_slot.type is 'chassis'
          empty_row    = available_slot.row + 1
          empty_column = available_slot.column + 1
          slot_id      = available_slot.id
        else
          empty_u = available_slot.row + 1

      # create array of options based upon option_keys
      options_html = []
      parsed       = []
      for option_set of ContextMenu.OPTIONS
        if option_keys.indexOf(option_set) isnt -1
          idx = parsed.length
          parsed.push([])
          for option in ContextMenu.OPTIONS[option_set]
            piece      = option.caption ? option.content
            option_url = option.url
            disabled   = option.disableForCommand and (((device instanceof Chassis) and device.model is ContextMenu.COMMAND_MODEL) or ((device instanceof Machine) and device.parent.model is ContextMenu.COMMAND_MODEL))
            on_click   = if disabled then null else option.onClick

            piece = Util.substitutePhrase(piece, 'device_id', device_id)
            piece = Util.substitutePhrase(piece, 'device_name', device_name)
            piece = Util.substitutePhrase(piece, 'device_type', device_type)
            piece = Util.substitutePhrase(piece, 'chassis_id', chassis_id)
            piece = Util.substitutePhrase(piece, 'chassis_name', chassis_name)
            piece = Util.substitutePhrase(piece, 'rack_id', rack_id)
            piece = Util.substitutePhrase(piece, 'rack_name', rack_name)
            piece = Util.substitutePhrase(piece, 'empty_row', empty_row)
            piece = Util.substitutePhrase(piece, 'empty_col', empty_column)
            piece = Util.substitutePhrase(piece, 'empty_u', empty_u)
            piece = Util.substitutePhrase(piece, 'slot_id', slot_id)
            piece = Util.substitutePhrase(piece, 'aspect', aspect)
            piece = Util.substitutePhrase(piece, 'spacer', ContextMenu.SPACER)

            piece = Util.cleanUpSubstitutions(piece)

            unless disabled
              if option_url?
                option_url = Util.substitutePhrase(option_url, 'device_id', device_id)
                option_url = Util.substitutePhrase(option_url, 'device_name', device_name)
                option_url = Util.substitutePhrase(option_url, 'device_type', device_type)
                option_url = Util.substitutePhrase(option_url, 'chassis_id', chassis_id)
                option_url = Util.substitutePhrase(option_url, 'chassis_name', chassis_name)
                option_url = Util.substitutePhrase(option_url, 'rack_id', rack_id)
                option_url = Util.substitutePhrase(option_url, 'rack_name', rack_name)
                option_url = Util.substitutePhrase(option_url, 'empty_row', empty_row)
                option_url = Util.substitutePhrase(option_url, 'empty_col', empty_column)
                option_url = Util.substitutePhrase(option_url, 'empty_u', empty_u)
                option_url = Util.substitutePhrase(option_url, 'aspect', aspect)
                option_url = Util.substitutePhrase(option_url, 'slot_id', slot_id)

                option_url = Util.cleanUpSubstitutions(option_url)

              if on_click?
                on_click = Util.substitutePhrase(on_click, 'device_id', device_id)
                on_click = Util.substitutePhrase(on_click, 'device_name', device_name)
                on_click = Util.substitutePhrase(on_click, 'device_type', device_type)
                on_click = Util.substitutePhrase(on_click, 'chassis_id', chassis_id)
                on_click = Util.substitutePhrase(on_click, 'chassis_name', chassis_name)
                on_click = Util.substitutePhrase(on_click, 'rack_id', rack_id)
                on_click = Util.substitutePhrase(on_click, 'rack_name', rack_name)
                on_click = Util.substitutePhrase(on_click, 'empty_row', empty_row)
                on_click = Util.substitutePhrase(on_click, 'empty_col', empty_column)
                on_click = Util.substitutePhrase(on_click, 'empty_u', empty_u)
                on_click = Util.substitutePhrase(on_click, 'aspect', aspect)
                on_click = Util.substitutePhrase(on_click, 'slot_id', slot_id)

                on_click = Util.cleanUpSubstitutions(on_click)

            if piece.length > 0
              if option_url?
                if disabled
                  parsed[idx].push("<a href='javascript: void(0);'><div class='disabled_context_menu_item'>#{piece}</div></a>")
                else if on_click?
                  parsed[idx].push("<a href='#{option_url}' onclick=\"#{on_click}\" ><div class='context_menu_item'>#{piece}</div></a>")
                else
                  parsed[idx].push("<a href='#{option_url}'><div class='context_menu_item'>#{piece}</div></a>")
              else
                parsed[idx].push(piece)

          parsed[idx] = parsed[idx].join('')
          parsed.splice(idx, 1) if parsed[idx].length is 0

      parsed = parsed.reverse()
      @menuEl.innerHTML = parsed.join(ContextMenu.SPACER)

      # adjust when near to edges of the screen
      div_x = Util.getStyle(@containerEl, 'left')
      div_x = div_x.substr(0, div_x.length - 2)

      container_dims = Util.getElementDimensions(@containerEl)
      menu_dims      = Util.getElementDimensions(@menuEl)

      x = if x + menu_dims.width > container_dims.width then container_dims.width - menu_dims.width else x
      y = if y + menu_dims.height > container_dims.height then container_dims.height - menu_dims.height else y

      Util.setStyle(@menuEl, 'left', x + 'px')
      Util.setStyle(@menuEl, 'top', y + 'px')
      Util.setStyle(@menuEl, 'visibility', 'visible')


    hide: ->
      if @visible
        @visible = false
        Util.setStyle(@menuEl, 'visibility', 'hidden')


    evClick: (ev) =>
      url = ev.target.parentElement.getAttribute('href')

      if url.substr(0, ContextMenu.URL_INTERNAL_PREFIX.length) is ContextMenu.URL_INTERNAL_PREFIX
        ev.preventDefault()
        ev.stopPropagation()

        @internalCallback(url.substr(ContextMenu.URL_INTERNAL_PREFIX.length))

      @hide()
