define ['common/util/Util', 'common/gfx/SimpleRenderer'], (Util, SimpleRenderer) ->

  class Chart

    # statics overwritten by config
    @WIDTH                 : '100%'
    @HEIGHT                : '100%'
    @DEPTH_3D              : 0
    @ANGLE                 : 10
    @BG_FILL               : '#ffffff'
    @BG_ALPHA              : 0
    @BORDER_ALPHA          : 0
    @BORDER_COLOUR         : '#ff0000'
    @X_AXIS_LABEL_ROTATION : 0
    @X_AXIS_GRID_COUNT     : 0
    @THRESHOLD_LINE_ALPHA  : 0.5
    @THRESHOLD_FILL_ALPHA  : 0.5

    @MARGIN_TOP   : 10
    @MARGIN_BTM   : 10
    @MARGIN_LEFT  : 10
    @MARGIN_RIGHT : 10

    @TEXT_COLOUR       : '#000000'
    @TEXT_FONT         : 'Karla'
    @TEXT_SIZE         : 11

    @GRAPH_TYPE           : 'column'
    @GRAPH_LINE_COLOUR    : '#000000'
    @GRAPH_LINE_ALPHA     : 1
    @GRAPH_LINE_THICKNESS : 1
    @GRAPH_FILL_ALPHA     : 1

    @BLN_BORDER_ALPHA       : 1
    @BLN_BORDER_COLOUR      : '#000000'
    @BLN_BORDER_THICKNESS   : 1
    @BLN_H_PADDING          : 5
    @BLN_V_PADDING          : 5
    @BLN_TEXT_COLOUR        : '#ffffff'
    @BLN_TEXT_SIZE          : 12
    @BLN_TEXT_ALIGN         : 'middle'
    @BLN_TEXT_SHADOW_COLOUR : '#000000'
    @BLN_CORNER_RADIUS      : 1
    @BLN_CAPTION            : '[[category]]: [[value]]'

    @TITLE_SIZE    : 14
    @TITLE_COLOUR  : '#000000'
    @TITLE_ALPHA   : .2
    @TITLE_BOLD    : true
    @TITLE_CAPTION : '[[metric_name]] [[num_metrics]] of [[total_metrics]] (max=[[max_val]], min=[[min_val]])'

    @POINTER_OFFSET_X : 10
    @POINTER_OFFSET_Y : 10


    constructor: (@containerEl, @model) ->
      @pointerEl = $('pointer')

      @chart                 = new AmCharts.AmSerialChart()
      @chart.categoryField   = 'label'
      @chart.marginTop       = Chart.MARGIN_TOP
      @chart.marginBottom    = Chart.MARGIN_BTM
      @chart.marginLeft      = Chart.MARGIN_LEFT
      @chart.marginRight     = Chart.MARGIN_RIGHT
      @chart.angle           = Chart.ANGLE
      @chart.depth3D         = Chart.DEPTH_3D
      @chart.backgroundColor = Chart.BG_FILL
      @chart.backgroundAlpha = Chart.BG_ALPHA
      @chart.borderColor     = Chart.BORDER_COLOUR
      @chart.borderAlpha     = Chart.BORDER_ALPHA
      @chart.color           = Chart.TEXT_COLOUR
      @chart.fontFamily      = Chart.TEXT_FONT
      @chart.fontSize        = Chart.TEXT_SIZE
      @chart.width           = Chart.WIDTH
      @chart.height          = Chart.HEIGHT

      @chart.addListener("rollOverGraphItem", =>
        @evRollOver(arguments[0].item.dataContext.group, arguments[0].item.dataContext.id))
      @chart.addListener('rollOutGraphItem', =>
        @evRollOut())

      balloon                   = @chart.balloon
      balloon.borderAlpha       = Chart.BLN_BORDER_ALPHA
      balloon.borderColor       = Chart.BLN_BORDER_COLOUR
      balloon.borderThickness   = Chart.BLN_BORDER_THICKNESS
      balloon.color             = Chart.BLN_TEXT_COLOUR
      balloon.fontSize          = Chart.BLN_TEXT_SIZE
      balloon.textAlign         = Chart.BLN_TEXT_ALIGN
      balloon.textShadowColor   = Chart.BLN_TEXT_SHADOW_COLOUR
      balloon.verticalPadding   = Chart.BLN_V_PADDING
      balloon.horizontalPadding = Chart.BLN_H_PADDING
      balloon.cornerRadius      = Chart.BLN_CORNER_RADIUS

      @graph               = new AmCharts.AmGraph()
      @graph.valueField    = 'metric'
      @graph.colorField    = 'colour'
      @graph.type          = Chart.GRAPH_TYPE
      @graph.lineColor     = Chart.GRAPH_LINE_COLOUR
      @graph.lineAlpha     = Chart.GRAPH_LINE_ALPHA
      @graph.lineThickness = Chart.GRAPH_LINE_THICKNESS
      @graph.fillAlphas    = Chart.GRAPH_FILL_ALPHA
      @graph.balloonText   = Chart.BLN_CAPTION

      @thresholds = []

      @chart.categoryAxis.labelsEnabled = false
      @chart.categoryAxis.axisAlpha = 0
      @chart.categoryAxis.gridCount = Chart.X_AXIS_GRID_COUNT
      @chart.autoMargins = false
      @chart.marginRight=90
      @chart.marginBottom = 10

      @chart.addGraph(@graph)
      @chart.write(@containerEl.getAttribute('id'))
      graph_parent = @containerEl.getElementsByTagName('svg')[0].parentElement ? @containerEl.getElementsByTagName('svg')[0].parentNode
      Util.setStyle(graph_parent, 'position', '')

      @model.showChart.subscribe(@setSubscriptions)
      @setSubscriptions(@model.showChart())


    update: =>
      data             = []
      metric_data      = @model.metricData()
      metric_templates = @model.metricTemplates()
      metric_template  = metric_templates[metric_data.metricId]
      selected_metric  = @model.selectedMetric()

      # ignore unrecognised metrics or redundant requests (when the metric data isn't
      # for the current metric)
      if not metric_template? or metric_data.metricId isnt selected_metric
        @clear()
        return

      device_lookup    = @model.deviceLookup()
      selected_devices = @model.selectedDevices()
      active_selection = @model.activeSelection()
      active_filter    = @model.activeFilter()
      filtered_devices = @model.filteredDevices()
      metric_level     = @model.metricLevel()
      @included        = { devices: {}, chassis: {}, racks: {} }

      col_map  = @model.colourMaps()[metric_data.metricId]
      col_high = col_map.high
      col_low  = col_map.low
      range    = col_high - col_low

      # extract subset of all metrics according to selection
      metric_count = 0
      for i of metric_data.values[metric_level]
        ++metric_count
        if (not active_selection or (active_selection and selected_devices[metric_level][i])) and (not active_filter or (active_filter and filtered_devices[metric_level][i]))
          device              = device_lookup[metric_level][i]
          @included[metric_level][i] = true

          metric     = metric_data.values[metric_level][i]
          temp       = (metric - col_low) / range
          col        = @getColour(temp).toString(16)
          col        = '0' + col while col.length < 6
          name       = if device then (device.name ? i) else 'unknown'
          
          data.push({ label: name, name: name, id: i, group: metric_level, pos: @posLookup[metric_level][i], metric: metric, numMetric: Number(metric), colour: '#' + col })
              
      if data.length > 0
        # sort
        switch @model.graphOrder()
          when 'ascending'
            Util.sortByProperty(data, 'numMetric', true)
            max = data[data.length - 1]
            min = data[0]
          when 'descending'
            Util.sortByProperty(data, 'numMetric', false)
            max = data[0]
            min = data[data.length - 1]
          when 'physical position'
            Util.sortByProperty(data, 'pos', true)
            min = data[0]
            max = data[0]
            for datum in data
              min = datum if datum.metric < min.metric
              max = datum if datum.metric > max.metric
          when 'name'
            Util.sortByProperty(data, 'device', true)
            min = data[0]
            max = data[0]
            for datum in data
              min = datum if datum.metric < min.metric
              max = datum if datum.metric > max.metric

        title = Chart.TITLE_CAPTION
        # swap in title variables
        title = Util.substitutePhrase(title, 'metric_name', metric_template.name)
        title = Util.substitutePhrase(title, 'num_metrics', data.length)
        title = Util.substitutePhrase(title, 'total_metrics', metric_count)
        title = Util.substitutePhrase(title, 'max_val', max.metric)
        title = Util.substitutePhrase(title, 'min_val', min.metric)
        title = Util.substitutePhrase(title, 'metric_units', metric_template.units)
        title = Util.cleanUpSubstitutions(title)

        @graph.valueAxis.removeGuide(threshold) for threshold in @thresholds

        @chart.titles = []
        @chart.addTitle(unescape(title), Chart.TITLE_SIZE, Chart.TITLE_COLOUR, Chart.TITLE_ALPHA, Chart.TITLE_BOLD)
        @chart.dataProvider = data
        @chart.validateData()
        @chart.validateNow()
        @chart.invalidateSize()


        @updateThreshold() if @model.selectedThreshold()?
      else
        @clear()


    getColour: (val) ->
      colours = @model.colourScale()
      return colours[0].col if val <= 0 or isNaN(val)
      return colours[colours.length - 1].col if val >= 1

      count = 0
      len   = colours.length
      while val > colours[count].pos
        ++count
      low  = colours[count - 1]
      high = colours[count]

      Util.blendColour(low.col, high.col, (val - low.pos) / (high.pos - low.pos))


    setSubscriptions: (show_chart) =>
      if show_chart
        @selSub    = @model.selectedDevices.subscribe(@update)
        @filterSub = @model.filteredDevices.subscribe(@update)
        @metSub    = @model.metricData.subscribe(@update)
        @metSub2   = @model.selectedMetric.subscribe(@clear)
        @colSub    = @model.colourMaps.subscribe(@update)
        @orderSub  = @model.graphOrder.subscribe(@update)
        @posSub    = @model.racks.subscribe(@makePositionLookup)
        @highSub   = @model.highlighted.subscribe(@highlightDatum)
        @tholdSub  = @model.selectedThreshold.subscribe(@updateThreshold)
        @mLevelSub = @model.metricLevel.subscribe(@update)
        @makePositionLookup()
        setTimeout(@update, 500)
      else
        if @metSub?
          @selSub.dispose()
          @filterSub.dispose()
          @metSub.dispose()
          @metSub2.dispose()
          @colSub.dispose()
          @orderSub.dispose()
          @posSub.dispose()
          @highSub.dispose()
          @tholdSub.dispose()
          @mLevelSub.dispose()


    # creates posLookup object which uses [device_type + id] as the key and pos idx as the value, for sorting by physical position
    makePositionLookup: =>
      groups            = @model.groups()
      @posLookup        = {}
      @posLookup[group] = {} for group in groups

      racks   = @model.racks()
      u_count = 0
      for rack in racks
        @posLookup.racks[rack.id] = u_count
        continue unless rack.chassis?
        for chassis in rack.chassis
          pos = chassis.uStart + u_count
          @posLookup.chassis[chassis.id] = pos
          for slot in chassis.Slots
            @posLookup.devices[slot.Machine.id] = pos if slot.Machine?
        u_count += rack.uHeight


    highlightDatum: =>
      device = @model.highlighted()

      if not @over and device? and @included? and @included[device.group][device.id]
        # hacky route in to amcharts to find datum coordinates, consider revision
        coords = @chart.lookupTable[device.name].axes.valueAxis0.graphs.graph0
        Util.setStyle(@pointerEl, 'left', coords.x + Chart.POINTER_OFFSET_X + 'px')
        Util.setStyle(@pointerEl, 'top', coords.y + Chart.POINTER_OFFSET_Y + 'px')
        Util.setStyle(@pointerEl, 'visibility', 'visible')
      else
        Util.setStyle(@pointerEl, 'visibility', 'hidden')


    evRollOver: (group, device_id) =>
      @over  = true
      device = @model.deviceLookup()[group][device_id]
      @model.highlighted(device.instance) if device?


    evRollOut: (device_id) =>
      @over = false
      @model.highlighted(null)


    getSelection: (box) ->
      groups           = @model.groups()
      selection        = {}
      selection[group] = {} for group in groups
      active_selection = false
      count            = 0
      # hacky route in to amcharts to find datum coordinates, consider revision
      for device of @chart.lookupTable
        datum = @chart.lookupTable[device]
        continue unless datum.axes?
        coords = datum.axes.valueAxis0.graphs.graph0
        if coords.x >= box.x and coords.x <= box.x + box.width
          selection[datum.axes.valueAxis0.graphs.graph0.dataContext.group][datum.axes.valueAxis0.graphs.graph0.dataContext.id] = true
          active_selection = true
          ++count

      { activeSelection: active_selection, selection: selection, count: count }

    selectWithinBox: (box) ->
      selected = @getSelection(box)
      @model.activeSelection(selected.activeSelection)
      @model.selectedDevices(selected.selection)


    clear: =>
      @chart.titles       = []
      @chart.dataProvider = []
      @chart.validateData()
      @chart.validateNow()
      @chart.invalidateSize()


    updateThreshold: =>
      threshold = @model.selectedThreshold()
      @graph.valueAxis.removeGuide(thold) for thold in @thresholds
      @thresholds = []

      unless threshold?
        @chart.validateNow()
        return

      @thresholds = []
      count       = 0
      len         = threshold.colours.length
      if len is 1
        thold           = new AmCharts.Guide()
        thold.lineColor = threshold.colours[0]
        thold.lineAlpha = Chart.THRESHOLD_LINE_ALPHA
        thold.value     = threshold.values[0]
        @thresholds.push(thold)
        @graph.valueAxis.addGuide(thold)
      else
        bottom_val = @graph.valueAxis.min
        while count < len
          thold           = new AmCharts.Guide()
          thold.lineColor = threshold.colours[count]
          thold.lineAlpha = Chart.THRESHOLD_LINE_ALPHA
          thold.fillColor = threshold.colours[count]
          thold.fillAlpha = Chart.THRESHOLD_FILL_ALPHA
          thold.value     = bottom_val
          thold.toValue   = threshold.values[count] ? @graph.valueAxis.max
          bottom_val      = threshold.values[count]
          @thresholds.push(thold)
          @graph.valueAxis.addGuide(thold)
          ++count

      @chart.validateNow()
      #@graph.valueAxis.invalidateDisplayList()
      #@chart.validateData()
      #@chart.validateNow()
      #@chart.invalidateSize()
