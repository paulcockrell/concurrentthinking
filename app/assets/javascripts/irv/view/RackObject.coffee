# super class to Rack, Chassis, and Machine
define ->

  class RackObject

    # statics overwritten by config
    @BLANK_FILL        : '#557744'
    @METRIC_FADE_FILL  : '#ffffff'
    @METRIC_FADE_ALPHA : .1
    @IMAGE_PATH        : ''
    @EXCLUDED_ALPHA    : 0.5
    @U_PX_HEIGHT       : 50

    # run-time assigned statics
    @RACK_GFX  : null
    @INFO_GFX  : null
    @ALERT_GFX : null
    @MODEL     : null


    constructor: (def, @group, @parent) ->
      @id            = def.id
      @name          = def.name
      @template      = def.template
      @included      = true
      @children      = []
      @subscriptions = []
      @frontFacing   = def.facing is 'f' or not def.facing?
      @bothView      = if @parent? then @parent.bothView else def.bothView
      #console.log(@frontFacing, @group, @id)
      # toggle frontFacing based upon parent direction, up to the top level
      parent = @parent
      #parent = if @parent? then @parent.parent else null
      while parent?
      #  console.log('   ', parent.group, parent.frontFacing)
        @frontFacing = not @frontFacing if not parent.frontFacing
        parent       = parent.parent
      @frontFacing = not @frontFacing if @parent? and not @parent.frontFacing
      #if @parent?
      #  parent = @parent
      #  while parent?
      #    top_node = parent
      #    parent   = parent.parent
      #  console.log(top_node.group, top_node.frontFacing)
      #  @frontFacing = not @frontFacing unless top_node.frontFacing
      #console.log(@frontFacing)
      # store class instance in device lookup
      RackObject.MODEL.deviceLookup()[@group][@id].instance = @


    destroy: ->
      sub.dispose() for sub in @subscriptions
      child.destroy() for child in @children


    getBlanker: (width, height) ->
      # create blank image where none has been supplied. Might be good to cache these
      # (using dims as a lookup) to reduce processing and memory usage a little
      cvs        = document.createElement('canvas')
      cvs.width  = width
      cvs.height = height

      ctx = cvs.getContext('2d')
      ctx.beginPath()
      ctx.fillStyle = RackObject.BLANK_FILL
      ctx.fillRect(0, 0, cvs.width, cvs.height)
      return cvs


    draw: ->
      child.draw() for child in @children


    getDeviceAt: (x, y) ->
      # search children in reverse order, this give presidence to children drawn last (topmost)
      count = @children.length
      while count > 0
        --count
        child = @children[count]
        if y > child.y and y < child.y + child.height and x > child.x and x < child.x + child.width and child.visible
          subchild = child.getDeviceAt(x, y)
          if subchild? then return subchild else return child

      return null


    # determins wether rack object should be drawn. Returns true if this instance or any
    # single child is included in the filter and/or selection
    setIncluded: ->
      child_included = false
      for child in @children
        child_included = true if child.setIncluded()
      @included = child_included or ((not RackObject.MODEL.activeSelection() or RackObject.MODEL.selectedDevices()[@group][@id]) and (not RackObject.MODEL.activeFilter() or RackObject.MODEL.filteredDevices()[@group][@id]))


    # detects if this rack object or any of it's children are positioned within a specified box.
    # Inclusive selections (object touches box) and exlusive selections (object is contained by box)
    # Box object requires properties: top, bottom, left, right
    selectWithin: (box, inclusive) ->
      groups   = RackObject.MODEL.groups()
      selected = {}
      selected[group] = {} for group in groups

      if inclusive
        test_left        = box.left >= @x and box.left <= @x + @width
        test_right       = box.right >= @x and box.right <= @x + @width
        test_contained_h = box.left < @x and box.right > @x + @width
        test_top         = box.top >= @y and box.top <= @y + @height
        test_bottom      = box.bottom >= @y and box.bottom <= @y + @height
        test_contained_v = box.top < @y and box.bottom > @y + @height

        if (test_left or test_right or test_contained_h) and (test_top or test_bottom or test_contained_v)
          selected[@group][@id] = true
          for child in @children
            subselection = child.selectWithin(box, inclusive)
            for group in groups
              for i of subselection[group]
                selected[group][i] = true if not isNaN(Number(subselection[group][i]))
      else
        test_contained_h = box.left <= @x and box.right >= @x + @width
        test_contained_v = box.top <= @y and box.bottom >= @y + @height
        
        if test_contained_h and test_contained_v
          selected[@group][@id] = true

        for child in @children
          subselection = child.selectWithin(box, inclusive)
          for group in groups
            for i of subselection[group]
              selected[group][i] = true if not isNaN(Number(subselection[group][i]))

      selected

