define ['irv/view/Hint', 'irv/view/Rack', 'irv/view/Chassis', 'irv/view/Machine', 'common/util/Events', 'common/util/Util'], (Hint, Rack, Chassis, Machine, Events, Util) ->

  class RackHint extends Hint

    # statics overwritten by config
    @RACK_TEXT       : '<span style="font-weight: 700">Rack: [[name]]</span><br><ul style="list-style-type: none;"><li>Height: [[u_height]]U<li>Free space: [[u_available]]<li>Used space: [[u_occupied]]</ul>'
    @CHASSIS_TEXT    : '<span style="font-weight: 700">Chassis: [[name]]</span><br><ul style="list-style-type: none;"><li>[[parent_name]]<li>[[metric_name]]<li>[[metric_value]]<li>[[u_height]]<li>[[num_rows]]<li>[[slots_per_row]]<li>[[u_position]]<li>[[slots_avaliable]]</ul>'
    @DEVICE_TEXT     : '<span style="font-weight: 700">Device: [[name]]</span><br><ul style="list-style-type: none;"><li>'
    @NO_METRIC       : 'No metric data available'
    @MORE_INFO_DELAY : 1000


    constructor: (container_el, model) ->
      super(container_el, model)


    show: (device, x, y) ->
      metrics         = @model.metricData()
      metric_template = if @model.metricTemplates()[metrics.metricId]? then @model.metricTemplates()[metrics.metricId] else {}

      if device instanceof Rack
        caption = RackHint.RACK_TEXT
        # when hovering over a rack, calculate how many u are occupied. This probably should be a
        # property maintained by the rack class rather than calculated here
        u_occupied = 0
        for slot in device.uOccupied
          ++u_occupied if slot?

        caption = Util.substitutePhrase(caption, 'name', device.name)
        caption = Util.substitutePhrase(caption, 'u_height', device.uHeight)
        caption = Util.substitutePhrase(caption, 'u_available', device.uHeight - u_occupied)
        caption = Util.substitutePhrase(caption, 'u_occupied', u_occupied)

        caption = Util.cleanUpSubstitutions(caption)
      else if device instanceof Chassis
        caption = RackHint.CHASSIS_TEXT
        caption = Util.substitutePhrase(caption, 'name', device.name)
        caption = Util.substitutePhrase(caption, 'parent_name', device.parent.name)
        caption = Util.substitutePhrase(caption, 'metric_name', metric_template.name)
        caption = Util.substitutePhrase(caption, 'metric_value', metrics.values[device.group][device.id])
        caption = Util.substitutePhrase(caption, 'u_height', device.uHeight)
        caption = Util.substitutePhrase(caption, 'num_rows', if device.complex then device.template.rows else null)
        caption = Util.substitutePhrase(caption, 'slots_per_row', if device.complex then device.template.columns else null)
        caption = Util.substitutePhrase(caption, 'u_position', if device.uHeight > 1 then "#{(device.uStart + 1)}U-#{(device.uStart + device.uHeight)}U" else (device.uStart + 1) + 'U')
        caption = Util.substitutePhrase(caption, 'slots_available', if device.complex then (device.template.rows * device.template.columns) - device.children.length else null)

        caption = Util.cleanUpSubstitutions(caption)
      else
        caption = RackHint.DEVICE_TEXT

        if device.parent.complex
          parent_name = device.parent.name
          u_height    = null
          position    = "column: #{device.column + 1}, row: #{device.row + 1}"
        else
          parent_name = null
          u_height    = device.parent.uHeight
          position    = if device.parent.uHeight > 1 then "#{(device.parent.uStart + 1)}U-#{(device.parent.uStart + device.parent.uHeight)}U" else (device.parent.uStart + 1) + 'U'

        caption = Util.substitutePhrase(caption, 'name', device.name)
        caption = Util.substitutePhrase(caption, 'parent_name', parent_name)
        caption = Util.substitutePhrase(caption, 'rack_name', device.parent.parent.name)
        caption = Util.substitutePhrase(caption, 'metric_name', metric_template.name)
        caption = Util.substitutePhrase(caption, 'metric_value', if metrics.values[device.group][device.id]? then metric_template.format.replace(/%s/, metrics.values[device.group][device.id]) else null)
        caption = Util.substitutePhrase(caption, 'position', position)
        caption = Util.substitutePhrase(caption, 'u_height', u_height)

        caption = Util.cleanUpSubstitutions(caption)

      @device  = device
      @moreTmr = setTimeout(@getMore, RackHint.MORE_INFO_DELAY)
      super(caption, x, y)


    hide: ->
      super()
      clearTimeout(@moreTmr)


    getMore: =>
      Events.dispatchEvent(@hintEl, 'getHintInfo')


    appendData: (data) =>
      if @visible
        append = '<br>'
        for datum of data
          append += datum + ': ' + data[datum] + '<br>' if data[datum]? and data[datum] isnt ''

        @hintEl.innerHTML += append

