define ['common/util/Util', 'common/gfx/Easing'], (Util, Easing) ->

  class Metric

    # statics overwritten by config
    @ALPHA         : .5
    @ANIM_DURATION : 500
    @FADE_DURATION : 500


    constructor: (@group, @id, @gfx, @x, @y, @width, @height, @model) ->
      @visible    = false
      @active     = false
      @horizontal = @width > @height
      @scale      = 0

      @subscriptions = []
      @subscriptions.push(@model.scaleMetrics.subscribe(@toggleScale))
      @subscriptions.push(@model.metricData.subscribe(@update))
      @subscriptions.push(@model.colourMaps.subscribe(@update))

      @update()


    destroy: ->
      sub.dispose() for sub in @subscriptions


    setActive: (@active) ->
      if @active and @value? then @show() else @hide()

 
    update: =>
      metrics = @model.metricData()
      @value  = metrics.values[@group][@id]

      if @value?
        colour_scale = @model.colourScale()
        colour_map   = @model.colourMaps()[metrics.metricId]
        range        = colour_map.range
        @scale       = (@value - colour_map.low) / range
        @scale       = 0 if @scale < 0
        @scale       = 1 if @scale > 1
        if @active
          @show()
          @updateBar()
        else
          @hide()
      else
        @scale = null
        @hide()


    setCoords: (@x, @y) ->
      if @asset?
        if not @horizontal and @model.scaleMetrics()
          @gfx.setAttributes(@asset, { x: @x, y: @y + @height - (@height * @scale) })
        else
          @gfx.setAttributes(@asset, { x: @x, y: @y }) if @asset?


    updateBar: () ->
      colour = '#' + @getColour().toString(16)
      unless @model.scaleMetrics()
        attrs =
          fill: colour
          width: @width
          height: @height
          y: @y
      else

        if @horizontal
          attrs =
            fill: colour
            width: @width * @scale

        else
          new_height = @height * @scale
          attrs =
            fill: colour
            height: new_height
            y: @y + @height - new_height

      @gfx.animate(@asset, attrs, Metric.ANIM_DURATION, Easing.Cubic.easeOut)


    getColour: ->
      colours = @model.colourScale()

      return colours[0].col if @scale <= 0 or isNaN(@scale)
      return colours[colours.length - 1].col if @scale >= 1

      count = 0
      len   = colours.length
      while @scale > colours[count].pos
        ++count
      low  = colours[count - 1]
      high = colours[count]
      Util.blendColour(low.col, high.col, (@scale - low.pos) / (high.pos - low.pos))


    draw: ->
      # clear
      @gfx.remove(@asset) if @asset?
      colour = '#' + @getColour().toString(16)

      unless @model.scaleMetrics()
        @asset = @gfx.addRect({ x: @x, y: @y, width: @width, height: @height, alpha: Metric.ALPHA, fill: colour })
      else

        if @horizontal
          @asset = @gfx.addRect({ x: @x, y: @y, width: @width * @scale, height: @height, alpha: Metric.ALPHA, fill: colour })
        else
          new_height = @height * @scale
          @asset = @gfx.addRect({ x: @x, y: @y + @height - new_height, width: @width, height: new_height, alpha: Metric.ALPHA, fill: colour })


    show: =>
      unless @visible
        @visible = true
        @draw()


    hide: =>
      if @visible
        @visible = false
        @gfx.remove(@asset)
        @asset = null


    fadeOut: ->
      if @asset?
        @visible = false
        @gfx.animate(@asset, { alpha: 0 }, Metric.FADE_DURATION, Easing.Quad.easeOut, @hide)


    fadeIn: ->
      unless @visible
        @show()
        @gfx.setAttributes(@asset, { alpha: 0 })
        @gfx.animate(@asset, { alpha: Metric.ALPHA }, Metric.FADE_DURATION, Easing.Quad.easeOut, @evAnimComplete)


    toggleScale: =>
      @updateBar() if @active and @value?

