define ['irv/view/RackObject', 'irv/view/Chassis', 'common/gfx/SimpleRenderer', 'common/gfx/Easing', 'irv/util/AssetManager', 'common/util/Events', 'common/util/Util', 'irv/ViewModel'], (RackObject, Chassis, SimpleRenderer, Easing, AssetManager, Events, Util, ViewModel) ->

  class Rack extends RackObject

    # statics overwritten by config
    @IMG_FRONT_BTM    : 'images/rack-rear_edit.png'
    @IMG_FRONT_TOP    : 'images/rack-rear_edit.png'
    @IMG_FRONT_REPEAT : 'images/rack-rear_edit.png'
    @IMG_REAR_BTM     : 'images/rack-rear_edit.png'
    @IMG_REAR_TOP     : 'images/rack-rear_edit.png'
    @IMG_REAR_REPEAT  : 'images/rack-rear_edit.png'

    @U_LBL_OFFSET_X : 70
    @U_LBL_OFFSET_Y : 39
    @U_LBL_FONT     : '10px Karla'
    @U_LBL_COLOUR   : 'white'
    @U_LBL_ALIGN    : 'right'

    @NAME_LBL_OFFSET_X         : 0
    @NAME_LBL_OFFSET_Y         : 22
    @NAME_LBL_OFFSET_MAX_WIDTH : 0
    @NAME_LBL_FONT             : 'Karla'
    @NAME_LBL_COLOUR           : 'white'
    @NAME_LBL_ALIGN            : 'center'
    @NAME_LBL_BG_FILL          : 'black'
    @NAME_LBL_BG_PADDING       : 4
    @NAME_LBL_BG_ALPHA         : .4
    @NAME_LBL_MIN_SIZE         : 13
    @NAME_LBL_SIZE             : 60

    @CAPTION_FRONT : '[ front ]'
    @CAPTION_REAR  : '[ rear ]'

    @SPACE_FILL          : '#ff00ff'
    @SPACE_ALPHA         : .3
    @SPACE_FADE_DURATION : 500
    @SPACE_PADDING       : 70

    @FADE_IN_METRIC_MODE : true

    # hard coded and run-time assigned statics
    @CHASSIS_OFFSET_Y : null # assigned dynamically from the height of the rack top slice image
    @IMAGES           : {}


    @initialise: ->
      # static function called once to set certain static properties
      Rack.IMAGES =
        front:
          top: AssetManager.CACHE[RackObject.IMAGE_PATH + Rack.IMG_FRONT_TOP]
          btm: AssetManager.CACHE[RackObject.IMAGE_PATH + Rack.IMG_FRONT_BTM]
          repeat: AssetManager.CACHE[RackObject.IMAGE_PATH + Rack.IMG_FRONT_REPEAT]
        rear:
          top: AssetManager.CACHE[RackObject.IMAGE_PATH + Rack.IMG_REAR_TOP]
          btm: AssetManager.CACHE[RackObject.IMAGE_PATH + Rack.IMG_REAR_BTM]
          repeat: AssetManager.CACHE[RackObject.IMAGE_PATH + Rack.IMG_REAR_REPEAT]

      # assign measurements based on image dimensions
      Rack.CHASSIS_OFFSET_Y = Rack.IMAGES.front.top.height


    constructor: (def) ->
      super(def, 'racks')
      @uHeight = def.uHeight

      @images = {}
      if @frontFacing
        @images.front = Rack.IMAGES.front
        @images.rear  = Rack.IMAGES.rear
        @images.both  = Rack.IMAGES.front
      else
        @images.front = Rack.IMAGES.rear
        @images.rear  = Rack.IMAGES.front
        @images.both  = Rack.IMAGES.rear

      face_slices = @images[RackObject.MODEL.face()]

      @x      = 0
      @y      = 0
      @width  = face_slices.top.width
      @height = face_slices.top.height + face_slices.btm.height + (RackObject.U_PX_HEIGHT * @uHeight)

      @uLbls           = []
      @selected        = false
      @uOccupied       = []
      @highlights      = []
      @availableSpaces = []
      @assets          = []

      count    = 0
      len      = def.chassis.length
      @chassis = []
      for chassis_def in def.chassis
        chassis = new Chassis(chassis_def, @)
        @children.push(chassis)
        # calculate which slots are occupied
        count = 0
        while count < chassis.uHeight
          @uOccupied[chassis.uStart + count] = chassis
          ++count

      # frontFirst and rearFirst are the list of children (chassis) ordered according to
      # wether they are front mounted. By switching between these when re-drawing we effect
      # the draw order of the children, dictating wether rear mounted chassis will be drawn
      # over the top of front mounted chassis, thus facilitating the different rear and front
      # view layering. E.g. rearFirst will first draw rear mounted chassis, then front mounted
      # over the top giving the appropriate front view chassis layering
      @frontFirst = Util.sortByProperty(@children, 'frontFacing', false)
      @rearFirst  = @frontFirst.slice(0)
      @rearFirst.reverse()


    setCoords: (@x, @y) ->
      x = @x + (@width / 2)
      for child in @children
        y = (RackObject.U_PX_HEIGHT * (@uHeight - child.uStart - child.uHeight)) + Rack.CHASSIS_OFFSET_Y + @y
        child.setCoords(x, y)


    draw: (show_u_labels, show_name_label) ->
      # clear
      RackObject.RACK_GFX.remove(asset) for asset in @assets
      @assets = []

      # add labels as necessary
      @showNameLabel(show_name_label)
      @showULabels(show_u_labels)

      face = RackObject.MODEL.face()
      face = @bothView if face is 'both'

      # construct rack image
      face_slices = @images[face]
      alpha       = if @included then 1 else RackObject.EXCLUDED_ALPHA

      @assets.push(RackObject.RACK_GFX.addImg({ img: face_slices.top, x: @x, y: @y, alpha: alpha }))

      repeat_slice_offset = @y + Rack.CHASSIS_OFFSET_Y
      count = 0
      while count < @uHeight
        @assets.push(RackObject.RACK_GFX.addImg({ img: face_slices.repeat, x: @x, y: count * RackObject.U_PX_HEIGHT + repeat_slice_offset, alpha: alpha }))
        ++count
      #@assets.push(RackObject.RACK_GFX.addRect({ fill: face_slices.repeat, x: @x, y: repeat_slice_offset, width: face_slices.repeat.width, height: RackObject.U_PX_HEIGHT * @uHeight }))
      @assets.push(RackObject.RACK_GFX.addImg({ img: face_slices.btm, x: @x, y: @uHeight * RackObject.U_PX_HEIGHT + repeat_slice_offset, alpha: alpha }))

      # add a fade if in metric view mode
      @assets.push(RackObject.RACK_GFX.addRect({ fx: 'source-atop', x: @x, y: @y, width: @width, height: @height, fill: RackObject.METRIC_FADE_FILL, alpha: RackObject.METRIC_FADE_ALPHA })) if RackObject.MODEL.viewMode() is ViewModel.VIEW_MODE_METRICS and Rack.FADE_IN_METRIC_MODE

      # determine draw order according to current view
      @children = if face is 'front' then @rearFirst else @frontFirst
      super()
      #chassis.draw() for chassis in @chassis


    showULabels: (visible) ->
      if visible and @uLbls.length is 0
        @uLbls = []
        count  = 0
        while count < @uHeight
          lbl_y = (RackObject.U_PX_HEIGHT * (@uHeight - count)) + Rack.U_LBL_OFFSET_Y + @y
          @uLbls.push(RackObject.INFO_GFX.addText(
            x       : @x + Rack.U_LBL_OFFSET_X
            y       : lbl_y
            caption : count + 1
            font    : Rack.U_LBL_FONT
            align   : Rack.U_LBL_ALIGN
            fill    : Rack.U_LBL_COLOUR)
          )
          ++count

      else if not visible and @uLbls.length > 0
        RackObject.INFO_GFX.remove(lbl) for lbl in @uLbls
        @uLbls = []


    showNameLabel: (visible) ->
      if visible
        RackObject.INFO_GFX.remove(@nameLbl) if @nameLbl?

        size = Rack.NAME_LBL_SIZE * RackObject.INFO_GFX.scale
        if size < Rack.NAME_LBL_MIN_SIZE then size = Rack.NAME_LBL_MIN_SIZE

        name = "#{@name} #{if RackObject.MODEL.face() is 'front' then Rack.CAPTION_FRONT else Rack.CAPTION_REAR}"
        
        @nameLbl = RackObject.INFO_GFX.addText(
          x         : @x + (@width / 2) + Rack.NAME_LBL_OFFSET_X
          y         : @y + Rack.NAME_LBL_OFFSET_Y
          caption   : name
          font      : size + 'px ' + Rack.NAME_LBL_FONT
          align     : Rack.NAME_LBL_ALIGN
          fill      : Rack.NAME_LBL_COLOUR
          bgFill    : Rack.NAME_LBL_BG_FILL
          bgAlpha   : Rack.NAME_LBL_BG_ALPHA
          bgPadding : Rack.NAME_LBL_BG_PADDING
          maxWidth  : @width - (Rack.NAME_LBL_BG_PADDING * 2))

      else if not visible and @nameLbl?
        RackObject.INFO_GFX.remove(@nameLbl)
        @nameLbl = null


    showSpaces: (min_height) ->
      child.fadeOutMetrics() for child in @children

      @availableSpaces = []
      u_height         = @uHeight
      u_px_height      = RackObject.U_PX_HEIGHT
      centre_x         = @x + (@width / 2)
      space_left       = @x + Rack.SPACE_PADDING
      space_right      = @x + @width - Rack.SPACE_PADDING
      y                = @y + Rack.CHASSIS_OFFSET_Y
      count            = 0
      while count < u_height

        space_u_height = 0
        # scan unoccupied region
        while not @uOccupied[count]? and count < u_height
          ++space_u_height
          ++count

        delta = space_u_height - min_height
        if delta >= 0

          # calculate available spaces for the given height
          count2 = 0
          while count2 <= delta
            @availableSpaces.push(
              u      : count + count2,
              left   : space_left
              right  : space_right
              top    : y + ((u_height - count + count2) * u_px_height)
              bottom : y + ((u_height - count + count2) * u_px_height) + (u_px_height * min_height)
            )
            ++count2

          # add highlight
          highlight = RackObject.INFO_GFX.addRect(
            x      : @x + Rack.SPACE_PADDING
            y      : y + (u_height - count) * u_px_height
            width  : @width - (Rack.SPACE_PADDING * 2)
            height : u_px_height * space_u_height
            fill   : Rack.SPACE_FILL
            alpha  : 0
          )
          RackObject.INFO_GFX.animate(highlight, { alpha: Rack.SPACE_ALPHA }, Rack.SPACE_FADE_DURATION, Easing.Cubic.easeOut)
          @highlights.push(highlight)

        ++count


    hideSpaces: ->
      RackObject.INFO_GFX.animate(@highlights[0], { alpha: 0 }, Rack.SPACE_FADE_DURATION, Easing.Cubic.easeOut, @evSpaceHidden) if @highlights.length > 0
      @highlights = []
      @fadeInMetrics()


    deselect: ->
      if @selected
        @selected = false
        if child.selected then child.deselect() for child in @children


    fadeOutMetrics: ->
      child.fadeOutMetrics() for child in @children


    fadeInMetrics: ->
      child.fadeInMetrics() for child in @children


    evSpaceHidden: (id) =>
      RackObject.INFO_GFX.remove(id)


    getNearestSpace: (x, y) ->
      min_dist = Number.MAX_VALUE
      for space in @availableSpaces
        if Math.abs(space.left - x) < Math.abs(space.right - x)
          dist_x = space.left - x
        else
          dist_x = space.right - x
        if Math.abs(space.top - y) < Math.abs(space.bottom - y)
          dist_y = space.top - y
        else
          dist_y = space.bottom - y
        dist = Math.pow(dist_x, 2) + Math.pow(dist_y, 2)

        if dist < min_dist
          nearest    = space
          min_dist   = dist
          space.dist = dist

      nearest


    getSlot: (x, y) ->
      x -= @x
      y -= @y + Rack.CHASSIS_OFFSET_Y

      row = @uHeight - Math.floor(y / RackObject.U_PX_HEIGHT) - 1

      if row >= 0 and row < @uHeight
        device = @uOccupied[row]
        return { row: row, column: 0, device: device }

      return null
