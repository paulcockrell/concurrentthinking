define ['irv/view/RackObject', 'irv/util/AssetManager', 'common/util/Events', 'irv/view/Metric', 'irv/view/Highlight', 'irv/view/Breacher', 'irv/ViewModel'], (RackObject, AssetManager, Events, Metric, Highlight, Breacher, ViewModel) ->

  class Machine extends RackObject


    constructor: (def, parent) ->
      super(def, 'devices', parent)
      
      @images       = {}
      @images.front = AssetManager.CACHE[RackObject.IMAGE_PATH + def.template.images.front] if def.template.images.front?
      @images.rear  = AssetManager.CACHE[RackObject.IMAGE_PATH + def.template.images.rear] if def.template.images.rear?

      # swap images if rear mounted
      if not @frontFacing
        tmp           = @images.front
        @images.front = @images.rear
        @images.rear  = tmp

      @images.both = @images.front

      # rotate images if necessary
      slot_ratio = @parent.slotWidth / @parent.slotHeight

      # find the orientation which best fits the slot dimensions and rotate images if necessary
      if @images.front?
        natural_ratio = (@images.front.width / @template.width) / (@images.front.height / @template.height)
        rotated_ratio = (@images.front.height / @template.height) / (@images.front.width / @template.width)
        @images.front = @rotateImage(@images.front) if  Math.abs(slot_ratio - rotated_ratio) < Math.abs(slot_ratio - natural_ratio)
        dim_image     = @images.front

      if @images.rear?
        natural_ratio = (@images.rear.width / @template.width) / (@images.rear.height / @template.height)
        rotated_ratio = (@images.rear.height / @template.height) / (@images.rear.width / @template.width)
        @images.rear  = @rotateImage(@images.rear) if  Math.abs(slot_ratio - rotated_ratio) < Math.abs(slot_ratio - natural_ratio)
        dim_image     = @images.rear

      @x      = 0
      @y      = 0
      @width  = if dim_image? then dim_image.width else @parent.slotWidth
      @height = if dim_image? then dim_image.height else @parent.slotHeight

      @pluggable = @parent.complex
      @visible   = false
      @row       = def.row
      @column    = def.column
      @selected  = false
      @assets    = []

      @metric = new Metric(@group, @id, RackObject.INFO_GFX, @x, @y, @width, @height, RackObject.MODEL)
      
      @subscriptions.push(RackObject.MODEL.metricLevel.subscribe(@setMetricVisibility))
      @subscriptions.push(RackObject.MODEL.breaches.subscribe(@setBreaching))


    destroy: ->
      @breach.destroy() if @breach?
      @highlight.destroy() if @highlight?
      @metric.destroy()
      super()


    rotateImage: (img) ->
      cvs        = document.createElement('canvas')
      cvs.width  = img.height
      cvs.height = img.width

      ctx = cvs.getContext('2d')
      if @template.rotateClockwise
        ctx.rotate(Math.PI / 2)
        ctx.drawImage(img, 0, -img.height)
      else
        ctx.rotate(-Math.PI / 2)
        ctx.drawImage(img, -img.width, 0)
      return cvs


    setCoords: (@x, @y) ->
      @y -= @height
      @metric.setCoords(@x, @y)


    select: ->
      unless @highlight?
        if @breaching
          offset     = Breacher.STROKE_WIDTH
          double_off = offset * 2
          @highlight = new Highlight(Highlight.MODE_SELECT, @x + offset, @y + offset, @width - double_off, @height - double_off, RackObject.ALERT_GFX) 
        else
          @highlight = new Highlight(Highlight.MODE_SELECT, @x, @y, @width, @height, RackObject.ALERT_GFX) 


    deselect: ->
      if @highlight?
        @highlight.destroy()
        delete @highlight


    draw: =>
      # clear
      RackObject.RACK_GFX.remove(asset) for asset in @assets
      @assets = []

      if @breach?
        @breach.destroy()
        @breach = null

      face = RackObject.MODEL.face()
      face = @bothView if face is 'both'

      @img = @images[face]
      @visible = @img? or not @pluggable or (face is 'front' and @frontFacing) or (face is 'rear' and not @frontFacing)
      @setMetricVisibility()
      if @img?
        @assets.push(RackObject.RACK_GFX.addImg({ img: @img, x: @x, y: @y, alpha: if @included then 1 else RackObject.EXCLUDED_ALPHA }))
        # add a fade if in metric view mode
        @assets.push(RackObject.RACK_GFX.addRect({ fx: 'source-atop', x: @x, y: @y, width: @width, height: @height, fill: RackObject.METRIC_FADE_FILL, alpha: RackObject.METRIC_FADE_ALPHA })) if RackObject.MODEL.viewMode() is ViewModel.VIEW_MODE_METRICS

      @breach = new Breacher(@group, @id, RackObject.ALERT_GFX, @x, @y, @width, @height, RackObject.MODEL) if @breaching


    fadeOutMetric: ->
      #@metric.fadeOut()


    fadeInMetric: ->
      #@metric.fadeIn()


    showMetric: ->
      metric_level     = RackObject.MODEL.metricLevel()
      face             = RackObject.MODEL.face()
      face             = @bothView if face is 'both'
      view_mode        = RackObject.MODEL.viewMode()
      active_selection = RackObject.MODEL.activeSelection()
      selected         = RackObject.MODEL.selectedDevices()
      active_filter    = RackObject.MODEL.activeFilter()
      filtered         = RackObject.MODEL.filteredDevices()

      return metric_level is @group and ((not @frontFacing and face is 'rear') or (@frontFacing and face is 'front')) and view_mode isnt ViewModel.VIEW_MODE_IMAGES and (not active_selection or selected[@group][@id]) and (not active_filter or filtered[@group][@id])


    setMetricVisibility: =>
      @metric.setActive(@showMetric())


    setBreaching: (breaches) =>
      if breaches[@group]? and breaches[@group][@id]
        unless @breaching
          @breaching = true
          @breach    = new Breacher(@group, @id, RackObject.ALERT_GFX, @x, @y, @width, @height, RackObject.MODEL)
      else
        @breaching = false
        if @breach?
          @breach.destroy()
          @breach = null

