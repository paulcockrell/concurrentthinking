define ['common/gfx/Easing'], (Easing) ->

  class Breacher

    @STROKE_WIDTH  : 5
    @STROKE        : '#ff0000'
    @ALPHA_MAX     : 1
    @ALPHA_MIN     : .5
    @ANIM_DURATION : 900


    constructor: (@group, @id, @gfx, x, y, width, height, @model) ->
      offset = Breacher.STROKE_WIDTH / 2

      @asset = @gfx.addRect(
        x           : x + offset
        y           : y + offset
        width       : width - Breacher.STROKE_WIDTH
        height      : height - Breacher.STROKE_WIDTH
        alpha       : Breacher.ALPHA_MIN
        stroke      : Breacher.STROKE
        strokeWidth : Breacher.STROKE_WIDTH)

      # delay animation start by a random amount to desynchronise breachers (looks cooler)
      @delay = setTimeout(@fadeToMax, Math.ceil(Math.random() * Breacher.ANIM_DURATION * 2))
      
      breaches              = @model.breachZones()
      breaches[@group][@id] = { x: x, y: y, width: width, height: height }
      @model.breachZones(breaches)


    fadeToMax: =>
      @gfx.animate(@asset, { alpha: Breacher.ALPHA_MAX }, Breacher.ANIM_DURATION, Easing.Cubic.easeOut, @fadeToMin)


    fadeToMin: =>
      @gfx.animate(@asset, { alpha: Breacher.ALPHA_MIN }, Breacher.ANIM_DURATION, Easing.Cubic.easeOut, @fadeToMax)


    destroy: ->
      @gfx.remove(@asset)
      clearTimeout(@delay)
      breaches = @model.breachZones()
      delete breaches[@group][@id]
      @model.breachZones(breaches)
      
