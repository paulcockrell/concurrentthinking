define ['common/gfx/Easing'], (Easing) ->

  class Highlight

    @MODE_SELECT: 'select'
    @MODE_DRAG: 'drag'

    @SELECTED_FILL: '#ffffff'
    @SELECTED_ANIM_DURATION: 1000
    @SELECTED_MAX_ALPHA: .7
    @SELECTED_MIN_ALPHA: .3
    @DRAGGED_FILL: '#ffffff'
    @DRAGGED_ANIM_DURATION: 1000
    @DRAGGED_MAX_ALPHA: .7
    @DRAGGED_MIN_ALPHA: .3


    constructor: (mode, @x, @y, @width, @height, @gfx) ->
      switch mode
        when Highlight.MODE_SELECT
          @rect = @gfx.addRect({ fill: Highlight.SELECTED_FILL, x: @x, y: @y, width: @width, height: @height, alpha: Highlight.SELECTED_MAX_ALPHA })
          @evSelectFadedIn()
        when Highlight.MODE_DRAG
          @rect = @gfx.addRect({ fill: Highlight.DRAGGED_FILL, x: @x, y: @y, width: @width, height: @height, alpha: Highlight.DRAGGED_MAX_ALPHA })
          @evDragFadedIn()


    evSelectFadedOut: =>
      @gfx.animate(@rect, { alpha: Highlight.SELECTED_MAX_ALPHA }, Highlight.SELECTED_ANIM_DURATION, Easing.Quint.easeOut, @evSelectFadedIn)


    evSelectFadedIn: =>
      @gfx.animate(@rect, { alpha: Highlight.SELECTED_MIN_ALPHA }, Highlight.SELECTED_ANIM_DURATION, Easing.Quint.easeIn, @evSelectFadedOut)


    evDragFadedOut: =>
      @gfx.animate(@rect, { alpha: Highlight.DRAGGED_MAX_ALPHA }, Highlight.DRAGGED_ANIM_DURATION, Easing.Quint.easeOut, @evDragFadedIn)


    evDragFadedIn: =>
      @gfx.animate(@rect, { alpha: Highlight.DRAGGED_MIN_ALPHA }, Highlight.DRAGGED_ANIM_DURATION, Easing.Quint.easeIn, @evDragFadedOut)


    destroy: ->
      @gfx.remove(@rect)

