define [], () ->

  class Tooltip

    constructor: () ->
      @connectToolTip()

    connectToolTip : ->
      tooltip_els = $$('.toolTip')

      return if tooltip_els.length <= 0
      tip = new Tips(tooltip_els,
        onShow: (toolTip) ->
          toolTip.tween('opacity', 1)
        
        onHide: (toolTip) ->
          toolTip.tween('opacity', 0)
      )
