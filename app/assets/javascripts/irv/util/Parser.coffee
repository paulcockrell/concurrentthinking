define ['common/util/Util'], (Util) ->

  # takes the various JSON structures, reformats if necessary and populates the view model
  class Parser

    @OFFLINE_METRIC_VARIANCE : .2
    @OFFLINE                 : true
    @YAML_BLANK_STRING       : '""'


    constructor: (@model) ->
      # !! dummy thresholds
      @thresholds = {}


    parseRackDefs: (rack_defs, filter) ->
      console.log filter
      apply_filter = false
      for id of filter
        apply_filter = true
        break

      # format data and create lookup table based on rack/chassis/device id
      groups        = @model.groups()
      device_lookup = {}
      device_lookup[group] = {} for group in groups
      
      rack_defs = rack_defs.Racks.Rack
      rack_defs = [rack_defs] unless rack_defs instanceof Array

      # colate list of assets to preload, use an object to negate duplication
      assets = {}
      count  = 0
      len    = rack_defs.length
      while count < len
        rack = rack_defs[count]

        if apply_filter and not filter[rack.id]
          --len
          rack_defs.splice(count, 1)
          continue

        rack.uHeight = Number(rack.uHeight)
        device_lookup.racks[rack.id] = rack

        rack.Chassis = [] unless rack.Chassis?
        rack.chassis = rack.Chassis
        rack.chassis = [rack.chassis] unless rack.chassis instanceof Array
        delete rack.Chassis

        count2 = 0
        len2   = rack.chassis.length
        while count2 < len2
          chassis = rack.chassis[count2]
          # !! ignore zero U devices until a later version 
          unless chassis.type is 'Chassis'
            rack.chassis.splice(count2, 1)
            --len2
            continue
          
          # the api supplies all numeric values as strings
          chassis.rows   = Number(chassis.rows)
          chassis.slots  = Number(chassis.slots)
          chassis.cols   = Number(chassis.cols)
          chassis.uStart = Number(chassis.uStart)
          chassis.uEnd   = Number(chassis.uEnd)

          device_lookup.chassis[chassis.id] = chassis

          # offset uStart to be zero indexed
          --chassis.uStart

          # add any omitted template data
          if chassis.data?
            template = @parseChassisYAML(chassis, { rows: 1, columns: 1, images: {}, height: 1, depth: 'f', padding: { left: 0, right: 0, top: 0, bottom: 0 } })
          else
            template = { rows: 1, columns: 1, images: {}, height: 1, depth: 'f', padding: { left: 0, right: 0, top: 0, bottom: 0 } }

          template.height  = chassis.uEnd - chassis.uStart if template.height is 1 and chassis.uStart? and chassis.uEnd?
          chassis.template = template
          #chassis.yaml = template_yaml

          unit = if chassis.template.images? then chassis.template.images.unit else null

          # delete any images with a zero length string, otherwise add to asset list
          for image of chassis.template.images
            if chassis.template.images[image].length is 0
              delete chassis.template.images[image]
            else
              assets[chassis.template.images[image]] = true

          facing = chassis.facing

          unless chassis.Slots?
            ++count2
            continue
          # a chassis with a single machine is described as an object, whereas blade centres
          # are described as an array of object. Make consistent so all are an array of objects
          chassis.Slots = [chassis.Slots] unless chassis.Slots instanceof Array

          count3 = 0
          len3   = chassis.Slots.length
          while(count3 < len3)
            slot    = chassis.Slots[count3]
            machine = slot.Machine

            # make column naming consistant
            slot.column = Number(slot.col)
            slot.row    = Number(slot.row)

            # zero index column and row
            --slot.column
            --slot.row

            if machine? and machine.id?
              device_lookup.devices[machine.id] = machine
              
              machine.column = slot.column
              machine.row    = slot.row

              # carry over parent facing value
              machine.facing = facing

              # create machine template
              machine.template = { images: {}, width: 1, height: 1, rotateClockwise: true }
              machine.template.images = { front: unit } if unit?
            ++count3
            #else
            #  chassis.machines.splice(count, 1)
            #  --len2
          ++count2
        ++count
        
      # turn asset object into array
      asset_list = []
      asset_list.push(asset) for asset of assets

      { assetList: asset_list, racks: rack_defs, deviceLookup: device_lookup }


    parseMetrics: (metrics) ->
      metrics = { name: '', values: {} } unless metrics?

      metrics.metricId = metrics.name
      delete metrics.name

      min = Number.MAX_VALUE
      max = -Number.MAX_VALUE

      device_lookup     = @model.deviceLookup()
      groups            = @model.groups()
      metrics.breachers = {}
      metrics.breachers[group] = {} for group in groups

      # turn array into an object indexed by id for fast access
      if metrics.values?
        metrics.values.devices = {} unless metrics.values.devices?
        metrics.values.chassis = {} unless metrics.values.chassis?
        metrics.values.racks   = {} unless metrics.values.racks?
        values_obj = {}
        for group of metrics.values
          set = {}
          for metric in metrics.values[group]

            # ignore unrecognised metrics, may be present due to zero U devices
            continue unless device_lookup[group][metric.id]?

            # !! inject dummy breach data
            #if Math.random() < .05
            #  metrics.breachers                   = {} unless metrics.breachers?
            #  metrics.breachers[group]            = {} unless metrics.breachers[group]?
            #  metrics.breachers[group][metric.id] = true
            metric.value = Number(metric.value)

            # !! offline jazz
            if Parser.OFFLINE
              variance       = metric.value * Parser.OFFLINE_METRIC_VARIANCE
              set[metric.id] = Util.formatValue(metric.value + (Math.random() * variance * 2) - variance)
            else
              set[metric.id] = Util.formatValue(metric.value)

            metrics.breachers[group][metric.id] = metric.breaching

          values_obj[group] = set

        metrics.values = values_obj
      else
        metrics.values = { devices: {}, chassis: {}, racks: {} }

      return metrics


    parseMetricTemplates: (metric_templates) ->
      metric_obj = {}
      metric_obj[metric.id] = metric for metric in metric_templates
      metric_obj


    parseThresholds: (thresholds) ->
      tholds_by_metric = {}
      tholds_by_id     = {}

      for thold of thresholds
        if thresholds.hasOwnProperty(thold)
          raw_thold = thresholds[thold]
          metric    = raw_thold.metric
          thold_obj = { id: raw_thold.id, name: raw_thold.name, colours: [], values: [] }

          if raw_thold.breach_value?
            thold_obj.colours.push('#ff0000')
            thold_obj.values.push(Number(raw_thold.breach_value))
          else
            ranges = raw_thold.ranges
            ranges = Util.sortByProperty(ranges, 'upper_bound', true)
            for range in ranges
              thold_obj.colours.push(range.colour)
              thold_obj.values.push(range.upper_bound)

          tholds_by_metric[metric] = [] unless tholds_by_metric[metric]?
          tholds_by_metric[metric].push(thold_obj)

          tholds_by_id[thold_obj.id] = thold_obj

      { byMetric: tholds_by_metric, byId: tholds_by_id }


    parseYAML: (yaml_str) ->
      parts  = yaml_str.split('\n')
      parsed = {}
      @parseYAMLElement(parts, parsed) while parts.length > 0
      return parsed


    parseYAMLElement: (list, parent_obj, indent = 0) ->
      while(list.length > 0)
        el          = list[0]
        first_delim = el.indexOf(':')
        last_delim  = el.lastIndexOf(':')

        if first_delim is last_delim
          list.splice(0, 1)
          return

        if first_delim < indent
          return

        list.splice(0, 1)

        key   = el.substring(first_delim, last_delim)
        value = el.substr(el.lastIndexOf(':') + 1).replace(/(^\s*)|(\s*$)/g, '')

        if first_delim isnt last_delim
          if value is ''
            parent_obj[key] = {}
            @parseYAMLElement(list, parent_obj[key], first_delim + 1)
          else
            if isNaN(Number(value))
              if value is Parser.YAML_BLANK_STRING
                parent_obj[key] = ''
              else
                parent_obj[key] = value
            else
              parent_obj[key] = Number(value)



    parseChassisYAML: (chassis, default_template) ->
      template       = default_template
      template_yaml  = @parseYAML(chassis.data)

      #template_yaml = template_yaml[':device'];
      if(template_yaml[':metadata']?)
        template.rows    = chassis.rows ? template_yaml[':rows'] ? template_yaml[':metadata'][':rows'] ? 1
        template.columns = chassis.cols ? template_yaml[':cols'] ? template_yaml[':metadata'][':cols'] ? template_yaml[':slots_per_row'] ? template_yaml[':metadata'][':slots_per_row'] ? 1
        template.height  = template_yaml[':height'] ? 1
        template.depth   = template_yaml[':depth'] ? 'f'

        template.images  = {}
        if template_yaml[':metadata'][':images']?
          template.images[image.substr(1)] = template_yaml[':metadata'][':images'][image] for image of template_yaml[':metadata'][':images']

        template.padding = {}
        if template_yaml[':metadata'][':padding']?
          template.padding.left   = template_yaml[':metadata'][':padding'][':left'] ? 0
          template.padding.right  = template_yaml[':metadata'][':padding'][':right'] ? 0
          template.padding.top    = template_yaml[':metadata'][':padding'][':top'] ? 0
          template.padding.bottom = template_yaml[':metadata'][':padding'][':bottom'] ? 0

      if template_yaml[':device']?
        template.deviceType   = template_yaml[':device'][':type']
        template.model        = template_yaml[':device'][':model']
        template.manufacturer = template_yaml[':device'][':manufacturer']

      template
