# sets static properties of all classes with values defined in the config
# this is a static class/singleton

define ['irv/view/UpdateMsg', 'common/util/PresetManager', 'irv/util/Parser', 'common/util/Util', 'irv/util/AssetManager', 'irv/view/RackSpace', 'irv/view/RackObject', 'irv/view/Rack', 'irv/view/Chassis', 'irv/view/Machine', 'irv/view/Highlight', 'irv/view/Metric', 'common/gfx/Primitives', 'irv/view/Chart', 'irv/view/RackHint', 'irv/view/ThumbHint', 'irv/view/ContextMenu', 'irv/view/Breacher', 'common/widgets/ThumbNav', 'common/widgets/FilterBar', 'irv/ViewModel', 'common/util/StaticGroupManager'], (UpdateMsg, PresetManager, Parser, Util, AssetManager, RackSpace, RackObject, Rack, Chassis, Machine, Highlight, Metric, Primitives, Chart, RackHint, ThumbHint, ContextMenu, Breacher, ThumbNav, FilterBar, ViewModel, StaticGroupManager) ->

  class Configurator

    @setup: (IRVController, config) ->
      AssetManager.NUM_CONCURRENT_LOADS = config.ASSETMANAGER.numConcurrentLoads
   
      # parse colours into decimal ints
      obj.col = Configurator.parseColourString(obj.col) for obj in config.VIEWMODEL.colourScale
      Util.sortByProperty(config.VIEWMODEL.colourScale, 'pos', true)
   
      view_model_config                  = config.VIEWMODEL
      ViewModel.COLOUR_SCALE             = view_model_config.colourScale
      ViewModel.INIT_VIEW_MODE           = view_model_config.startUp.viewMode
      ViewModel.INIT_FACE                = view_model_config.startUp.face
      ViewModel.INIT_SCALE_BARS          = view_model_config.startUp.scaleBars
      ViewModel.INIT_SHOW_CHART          = view_model_config.startUp.showChart
      ViewModel.INIT_METRIC_LEVEL        = view_model_config.startUp.metricLevel
      ViewModel.INIT_GRAPH_ORDER         = view_model_config.startUp.graphOrder
      ViewModel.NO_THRESHOLDS_CAPTION    = view_model_config.noThresholdsCaption
      ViewModel.SELECT_THRESHOLD_CAPTION = view_model_config.selectThresholdCaption

      util_config     = config.IRVUTIL
      Util.SIG_FIG = util_config.sigFig
   
      controller_config                         = config.CONTROLLER
      IRVController.RACK_HINT_HOVER_DELAY       = controller_config.rackHintHoverDelay
      IRVController.THUMB_HINT_HOVER_DELAY      = controller_config.thumbHintHoverDelay
      IRVController.DOUBLE_CLICK_TIMEOUT        = controller_config.doubleClickTimeout
      IRVController.DRAG_ACTIVATION_DIST        = controller_config.dragActivationDist
      IRVController.METRIC_POLL_RATE            = controller_config.metricPollRate
      IRVController.LIVE                        = controller_config.resources.live
      IRVController.LIVE_RESOURCES              = controller_config.resources.liveResources
      IRVController.OFFLINE_RESOURCES           = controller_config.resources.offlineResources
      IRVController.NUM_RESOURCES               = controller_config.resources.numResources
      IRVController.DEFAULT_METRIC_ID           = controller_config.startUp.defaultMetricId
      IRVController.RACK_PAGE_HEIGHT_PROPORTION = controller_config.rackPageHeightProportion
      IRVController.STEP_ZOOM_AMOUNT            = controller_config.stepZoomAmount
      IRVController.ZOOM_KEY                    = controller_config.zoomKey
      IRVController.THUMB_WIDTH                 = config.THUMBNAV.width
      IRVController.THUMB_HEIGHT                = config.THUMBNAV.height
      IRVController.RESOURCE_LOAD_CAPTION       = controller_config.resourceLoadCaption
      IRVController.PRIMARY_IMAGE_PATH          = controller_config.resources.primaryImagePath
      IRVController.SECONDARY_IMAGE_PATH        = controller_config.resources.secondaryImagePath
      IRVController.SCREENSHOT_FILENAME         = controller_config.screenShotFilename
      IRVController.EXPORT_FILENAME             = controller_config.export.filename
      IRVController.EXPORT_HEADER               = controller_config.export.header
      IRVController.EXPORT_RECORD               = controller_config.export.record
      IRVController.EXPORT_MESSAGE              = controller_config.export.message
      IRVController.THUMB_HINT_OFFSET_X         = controller_config.thumbHintOffsetX
      IRVController.THUMB_HINT_OFFSET_Y         = controller_config.thumbHintOffsetY
      IRVController.BREACH_POLL_RATE            = controller_config.breachPollRate
   
      parser_config                  = config.PARSER
      Parser.OFFLINE_METRIC_VARIANCE = parser_config.offlineMetricVariance
      Parser.OFFLINE                 = !controller_config.resources.live
   
      preset_config                       = config.PRESETMANAGER
      PresetManager.PATH                  = preset_config.path
      PresetManager.GET                   = preset_config.get
      PresetManager.NEW                   = preset_config.new
      PresetManager.UPDATE                = preset_config.update
      PresetManager.VALUES                = preset_config.values
      PresetManager.ERR_CAPTION           = preset_config.errors.caption
      PresetManager.ERR_INVALID_NAME      = preset_config.errors.invalidName
      PresetManager.ERR_WHITE_NAME        = preset_config.errors.whiteName
      PresetManager.ERR_DUPLICATE_NAME    = preset_config.errors.duplicateName
      PresetManager.WARN_THRESHOLD        = preset_config.warnThreshold
      PresetManager.MESSAGE_HOLD_DURATION = preset_config.msgHoldDuration
      PresetManager.MODEL_DEPENDENCIES    = preset_config.modelDependencies
      PresetManager.DOM_DEPENDENCIES      = preset_config.domDependencies
      PresetManager.MSG_CONFIRM_UPDATE    = preset_config.msgConfirmUpdate

      group_config = config.STATICGROUPMANAGER
      console.log group_config
      StaticGroupManager.PATH                  = group_config.path
      StaticGroupManager.GET                   = group_config.get
      StaticGroupManager.NEW                   = group_config.new
      StaticGroupManager.UPDATE                = group_config.update
      StaticGroupManager.ERR_CAPTION           = group_config.errors.caption
      StaticGroupManager.ERR_INAVLID_NAME      = group_config.errors.invalidName
      StaticGroupManager.ERR_WHITE_NAME        = group_config.errors.whiteName
      StaticGroupManager.ERR_DUPLICATE_NAME    = group_config.errors.duplicateName
      StaticGroupManager.MODEL_DEPENDENCIES    = group_config.modelDependencies
      StaticGroupManager.DOM_DEPENDENCIES      = group_config.domDependencies
      StaticGroupManager.MSG_CONFIRM_UPDATE    = group_config.msgConfirmUpdate
   
      thumb_nav_config            = config.THUMBNAV
      ThumbNav.MASK_FILL          = thumb_nav_config.maskFill
      ThumbNav.MASK_FILL_ALPHA    = thumb_nav_config.maskFillAlpha
      ThumbNav.SHADE_FILL         = thumb_nav_config.shadeFill
      ThumbNav.SHADE_ALPHA        = thumb_nav_config.shadeAlpha
      ThumbNav.BREACH_FILL        = thumb_nav_config.breachFill
      ThumbNav.BREACH_ALPHA       = thumb_nav_config.breachAlpha
      ThumbNav.MODEL_DEPENDENCIES = thumb_nav_config.modelDependencies

      update_msg_config = config.UPDATEMSG
      UpdateMsg.MESSAGE = update_msg_config.message
   
      rackspace_config                         = config.RACKSPACE
      RackSpace.PADDING                        = rackspace_config.padding
      RackSpace.RACK_H_SPACING                 = rackspace_config.rackHSpacing
      RackSpace.RACK_V_SPACING                 = rackspace_config.rackVSpacing
      RackSpace.U_LBL_SCALE_CUTOFF             = rackspace_config.uLblScaleCutoff
      RackSpace.NAME_LBL_SCALE_CUTOFF          = rackspace_config.nameLblScaleCutoff
      RackSpace.ZOOM_DURATION                  = rackspace_config.zoomDuration
      RackSpace.FPS                            = rackspace_config.fps
      RackSpace.DRAG_FADE_FILL                 = rackspace_config.drag.fadeFill
      RackSpace.DRAG_FADE_ALPHA                = rackspace_config.drag.fadeAlpha
      RackSpace.DRAG_SNAP_RANGE                = rackspace_config.drag.snapRange
      RackSpace.INFO_FADE_DURATION             = rackspace_config.infoFadeDuration
      RackSpace.FLIP_DURATION                  = rackspace_config.flipDuration
      RackSpace.FLIP_DELAY                     = rackspace_config.flipDelay
      RackSpace.METRIC_FADE_FILL               = rackspace_config.metricFadeFill
      RackSpace.METRIC_FADE_ALPHA              = rackspace_config.metricFadeAlpha
      RackSpace.SELECT_BOX_STROKE              = rackspace_config.selectBox.stroke
      RackSpace.SELECT_BOX_STROKE_WIDTH        = rackspace_config.selectBox.strokeWidth
      RackSpace.SELECT_BOX_ALPHA               = rackspace_config.selectBox.alpha
      RackSpace.ADDITIONAL_ROW_TOLERANCE       = rackspace_config.additionalRowTolerance
      RackSpace.LAYOUT_UPDATE_DELAY            = rackspace_config.layoutUpdateDelay
      RackSpace.CHART_SELECTION_COUNT_FILL     = rackspace_config.selectionCount.fill
      RackSpace.CHART_SELECTION_COUNT_FONT     = rackspace_config.selectionCount.font
      RackSpace.CHART_SELECTION_COUNT_BG_FILL  = rackspace_config.selectionCount.bgFill
      RackSpace.CHART_SELECTION_COUNT_BG_ALPHA = rackspace_config.selectionCount.bgAlpha
      RackSpace.CHART_SELECTION_COUNT_CAPTION  = rackspace_config.selectionCount.caption
      RackSpace.CHART_SELECTION_COUNT_OFFSET_X = rackspace_config.selectionCount.offsetX
      RackSpace.CHART_SELECTION_COUNT_OFFSET_Y = rackspace_config.selectionCount.offsetY
      RackSpace.CANVAS_MAX_DIMENSION           = rackspace_config.canvasMaxDimension
   
      rack_object_config           = config.RACKSPACE.RACKOBJECT
      RackObject.BLANK_FILL        = rack_object_config.blankFill
      RackObject.METRIC_FADE_FILL  = rack_object_config.metricFadeFill
      RackObject.METRIC_FADE_ALPHA = rack_object_config.metricFadeAlpha
      RackObject.IMAGE_PATH        = controller_config.resources.primaryImagePath
      RackObject.EXCLUDED_ALPHA    = rack_object_config.excludedAlpha
      RackObject.U_PX_HEIGHT       = rack_object_config.uPxHeight
   
      rack_config              = config.RACKSPACE.RACKOBJECT.RACK
      Rack.IMG_FRONT_TOP       = rack_config.images.front.top
      Rack.IMG_FRONT_BTM       = rack_config.images.front.bottom
      Rack.IMG_FRONT_REPEAT    = rack_config.images.front.repeat
      Rack.IMG_REAR_TOP        = rack_config.images.rear.top
      Rack.IMG_REAR_BTM        = rack_config.images.rear.bottom
      Rack.IMG_REAR_REPEAT     = rack_config.images.rear.repeat
      Rack.IMG_REAR            = rack_config.imgRear
      Rack.U_LBL_OFFSET_X      = rack_config.uLbl.offsetX
      Rack.U_LBL_OFFSET_Y      = rack_config.uLbl.offsetY
      Rack.U_LBL_FONT          = rack_config.uLbl.font
      Rack.U_LBL_COLOUR        = rack_config.uLbl.colour
      Rack.U_LBL_ALIGN         = rack_config.uLbl.align
      Rack.NAME_LBL_OFFSET_X   = rack_config.nameLbl.offsetX
      Rack.NAME_LBL_OFFSET_Y   = rack_config.nameLbl.offsetY
      Rack.NAME_LBL_FONT       = rack_config.nameLbl.font
      Rack.NAME_LBL_COLOUR     = rack_config.nameLbl.colour
      Rack.NAME_LBL_ALIGN      = rack_config.nameLbl.align
      Rack.NAME_LBL_BG_FILL    = rack_config.nameLbl.bg.fill
      Rack.NAME_LBL_BG_PADDING = rack_config.nameLbl.bg.padding
      Rack.NAME_LBL_BG_ALPHA   = rack_config.nameLbl.bg.alpha
      Rack.NAME_LBL_SIZE       = rack_config.nameLbl.size
      Rack.NAME_LBL_MIN_SIZE   = rack_config.nameLbl.minSize
      Rack.CAPTION_FRONT       = rack_config.captionFront
      Rack.CAPTION_REAR        = rack_config.captionRear
      Rack.SPACE_ALPHA         = rack_config.space.alpha
      Rack.SPACE_FILL          = rack_config.space.fill
      Rack.SPACE_FADE_DURATION = rack_config.space.fadeDuration
      Rack.FADE_IN_METRIC_MODE = rack_config.fadeInMetricMode
   
      chassis_config        = config.RACKSPACE.RACKOBJECT.CHASSIS
      Chassis.DEFAULT_WIDTH = chassis_config.defaultWidth
      Chassis.U_PX_HEIGHT   = chassis_config.uPxHeight
      Chassis.UNKNOWN_FILL  = chassis_config.unknownFill
   
      highlight_config                 = config.RACKSPACE.HIGHLIGHT
      Highlight.SELECTED_FILL          = highlight_config.selected.fill
      Highlight.SELECTED_ANIM_DURATION = highlight_config.selected.animDuration
      Highlight.SELECTED_MAX_ALPHA     = highlight_config.selected.maxAlpha
      Highlight.SELECTED_MIN_ALPHA     = highlight_config.selected.minAlpha
      Highlight.DRAGGED_FILL           = highlight_config.dragged.fill
      Highlight.DRAGGED_ANIM_DURATION  = highlight_config.dragged.animDuration
      Highlight.DRAGGED_MAX_ALPHA      = highlight_config.dragged.maxAlpha
      Highlight.DRAGGED_MIN_ALPHA      = highlight_config.dragged.minAlpha
   
      breacher_config        = config.RACKSPACE.BREACHER
      Breacher.STROKE        = breacher_config.stroke
      Breacher.STROKE_WIDTH  = breacher_config.strokeWidth
      Breacher.ALPHA_MAX     = breacher_config.maxAlpha
      Breacher.ALPHA_MIN     = breacher_config.minAlpha
      Breacher.ANIM_DURATION = breacher_config.animDuration
      
      metric_config        = config.RACKSPACE.METRIC
      Metric.ALPHA         = metric_config.alpha
      Metric.FADE_DURATION = metric_config.fadeDuration
      Metric.ANIM_DURATION = metric_config.animDuration
      
      Primitives.Text.TRUNCATION_SUFFIX = config.RACKSPACE.PRIMITIVES.text.truncationSuffix
   
      chart_config                 = config.RACKSPACE.CHART
      Chart.DEPTH_3D               = chart_config.depth3D
      Chart.ANGLE                  = chart_config.angle
      Chart.MARGIN_TOP             = chart_config.margins.top
      Chart.MARGIN_BTM             = chart_config.margins.btm
      Chart.MARGIN_LEFT            = chart_config.margins.left
      Chart.MARGIN_RIGHT           = chart_config.margins.right
      Chart.GRAPH_TYPE             = chart_config.graph.type
      Chart.BG_FILL                = chart_config.bgFill
      Chart.BG_ALPHA               = chart_config.bgAlpha
      Chart.BORDER_COLOUR          = chart_config.borderColour
      Chart.BORDER_ALPHA           = chart_config.borderAlpha
      Chart.TEXT_COLOUR            = chart_config.text.colour
      Chart.TEXT_FONT              = chart_config.text.font
      Chart.TEXT_SIZE              = chart_config.text.size
      Chart.WIDTH                  = chart_config.width
      Chart.HEIGHT                 = chart_config.height
      Chart.GRAPH_LINE_ALPHA       = chart_config.graph.lineAlpha
      Chart.GRAPH_LINE_COLOUR      = chart_config.graph.lineColour
      Chart.GRAPH_LINE_THICKNESS   = chart_config.graph.lineThickness
      Chart.GRAPH_FILL_ALPHA       = chart_config.graph.fillAlpha
      Chart.BLN_BORDER_ALPHA       = chart_config.balloon.border.alpha
      Chart.BLN_BORDER_COLOUR      = chart_config.balloon.border.colour
      Chart.BLN_BORDER_THICKNESS   = chart_config.balloon.border.thickness
      Chart.BLN_H_PADDING          = chart_config.balloon.hPadding
      Chart.BLN_V_PADDING          = chart_config.balloon.vPadding
      Chart.BLN_CORNER_RADIUS      = chart_config.balloon.cornerRadius
      Chart.BLN_TEXT_COLOUR        = chart_config.balloon.text.colour
      Chart.BLN_TEXT_SIZE          = chart_config.balloon.text.size
      Chart.BLN_TEXT_ALIGN         = chart_config.balloon.text.align
      Chart.BLN_TEXT_SHADOW_COLOUR = chart_config.balloon.text.shadowColour
      Chart.BLN_CAPTION            = chart_config.balloon.caption
      Chart.TITLE_CAPTION          = chart_config.title.caption
      Chart.TITLE_SIZE             = chart_config.title.size
      Chart.TITLE_COLOUR           = chart_config.title.colour
      Chart.TITLE_ALPHA            = chart_config.title.alpha
      Chart.TITLE_BOLD             = chart_config.title.bold
      Chart.POINTER_OFFSET_X       = chart_config.pointerOffsetX
      Chart.POINTER_OFFSET_Y       = chart_config.pointerOffsetY
      Chart.THRESHOLD_FILL_ALPHA   = chart_config.thresholdFillAlpha
      Chart.THRESHOLD_LINE_ALPHA   = chart_config.thresholdLineAlpha
      Chart.X_AXIS_LABEL_ROTATION  = chart_config.graph.axis.x.lblRotation
      Chart.X_AXIS_GRID_COUNT      = chart_config.graph.axis.x.gridCount
   
      rack_hint_config         = config.RACKSPACE.HINT.RACKHINT
      RackHint.RACK_TEXT       = rack_hint_config.rackText
      RackHint.CHASSIS_TEXT    = rack_hint_config.chassisText
      RackHint.DEVICE_TEXT     = rack_hint_config.deviceText
      RackHint.MORE_INFO_DELAY = rack_hint_config.moreInfoDelay
   
      thumb_hint_config = config.RACKSPACE.HINT.THUMBHINT
      ThumbHint.CAPTION = thumb_hint_config.caption
   
      context_menu_config             = config.RACKSPACE.CONTEXTMENU
      ContextMenu.OPTIONS             = context_menu_config.options
      ContextMenu.LAYOUT              = context_menu_config.layout
      ContextMenu.VERBOSE             = context_menu_config.verbose
      ContextMenu.SPACER              = context_menu_config.spacer
      ContextMenu.URL_INTERNAL_PREFIX = context_menu_config.urlInternalPrefix
      ContextMenu.ASPECT_MAP          = context_menu_config.aspectMap
      ContextMenu.DEVICE_TYPE_URL_MAP = context_menu_config.deviceTypeURLMap
      ContextMenu.COMMAND_MODEL       = context_menu_config.commandModel
   
      filter_bar_config                  = config.FILTERBAR
      FilterBar.THICKNESS                = filter_bar_config.thickness
      FilterBar.LENGTH                   = filter_bar_config.length
      FilterBar.PADDING                  = filter_bar_config.padding
      FilterBar.MODEL_UPDATE_DELAY       = filter_bar_config.slider.updateDelay
      FilterBar.DRAG_TAB_FILL            = filter_bar_config.slider.fill
      FilterBar.DRAG_TAB_SHAPE           = filter_bar_config.slider.shape
      FilterBar.DRAG_TAB_STROKE          = filter_bar_config.slider.stroke
      FilterBar.DRAG_TAB_STROKE_WIDTH    = filter_bar_config.slider.strokeWidth
      FilterBar.CUTOFF_LINE_STROKE       = filter_bar_config.cutoffLine.stroke
      FilterBar.CUTOFF_LINE_STROKE_WIDTH = filter_bar_config.cutoffLine.strokeWidth
      FilterBar.CUTOFF_LINE_ALPHA        = filter_bar_config.cutoffLine.alpha
      FilterBar.DRAG_TAB_DISABLED_ALPHA  = filter_bar_config.slider.disabledAlpha
      FilterBar.INPUT_WIDTH              = filter_bar_config.input.width
      FilterBar.INPUT_SPACING            = filter_bar_config.input.spacing
      FilterBar.INPUT_UPDATE_DELAY       = filter_bar_config.input.updateDelay
      FilterBar.FONT                     = filter_bar_config.font
      FilterBar.FONT_SIZE                = filter_bar_config.fontSize
      FilterBar.FONT_FILL                = filter_bar_config.fontFill
      FilterBar.DRAG_BOX_STROKE          = filter_bar_config.dragBox.stroke
      FilterBar.DRAG_BOX_STROKE_WIDTH    = filter_bar_config.dragBox.strokeWidth
      FilterBar.DRAG_BOX_ALPHA           = filter_bar_config.dragBox.alpha
      FilterBar.MODEL_DEPENDENCIES       = filter_bar_config.modelDependencies
   
      switch filter_bar_config.defaultAlign
            when 'top'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_TOP
            when 'bottom'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_BOTTOM
            when 'left'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_LEFT
            when 'right'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_RIGHT
            else
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_BOTTOM
   
      config = null
   
   
   
    @parseColourString: (col_str) ->
      switch col_str.charAt(0)
        when '#'
          parseInt('0x' + col_str.substr(1))
        else
          parseInt(col_str)
   
