define ['dcpv/util/DCPVStaticGroups', 'dcpv/view/Hint', 'common/util/CrossAppSettings', 'dcpv/util/Configurator', 'dcpv/ViewModel', 'dcpv/view/DataCentre', 'dcpv/view/Chart', 'common/widgets/ThumbNav', 'common/widgets/FilterBar', 'common/util/Util', 'common/util/PresetManager', 'dcpv/util/AssetManager', 'common/util/Events', 'dcpv/util/Parser', 'dcpv/view/LHNav', 'common/util/ComboBox', 'irv/view/Tooltip'], (DCPVStaticGroups, Hint, CrossAppSettings, Configurator, ViewModel, DataCentre, Chart, ThumbNav, FilterBar, Util, PresetManager, AssetManager, Events, Parser, LHNav, ComboBox, Tooltip) ->
  
  class DCPVController

    # statics overwritten by config
    @CONFIG_URL          : '/plan_view/configuration'
    @DCPV_DEF_URL        : '/-/api/v1/data_centre/data_centre'
    @METRIC_DEF_URL      : '/meca/group_metrics/all_metric_metadata'
    @THRESHOLD_URL       : '/-/api/v1/irv/thresholds'
    @BREACH_URL          : '/-/api/v1/metrics/breaches'
    @BREACH_POLL_RATE    : 60000
    @ZOOM_KEY            : 90
    @SUPER_NUDGE_KEY     : 16
    @MULTI_SELECT_KEY    : 17
    @DELETE_KEY          : 46
    @METRIC_POLL_RATE    : 60000
    @METRIC_DATA_URL     : '/-/api/v1/data_centre/metrics/?[[group]][[metric_selection]]'
    @METRIC_GROUP        : 'group_ids%5b[[idx]]%5d=[[id]]'
    @METRIC_CHASSIS      : 'chassis_ids%5b[[idx]]%5d=[[id]]'
    @METRIC_SENSOR       : 'sensor_ids%5b[[idx]]%5d=[[id]]'
    @METRIC_GROUP_JOIN   : '&'
    @METRIC_SELECTION    : '&selected_metrics%5b[[selected_metric]]%5d[0]=mean&selected_metrics%5b[[selected_metric]]%5d[1]=num&selected_metrics%5b[[selected_metric]]%5d[2]=min&selected_metrics%5b[[selected_metric]]%5d[3]=max&selected_metrics%5b[[selected_metric]]%5d[4]=sum'
    @THUMB_WIDTH         : 170
    @THUMB_HEIGHT        : 170
    @LAYOUT_UPDATE_DELAY : 50
    @DATACENTRE_HEIGHT_PROPORTION : 0.70
    @DRAG_ACTIVATION_DIST : 8
    @DOUBLE_CLICK_TIMEOUT : 250
    @ALERT_UNSAVED        : 'You have unsaved changes to your data centre, discard these changes and continue?'
    @EXPORT_HEADER        : 'Metric Name,[[metric_name]]\nUnits,[[metric_units]]\nDevice Name,Metric Value'
    @EXPORT_RECORD        : '[[device_name]],[[value]]'
    @EXPORT_FILENAME      : '[[metric_name]]_[[day]]-[[month]]-[[year]]_[[hours]]-[[minutes]]-[[seconds]].csv'
    @EXPORT_IMAGE_URL     : '/-/api/v1/irv/racks/export_image'
    @EXPORT_MESSAGE       : 'Saving IRV image, please wait...'
    @SCREENSHOT_FILENAME  : 'irv_[[day]]-[[month]]-[[year]]_[[hours]]-[[minutes]]-[[seconds]].png'
    @HINT_DELAY           : 150
    @HINT_DATA_URL        : '/-/api/v1/irv/[[group]]/tooltip/[[device_id]]'


    constructor: ->
      document.DCPV = @
      @loadConfig()


    outputModel: ->
      console.log("**************** VIEW MODEL DUMP ****************")
      for i of @model
        try
          console.log(i, @model[i]()) if typeof @model[i] is 'function'
        catch e
          console.log("*** failed to fetch #{i} ***")

      console.log("*************************************************")


    loadConfig: =>
      req = $.get(DCPVController.CONFIG_URL + '?' + (new Date()).getTime()).done((data) => @evReceivedConfig(jQuery.parseJSON(data))).fail(@evLoadError)
    

    evMouseUpMetricSelect: (ev) =>
      console.log 'DCPVController.evMouseUpMetricSelect'


    evFocusMetricSelect: (ev) =>
      console.log 'DCPVController.evFocuMetricSelect'
      
      
    evLoadError: =>
      console.log 'DCPVController.evLoadError', arguments


    loadDef: ->
      console.log "In loadDef"
      $.get(DCPVController.DCPV_DEF_URL + '?' + (new Date()).getTime()).done((data) => @evReceivedDef(jQuery.parseJSON(data))).fail(() => @evReceivedDefFake())
      $.get(DCPVController.METRIC_DEF_URL + '?' + (new Date()).getTime()).done((data) => @evReceivedMetricDef(jQuery.parseJSON(data))).fail(() => @evReceivedMetricDefFake())
      $.get(DCPVController.THRESHOLD_URL + '?' + (new Date()).getTime()).done((data) => @evReceivedThresholds(jQuery.parseJSON(data))).fail(() => @evReceivedThresholdsFake())


    init: ->
      @dCEl        = document.getElementById('dcpv_container')
      @chartEl     = document.getElementById('graph_container')
      @thumbEl     = document.getElementById('thumb_container')
      @dCParentEl  = document.getElementById('dc_view')
      @filterBarEl = document.getElementById('filter_bar')
      @dataCentre = new DataCentre(@model.dataCentreDef()[0].planViewDef, @dCEl, @model)
      #@chart      = $('<div style="width:100px; height:100px" height="100" width="200">') 
      new Chart(@chartEl, @model)
      @thumb      = new ThumbNav(@thumbEl, DCPVController.THUMB_WIDTH, DCPVController.THUMB_HEIGHT, @model)
      @filterBar  = new FilterBar(@filterBarEl, @dCParentEl, @model)
      #XXX reinstate this when we have made the left hand menu
      #
      @nav        = null#new LHNav(@model)
      @presets    = new PresetManager(@model)
      @hint       = new Hint(document.body, @model)
      @groups     = new DCPVStaticGroups(@model)

      @scrollbarOffset      = Util.getScrollbarThickness()
      @clickAssigned        = true
      @keysPressed          = {}
      @viewModeChartVisible = ViewModel.DEFAULT_SHOW_CHART
      
      @connectMetricCombos()
      @setSubscriptions()
      @evSwitchMode()

      # Events.addEventListener(@dCEl, 'dataCentreZoomComplete', @evZoomComplete)
      # Events.addEventListener(@dCEl, 'dataCentreReset', @evReset)
      # Events.addEventListener(@dCEl, 'scroll', @evDCScroll)
      # Events.addEventListener(@dCEl, 'mousemove', @evDCMouseMove)
      # Events.addEventListener(@dCEl, 'mousedown', @evDCMouseDown)
      # Events.addEventListener(@dCEl, 'mouseup', @evDCMouseUp)
      # Events.addEventListener(@dCEl, 'mousewheel', @evDCMouseWheel)
      # Events.addEventListener(@dCEl, 'DOMMouseScroll', @evDCMouseWheel)
      # Events.addEventListener(@dCEl, 'contextmenu', @evDCRightClick)

      # Events.addEventListener(document.window, 'resize', @evResize)
      # Events.addEventListener(document, 'keyup', @evKeyUp)
      # Events.addEventListener(document, 'keydown', @evKeyDown)

      # Events.addEventListener(@thumbEl, 'mousedown', @evThumbMouseDown)
      # Events.addEventListener(@thumbEl, 'mouseup', @evThumbMouseUp)
      # Events.addEventListener(@thumbEl, 'mousewheel', @evThumbMouseWheel)
      # Events.addEventListener(@thumbEl, 'DOMMouseScroll', @evThumbMouseWheel)
      # Events.addEventListener(@thumbEl, 'dblclick', @evThumbDoubleClick)
      # Events.addEventListener(@thumbEl, 'mousemove', @evThumbMouseMove)

      # Events.addEventListener(@filterBarEl, 'filterBarSetAnchor', @evDropFilterBar)
      # Events.addEventListener(@filterBarEl, 'mousedown', @evFilterMouseDown)
      # Events.addEventListener(@filterBarEl, 'mouseup', @evFilterMouseUp)
      # Events.addEventListener(@filterBarEl, 'mouseout', @evFilterMouseOut)

      # Events.addEventListener(document, 'navZoomIn', @evNavZoomIn)
      # Events.addEventListener(document, 'navZoomOut', @evNavZoomOut)
      # Events.addEventListener(document, 'navResetZoom', @evNavResetZoom)
      # Events.addEventListener(document, 'navItemEdited', @evNavItemEdited)
      # Events.addEventListener(document, 'navAddItem', @evNavAddItem)
      # Events.addEventListener(document, 'navSave', @evNavSave)
      # Events.addEventListener(document, 'navSwitchToView', @evNavSwitchToView)
      # Events.addEventListener(document, 'navUpdateItem', @evNavUpdateItem)
      # Events.addEventListener(document, 'navResetFilters', @evNavResetFilters)
      # Events.addEventListener(document, 'navDeleteSelection', @evNavDeleteSelection)

      # Events.addEventListener($('irv_link'), 'click', @evExitToIRV)
      # Events.addEventListener($('print_link'), 'click', @evPrint)
      # Events.addEventListener($('export_link'), 'click', @evExport)
      # Events.addEventListener($('save_link'), 'click', @evGrabScreen)
      # Events.addEventListener(window, 'getHintInfo', @evGetHintInfo)

      # Events.addEventListener(@chartEl, 'mousedown', @evChartMouseDown)
      # Events.addEventListener(@chartEl, 'mouseup', @evChartMouseUp)

      # @tooltip = new Tooltip()
      @setLayout()
      # @loadBreaches()

      # since the metrics and breaches poll at the same (default) rate, put
      # the requests out of phase with each other
      # setTimeout(=>
      #   @breachTmr = setInterval(@loadBreaches, DCPVController.BREACH_POLL_RATE)
      # , DCPVController.BREACH_POLL_RATE / 2)


    loadBreaches: =>
      $.get(DCPVController.BREACH_URL + '?' + (new Date()).getTime()).done((data) => @evReceivedBreaches())


    setSubscriptions: ->
      @model.mode.subscribe(@evSwitchMode)
      @model.filters.subscribe(@applyFilter)
      @model.selectedMetric.subscribe(@evSwitchMetric)
      @model.showChart.subscribe(@setLayout)

      
    updateLayout: ->
      # throttle layout updates using a timeout
      clearTimeout(@layoutTmr)
      @layoutTmr = setTimeout(@setLayout, DCPVController.LAYOUT_UPDATE_DELAY)


    connectMetricCombos: ->
      ComboBox.connect_all('cbox')
      for id, cb of ComboBox.boxes
        cb.add_change_callback =>
          @model.selectedMetric cb.value


    setLayout: =>
      document.poo=@dcParentEl
      dc_height_proportion = if @model.showChart() then DCPVController.DATACENTRE_HEIGHT_PROPORTION else 1
      console.log "1"
      Util.setStyle(@dCParentEl, 'height', dc_height_proportion * 100 + '%')
      console.log "2"
      Util.setStyle(@chartEl, 'top', dc_height_proportion * 100 + '%')
      console.log "3"
      Util.setStyle(@chartEl, 'height', (1 - dc_height_proportion) * 100 + '%')

      dims     = {width: $(@dCParentEl).width(), height: $(@dCParentEl).height()} #@dCParentEl.position() #@dCParentEl.getCoordinates()
      fb_dims  = {width: $(@filterBarEl).width(), height: $(@filterBarEl).height()} #@filterBarEl.position() #@filterBarEl.getCoordinates()
      fb_align = if @filterBar? then @filterBar.alignment else FilterBar.DEFAULT_ALIGN
      switch fb_align
        when FilterBar.ALIGN_TOP
          Util.setStyle(@dCEl, 'top', FilterBar.THICKNESS)
          Util.setStyle(@dCEl, 'left', 0)
          Util.setStyle(@dCEl, 'width', '100%')
          console.log "ASDFASFASDASDF"
          Util.setStyle(@dCEl, 'height', (dims.height - FilterBar.THICKNESS) + 'px')
        when FilterBar.ALIGN_BOTTOM
          Util.setStyle(@dCEl, 'top', 0)
          Util.setStyle(@dCEl, 'left', 0)
          Util.setStyle(@dCEl, 'width', '100%')
          console.log "ASDFASFASDASDF", @dCParentEl
          Util.setStyle(@dCEl, 'height', (dims.height - FilterBar.THICKNESS) + 'px')
        when FilterBar.ALIGN_LEFT
          Util.setStyle(@dCEl, 'top', 0)
          Util.setStyle(@dCEl, 'left', FilterBar.THICKNESS)
          Util.setStyle(@dCEl, 'width', (dims.width - FilterBar.THICKNESS) + 'px')
          Util.setStyle(@dCEl, 'height', '100%')
        when FilterBar.ALIGN_RIGHT
          Util.setStyle(@dCEl, 'top', 0)
          Util.setStyle(@dCEl, 'left', 0)
          Util.setStyle(@dCEl, 'width', (dims.width - FilterBar.THICKNESS) + 'px')
          Util.setStyle(@dCEl, 'height', '100%')

      @dataCentre.updateLayout()
      @filterBar.updateLayout()


    updateThumb: =>
      dims = @dCEl.position() # @dCEl.getCoordinates()
      @thumb.update(dims.left, dims.top, @dataCentre.coordReferenceEl.left, @dataCentre.coordReferenceEl.top, @dCEl.scrollLeft, @dCEl.scrollTop)


    resetZoom: ->
      @dataCentre.zoomToPreset(9999)


    resetFilters: ->
      groups       = @model.groups()
      blank        = {}
      blank[group] = {} for group in groups

      @model.activeFilter(false)
      @model.filteredDevices(blank)
      @model.activeSelection(false)
      @model.selectedDevices(blank)

      selected_metric          = @model.selectedMetric()
      filters                  = @model.filters()
      filters[selected_metric] = {}
      @model.filters(filters)


    zoomToPreset: (direction = 1, x, y) ->
      x    = @dCEl.scrollLeft + ($(@dCEl).width() / 2) unless x?
      y    = @dCEl.scrollTop + ($(@dCEl).height() / 2) unless y?

      @zooming = true
      clearTimeout(@hintTmr)
      #@disableMouse()
      @dataCentre.zoomToPreset(direction, x, y)


    loadMetrics: =>
      selected_metric = @model.selectedMetric()
      url             = DCPVController.METRIC_DATA_URL
      selection       = Util.substitutePhrase(DCPVController.METRIC_SELECTION, 'selected_metric', selected_metric)

      item_list = []
      dc_def    = @model.dataCentreDef()
      groups    = @model.groups()
      for group in groups

        switch group
          when 'racks'
            item_str = DCPVController.METRIC_GROUP
          when 'chassis'
            item_str = DCPVController.METRIC_CHASSIS
          when 'sensors'
            item_str = DCPVController.METRIC_SENSOR

        idx = 0

        for item in dc_def[0].planViewDef.items[group]
          continue unless item.position?
          ++idx

          member = Util.substitutePhrase(item_str, 'id', item.groupId ? item.itemId)
          member = Util.substitutePhrase(member, 'idx', idx)
          item_list.push(member)

      url = Util.substitutePhrase(url, 'metric_selection', selection)
      url = Util.substitutePhrase(url, 'group', item_list.join(DCPVController.METRIC_GROUP_JOIN))
      url = unescape(url)

      on_success = (data) =>
        @evReceivedMetricData(data, selected_metric)

      $.get(url: url + '&' + (new Date()).getTime(), onSuccess: on_success)


    evReceivedConfig: (config) =>
      Configurator.setup(config)
      @model = new ViewModel()
      @loadDef()


    evReceivedDef: (def) =>
      parsed = Parser.parseDCDef(def)
      @model.dataCentreDef(parsed.definition)
      @model.deviceLookup(parsed.deviceLookup)
      @model.groupMemberLookup(parsed.groupMemberLookup)
      console.log ">>>>>>>>>>>>>>>>>evReceivedDef", def[0].planViewDef.img
      AssetManager.get(def[0].planViewDef.img, @evImgLoaded)


    evReceivedDefFake: () =>
      console.log "Processing fake plan view data for now..."
      data = [{"planViewDef":{"id":1,"default":false,"xDim":10.0,"yDim":100.0,"xPx":500.0,"yPx":500.0,"img":"assets/images/datacentreplan.png","items":{"chassis":[],"sensors":[],"racks":[{"itemId":1,"groupId":11,"name":"Cluster rack 1","numDevices":3,"pos_id":1,"memberIds":{"sensorIds":[],"deviceIds":[73,2,4],"chassisIds":[]},"position":{"x":4.932697029967,"y":51.0142242850317,"w":0.6,"d":1.2}}]}}}]
      @evReceivedDef(data)


    evReceivedMetricDef: (def) =>
      @model.metricTemplates(Parser.parseMetricDefs(def))


    evReceivedMetricDefFake: () =>
      console.log "Processing fake metric data now"
      data = [{"type": "numeric", "unit": "%", "prioritized": false, "format": "%s %", "min": 88.0, "top": 89, "bottom": 88, "name": "cpu_idle", "max": 88.0, "id": "cpu_idle"}, {"type": "numeric", "unit": "%", "prioritized": false, "format": "%s %", "min": 5.0, "top": 6, "bottom": 5, "name": "cpu_nice", "max": 5.0, "id": "cpu_nice"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 2.0, "top": 3, "bottom": 2, "name": "cpu_num", "max": 2.0, "id": "cpu_num"}, {"type": "numeric", "unit": "%", "prioritized": false, "format": "%s %", "min": 4.5, "top": 5, "bottom": 4, "name": "cpu_system", "max": 4.5, "id": "cpu_system"}, {"type": "numeric", "unit": "%", "prioritized": false, "format": "%s %", "min": 73.0, "top": 74, "bottom": 73, "name": "cpu_user", "max": 73.0, "id": "cpu_user"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 0.5, "top": 1, "bottom": 0, "name": "load_five", "max": 0.5, "id": "load_five"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 4.0, "top": 5, "bottom": 4, "name": "load_one", "max": 4.0, "id": "load_one"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 1024.0, "top": 1025, "bottom": 1024, "name": "mem_buffers", "max": 1024.0, "id": "mem_buffers"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 128.0, "top": 129, "bottom": 128, "name": "mem_cached", "max": 128.0, "id": "mem_cached"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 256.0, "top": 257, "bottom": 256, "name": "mem_free", "max": 256.0, "id": "mem_free"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 512.0, "top": 513, "bottom": 512, "name": "mem_shared", "max": 512.0, "id": "mem_shared"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 2048.0, "top": 2049, "bottom": 2048, "name": "mem_total", "max": 2048.0, "id": "mem_total"}, {"type": "numeric", "unit": "W", "prioritized": false, "format": "%s W", "min": 166.0, "top": 167, "bottom": 166, "name": "power_draw", "max": 166.0, "id": "power_draw"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 1.0, "top": 2, "bottom": 1, "name": "proc_run", "max": 1.0, "id": "proc_run"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 768.0, "top": 769, "bottom": 768, "name": "swap_free", "max": 768.0, "id": "swap_free"}, {"type": "numeric", "unit": "", "prioritized": false, "format": "%s", "min": 768.0, "top": 769, "bottom": 768, "name": "swap_total", "max": 768.0, "id": "swap_total"}]
      @evReceivedMetricDef(data)


    evReceivedThresholds: (thresholds) =>
      parsed = Parser.parseThresholds(thresholds)
      @model.thresholdsByMetric(parsed.byMetric)
      @model.thresholdsById(parsed.byId)
      

    evReceivedThresholdsFake: () =>
      console.log "Processing fake threshold data now"
      data = []
      @evReceivedThresholds(data)

    evReceivedMetricData: (data, metric_id) =>
      metrics = Parser.parseMetricData(data, @model.deviceLookup(), metric_id)
      metrics.data.metricId = @model.selectedMetric() unless metrics.data.metricId

      filters  = @model.filters()
      filter   = filters[metrics.data.metricId]
      col_maps = @model.colourMaps()
      col_map  = col_maps[metrics.data.metricId]

      if col_map?
        @model.metricData(metrics.data)
        @applyFilter() if (filter.max? and filter.max isnt col_map.high) or (filter.min? and filter.min isnt col_map.low)
      else
        # set default maps/filters if not already set
        unless filters[metrics.data.metricId]?
          filters[metrics.data.metricId] = {}
          @model.filters(filters)

        range = if metrics.min is metrics.max then 1e-100 else metrics.max - metrics.min
        col_maps[metrics.data.metricId] = { low: metrics.min, high: metrics.max, range: range, inverted: false }
        @model.metricData(metrics.data)
        @model.colourMaps(col_maps)
  

    evReceivedBreaches: (breaches) =>
      dc_def              = @model.dataCentreDef()
      device_lookup       = @model.deviceLookup()
      group_member_lookup = @model.groupMemberLookup()
      breaching           = { racks: {} }

      for group of breaches
        breaching[group] = {}
        breach_group     = breaches[group]

        for id in breach_group
          parent_group_id  = group_member_lookup.byMember[group][id]
          parent_id        = device_lookup.byGroupId[parent_group_id].itemId
          if parent_id?
            breaching.racks[parent_id] = true
          else
            breaching[group][id] = true

      @model.breaches(breaching)
    
    
    # applies above/below/between filter and stores subset in the view model
    applyFilter: =>
      filters         = @model.filters()
      selected_metric = @model.selectedMetric()
      metrics         = @model.metricData()

      min = filters[selected_metric].min
      max = filters[selected_metric].max

      filtered_devices = {}
      groups           = @model.groups()
      filtered_devices[group] = {} for group in groups

      gt = (val) =>
        return val > min

      lt = (val) =>
        return val < max

      between = (val) =>
        return val > min and val < max

      if min? and max?
        filter = between
      else if min?
        filter = gt
      else if max?
        filter = lt
      else
        @model.activeFilter(false)
        @model.filteredDevices(filtered_devices)
        return

      # apply filter to each device
      for group in groups
        filtered_devices[group][id] = filter(metrics.values[group][id].value) for id of metrics.values[group]

      @model.activeFilter(true)
      @model.filteredDevices(filtered_devices)


    evImgLoaded: (img) =>
      @init()


    evResize: (ev) =>
      @updateLayout()


    evDCMouseWheel: (ev) =>
      if @keysPressed[DCPVController.ZOOM_KEY] and not @zooming
        ev.preventDefault()
        ev.stopPropagation()
        @zooming = true
        coords   = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
        delta    = ev.wheelDelta ? -ev.detail

        @dataCentre.stepZoom(Math.abs(delta) / delta, coords.x, coords.y)


    evThumbMouseWheel: (ev) =>
      if @keysPressed[DCPVController.ZOOM_KEY] and not @zooming
        ev.preventDefault()
        ev.stopPropagation()
        @zooming = true

        scale     = Util.getStyleNumeric(@dataCentre.coordReferenceEl, 'width') / Util.getStyleNumeric(@thumbEl, 'width')
        coords    = Util.resolveMouseCoords(@thumbEl, ev)
        coords.x *= scale
        coords.y *= scale

        delta = ev.wheelDelta ? -ev.detail

        @dataCentre.stepZoom(Math.abs(delta) / delta, coords.x, coords.y)


    evSwitchMode: (mode = @model.mode()) =>
      if mode is ViewModel.MODE_EDIT
        @viewModeChartVisible = @model.showChart()
        @model.showChart(false) if @viewModeChartVisible
      else
        @model.showChart(@viewModeChartVisible) if @model.showChart() isnt @viewModeChartVisible

      @mode = mode


    evSwitchMetric: (metric) =>
      clearInterval(@metricTmr)
      @metricTmr = setInterval(@loadMetrics, DCPVController.METRIC_POLL_RATE)
      @loadMetrics()

      # reset filter
      if @model.activeFilter() and (not filter? or (filter.max is colour_map.high and filter.min is colour_map.low))
        groups       = @model.groups()
        blank        = {}
        blank[group] = {} for group in groups
        @model.activeFilter(false)
        @model.filteredDevices(blank)


    evDCMouseMove: (ev) =>
      coords = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
      @hint.hide()

      if @dataCentre.draggingItem
        @dataCentre.dragItem(coords.x, coords.y)
      else if @dataCentre.draggingBox
        @dataCentre.dragBox(coords.x, coords.y)
      else if @mouseDown
        if @mode is ViewModel.MODE_EDIT and @dataCentre.hoverDevice?
          @dataCentre.startItemDrag(coords.x, coords.y)
        else
          @dataCentre.startBoxDrag(coords.x, coords.y)
      else unless @dataCentre.ctxMenu.visible
        clearTimeout(@hintTmr)
        @hintTmr = setTimeout(@showHint, DCPVController.HINT_DELAY, ev)
        @dataCentre.testHover(coords.x, coords.y)


    showHint: (ev) =>
      if @dataCentre.hoverDevice?
        coords = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
        @hint.show(@dataCentre.hoverDevice, coords.x, coords.y)


    evDCMouseDown: (ev) =>
      # ignore anything other than left-clicks
      return if (ev.which? and ev.which isnt 1) or (ev.button? and ev.button isnt 0)

      coords = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
      dims   = @dCEl.getCoordinates()

      return if coords.x - @dCEl.scrollLeft > dims.width - @scrollbarOffset or coords.y - @dCEl.scrollTop > dims.height - @scrollbarOffset

      @mouseDown = true
      #coords = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
      #@dataCentre.startDrag(coords.x, coords.y)


    evDCMouseUp: (ev) =>
      # ignore anything other than left-clicks
      return if (ev.which? and ev.which isnt 1) or (ev.button? and ev.button isnt 0)

      @mouseDown = false
      clearTimeout(@clickTmr)

      if @dataCentre.draggingItem
        @clickAssigned = true
        coords         = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
        @dataCentre.stopItemDrag(coords.x, coords.y)
      if @dataCentre.draggingBox
        @clickAssigned = true
        coords         = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
        @dataCentre.stopBoxDrag(coords.x, coords.y)
      else if @clickAssigned
        @clickAssigned = false
        @clickTmr      = setTimeout(@evDCClick, DCPVController.DOUBLE_CLICK_TIMEOUT, ev)
      else
        @evDCDoubleClick(ev)

   
    evDCDoubleClick: (ev) =>
      @clickAssigned = true
      coords         = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
      dims           = @dCEl.getCoordinates()

      return if coords.x - @dCEl.scrollLeft > dims.width - @scrollbarOffset or coords.y - @dCEl.scrollTop > dims.height - @scrollbarOffset

      @zoomToPreset(1, coords.x, coords.y)


    evDCClick: (ev) =>
      # ignore right and middle clicks
      return if (ev.which? and ev.which isnt 1) or (ev.button? and ev.button isnt 0)

      @clickAssigned = true
      coords         = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
      dims           = @dCEl.getCoordinates()

      return if coords.x - @dCEl.scrollLeft > dims.width - @scrollbarOffset or coords.y - @dCEl.scrollTop > dims.height - @scrollbarOffset

      @dataCentre.selectItem(coords.x, coords.y, @keysPressed[DCPVController.MULTI_SELECT_KEY])


    evDCRightClick: (ev) =>
      rel_coords = Util.resolveMouseCoords(@dataCentre.coordReferenceEl, ev)
      dims       = @dCEl.getCoordinates()

      return if rel_coords.x - @dCEl.scrollLeft > $(@dCEl).width() - @scrollbarOffset or rel_coords.y - @dCEl.scrollTop > $(@dCEl).height() - @scrollbarOffset
      
      @hint.hide()
      clearTimeout(@hintTmr)
      ev.preventDefault()
      ev.stopPropagation()

      pos        = $(@dCEl).position()
      abs_coords = { x: ev.clientX - pos.left, y: ev.clientY - pos.top}
      @dataCentre.showContextMenu(abs_coords, rel_coords)


    evZoomComplete: (ev) =>
      @zooming = false
      @updateThumb()


    evDCScroll: (ev) =>
      @updateThumb() unless @zooming


    evThumbMouseDown: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      @evThumbScroll(ev)
      Events.addEventListener(@thumbEl, 'mousemove', @evThumbScroll)


    evThumbMouseUp: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      @evThumbScroll(ev)
      Events.removeEventListener(@thumbEl, 'mousemove', @evThumbScroll)


    evThumbDoubleClick: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      coords = Util.resolveMouseCoords(@thumbEl, ev)
      @zoomToPreset(1, coords.x / @thumb.width * @dataCentre.coordReferenceEl.width, coords.y / @thumb.height * @dataCentre.coordReferenceEl.height)


    evThumbScroll: (ev) =>
      coords = Util.resolveMouseCoords(@thumbEl, ev)

      @dCEl.scrollLeft = (coords.x / @thumb.width * @dataCentre.coordReferenceEl.width) - ($(@dCEl).width() / 2)
      @dCEl.scrollTop  = (coords.y / @thumb.height * @dataCentre.coordReferenceEl.height) - ($(@dCEl).height() / 2)


    evFilterMouseDown: (ev) =>
      return if ev.target instanceof HTMLInputElement

      ev.preventDefault()
      ev.stopPropagation()

      coords      = Util.resolveMouseCoords(@filterBarEl, ev)
      @slider     = @filterBar.getSliderAt(coords.x, coords.y)
      @dragging   = false
      @downCoords = coords

      if @slider?
        Events.addEventListener(@filterBarEl, 'mousemove', @evFilterMouseMove)
      else
        Events.addEventListener(document.window, 'mousemove', @evFilterMouseMove)


    evFilterMouseOut: (ev) =>
      Events.removeEventListener(@filterBarEl, 'mousemove', @evFilterMouseMove)


    evFilterMouseUp: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      @filterBar.stopDrag()

      Events.removeEventListener(@filterBarEl, 'mousemove', @evFilterMouseMove)
      Events.removeEventListener(document.window, 'mousemove', @evFilterMouseMove)


    evFilterMouseMove: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      coords = Util.resolveMouseCoords(@filterBarEl, ev)

      if @slider?
        @filterBar.dragSlider(@slider, coords.x, coords.y)
      else
        if @dragging
          @filterBar.dragBar(ev.pageX, ev.pageY)
        else
          # commence dragging only if the user has moved the mouse a certain distance
          @dragging = Math.sqrt(Math.pow(coords.x - @downCoords.x, 2) + Math.pow(coords.y - @downCoords.y, 2)) > DCPVController.DRAG_ACTIVATION_DIST
          if @dragging
            @filterBar.startDrag()
            Events.addEventListener(document.window, 'mouseup', @evFilterStopDrag)


    evFilterStopDrag: (ev) =>
      @filterBar.stopDrag()
      Events.removeEventListener(document.window, 'mousemove', @evFilterMouseMove)


    evDropFilterBar: (ev) =>
      @updateLayout()


    evKeyDown: (ev) =>
      @keysPressed[ev.keyCode] = true

      if @mode is ViewModel.MODE_EDIT
        if ev.keyCode >= 37 and ev.keyCode <= 40
          super_nudge = @keysPressed[DCPVController.SUPER_NUDGE_KEY]

          switch ev.keyCode
            when 37
              nudged = @dataCentre.nudgeSelection(-1, 0, super_nudge)
            when 38
              nudged = @dataCentre.nudgeSelection(0, -1, super_nudge)
            when 39
              nudged = @dataCentre.nudgeSelection(1, 0, super_nudge)
            when 40
              nudged = @dataCentre.nudgeSelection(0, 1, super_nudge)

          if nudged
            ev.preventDefault()
            ev.stopPropagation()
        else if ev.keyCode is DCPVController.DELETE_KEY
          @dataCentre.deleteSelection()


    evKeyUp: (ev) =>
      @keysPressed[ev.keyCode] = false


    evNavZoomIn: (ev) =>
      @dataCentre.stepZoom(1)


    evNavZoomOut: (ev) =>
      @dataCentre.stepZoom(-1)


    evNavResetZoom: (ev) =>
      @resetZoom()


    evNavItemEdited: (ev) =>
      @dataCentre.requestDCImageUpdate()


    evNavAddItem: (ev) =>
      item = @model.getItemFromSelect($('unpositioned_select').value)
      @dataCentre.addItem(item) if item?


    evNavSave: (ev) =>
      change_set     = @dataCentre.saveChanges()
      @additions     = []
      @additions[id] = change_set.dcpv_positions[id].additions for id of change_set.dcpv_positions

      $.get(
        headers    : {'X-CSRF-Token': $$('meta[name="csrf-token"]')[0].getAttribute('content')}
        url        : '/-/api/v1/data_centre/positions'
        method     : 'post'
        data       : change_set
        onComplete : @evSaveComplete
      ).send()


    evNavSwitchToView: (ev) =>
      if @dataCentre.isUnsaved()
        confirm_dialog(DCPVController.ALERT_UNSAVED, 'document.DCPV.evDiscardChanges();', 'void(0);', 'Please confirm', true, true)
      else
        @model.mode(ViewModel.MODE_VIEW)


    evNavUpdateItem: (ev) =>
      @dataCentre.setItemSize(@model.selection()[0], Number($('item_width').value), Number($('item_depth').value))


    evNavResetFilters: (ev) =>
      @resetFilters()

    evNavDeleteSelection: (ev) =>
      @dataCentre.deleteSelection()


    evDiscardChanges: (ev) =>
      @dataCentre.discardChanges()
      @model.mode(ViewModel.MODE_VIEW)


    evSaveComplete: (data) =>
      return unless data.success

      device_lookup = @model.deviceLookup()

      # assign position ID's to any additions
      for dc_id of @additions
        continue unless @additions.hasOwnProperty(dc_id)

        dc = @additions[dc_id]
        for key of dc
          addition = dc[key]
          item_id  = addition.item_id
          group    = addition.item_type
          pos_id   = null

          continue unless data[group]

          for item in data[group]
            if item.item_id is item_id
              pos_id = item.pos_id
              break

          device_lookup[group][item_id].pos_id = pos_id

      @dataCentre.saveComplete()


    evChartMouseDown: (ev) =>
      coords      = Util.resolveMouseCoords(@chartEl, ev)
      @downCoords = coords
      Events.addEventListener(@chartEl, 'mousemove', @evDragChart)


    evChartMouseUp: (ev) =>
      @upCoords = Util.resolveMouseCoords(@chartEl, ev)
      Events.removeEventListener(@chartEl, 'mousemove', @evDragChart)

      if @dragging
        coords = Util.resolveMouseCoords(@chartEl, ev)
        @chart.stopDrag(coords.x, coords.y)
        @dragging = false


    evDragChart: (ev) =>
      coords = Util.resolveMouseCoords(@chartEl, ev)
      if not @dragging
        @dragging = Math.sqrt(Math.pow(coords.x - @downCoords.x, 2) + Math.pow(coords.y - @downCoords.y, 2)) > DCPVController.DRAG_ACTIVATION_DIST
        if @dragging
          @chart.startDrag(@downCoords.x, @downCoords.y)
      else
        @chart.drag(coords.x, coords.y)


    evExitToIRV: (ev) =>
      rack_selection = {}

      selected_metric  = @model.selectedMetric()
      active_filter    = @model.activeFilter()
      filtered_racks   = @model.filteredDevices().racks
      active_selection = @model.activeSelection()
      selected_racks   = @model.selectedDevices().racks

      if active_filter and active_selection
        for id of selected_racks
          rack_selection[id] = true if filtered_racks[id] and filtered_selection[id]

      else if active_filter
        for id of filtered_racks
          rack_selection[id] = true if filtered_racks[id]

      else if active_selection
        for id of selected_racks
          rack_selection[id] = true if selected_racks[id]

      settings = { selectedRacks: rack_selection }

      if selected_metric?
        settings.selectedMetric              = @model.selectedMetric()
        settings.colourMaps                  = {}
        settings.colourMaps[selected_metric] = @model.colourMaps()[selected_metric]
        settings.filters                     = {}
        settings.filters[selected_metric]    = @model.filters()[selected_metric]
        
      CrossAppSettings.set(settings)


    evReset: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @resetFilters()
      @resetZoom()


    evPrint: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      html = '<div style="height: 512px;">' + document.getElementById('dcpv_container').innerHTML + '</div><div></div><div>' + document.getElementById('graph_container').innerHTML + '</div>'
      Util.printHtmlInNewPage( html )

    evExport: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()

      selected_metric = @model.selectedMetric()
      return unless selected_metric?

      data          = @model.metricData()
      metric        = @model.metricTemplates()[selected_metric]
      groups        = @model.groups()
      device_lookup = @model.deviceLookup()

      output = DCPVController.EXPORT_HEADER
      output = Util.substitutePhrase(output, 'metric_name', metric.name)
      output = Util.substitutePhrase(output, 'metric_units', metric.unit)
      output = Util.substitutePhrase(output, 'metric_name', metric.unit)

      for group in groups
        group_lookup = device_lookup[group]
        values       = data.values[group]

        for id of values
          device = group_lookup[id]

          if device?
            record = DCPVController.EXPORT_RECORD
            record = Util.substitutePhrase(record, 'device_name', device.name)
            record = Util.substitutePhrase(record, 'device_id', device.id)
            record = Util.substitutePhrase(record, 'value', values[id].value)

            output += record + String.fromCharCode(10)

      ts       = new Date()
      filename = DCPVController.EXPORT_FILENAME
      filename = Util.substitutePhrase(filename, 'metric_name', metric.name)
      filename = Util.substitutePhrase(filename, 'day', Util.addLeadingZeros(ts.getUTCDate()))
      filename = Util.substitutePhrase(filename, 'month', Util.addLeadingZeros(ts.getUTCMonth() + 1))
      filename = Util.substitutePhrase(filename, 'year', Util.addLeadingZeros(ts.getUTCFullYear()))
      filename = Util.substitutePhrase(filename, 'hours', Util.addLeadingZeros(ts.getUTCHours()))
      filename = Util.substitutePhrase(filename, 'minutes', Util.addLeadingZeros(ts.getUTCMinutes()))
      filename = Util.substitutePhrase(filename, 'seconds', Util.addLeadingZeros(ts.getUTCSeconds()))
      filename = Util.cleanUpSubstitutions(filename)

      dl          = document.createElement('a')
      dl.href     = 'data:text/octet-stream;charset=utf-8,' + encodeURIComponent(output)
      dl.download = filename

      document.body.appendChild(dl)
      dl.click()
      document.body.removeChild(dl)


    evGrabScreen: =>
      alert_dialog DCPVController.EXPORT_MESSAGE
      ts       = new Date()
      filename = DCPVController.SCREENSHOT_FILENAME
      filename = Util.substitutePhrase(filename, 'day', Util.addLeadingZeros(ts.getUTCDate()))
      filename = Util.substitutePhrase(filename, 'month', Util.addLeadingZeros(ts.getUTCMonth() + 1))
      filename = Util.substitutePhrase(filename, 'year', Util.addLeadingZeros(ts.getUTCFullYear()))
      filename = Util.substitutePhrase(filename, 'hours', Util.addLeadingZeros(ts.getUTCHours()))
      filename = Util.substitutePhrase(filename, 'minutes', Util.addLeadingZeros(ts.getUTCMinutes()))
      filename = Util.substitutePhrase(filename, 'seconds', Util.addLeadingZeros(ts.getUTCSeconds()))
      filename = Util.cleanUpSubstitutions(filename)

      b64_img     = @dataCentre.getImage().toDataURL()
      dl          = document.createElement('a')
      dl.href     = b64_img.replace('image/png', 'image/octet-stream')
      dl.download = filename

      addFrmVal = (name, value) ->
        val       = document.createElement('input')
        val.name  = name
        val.type  = 'hidden'
        val.value = value
        frm.appendChild(val)

      svg = document.getElementsByTagName('svg')[0]

      # IE doesn't report the parent of our SVG graph. If you're sitting there thinking
      # hmmmm this is a particularly hooky way of extracting the SVG string, you're
      # probably right
      if svg.parentElement?
        dim_ref = svg.parentElement
        svg_str = svg.parentElement.innerHTML
      else
        dim_ref = $('graph_container')
        svg_str = '<svg' + $('graph_container').innerHTML.split('<svg')[1].split('</svg>')[0] + '</svg>'

      svg_dims = dim_ref.getCoordinates()

      frm         = document.createElement('form')
      frm.name    = 'imagePost-a-tron'
      frm.method  = 'post'
      frm.enctype = 'multipart/form-data'
      addFrmVal('filename', filename)
      addFrmVal('type[]', 'text')
      addFrmVal('data[]', b64_img.split(',')[1])
      addFrmVal('size[]', String(@dataCentre.coordReferenceEl.width) + 'x' + String(@dataCentre.coordReferenceEl.height))
      addFrmVal('type[]', 'svg')
      addFrmVal('data[]', svg_str)
      addFrmVal('size[]', String(Math.ceil(svg_dims.width)) + 'x' + String(Math.ceil(svg_dims.height)))
      addFrmVal('authenticity_token', $$('meta[name="csrf-token"]')[0].getAttribute('content'))

      document.body.appendChild(frm)
      frm.action = DCPVController.EXPORT_IMAGE_URL
      frm.submit()


    evGetHintInfo: (ev) =>
      url = Util.substitutePhrase(DCPVController.HINT_DATA_URL, 'device_id', @dataCentre.hoverDevice.itemId) + '?' + (new Date()).getTime()
      url = Util.substitutePhrase(url, 'group', @dataCentre.hoverDevice.group)

      $.get(url: url, onComplete: @hintInfoReceived)


    hintInfoReceived: (hint_info) =>
      @hint.appendData(hint_info)
