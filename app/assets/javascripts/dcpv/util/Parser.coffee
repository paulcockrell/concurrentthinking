define ['common/util/Util'], (Util) ->

  class Parser

    @GROUP_DEVICE_ID_MAP: { racks: "rackId", sensors: "sensorId", chassis: "chassisId" }
    @DEVICE_ID_GROUP_MAP: { racksIds: "racks", sensorIds: "sensors", chassisIds: "chassis", deviceIds: "devices" }
    @GROUP_MAP: { chassis: "chassis", sensors: "sensors", groups: "racks" }


    @parseDCDef: (def) ->
      data_centres        = []
      device_lookup       = { byGroupId: {} }
      group_member_lookup = { byMember: { devices: {}, chassis: {}, racks: {}, sensors: {} }, byGroup: {} }
      for dc_def in def
        for group of dc_def.planViewDef.items
          device_lookup[group] = {} unless device_lookup[group]?

          device_list = dc_def.planViewDef.items[group]
          #device_id_key = Parser.GROUP_DEVICE_ID_MAP[group]
          for device in device_list
            #device.position = { x: Math.random() * 20, y: Math.random() * 20, w: 1, d: 1 } if group is 'sensors'
            device.group                                = group
            device_lookup[group][device.itemId]         = device
            device_lookup.byGroupId[device.groupId]     = device if device.groupId?
            group_member_lookup.byGroup[device.groupId] = { chassis: {}, devices: {}, sensors: {} }

            for key of device.memberIds
              member_group = Parser.DEVICE_ID_GROUP_MAP[key]
              for id in device.memberIds[key]
                group_member_lookup.byGroup[device.groupId][member_group][id] = true
                group_member_lookup.byMember[member_group][id] = device.groupId

      #device_lookup = { racks: {}, chassis: {}, sensors: {} }
      #device_lookup.racks[rack.group_id]      = rack for rack in def.rack_positions
      #device_lookup.racks[rack.group_id]      = rack for rack in def.unpositioned_racks
      #device_lookup.chassis[chassis.group_id] = chassis for chassis in def.chassis_positions
      #device_lookup.chassis[chassis.group_id] = chassis for chassis in def.unpositioned_chassis
      #device_lookup.sensors[sensor.group_id]  = sensor for sensor in def.sensor_positions
      #device_lookup.sensors[sensor.group_id]  = sensor for sensor in def.unpositioned_sensors
      { definition: def, deviceLookup: device_lookup, groupMemberLookup: group_member_lookup }


    @parseMetricDefs: (templates) ->
      metric_templates = {}
      metric_templates[template.id] = template for template in templates
      metric_templates


    @parseMetricData: (data, device_lookup, metric_id) ->
      metric_data = { values: {} }
      min         = Number.MAX_VALUE
      max         = -Number.MAX_VALUE
      no_metrics  = true

      #for group_id of data
      #  values = data[group_id][0]
      #
      #   continue unless values?
      #
      #   group   = device_lookup.byGroupId[group_id].group
      #   item_id = device_lookup.byGroupId[group_id][Parser.GROUP_DEVICE_ID_MAP[group]]
      #   console.log group, item_id
      #   metric_data.values[group] = {} unless metric_data.values[group]?
      #   metric_data.values[group][item_id] = values.mean
      #   min = values.min if values.min < min
      #   max = values.max if values.max > max
      #   if not set_id
      #     set_id = true
      #     metric_data.metricId = values.name

      for group of data
        real_group = Parser.GROUP_MAP[group]
        fetch_id   = real_group is 'racks'

        metric_data.values[real_group] = {}
        list = data[group][0]

        for item of list
          values = list[item][0]

          continue unless values?

          no_metrics = false

          # some values are supplied by the 'api' as numbers, some as strings
          # cast everything to be sure
          for key of values
            values[key] = Number(values[key]) unless isNaN(Number(values[key]))

          item_id = if fetch_id then device_lookup.byGroupId[item].itemId else item

          metric_data.values[real_group][item_id] = values
          min = values.value if values.value < min
          max = values.value if values.value > max

      if no_metrics
        metric_data.min = 0
        metric_data.max = 0
      else
        metric_data.min = min
        metric_data.max = max

      metric_data.metricId = metric_id
      { data: metric_data, min: min, max: max }


    @parseThresholds: (thresholds) ->
      tholds_by_metric = {}
      tholds_by_id     = {}

      for thold of thresholds
        if thresholds.hasOwnProperty(thold)
          raw_thold = thresholds[thold]
          metric    = raw_thold.metric
          thold_obj = { id: raw_thold.id, name: raw_thold.name, colours: [], values: [] }

          if raw_thold.breach_value?
            thold_obj.colours.push('#ff0000')
            thold_obj.values.push(Number(raw_thold.breach_value))
          else
            ranges = raw_thold.ranges
            ranges = Util.sortByProperty(ranges, 'upper_bound', true)
            for range in ranges
              thold_obj.colours.push(range.colour)
              thold_obj.values.push(range.upper_bound)

          tholds_by_metric[metric] = [] unless tholds_by_metric[metric]?
          tholds_by_metric[metric].push(thold_obj)

          tholds_by_id[thold_obj.id] = thold_obj

      { byMetric: tholds_by_metric, byId: tholds_by_id }
