define ['common/util/StaticGroupManager'], (StaticGroupManager) ->

  class DCPVStaticGroups extends StaticGroupManager

    constructor: (@model) ->
      console.log 'new DCPVStaticGroups'
      super(@model)


    displayGroup: (static_group) =>
      device_lookup       = @model.deviceLookup()
      selection           = {}
      groups              = @model.groups()
      selection[group]    = {} for group in groups
      group_member_lookup = @model.groupMemberLookup()
      active              = false

      for group of static_group.memberIds
        console.log group
        real_group = null
        for map_group of StaticGroupManager.GROUP_ID_MAP
          console.log map_group, group
          if StaticGroupManager.GROUP_ID_MAP[map_group] is group
            real_group = map_group
            break

        set = static_group.memberIds[group]
        for id in set
          parent_id = group_member_lookup.byMember[real_group][id]
          console.log id, real_group, parent_id, group_member_lookup.byMember
          if parent_id and device_lookup.byGroupId[parent_id].position?
            active  = true
            rack_id = device_lookup.byGroupId[parent_id].itemId
            selection.racks[rack_id] = true

          else if device_lookup[real_group]? and device_lookup[real_group][id]? and device_lookup[real_group][id].position?
            selection[group][id] = true
            active = true

            #else if group_member_lookup.byMember[real_group]? and group_member_lookup.byMember[real_group][id]? and device_lookup.byGroupId[group_member_lookup.byMember[real_group][id]].position?
            #selection.racks[device_lookup.byGroupId[group_member_lookup.byMember[real_group][id]].itemId] = true
            #active = true

      @model.activeSelection(active)
      @model.selectedDevices(selection) if active
