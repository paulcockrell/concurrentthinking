define ['dcpv/view/Rack', 'dcpv/view/Sensor', 'dcpv/view/NRAD', 'common/util/Util', 'common/util/Events'], (Rack, Sensor, NRAD, Util, Events) ->

  class ContextMenu

    # statics overwritten by config
    @OPTIONS             : {}
    @URL_INTERNAL_PREFIX : 'internal::'
    @SPACER              : '<br>'


    constructor: (@containerEl, @model, @internalCallback) ->
      @visible = false
      @menuEl  = $('context_menu')
      Events.addEventListener(@menuEl, 'click', @evClick)


    show: (device, x, y) ->
      @visible    = true
      option_keys = ['common']

      if device?
        device_id   = device.itemId
        device_name = device.name
        device_type = if device.type? then ContextMenu.DEVICE_TYPE_URL_MAP[device.type] else undefined
        option_keys.push(device.group)
      else
        option_keys.push('global')
  
      # create array of options based upon option_keys
      options_html = []
      parsed       = []
      for option_set of ContextMenu.OPTIONS
        if option_keys.indexOf(option_set) isnt -1
          idx = parsed.length
          parsed.push([])
          for option in ContextMenu.OPTIONS[option_set]
            piece      = option.caption ? option.content
            option_url = option.url
            on_click   = option.onClick

            piece = Util.substitutePhrase(piece, 'device_id', device_id)
            piece = Util.substitutePhrase(piece, 'device_name', device_name)
            piece = Util.substitutePhrase(piece, 'spacer', ContextMenu.SPACER)

            piece = Util.cleanUpSubstitutions(piece)

            if option_url?
              option_url = Util.substitutePhrase(option_url, 'device_id', device_id)
              option_url = Util.substitutePhrase(option_url, 'device_name', device_name)

              option_url = Util.cleanUpSubstitutions(option_url)

            if on_click?
              on_click = Util.substitutePhrase(on_click, 'device_id', device_id)
              on_click = Util.substitutePhrase(on_click, 'device_name', device_name)

              on_click = Util.cleanUpSubstitutions(on_click)

            if piece.length > 0
              if option_url?
                if on_click?
                  parsed[idx].push("<a href='#{option_url}' onclick=\"#{on_click}\" ><div class='context_menu_item'>#{piece}</div></a>")
                else
                  parsed[idx].push("<a href='#{option_url}'><div class='context_menu_item'>#{piece}</div></a>")
              else
                parsed[idx].push(piece)

          parsed[idx] = parsed[idx].join('')
          parsed.splice(idx, 1) if parsed[idx].length is 0

      parsed = parsed.reverse()
      @menuEl.innerHTML = parsed.join(ContextMenu.SPACER)

      # adjust when near to edges of the screen
      div_x = Util.getStyle(@containerEl, 'left')
      div_x = div_x.substr(0, div_x.length - 2)

      container_dims = Util.getElementDimensions(@containerEl)
      menu_dims      = Util.getElementDimensions(@menuEl)

      x = if x + menu_dims.width > container_dims.width then container_dims.width - menu_dims.width else x
      y = if y + menu_dims.height > container_dims.height then container_dims.height - menu_dims.height else y

      Util.setStyle(@menuEl, 'left', x + 'px')
      Util.setStyle(@menuEl, 'top', y + 'px')
      Util.setStyle(@menuEl, 'visibility', 'visible')


    hide: ->
      if @visible
        @visible = false
        Util.setStyle(@menuEl, 'visibility', 'hidden')


    evClick: (ev) =>
      url = ev.target.parentElement.getAttribute('href')

      return unless url?

      if url.substr(0, ContextMenu.URL_INTERNAL_PREFIX.length) is ContextMenu.URL_INTERNAL_PREFIX
        ev.preventDefault()
        ev.stopPropagation()

        @internalCallback(url.substr(ContextMenu.URL_INTERNAL_PREFIX.length))

      @hide()
