define ['common/util/Events', 'common/util/Util', 'dcpv/view/NRAD'], (Events, Util, NRAD) ->

  class Hint

    @GROUP_CONTENT   : '[[device_name]]<br>[[metric_name]]<br>&nbsp;hi there: [[metric_value]][[metric_units]]'
    @OTHER_CONTENT   : '[[device_name]]<br>[[metric_name]]'
    @MORE_INFO_DELAY : 2000


    constructor: (@containerEl, @model) ->
      @hintEl  = $('tooltip')
      @visible = false


    show: (device, x, y) ->
      selected_metric = @model.selectedMetric()
      selected_stat   = @model.selectedMetricStat()
      metric_data     = @model.metricData()

      content = if device.groupId? then Hint.GROUP_CONTENT else Hint.OTHER_CONTENT

      val_obj    = metric_data.values[device.group][device.itemId]
      device_def = @model.deviceLookup()[device.group][device.itemId]
      hl_total   = if selected_stat is 'sum' then true else null
      hl_mean    = if selected_stat is 'mean' then true else null
      hl_max     = if selected_stat is 'max' then true else null
      hl_min     = if selected_stat is 'min' then true else null


      content = Util.substitutePhrase(content, 'device_name', device.name)
      content = Util.substitutePhrase(content, 'highlight_total', hl_total)
      content = Util.substitutePhrase(content, 'highlight_mean', hl_mean)
      content = Util.substitutePhrase(content, 'highlight_max', hl_max)
      content = Util.substitutePhrase(content, 'highlight_min', hl_min)
      content = Util.substitutePhrase(content, 'metric_name', selected_metric)
      content = Util.substitutePhrase(content, 'metric_units', if selected_metric? then @model.metricTemplates()[selected_metric].unit)
      content = Util.substitutePhrase(content, 'num_devices', device_def.numDevices)
      content = Util.substitutePhrase(content, 'metric_total', if val_obj? then val_obj.sum else null)
      content = Util.substitutePhrase(content, 'metric_min', if val_obj? then val_obj.min else null)
      content = Util.substitutePhrase(content, 'metric_max', if val_obj? then val_obj.max else null)
      content = Util.substitutePhrase(content, 'metric_mean', if val_obj? then val_obj.mean else null)
      content = Util.substitutePhrase(content, 'metric_value', if val_obj? then val_obj.value else null)
      content = Util.substitutePhrase(content, 'metric_num', if val_obj? then val_obj.num else null)
      content = Util.cleanUpSubstitutions(content)
      
      @visible          = true
      @hintEl.innerHTML = content

      # adjust when near to edges of the screen
      container_dims = Util.getElementDimensions(@containerEl)
      hint_dims      = Util.getElementDimensions(@hintEl)

      x = if x + hint_dims.width > container_dims.width then x - hint_dims.width else x
      y = if y + hint_dims.height > container_dims.height then y - hint_dims.height else y

      Util.setStyle(@hintEl, 'left', x + 'px')
      Util.setStyle(@hintEl, 'top', y + 'px')
      Util.setStyle(@hintEl, 'visibility', 'visible')

      @hintTmr = setTimeout(@getMore, Hint.MORE_INFO_DELAY)


    hide: ->
      if @visible
        clearTimeout(@hintTmr)
        Util.setStyle(@hintEl, 'visibility', 'hidden')
        @hintEl.innerHTML = ' '
        @visible = false


    getMore: =>
      console.log @hintEl
      Events.dispatchEvent(@hintEl, 'getHintInfo')


    appendData: (data) =>
      if @visible
        append = '<br>'
        for datum of data
          append += datum + ': ' + data[datum] + '<br>' if data[datum]? and data[datum] isnt ''

        @hintEl.innerHTML += append
