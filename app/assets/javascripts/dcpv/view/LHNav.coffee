define ['dcpv/view/DataCentreObject', 'dcpv/ViewModel', 'common/util/Util', 'dcpv/view/Rack', 'dcpv/view/NRAD', 'common/util/Events'], (DataCentreObject, ViewModel, Util, Rack, NRAD, Events) ->

  class LHNav

    @EDIT_MODE_ELEMENTS : [ 'something' ]
    @VIEW_MODE_ELEMENTS : [ 'something_else' ]

    @ITEM_EDIT_UPDATE_DELAY : 1000


    constructor: (@model) ->
      @editEls            = []
      @editElsNativeState = []
      for el_str in LHNav.EDIT_MODE_ELEMENTS
        el = $(el_str)
        if el
          @editEls.push(el)
          @editElsNativeState.push(Util.getStyle(el, 'display'))

      @viewEls            = []
      @viewElsNativeState = []
      for el_str in LHNav.VIEW_MODE_ELEMENTS
        el = $(el_str)
        if el
          @viewEls.push(el)
          @viewElsNativeState.push(Util.getStyle(el, 'display'))

      @itemWidthEl = $('item_width')
      @itemDepthEl = $('item_depth')

      @itemWidthEl.disabled = true
      @itemDepthEl.disabled = true

      manageable = $('edit_link').get('data-manageable') is 'true'

      Events.addEventListener($('edit_link'), 'click', @evSwitchToEdit) if manageable
      Events.addEventListener($('view_link'), 'click', @evSwitchToView)
      Events.addEventListener($('zoom_in_btn'), 'click', @evZoomIn)
      Events.addEventListener($('zoom_out_btn'), 'click', @evZoomOut)
      Events.addEventListener($('reset_zoom_btn'), 'click', @evResetZoom)
      Events.addEventListener($('unpositioned_select'), 'click', @evAddItem)
      Events.addEventListener($('save_btn'), 'click', @evSave)
      Events.addEventListener($('reset_filters'), 'click', @evResetFilters)
      Events.addEventListener($('delete_btn'), 'click', @evDeleteSelection)
      Events.addEventListener(@itemWidthEl, 'keyup', @evEditItem)
      Events.addEventListener(@itemDepthEl, 'keyup', @evEditItem)
      Events.addEventListener(@itemWidthEl, 'blur', @evBlurItemEdit)
      Events.addEventListener(@itemDepthEl, 'blur', @evBlurItemEdit)

      Util.setStyle($('edit_link'), 'display', 'none') unless manageable

      @model.mode.subscribe(@evSwitchMode)
      @model.selection.subscribe(@evSelectionChanged)

      @evSwitchMode(@model.mode())


    evSwitchMode: (mode) =>
      @mode = mode

      if mode is ViewModel.MODE_VIEW
        show_list = @viewEls
        disp_list = @viewElsNativeState
        hide_list = @editEls
      else
        show_list = @editEls
        disp_list = @editElsNativeState
        hide_list = @viewEls

      Util.setStyle(el, 'display', 'none') for el in hide_list
      Util.setStyle(el, 'display', disp_list[idx]) for el, idx in show_list


    evSelectionChanged: (selection) =>
      disabled = selection.length isnt 1 or not (selection[0] instanceof Rack or selection[0] instanceof NRAD)

      @itemWidthEl.disabled = disabled
      @itemDepthEl.disabled = disabled

      unless disabled
        item = selection[0]

        @itemWidthEl.value = DataCentreObject.pxToDim('x', item.width)
        @itemDepthEl.value = DataCentreObject.pxToDim('y', item.height)


    evSwitchToEdit: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      @model.mode(ViewModel.MODE_EDIT)


    evSwitchToView: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navSwitchToView')


    evEditItem: (ev) =>
      clearTimeout(@itemEditTmr)
      @itemEditTmr = setTimeout(@updateItem, LHNav.ITEM_EDIT_UPDATE_DELAY)


    evBlurItemEdit: (ev) =>
      clearTimeout(@itemEditTmr)
      @updateItem()


    updateItem: =>
      clearTimeout(@itemEditTmr)
      Events.dispatchEvent(document, 'navUpdateItem')


    evZoomIn: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navZoomIn')


    evZoomOut: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navZoomOut')


    evResetZoom: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navResetZoom')


    evAddItem: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navAddItem')


    evSave: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navSave')


    evResetFilters: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navResetFilters')


    evDeleteSelection: (ev) =>
      ev.preventDefault()
      ev.stopPropagation()
      Events.dispatchEvent(document, 'navDeleteSelection')
