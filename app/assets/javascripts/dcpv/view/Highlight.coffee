define ['common/gfx/SimpleRenderer', 'common/gfx/Easing', 'common/util/Util'], (SimpleRenderer, Easing, Util) ->

  class Highlight

    # statics overwritten by config
    @FILL          : '#ff00ff'
    @STATE_A       : { alpha: 0.5 }
    @STATE_B       : { alpha: 1 }
    @ANIM_DURATION : 500
    @FPS           : 60
    @ALPHA         : 0.3

    # cosntants and run-time assigned statics
    @TYPE_ELLIPSE   : 0
    @TYPE_RECTANGLE : 1


    constructor: (@containerEl) ->
      @gfx = new SimpleRenderer(@containerEl, 1, 1, 1, Highlight.FPS)
      Util.setStyle(@gfx.cvs, 'position', 'absolute')

      @initState       = {}
      @initState[prop] = Highlight.STATE_A[prop] for prop in Highlight.STATE_A
      

    show: (type, x, y, width, height) ->
      @hide()

      @visible = true
      @containerEl.appendChild(@gfx.cvs)
      Util.setStyle(@gfx.cvs, 'left', x + 'px')
      Util.setStyle(@gfx.cvs, 'top', y + 'px')

      @gfx.setDims(width, height)

      if type is Highlight.TYPE_ELLIPSE
        @asset = @gfx.addEllipse(width: width, height: height, fill: Highlight.FILL, alpha: Highlight.STATE_B.alpha ? Highlight.ALPHA)
      else
        @asset = @gfx.addRect(width: width, height: height, fill: Highlight.FILL, alpha: Highlight.STATE_B.alpha ? Highlight.ALPHA)

      @evStartPhaseA()


    hide: ->
      return unless @visible

      @visible = false

      @containerEl.removeChild(@gfx.cvs)
      @gfx.stopAnim(@asset)
      @gfx.remove(@asset)


    evStartPhaseA: =>
      @gfx.animate(@asset, Highlight.STATE_B, Highlight.ANIM_DURATION, Easing.Cubic.easeIn, @evStartPhaseB)


    evStartPhaseB: =>
      @gfx.animate(@asset, Highlight.STATE_A, Highlight.ANIM_DURATION, Easing.Cubic.easeOut, @evStartPhaseA)
