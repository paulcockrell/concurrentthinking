define ['dcpv/view/Breacher', 'dcpv/view/DataCentreObject', 'common/widgets/Metric', 'common/gfx/Easing', 'dcpv/ViewModel'], (Breacher, DataCentreObject, Metric, Easing, ViewModel) ->

  class Rack extends DataCentreObject

    # static overwritten by config
    @VIEW_STROKE       : '#000000'
    @VIEW_STROKE_WIDTH : 2
    @VIEW_ALPHA        : 0.5
    @VIEW_FILL         : '#ff00ff'
    @VIEW_FILL_ALPHA   : 0.9

    @EDIT_STROKE       : '#000000'
    @EDIT_STROKE_WIDTH : 2
    @EDIT_FILL         : '#5555ff'
    @EDIT_ALPHA        : 0.5

    @DRAG_STROKE       : '#000000'
    @DRAG_STROKE_WIDTH : 2
    @DRAG_FILL         : '#5555ff'
    @DRAG_ALPHA        : 0.2

    @SELECTED_ANIM : {}


    constructor: (def) ->
      super(def)

      @subscriptions.push(DataCentreObject.MODEL.mode.subscribe(@evSwitchMode))
      @subscriptions.push(DataCentreObject.MODEL.breaches.subscribe(@evSetBreaching))

      @metric = new Metric(@group, @itemId, DataCentreObject.INFO_GFX, @x, @y, @width, @height, DataCentreObject.MODEL)
      @metric.setActive(true)

      @groupLookup          = DataCentreObject.MODEL.deviceLookup().byGroupId[@groupId]
      @groupLookup.instance = @
      
      @draw()


    destroy: ->
      super()
      console.log 'Rack.destroy'
      @groupLookup.instance = null
      @groupLookup.position = null

      @metric.destroy()
      @breacher.destroy() if @breaching
      DataCentreObject.HWARE_GFX.remove(asset_id) for asset_id in @assets


    setCoords: (x, y, use_dims = false) ->
      super(x, y, use_dims)

      if @breaching
        @breacher.destroy()
        @breacher = new Breacher(Breacher.TYPE_RECTANGULAR, @group, @itemId, DataCentreObject.ALERT_GFX, @x, @y, @width, @height, DataCentreObject.MODEL)

      @metric.setCoords(@x, @y)


    setSize: (width, height, use_dims = false) ->
      super(width, height, use_dims)

      if @breaching
        @breacher.destroy()
        @breacher = new Breacher(Breacher.TYPE_RECTANGULAR, @group, @itemId, DataCentreObject.ALERT_GFX, @x, @y, @width, @height, DataCentreObject.MODEL)

      @metric.setSize(@width, @height)
      @setLabel(DataCentreObject.MODEL.showLabels())
      @draw()


    getDragImg: ->
      cvs        = document.createElement('canvas')
      cvs.width  = @width
      cvs.height = @height
      ctx        = cvs[0].getContext('2d')

      ctx.rect(0, 0, @width, @height)
      ctx.globalAlpha = Rack.EDIT_ALPHA
      ctx.strokeStyle = Rack.EDIT_STROKE
      ctx.lineWidth   = Rack.EDIT_STROKE_WIDTH
      ctx.stroke()
      ctx.fillStyle   = Rack.EDIT_FILL
      ctx.fill()
      ctx.drawImage(@labelImg, (@width - @labelImg.width) / 2, (@height - @labelImg.height) / 2)
      cvs


    draw: =>
      renew_selection = @selected
      @deselect() if renew_selection

      DataCentreObject.HWARE_GFX.remove(asset_id) for asset_id in @assets
      @assets = []

      fade_factor = if @included then 1 else DataCentreObject.EXCLUSION_ALPHA

      if DataCentreObject.MODEL.mode() is ViewModel.MODE_VIEW
        @metric.setActive(@included)
        @assets.push(DataCentreObject.HWARE_GFX.addRect(x: @x, y: @y, width: @width, height: @height, alpha: Rack.VIEW_ALPHA * fade_factor, stroke: Rack.VIEW_STROKE, strokeWidth: Rack.VIEW_STROKE_WIDTH))
        @assets.push(DataCentreObject.HWARE_GFX.addRect(x: @x, y: @y, width: @width, height: @height, alpha: Rack.VIEW_FILL_ALPHA * fade_factor, fill: Rack.VIEW_FILL))
      else
        @metric.setActive(false)
        @assets.push(DataCentreObject.HWARE_GFX.addRect(x: @x, y: @y, width: @width, height: @height, alpha: Rack.EDIT_ALPHA * fade_factor, stroke: Rack.EDIT_STROKE, strokeWidth: Rack.EDIT_STROKE_WIDTH, fill: Rack.EDIT_FILL))

      @assets.push(DataCentreObject.HWARE_GFX.addImg(img: @labelImg, x: @x + ((@width - @labelImg.width) / 2), y: @y + ((@height - @labelImg.height) / 2), alpha: fade_factor))

      @select() if renew_selection
        

    select: ->
      return if @selected

      @selected = true
      DataCentreObject.HWARE_GFX.setAttributes(@assets[Rack.SELECTED_ANIM.assetIdx], Rack.SELECTED_ANIM.targetState)
      @evSelectFadedOut()


    evSelectFadedIn: =>
      DataCentreObject.HWARE_GFX.animate(@assets[Rack.SELECTED_ANIM.assetIdx], Rack.SELECTED_ANIM.targetState, Rack.SELECTED_ANIM.duration, Easing.Cubic.easeOut, @evSelectFadedOut, null, [@assets[1]])


    evSelectFadedOut: =>
      DataCentreObject.HWARE_GFX.animate(@assets[Rack.SELECTED_ANIM.assetIdx], Rack.SELECTED_ANIM.nativeState, Rack.SELECTED_ANIM.duration, Easing.Cubic.easeIn, @evSelectFadedIn, null, [@assets[1]])


    deselect: ->
      return unless @selected

      @selected = false
      DataCentreObject.HWARE_GFX.stopAnim(@assets[Rack.SELECTED_ANIM.assetIdx])
      @draw()


    setBreaching: (breaches, mode) ->
      if breaches[@group][@itemId] and mode is ViewModel.MODE_VIEW
        return if @breaching

        @breaching = true
        @breacher  = new Breacher(Breacher.TYPE_RECTANGULAR, @group, @itemId, DataCentreObject.ALERT_GFX, @x, @y, @width, @height, DataCentreObject.MODEL)
      else
        @breacher.destroy() if @breaching
        @breaching = false


    evSetBreaching: (breaches) =>
      @setBreaching(breaches, DataCentreObject.MODEL.mode())


    evSwitchMode: (mode) =>
      @draw()
      @setBreaching(DataCentreObject.MODEL.breaches(), mode)
      
