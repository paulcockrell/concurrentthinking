define ['dcpv/view/DataCentreObject', 'dcpv/view/Rack', 'dcpv/view/Sensor', 'dcpv/view/NRAD', 'dcpv/util/AssetManager', 'dcpv/view/Highlight', 'common/gfx/SimpleRenderer', 'common/util/Util', 'common/util/Events', 'common/gfx/Easing', 'dcpv/ViewModel', 'dcpv/view/ContextMenu'], (DataCentreObject, Rack, Sensor, NRAD, AssetManager, Highlight, SimpleRenderer, Util, Events, Easing, ViewModel, ContextMenu) ->

  class DataCentre

    # statics overwritten by config
    @FPS                   : 60
    @PADDING               : 10
    @CANVAS_MAX_DIMENSION  : 8000
    @INFO_FADE_DURATION    : 5
    @ZOOM_DURATION         : 500
    @STEP_ZOOM_AMOUNT      : 0.2
    @NUDGE_AMOUNT          : 1
    @SUPER_NUDGE_AMOUNT    : 10
    @SNAP_DISTANCE         : 5
    @IMAGE_UPDATE_THROTTLE : 100
    @DEFAULT_RACK_WIDTH    : 1
    @DEFAULT_RACK_DEPTH    : 1
    @DEFAULT_CHASSIS_WIDTH : 1
    @DEFAULT_CHASSIS_DEPTH : 1
    @OUTPUT_IMAGE_MAX_DIM  : 6000

    @SELECTION_BOX_STROKE       : '#ff00ff'
    @SELECTION_BOX_STROKE_WIDTH : 2
    @SELECTION_BOX_ALPHA        : 0.5

    @ERR_SAVE_FAIL : 'Unable to process youre request at this time, please try again later'


    # constants and run-time assigned statics
    @MIN_ZOOM : null
    @MAX_ZOOM : null


    constructor: (def, @dCEl, @model) ->
      @id        = def.id
      double_pad = DataCentre.PADDING * 2
      img        = AssetManager.CACHE[def.img]
      scale      = (DataCentre.CANVAS_MAX_DIMENSION - double_pad) / (if img.width > img.height then img.width else img.height)
      console.log "DataCentre.CANVAS_MAX_DIMENSION", DataCentre.CANVAS_MAX_DIMENSION
      console.log "DC#constructor - scale", scale
      cvs        = document.createElement('canvas')
      cvs.width  = img.width * scale
      cvs.height = img.height * scale
      console.log "++++++ img", img
      console.log "++++++ cvs.width", cvs.width
      console.log "++++++ cvs.hieght", cvs.height
      cvs.getContext('2d').drawImage(img, 0, 0, cvs.width, cvs.height)

      img = cvs

      @createGfx(@dCEl, img.width + double_pad, img.height + double_pad)

      @coordReferenceEl = @bgGfx.cvs
      @dCUnitDims       = { width: def.xDim, height: def.yDim }
      @selection        = []

      DataCentreObject.init(
        @model,
        @hwareGfx,
        @infoGfx,
        @alertGfx,
        scale,
        { x: def.xPx, y: def.yPx },
        { x: def.xDim, y: def.yDim },
        { x: DataCentre.PADDING, y: DataCentre.PADDING }
      )
      Sensor.init()

      @highlight = new Highlight(@dCEl)
      @ctxMenu   = new ContextMenu(@coordReferenceEl, @model, @evActionContextMenu)

      @setSelectionBounds()
      @setModelSelection()

      @scrollbarOffset = Util.getScrollbarThickness()
      @initZoom()
      @centreDC()
      
      @bgGfx.addImg(img: cvs, x: DataCentre.PADDING, y: DataCentre.PADDING)

      @model.mode.subscribe(@evSwitchMode)
      @model.highlighted.subscribe(@evHighlight)

      @racks = []
      for rack_def in def.items.racks
        @racks.push(new Rack(rack_def)) if rack_def.position?

      @sensors = []
      for sensor_def in def.items.sensors
        @sensors.push(new Sensor(sensor_def)) if sensor_def.position?

      @chassis = []
      for chassis_def in def.items.chassis
        @chassis.push(new NRAD(chassis_def)) if chassis_def.position?

      @requestDCImageUpdate()


    createGfx: (container_el, width, height) ->
      console.log "++ container_el", container_el
      console.log "width", width
      console.log "height", height
      @bgGfx    = new SimpleRenderer(container_el, width, height, 1, DataCentre.FPS)
      @hwareGfx = new SimpleRenderer(container_el, width, height, 1, DataCentre.FPS)
      @infoGfx  = new SimpleRenderer(container_el, width, height, 1, DataCentre.FPS)
      @alertGfx = new SimpleRenderer(container_el, width, height, 1, DataCentre.FPS)
      console.log "DataCentre#createGfx", @bgGfx.cvs
      Util.setStyle(@bgGfx.cvs, 'position', 'absolute')
      Util.setStyle(@hwareGfx.cvs, 'position', 'absolute')
      Util.setStyle(@hwareGfx.cvs, 'top', 0)
      Util.setStyle(@hwareGfx.cvs, 'left', 0)
      Util.setStyle(@infoGfx.cvs, 'position', 'absolute')
      Util.setStyle(@infoGfx.cvs, 'top', 0)
      Util.setStyle(@infoGfx.cvs, 'left', 0)
      Util.setStyle(@alertGfx.cvs, 'position', 'absolute')
      Util.setStyle(@alertGfx.cvs, 'top', 0)
      Util.setStyle(@alertGfx.cvs, 'left', 0)

    
    updateLayout: ->
      showing_all = @bgGfx.scale is @zoomPresets[0]
      @calculateZoomPresets()
      @setScale(@zoomPresets[0]) if showing_all
      @centreDC()


    dcImageThrottle: =>
      @hwareGfx.drawComplete = null
      clearTimeout(@dcImgThrottler)
      @dcImgThrottler = setTimeout(@updateDCImage, DataCentre.IMAGE_UPDATE_THROTTLE)


    updateDCImage: =>
      start = (new Date()).getTime()
      width      = @bgGfx.cvs.width
      height     = @bgGfx.cvs.height
      cvs        = document.createElement('canvas')
      cvs.width  = width
      cvs.height = height
      ctx        = cvs.getContext('2d')
      
      ctx.drawImage(@bgGfx.cvs, 0, 0)
      ctx.drawImage(@hwareGfx.cvs, 0, 0)

      @model.dcImage(cvs)


    # takes into account scroll, positioning of canvas layers and current zoom level
    # returns the centre coordinates relative to the canvas elements of the users current view
    getViewCentre: ->
      # dims         = @dCEl.getCoordinates()
      centre_x     = @dCEl.scrollLeft + (@dCEl.width()) / 2
      centre_y     = @dCEl.scrollTop + (@dCEl.height()) / 2
      cvs_offset_x = Util.getStyleNumeric(@coordReferenceEl, 'left')
      cvs_offset_y = Util.getStyleNumeric(@coordReferenceEl, 'top')

      return { x: (centre_x - cvs_offset_x) / @scale, y: (centre_y - cvs_offset_y) / @scale }


    addItem: (def) ->
      return if def.position?

      view_centre   = @getViewCentre()
      view_centre.x = DataCentreObject.pxToDim('x', view_centre.x)
      view_centre.y = DataCentreObject.pxToDim('y', view_centre.y)

      if def.group is 'racks'
        width  = DataCentre.DEFAULT_RACK_WIDTH
        height = DataCentre.DEFAULT_RACK_DEPTH
      else
        width  = DataCentre.DEFAULT_CHASSIS_WIDTH
        height = DataCentre.DEFAULT_CHASSIS_DEPTH

      def.position =
        x: view_centre.x - (width / 2)
        y: view_centre.y - (height / 2)
        w: width
        d: height

      switch def.group
        when 'racks'
          item = new Rack(def)
          @racks.push(item)
        when 'sensors'
          item = new Sensor(def)
          @sensors.push(item)
        when 'chassis'
          @chassis.push(new NRAD(def))

      unless @modified
        @modified = true
        @model.modified(true)

      # this looks odd, but is necessary. When making defining the position
      # attribute above we're making changes to a complex object which is 
      # already referenced in the model. Because of this the changes are already
      # present in the model as it is one and the same object, however knockout
      # cannot detect such changes and so we have to set the value as itself
      # to force knockout to notify any subscribers and update dependent values
      @model.deviceLookup(@model.deviceLookup())


    # update the model data centre image once the next frame has completed drawing
    requestDCImageUpdate: =>
      @hwareGfx.drawComplete = @dcImageThrottle


    setItemSize: (item, width, depth) ->
      console.log 'setItemSize'
      changed   = false
      new_width = DataCentreObject.pxToDim('x', item.width)
      new_depth = DataCentreObject.pxToDim('y', item.height)

      if not isNaN(width) and width isnt new_width
        changed   = true
        new_width = width
      
      if not isNaN(depth) and depth isnt new_depth
        changed   = true
        new_depth = depth

      if changed
        item.setSize(new_width, new_depth, true)
        @setSelectionBounds()

        unless @modified
          @modified = true
          @model.modified(true)


    calculateZoomPresets: ->
      dims = {height: $(@dCEl).height(), width: $(@dCEl).width()}
      x_scale  = (dims.width - @scrollbarOffset) / @bgGfx.width
      console.log "1"
      y_scale  = (dims.height - @scrollbarOffset) / @bgGfx.height
      console.log "2"
      show_all = if x_scale > y_scale then y_scale else x_scale
      console.log "3"
      console.log "++ calculateZoomPresets"
      console.log "x_scale", x_scale
      console.log "y_scale", y_scale
      console.log "show_all", show_all
      max_dim             = if @bgGfx.width > @bgGfx.height then @bgGfx.width else @bgGfx.height
      DataCentre.MAX_ZOOM = if max_dim > DataCentre.CANVAS_MAX_DIMENSION then DataCentre.CANVAS_MAX_DIMENSION / max_dim else 1

      show_all            = DataCentre.MAX_ZOOM if show_all > DataCentre.MAX_ZOOM
      DataCentre.MIN_ZOOM = show_all
      @zoomPresets = []
      @zoomPresets.push(show_all)
      @zoomPresets.push(DataCentre.MAX_ZOOM) if show_all < DataCentre.MAX_ZOOM


    initZoom: ->
      @zoomPresets = []
      @zoomIdx     = 0

      @calculateZoomPresets()
      @setScale(@zoomPresets[0])


    setScale: (scale) ->
      console.log "DataCentre#setScale", scale
      @scale = scale
      @model.scale(scale)
      @bgGfx.setScale(scale)
      @hwareGfx.setScale(scale)
      @infoGfx.setScale(scale)
      @alertGfx.setScale(scale)


    centreDC: ->
      dc_dims = {width: $(@dCEl).width(), height: $(@dCEl).height()}
      cvs_dims = {width: @bgGfx.cvs.width, height: @bgGfx.cvs.height}
      offset   = { x: dc_dims.width - @scrollbarOffset - cvs_dims.width, y: dc_dims.height - @scrollbarOffset - cvs_dims.height }
      console.log "offset", offset
      offset.x = 0 if offset.x < 0
      offset.y = 0 if offset.y < 0

      centre_x = offset.x / 2 + 'px'
      centre_y = offset.y / 2 + 'px'

      Util.setStyle(@dCEl, 'overflow', 'scroll')

      Util.setStyle(@bgGfx.cvs, 'left', centre_x)
      Util.setStyle(@hwareGfx.cvs, 'left', centre_x)
      Util.setStyle(@infoGfx.cvs, 'left', centre_x)
      Util.setStyle(@alertGfx.cvs, 'left', centre_x)

      Util.setStyle(@bgGfx.cvs, 'top', centre_y)
      Util.setStyle(@hwareGfx.cvs, 'top', centre_y)
      Util.setStyle(@infoGfx.cvs, 'top', centre_y)
      Util.setStyle(@alertGfx.cvs, 'top', centre_y)


    getDeviceAt: (x, y) ->
      # if the hardware canvas layer has an empty pixel at the given coordinates
      # we can safely assume there is no object there
      return null if @hwareGfx.ctx.getImageData(x * @scale, y * @scale, 1, 1).data[3] is 0

      for rack in @racks
        if x > rack.x and x < rack.x + rack.width and y > rack.y and y < rack.y + rack.height
          return rack

      for sensor in @sensors
        if x > sensor.x and x < sensor.x + sensor.width and y > sensor.y and y < sensor.y + sensor.height
          return sensor

      for chassis in @chassis
        if x > chassis.x and x < chassis.x + chassis.width and y > chassis.y and y < chassis.y + chassis.height
          return chassis

      return null


    showContextMenu: (abs_coords, rel_coords) ->
      item = @getDeviceAt(rel_coords.x / @scale, rel_coords.y / @scale)
      console.log 'DataCentre.showContextMenu', item

      @ctxMenu.show(item, abs_coords.x, abs_coords.y)


    selectItem: (x, y, multi_select) ->
      @ctxMenu.hide()

      x /= @scale
      y /= @scale
      
      item = @getDeviceAt(x, y)

      if item?
        if @model.mode() is ViewModel.MODE_EDIT then @editSelectItem(item, multi_select) else @viewSelectItem(item, multi_select)
      else
        return if multi_select
        if @model.mode() is ViewModel.MODE_EDIT
          @clearSelection()
        else
          groups       = @model.groups()
          blank        = {}
          blank[group] = {} for group in groups
          @model.activeSelection(false)
          @model.selectedDevices(blank)


    editSelectItem: (item, multi_select) ->
      if multi_select
        selection_idx = @getSelectionIdx(item)
        if selection_idx is -1
          item.select()
          @selection.push(item)
        else
          item.deselect()
          @selection.splice(selection_idx, 1)

        @setSelectionBounds()
        @setModelSelection()
      else
        @clearSelection()

        item.select()
        @selection = [item]
        @setSelectionBounds()

        @setModelSelection()


    viewSelectItem: (item, multi_select) ->
      selected_devices = @model.selectedDevices()

      if multi_select
        new_val = selected_devices[item.group][item.itemId] = !selected_devices[item.group][item.itemId]

        if new_val
          active_selection = true
        else
          active_selection = false
          groups = @model.groups()
          for group in groups
            for id of selected_devices[group]
              if selected_devices[group][id]
                active_selection = true
                break

            break if active_selection

        @model.activeSelection(active_selection)
        @model.selectedDevices(selected_devices)

      else
        groups  = @model.groups()
        new_sel = {}
        new_sel[group] = {} for group in groups
        new_sel[item.group][item.itemId] = true
        @model.activeSelection(true)
        @model.selectedDevices(new_sel)


    setSelectionBounds: ->
      if @selection.length is 0
        @selectionBounds = { left: 0, right: 0, top: 0, bottom: 0 }
        @model.enableDelete(false)
        return

      @model.enableDelete(true)
      @selectionBounds = { left: Number.MAX_VALUE, right: -Number.MAX_VALUE, top: Number.MAX_VALUE, bottom: -Number.MAX_VALUE }
      for item in @selection
        item_right  = item.x + item.width
        item_bottom = item.y + item.height

        @selectionBounds.left   = item.x      if item.x < @selectionBounds.left
        @selectionBounds.right  = item_right  if item_right > @selectionBounds.right
        @selectionBounds.top    = item.y      if item.y < @selectionBounds.top
        @selectionBounds.bottom = item_bottom if item_bottom > @selectionBounds.bottom


    setModelSelection: ->
      @model.selection(@selection)
      @requestDCImageUpdate()


    clearSelection: ->
      @selectionBounds = { left: 0, right: 0, top: 0, bottom: 0 }
      item.deselect() for item in @selection
      @selection = []
      @model.enableDelete(false)


    deleteSelection: ->
      return unless @selection.length > 0

      for item in @selection
        item.destroy()
        @[item.group].splice(@[item.group].indexOf(item), 1)

      #@model.deviceLookup(@model.deviceLookup())
      @selection = []
      @clearSelection()
      @setModelSelection()

      unless @modified
        @modified = true
        @model.modified(true)

      @model.deviceLookup(@model.deviceLookup())


    # nudges the current selection. X and y define the direction and should be supplied as either 1, -1 or 0
    # returns boolean 'modified'
    nudgeSelection: (x, y, super_nudge) ->
      @highlight.hide()

      nudge_amount = if super_nudge then DataCentre.SUPER_NUDGE_AMOUNT else DataCentre.NUDGE_AMOUNT

      nudge_x = x * nudge_amount
      nudge_y = y * nudge_amount

      # retrict within canvas boundaries
      nudge_x = -@selectionBounds.left                     if nudge_x < 0 and @selectionBounds.left + nudge_x < 0
      nudge_x = @hwareGfx.width - @selectionBounds.right   if nudge_x > 0 and @selectionBounds.right + nudge_x > @hwareGfx.width
      nudge_y = -@selectionBounds.top                      if nudge_y < 0 and @selectionBounds.top + nudge_y < 0
      nudge_y = @hwareGfx.height - @selectionBounds.bottom if nudge_y > 0 and @selectionBounds.bottom + nudge_y > @hwareGfx.height

      @selectionBounds.left   += nudge_x
      @selectionBounds.right  += nudge_x
      @selectionBounds.top    += nudge_y
      @selectionBounds.bottom += nudge_y
      
      item.setCoords(item.x + nudge_x, item.y + nudge_y) for item in @selection

      modified = @selection.length > 0 and (nudge_x isnt 0 or nudge_y isnt 0)

      if modified
        unless @modified
          @modified = true
          @model.modified(true)

        @hwareGfx.redraw()
        @dcImageThrottle()

      return modified


    createFXLayer: (container, x, y, width, height, scale = 1, fps = DataCentre.FPS) ->
      fx = new SimpleRenderer(container, width, height, scale, fps)
      Util.setStyle(fx.cvs, 'position', 'absolute')
      Util.setStyle(fx.cvs, 'left', x + 'px')
      Util.setStyle(fx.cvs, 'top', y + 'px')
      fx
                                        
    
    testHover: (x, y) ->
      x = x / @scale
      y = y / @scale

      @oldHoverDevice = @hoverDevice
      @hoverDevice    = @getDeviceAt(x, y)

      @model.highlighted(@hoverDevice) if @hoverDevice isnt @oldHoverDevice

      Util.setStyle(@dCEl, 'cursor', if @hoverDevice? then 'move' else 'auto') if @model.mode() is ViewModel.MODE_EDIT


    startBoxDrag: (x, y) ->
      @boxStartCoords = x: x, y: y
      @draggingBox    = true

      offset_x = Util.getStyleNumeric(@bgGfx.cvs, 'left')
      offset_y = Util.getStyleNumeric(@bgGfx.cvs, 'top')

      @fx  = @createFXLayer(@dCEl, offset_x, offset_y, @bgGfx.cvs.width, @bgGfx.cvs.height, 1)
      @box = @fx.addRect(
        x           : x
        y           : y
        width       : 1
        height      : 1
        stroke      : DataCentre.SELECTION_BOX_STROKE
        strokeWidth : DataCentre.SELECTION_BOX_STROKE_WIDTH
        alpha       : DataCentre.SELECTION_BOX_ALPHA
      )


    dragBox: (x, y) ->
      if x < @boxStartCoords.x
        box_x = x
        box_w = @boxStartCoords.x - x
      else
        box_x = @boxStartCoords.x
        box_w = x - @boxStartCoords.x

      if y < @boxStartCoords.y
        box_y = y
        box_h = @boxStartCoords.y - y
      else
        box_y = @boxStartCoords.y
        box_h = y - @boxStartCoords.y

      @fx.setAttributes(@box, { x: box_x, y: box_y, width: box_w, height: box_h })
      @fx.redraw()
      

    stopBoxDrag: (x, y) ->
      @draggingBox = false

      @fx.destroy()

      if x < @boxStartCoords.x
        box_l = x
        box_r = @boxStartCoords.x
      else
        box_l = @boxStartCoords.x
        box_r = x

      if y < @boxStartCoords.y
        box_t = y
        box_b = @boxStartCoords.y
      else
        box_t = @boxStartCoords.y
        box_b = y

      box =
        left   : box_l / @scale
        right  : box_r / @scale
        top    : box_t / @scale
        bottom : box_b / @scale
      
      @selectWithinBox(box, true)
       

    startItemDrag: (x, y) ->
      return unless @hoverDevice?

      @highlight.hide()

      x /= @scale
      y /= @scale

      @startCoords = { x: x, y: y }

      if @getSelectionIdx(@hoverDevice) is -1
        @selection.push(@hoverDevice)
        @setSelectionBounds()
        @setModelSelection()

      @draggingItem = true
      @dragOffset   = { x: (x - @hoverDevice.x) * @scale, y: (y - @hoverDevice.y) * @scale }
      @fx           = @createFXLayer(@dCEl, Util.getStyleNumeric(@bgGfx.cvs, 'left'), Util.getStyleNumeric(@bgGfx.cvs, 'top'), @bgGfx.width, @bgGfx.height, @scale)

      @dragAssets      = []
      @coordOffsets    = []
      for item, idx in @selection
        asset = @fx.addImg(img: item.getDragImg(), x: item.x, y: item.y)

        if item is @hoverDevice
          @snapReference    = asset
          @snapReferenceIdx = idx

        @dragAssets.push(asset)
        @coordOffsets.push({ x: item.x - x, y: item.y - y })

        item_right  = item.x + item.width
        item_bottom = item.y + item.height
      
      @selectionOffset = x: @selectionBounds.left - x, y: @selectionBounds.top - y
      @snapAnchors     = []

      if @model.deviceSnapping()
        device_lookup = @model.deviceLookup()
        groups        = @model.groups()
        for group in groups
          category = device_lookup[group]
          for id of category
            item = category[id].instance

            continue if not item? or @getSelectionIdx(item) isnt -1 or item instanceof Sensor

            x1 = item.x
            x2 = item.x + item.width
            y1 = item.y
            y2 = item.y + item.height

            @snapAnchors.push(
              { x: x1, y: y1 }
              { x: x2, y: y1 }
              { x: x2, y: y2 }
              { x: x1, y: y2 }
            )

      @hoverDevice.select()


    dragItem: (x, y) ->
      x /= @scale
      y /= @scale

      ref_x_1 = x + @coordOffsets[@snapReferenceIdx].x
      ref_x_2 = ref_x_1 + @fx.getAttribute(@snapReference, 'width')
      ref_y_1 = y + @coordOffsets[@snapReferenceIdx].y
      ref_y_2 = ref_y_1 + @fx.getAttribute(@snapReference, 'height')

      snap_tests = [
        { x: ref_x_1, y: ref_y_1 }
        { x: ref_x_1, y: ref_y_2 }
        { x: ref_x_2, y: ref_y_2 }
        { x: ref_x_2, y: ref_y_1 }
      ]

      # test snapping
      snap = dist: Number.MAX_VALUE

      for anchor in @snapAnchors
        for point in snap_tests
          dist = Math.pow(point.x - anchor.x, 2) + Math.pow(point.y - anchor.y, 2)
          snap = { dist: dist, p1: point, p2: anchor } if dist < snap.dist

      # override drag coords if within snap distance
      if snap.dist < Math.pow(DataCentre.SNAP_DISTANCE, 2)
        x += snap.p2.x - snap.p1.x
        y += snap.p2.y - snap.p1.y

      # override drag coords if selection bounds move outside canvas bounds
      sel_left   = @selectionOffset.x + x
      sel_right  = sel_left + (@selectionBounds.right - @selectionBounds.left)
      sel_top    = @selectionOffset.y + y
      sel_bottom = sel_top + (@selectionBounds.bottom - @selectionBounds.top)

      x -= sel_left if sel_left < 0
      x -= sel_right - @hwareGfx.width if sel_right > @hwareGfx.width
      y -= sel_top if sel_top < 0
      y -= sel_bottom - @hwareGfx.height if sel_bottom > @hwareGfx.height

      @dragCoords = x: x, y: y

      @fx.setAttributes(item, x: @coordOffsets[idx].x + x, y: @coordOffsets[idx].y + y) for item, idx in @dragAssets


    stopItemDrag: (x, y) ->
      return unless @dragCoords

      x /= @scale
      y /= @scale

      item.setCoords(@coordOffsets[idx].x + @dragCoords.x, @coordOffsets[idx].y + @dragCoords.y) for item, idx in @selection

      @draggingItem = false
      @dragCoords   = null

      unless @modified
        @modified = true
        @model.modified(true)

      @fx.destroy()
      @hwareGfx.redraw()


    highlightDevice: (device) ->
      if device?
        @highlight.show(
          if device instanceof Sensor then Highlight.TYPE_ELLIPSE else Highlight.TYPE_RECTANGLE,
          Util.getStyleNumeric(@bgGfx.cvs, 'left') + (device.x * @scale),
          Util.getStyleNumeric(@bgGfx.cvs, 'top') + (device.y * @scale),
          device.width * @scale,
          device.height * @scale
        )
      else
        @highlight.hide()


    getSelectionIdx: (item) ->
      select_idx = -1
      for selected, idx in @selection
        if item is selected
          select_idx = idx
          break

      return select_idx


    # Select devices within a box
    # Inclusive selections (object touches box) and exlusive selections (object is contained by box)
    # Box object requires properties: top, bottom, left, right
    selectWithinBox: (box, inclusive) ->
      groups        = @model.groups()
      device_lookup = @model.deviceLookup()
      mode          = @model.mode()

      if mode is ViewModel.MODE_EDIT
        @clearSelection()
      else
        active_selection = false
        new_sel          = {}
        new_sel[group]   = {} for group in groups

      if inclusive
        for group in groups
          for id of device_lookup[group]
            item = device_lookup[group][id].instance

            continue unless item?

            item_right  = item.x + item.width
            item_bottom = item.y + item.height

            # this would be a little more performant if the following conditions were mashed into one line
            # since not all would be evaluated every time. It is however a lot easier to read done this way
            test_left        = box.left >= item.x and box.left <= item_right
            test_right       = box.right >= item.x and box.right <= item_right
            test_contained_h = box.left < item.x and box.right > item_right
            test_top         = box.top >= item.y and box.top <= item_bottom
            test_bottom      = box.bottom >= item.y and box.bottom <= item_bottom
            test_contained_v = box.top < item.y and box.bottom > item_bottom

            if (test_left or test_right or test_contained_h) and (test_top or test_bottom or test_contained_v)
              if mode is ViewModel.MODE_EDIT
                @selection.push(item)
                item.select()
              else
                active_selection = true
                new_sel[item.group][item.itemId] = true
      else
        for group in groups
          for id of device_lookup[group]
            item = device_lookup[group][id].instance

            continue unless item?

            test_contained_h = box.left <= item.x and box.right >= item.x + item.width
            test_contained_v = box.top <= item.y and box.bottom >= item.y + item.height

            if test_contained_h and test_contained_v
              if mode is ViewModel.MODE_EDIT
                @selection.push(item)
                item.select()
              else
                active_selection = true
                new_sel[item.group][item.itemId] = true

      if mode is ViewModel.MODE_EDIT
        @setSelectionBounds()
        @setModelSelection()
      else
        @model.activeSelection(active_selection)
        @model.selectedDevices(new_sel)


    stepZoom: (direction, centre_x, centre_y) ->
      unless centre_x? and centre_y?
        view_centre = @getViewCentre()
        centre_x    = view_centre.x * @scale
        centre_y    = view_centre.y * @scale

      new_scale = @scale + (DataCentre.STEP_ZOOM_AMOUNT * @scale * direction)
      new_scale = DataCentre.MIN_ZOOM if new_scale < DataCentre.MIN_ZOOM
      new_scale = DataCentre.MAX_ZOOM if new_scale > DataCentre.MAX_ZOOM

      @quickZoom(centre_x, centre_y, new_scale)


    # direction indicates wether to zoom in or out, should be 1 or -1
    zoomToPreset: (direction = 1, centre_x, centre_y) ->
      return if @zooming

      centre_x  = @coordReferenceEl.width / 2 unless centre_x?
      centre_y  = @coordReferenceEl.height / 2 unless centre_y?
      @zoomIdx += direction
      # cycle through presets
      @zoomIdx = 0 if @zoomIdx >= @zoomPresets.length
      @zoomIdx = @zoomPresets.length - 1 if @zoomIdx < 0
      @quickZoom(centre_x, centre_y, @zoomPresets[@zoomIdx])


    # down render everything to a single image and scale that rather than each individual asset
    quickZoom: (centre_x, centre_y, new_scale) ->
      return if @zooming

      dims         = @dCEl.getCoordinates()
      dims.width  -= @scrollbarOffset
      dims.height -= @scrollbarOffset

      # restrict requested scale to max and min values
      if new_scale > DataCentre.MAX_ZOOM
        @zoomIdx  = 0
        new_scale = DataCentre.MAX_ZOOM
      if new_scale < DataCentre.MIN_ZOOM
        @zoomIdx  = @zoomPresets.length - 1
        new_scale = DataCentre.MIN_ZOOM

      # calculate centre coords according to target scale
      centre_x = centre_x / (@scale / new_scale)
      centre_y = centre_y / (@scale / new_scale)

      # store current scroll
      @scrollOffset =
        x: @dCEl.scrollLeft
        y: @dCEl.scrollTop

      # calculate target coords according to top-left
      target_x = centre_x - (dims.width / 2)
      target_y = centre_y - (dims.height / 2)

      target_width  = @bgGfx.width * new_scale
      target_height = @bgGfx.height * new_scale

      # determin rack centreing offset at target scale
      offset_x = (dims.width - target_width) / 2
      offset_y = (dims.height - target_height) / 2
      offset_x = 0 if offset_x < 0
      offset_y = 0 if offset_y < 0

      # calculate boundaries of zoomed canvas
      lh_bound  = -offset_x
      rh_bound  = target_width - dims.width + offset_x
      top_bound = -offset_y
      btm_bound = target_height - dims.height + offset_y

      # retrict target coords to zoomed boundaries
      if target_x > rh_bound then target_x = rh_bound
      if target_x < lh_bound then target_x = lh_bound
      if target_y > btm_bound then target_y = btm_bound
      if target_y < top_bound then target_y = top_bound

      # store target offset
      @targetOffset =
        x: target_x
        y: target_y

      current_offset_x = Util.getStyleNumeric(@bgGfx.cvs, 'left')
      current_offset_y = Util.getStyleNumeric(@bgGfx.cvs, 'top')
      lazy_factor      = 2

      # only zoom if new scale is different to current and target scroll
      # is a significant change (greater than lazy factor)
      if new_scale is @scale and (Math.abs(@targetOffset.x - @scrollOffset.x - current_offset_x) < lazy_factor and Math.abs(@targetOffset.y - @scrollOffset.y - current_offset_y) < lazy_factor)
        Events.dispatchEvent(@dCEl, 'dataCentreZoomComplete')
        return

      # commence zoom
      #@hint.hide()
      @ctxMenu.hide()
      #@removeHighlight()
      @zooming     = true
      @targetScale = new_scale

      current_offset_x = Util.getStyleNumeric(@bgGfx.cvs, 'left')
      current_offset_y = Util.getStyleNumeric(@bgGfx.cvs, 'top')

      # composite        = document.createElement('canvas')
      composite = $('<canvas>')
      composite.width  = @bgGfx.cvs.width
      composite.height = @bgGfx.cvs.height
      ctx              = composite[0].getContext('2d')
      ctx.drawImage(@bgGfx.cvs, 0, 0)
      ctx.drawImage(@hwareGfx.cvs, 0, 0)

      @fx      = @createFXLayer(@dCEl, 0, 0, dims.width, dims.height)
      @bgImg   = @fx.addImg({ img: composite, x: current_offset_x - @scrollOffset.x, y: current_offset_y - @scrollOffset.y })
      @fx2     = @createFXLayer(@dCEl, 0, 0, dims.width, dims.height)
      info_img = @fx2.addImg({ img: @infoGfx.cvs, x: current_offset_x - @scrollOffset.x, y: current_offset_y - @scrollOffset.y })
      @fx2.animate(info_img, { alpha: 0 }, DataCentre.INFO_FADE_DURATION, Easing.Quad.easeOut, @evZoomReady)

      @hwareGfx.pauseAnims()
      @infoGfx.pauseAnims()
      @alertGfx.pauseAnims()
      @dCEl.removeChild(@bgGfx.cvs)
      @dCEl.removeChild(@hwareGfx.cvs)
      @dCEl.removeChild(@infoGfx.cvs)
      @dCEl.removeChild(@alertGfx.cvs)

      # force immediate draw or we'll have a blank image for one frame
      @fx.redraw()
      @fx2.redraw()


    evZoomReady: =>
      @fx2.removeAll()

      relative_scale = @targetScale / @bgGfx.scale

      @infoGfx.setScale(@targetScale)
      @alertGfx.setScale(@targetScale)

      # decide wether to show rack labels
      #show_name_label = @targetScale >= RackSpace.NAME_LBL_SCALE_CUTOFF
      #show_u_labels   = @targetScale >= RackSpace.U_LBL_SCALE_CUTOFF

      #for rack in @racks
      #  rack.showNameLabel(show_name_label)
      #  rack.showULabels(show_u_labels)
      @fx.animate(@bgImg, { x: -@targetOffset.x, y: -@targetOffset.y, width: @bgGfx.cvs.width * relative_scale, height: @bgGfx.cvs.height * relative_scale }, DataCentre.ZOOM_DURATION, Easing.Cubic.easeOut, @evDCZoomComplete)


    evDCZoomComplete: =>
      # fade in info layer
      info_img = @fx2.addImg({ img: @infoGfx.cvs, x: -@targetOffset.x, y: -@targetOffset.y, alpha: 0 })
      @fx2.animate(info_img, { alpha: 1 }, DataCentre.INFO_FADE_DURATION, Easing.Quad.easeOut, @evZoomComplete)


    evZoomComplete: =>
      @fx.destroy()
      @fx2.destroy()

      @bgGfx.setScale(@targetScale)
      @hwareGfx.setScale(@targetScale)
      @infoGfx.setScale(@targetScale)
      @alertGfx.setScale(@targetScale)
      @dCEl.appendChild(@bgGfx.cvs)
      @dCEl.appendChild(@hwareGfx.cvs)
      @dCEl.appendChild(@infoGfx.cvs)
      @dCEl.appendChild(@alertGfx.cvs)

      @dCEl.scrollLeft = @targetOffset.x
      @dCEl.scrollTop  = @targetOffset.y

      @hwareGfx.resumeAnims()
      @infoGfx.resumeAnims()
      @alertGfx.resumeAnims()
      @scale = @targetScale
      @model.scale(@scale)
      @zooming = false
      #@synchroniseZoomIdx()
      @centreDC()

      Events.dispatchEvent(@dCEl, 'dataCentreZoomComplete')


    evSwitchMode: (mode) =>
      if mode is ViewModel.MODE_EDIT
        if @model.activeSelection()
          blank  = {}
          groups = @model.groups()
          blank[group] = {} for group in groups
          @model.activeSelection(false)
          @model.selectedDevices(blank)

        @saveState()
      else
        Util.setStyle(@dCEl, 'cursor', 'auto')
        @clearSelection()

      @requestDCImageUpdate()


    saveState: ->
      @savedState = @getState()


    getState: ->
      @modified = false
      @model.modified(false)

      state  = {}
      groups = @model.groups()
      
      for group in groups
        state[group] = {}
        instances     = @[group]
        for item in instances
          state[group][item.itemId] = {
            x: item.x
            y: item.y
            w: item.width
            d: item.height
          }

      return state


    saveChanges: ->
      @modified = false
      @model.modified(false)

      return @getChangeSet()


    getChangeSet: ->
      additions     = {}
      deletions     = []
      modifications = {}

      groups  = @model.groups()
      add_idx = 0
      
      for group in groups
        instances = @[group]
        for item in instances
          old_item    = @savedState[group][item.itemId]
          change_list = null

          if not old_item?
            change_list = additions
            ++add_idx
          else if item.x isnt old_item.x or item.y isnt old_item.y or item.width isnt old_item.w or item.height isnt old_item.d
            change_list = modifications

          if change_list?
            change_list[item.def.pos_id ? add_idx] =
              item_id   : item.itemId
              item_type : item.group
              x_pos     : DataCentreObject.pxToDim('x', item.x)
              y_pos     : DataCentreObject.pxToDim('y', item.y)
              x_dim     : DataCentreObject.pxToDim('x', item.width)
              y_dim     : DataCentreObject.pxToDim('y', item.height)

      device_lookup = @model.deviceLookup()

      for group in groups
        for item_id of @savedState[group]
          instances = @[group]
          found     = false
          for item in instances
            if Number(item.itemId) is Number(item_id)
              found = true
              break

          deletions.push(device_lookup[group][item_id].pos_id) unless found

      def = {dcpv_positions: {}}
      def.dcpv_positions[@id] = { additions: additions, deletions: deletions, modifications: modifications }

      return def


    saveComplete: ->
      @saveState()


    saveFailed: ->
      @modified = true
      @model.modified(true)
      alert_dialog(DataCentre.ERR_SAVE_FAILED)


    isUnsaved: ->
      groups = @model.groups()
      
      for group in groups
        instances = @[group]
        for item in instances
          old_item    = @savedState[group][item.itemId]
          if not old_item?
            return true
          else if item.x isnt old_item.x or item.y isnt old_item.y or item.width isnt old_item.w or item.height isnt old_item.d
            return true

      device_lookup = @model.deviceLookup()

      for group in groups
        for item_id of @savedState[group]
          instances = @[group]
          found     = false
          for item in instances
            if Number(item.itemId) is Number(item_id)
              found = true
              break

          return true unless found


      return false


    loadState: (state) ->
      @clearSelection()

      groups        = @model.groups()
      delete_lists  = {}
      device_lookup = @model.deviceLookup()

      for group in groups
        instances = @[group]

        idx = 0
        len = instances.length
        while idx < len
          item     = instances[idx]
          old_item = state[group][item.itemId]
          if old_item?
            item.setCoords(old_item.x, old_item.y) if item.x isnt old_item.x or item.y isnt old_item.y
            item.setSize(old_item.w, old_item.d) if item.width isnt old_item.w or item.height isnt old_item.d

            ++idx
          else
            update_device_lookup = true

            item.destroy()
            instances.splice(idx, 1)

            --len

      for group in groups
        for item_id of state[group]
          instances = @[group]
          found     = false
          for item in instances
            if Number(item.itemId) is Number(item_id)
              found = true
              break

          unless found
            console.log group, item_id, device_lookup
            def          = device_lookup[group][item_id]
            def.position =
              x: DataCentreObject.pxToDim('x', state[group][item_id].x)
              y: DataCentreObject.pxToDim('y', state[group][item_id].y)
              w: DataCentreObject.pxToDim('x', state[group][item_id].w)
              d: DataCentreObject.pxToDim('y', state[group][item_id].d)

            console.log def
            switch group
              when 'racks'
                @racks.push(new Rack(def))
              when 'sensors'
                @sensors.push(new Sensor(def))
              when 'chassis'
                @chassis.push(new NRAD(def))
                
        @model.deviceLookup(@model.deviceLookup()) if update_device_lookup


    discardChanges: ->
      @loadState(@savedState)


    evHighlight: (device) =>
      @highlightDevice(device)


    evActionContextMenu: (action_str) =>
      console.log 'DataCentre.evActionContextMenu'
      parts = action_str.split(',')

      switch parts[0]
        when 'focusOn'
          @focusOn(parts[1], parts[2])
        when 'reset'
          Events.dispatchEvent(@dCEl, 'dataCentreReset')


    getImage: ->
      width  = @coordReferenceEl.width
      height = @coordReferenceEl.height

      # cvs           = document.createElement('canvas')
      cvs = $('<canvas>')
      cvs.width     = width
      cvs.height    = height
      ctx           = cvs[0].getContext('2d')
      ctx.fillStyle = '#ffffff'

      ctx.fillRect(0, 0, width, height)

      ctx.drawImage(@bgGfx.cvs, 0, 0)
      ctx.drawImage(@hwareGfx.cvs, 0, 0)
      ctx.drawImage(@infoGfx.cvs, 0, 0)

      cvs


    focusOn: (group, id) ->
      target = @model.deviceLookup()[group][id]
      return unless target?

      target = target.instance

      centre_x = (target.x + (target.width / 2)) * @scale
      centre_y = (target.y + (target.height / 2)) * @scale

      dims    = Util.getElementDimensions(@dCEl)
      scale_x = (dims.width - @scrollbarOffset) / target.width
      scale_y = (dims.height - @scrollbarOffset) / target.height
      scale   = if scale_x > scale_y then scale_y else scale_x

      @quickZoom(centre_x, centre_y, scale)

      return if @model.mode() is ViewModel.MODE_EDIT

      new_sel            = {}
      groups             = @model.groups()
      new_sel[group2]    = {} for group2 in groups
      new_sel[group][id] = true
      @model.activeSelection(true)
      @model.selectedDevices(new_sel)
