require ['common', 'bootstrap', 'utils', 'jquery.dataTables', 'jquery.resizable', 'jquery.sortable'], () ->
  console.log "Tables JS loaded"

  #===== Check all checbboxes =====// 
  $(".titleIcon input:checkbox").click ->
    checkedStatus = @checked
    $("#checkAll tbody tr td:first-child input:checkbox").each ->
      @checked = checkedStatus
      if checkedStatus is @checked
        $(this).closest(".checker > span").removeClass "checked"
        $(this).closest("table tbody tr").removeClass "thisRow"
      if @checked
        $(this).closest(".checker > span").addClass "checked"
        $(this).closest("table tbody tr").addClass "thisRow"
  
  
  $ ->
    $("#checkAll tbody tr td:first-child input[type=checkbox]").change ->
      $(this).closest("tr").toggleClass "thisRow", @checked

