requirejs.config({
    "baseUrl": "/assets/javascripts",
    "paths": {
      "jquery": "lib/jquery/jquery.min",
      "jquery.ui": "lib/jquery/jquery-ui.min",
      "bootstrap": "lib/jquery/files/bootstrap",
      "excanvas": "lib/jquery/plugins/charts/excanvas.min",
      "jquery.flot": "lib/jquery/plugins/charts/jquery.flot",
      "jquery.flot.orderBars": "lib/jquery/plugins/charts/jquery.flot.orderBars",
      "jquery.flot.pie": "lib/jquery/plugins/charts/jquery.flot.pie",
      "jquery.flot.resize": "lib/jquery/plugins/charts/jquery.flot.resize",
      "jquery.flot.sparkline.min": "lib/jquery/plugins/charts/jquery.flot.sparkline.min",
      "jquery.dataTables": "lib/jquery/plugins/tables/jquery.dataTables",
      "jquery.resizable": "lib/jquery/plugins/tables/jquery.resizable",
      "jquery.sortable": "lib/jquery/plugins/tables/jquery.sortable",
      "jquery.select.2": "lib/jquery/plugins/forms/jquery.select2.min",
      "jquery.jgrowl": "lib/jquery/plugins/ui/jquery.jgrowl",
      "dialog": "lib/jquery/plugins/ui/jquery.dialog",
      "chart": "lib/jquery/charts/chart",
      "hBar": "lib/jquery/charts/hBar",
      "bar": "lib/jquery/charts/bar",
      "updating": "lib/jquery/charts/updating",
      "pie": "lib/jquery/charts/pie",
      "iButton": "lib/jquery/plugins/forms/jquery.ibutton",
      "uniform": "lib/jquery/plugins/forms/jquery.uniform",
      "collapsible": "lib/jquery/plugins/ui/jquery.collapsible.min",
      "elfinder": "lib/jquery/plugins/others/jquery.elfinder",
      "easytabs": "lib/jquery/plugins/ui/jquery.easytabs.min",
      "autotab": "lib/jquery/plugins/forms/jquery.autotab",
      "coords": "lib/jquery/plugins/ui/jquery.coords",
      "utils": "lib/jquery/plugins/ui/utils",
      "draggable": "lib/jquery/plugins/ui/jquery.draggable",
      "collision": "lib/jquery/plugins/ui/jquery.collision",
      "watch": "lib/jquery/plugins/others/jquery.watch",
      "common": "./common",
      "tables": "lib/jquery/files/tables",
      "sourcerer": "lib/jquery/plugins/ui/jquery.sourcerer",
      "resizable": "lib/jquery/plugins/tables/jquery.resizable",
      "sortable": "lib/jquery/plugins/tables/jquery.sortable",
      "fileTree": "lib/jquery/plugins/ui/jquery.fileTree",
      "fancybox": "lib/jquery/plugins/ui/jquery.fancybox",
      "colorpicker": "lib/jquery/plugins/ui/jquery.colorpicker",
      "timeentry": "lib/jquery/plugins/ui/jquery.timeentry.min",
      "validationEngine": "lib/jquery/plugins/forms/jquery.validationEngine",
      "validate": "lib/jquery/plugins/wizards/jquery.validate",
      "tipsy": "lib/jquery/plugins/ui/jquery.tipsy",
      "progress": "lib/jquery/plugins/ui/jquery.progress",
      "plupload": "lib/jquery/plugins/uploader/jquery.plupload.queue",
      "formwizard": "lib/jquery/plugins/wizards/jquery.form.wizard",
      "functions": "lib/jquery/files/functions",
      "login": "lib/jquery/files/login",
      "xBreadcrumbs": "lib/jquery/plugins/ui/jquery.breadcrumbs",
      "dynamicTable": "common/widgets/DynamicTable",
      "progressBar": "common/widgets/ProgressBar",
      "sliders": "common/widgets/Sliders",
      "threejs": "threejs/three",
      "firstPersonControls": "threejs/FirstPersonControls",
      "pointerLockControls": "threejs/PointerLockControls"
    },
    "shim": {
        "jquery.ui": ["jquery"],
        "bootstrap": ["jquery"],
        "common": ["jquery"],
        "progress": ["jquery"],
        "progressBar": ["progress"],
        "uniform": ["jquery"],
        "excanvas": ["jquery"],
        "jquery.flot": ["jquery", "excanvas"],
        "jquery.flot.orderBars": ["jquery.flot"],
        "jquery.flot.pie": ["jquery.flot"],
        "jquery.flot.resize": ["jquery.flot"],
        "jquery.flot.sparkline.min": ["jquery.flot"],
        "jquery.select.2": ["jquery"],
        "chart": ["jquery.flot"],
        "hBar": ["jquery.flot"],
        "bar": ["jquery.flot"],
        "updating": ["jquery.flot"],
        "pie": ["jquery.flot.pie", "chart"],
        "iButton": ["jquery"],
        "collapsible": ["jquery"],
        "elfinder": ["jquery", "jquery.ui"],
        "autotab": ["elfinder"],
        "easytabs": ["autotab"],
        "coords": ["jquery"],
        "utils": ["jquery"],
        "draggable": ["jquery"],
        "collision": ["jquery"],
        "watch": ["jquery"],
        "validationEngine": ["jquery"],
        "validate": ["jquery", "validationEngine"],
        "plupload": ["jquery"],
        "formwizard": ["jquery"],
        "login": ["jquery.ui"],
        "jquery.sortable": ["jquery"],
        "jquery.resizable": ["jquery"],
        "jquery.dataTables": ["jquery"],
        "tables": ["jquery.sortable", "jquery.resizable", "jquery.dataTables"],
        "functions": ["jquery", "sourcerer", "sortable", "fileTree", "resizable", "fancybox", "colorpicker", "timeentry", "validate", "plupload", "formwizard"],
        "xbreadcrumbs": ["jquery"],
        "sliders": ["jquery"],
        "progressBar": ["jquery"],
        "dialog": ["jquery", "jquery.ui", "draggable", "iButton", "resizable"],
        "dynamicTable": ["tables"],
        "jquery.jgrowl": ["jquery.ui"],
        "threejs": ["jquery"],
        "firstPersonControls": ["threejs"],
        "pointerLockControls": ["threejs"]
    }
});

define(['jquery', 'jquery.ui'], function($) {
  console.log("Require initiated");
});
