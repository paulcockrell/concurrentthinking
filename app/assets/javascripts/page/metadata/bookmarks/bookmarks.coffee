require ['common', 'bootstrap', 'utils', 'dialog', 'jquery.jgrowl'], () ->
  console.log "Bookmarks JS loaded"

  $(".gallery ul li").hover (->
    $(this).children(".actions").show "fade", 200
  ), ->
    $(this).children(".actions").hide "fade", 200

  $ ->
    $("#dialog-remove").dialog
      dialogClass: "ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable std42-dialog  elfinder-dialog elfinder-dialog-error elfinder-dialog-active"
      resizable: false
      height: 'auto'
      width: 320
      modal: true
      autoOpen: false
      buttons:
        "Delete bookmark": ->
          $(this).dialog "close"
          $.jGrowl('Bookmark deleted', {theme: 'nNote nWarning'})
  
        Cancel: ->
          $(this).dialog "close"

  $(".remove").click ->
    $("#dialog-remove").dialog( "open" )
    
  $ ->
    $("#dialog-edit").dialog
      dialogClass: "ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable std42-dialog  elfinder-dialog elfinder-dialog-error elfinder-dialog-active"
      resizable: false
      height: 'auto'
      width: 320
      modal: true
      autoOpen: false
      buttons:
        "Save": ->
          $(this).dialog "close"
          $.jGrowl('Bookmark updated', {theme: 'nNote nSuccess', sticky: true})
  
        Cancel: ->
          $(this).dialog "close"

  $(".edit").click ->
    $("#dialog-edit").dialog( "open" )
    
