require ['page/physical/datacentre/objects/device', 'page/physical/datacentre/objects/rack', 'common', 'bootstrap', 'utils', 'dynamicTable', 'autotab', 'easytabs', 'sliders', 'progress', 'jquery.select.2', 'threejs', 'pointerLockControls'], (Device, Rack) ->
  console.log "Datacenter 3D JS loaded"

  class ThreeDeePlanView
    container  = $('#dc_3D')
    clock      = new THREE.Clock()
    camera     = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 100000)
    #camera.position.set(-500, 1000, 1000)
    controls   = new THREE.PointerLockControls( camera );
    scene      = new THREE.Scene()
    renderer   = new THREE.WebGLRenderer(antialias: true)
    renderer.setClearColor(0x666633, 0.7);
    image_path = "/assets/images/physical/datacentre/3d/"
    time       = Date.now()
    racks      = []
    image_format = ".jpg"
    ball       = undefined
    mouseX     = 0
    mouseY     = 0
    windowHalfX = window.innerWidth * 0.5
    windowHalfY = window.innerHeight * 0.5
    a  = -200
    r  = 300
    da = 0.05
    leftPadding = 405
    topPadding  = 255
    ray = undefined
    objects = []

    constructor: (parameters = {}) ->
      this[key] = value for own key, value of parameters

      @pointerBlock()
      @buildStage()
      @setupControls()
      @buildEventListeners() 
      @attatchRenderer()

    setupControls: =>
      scene.add( controls.getObject() );
  
    buildStage: =>
      @buildLighting()
      @buildFloor()
      @buildRacks()
      renderer.setSize window.innerWidth - leftPadding, window.innerHeight - topPadding

    buildEventListeners: () =>
      # stage resize
      window.addEventListener "resize", @onWindowResize, false

    attatchRenderer: () =>
      document.body.appendChild renderer.domElement
      container.append renderer.domElement

    buildLighting: () ->
      scene.add new THREE.AmbientLight( 0x404040 )

      sunLight = new THREE.DirectionalLight( 0xffeedd )
      sunLight.position.set( 0.3, -1, -1 ).normalize()
      scene.add( sunLight )

      pointLight = new THREE.PointLight( 0xffeedd, 1.5 )
      pointLight.position.set( -500, 1000, 1000 )
      pointLight.castShadow = true
      scene.add( pointLight )

    buildRacks: ->
      for bob in [{num: 6, xStart: -650, xInc: 90, zStart: -390, zInc: 0}, {num: 6, xStart: 15, xInc: 90, zStart: -90, zInc: 0}, {num: 6, xStart: -650, xInc: 90, zStart: 200, zInc: 0}]
        console.log "Bob", bob
        for itter in  [1..bob.num]
          console.log "building a rack!"
          rack = new Rack
            id: itter
            name: "Rack "+itter
            height: 10
            position_x: bob.xStart += bob.xInc
            position_y: 0
            position_z: bob.zStart += bob.zInc

          rack.addDevices [
            id: 1
            name: "Device one"
            start_u: 0
            image_positive_z: 'dell-r210-front.png'
            image_negative_z: 'dell-r210-rear.png'
          ,
            id: 2
            name: "Device two"
            start_u: 2
            image_positive_z: 'voltaire_1u_24port_IB_switch_front.png'
            image_negative_z: 'voltaire_1u_24port_IB_switch_rear.png'
          ,
            id: 4
            name: "Device four"
            start_u: 3
            image_positive_z: 'lcd_console_1u.png'
            image_negative_z: 'dell-r310-rear.png'
          ,
            id: 5
            name: "Device five"
            start_u: 4
            image_positive_z: 'dell-r310-front.png'
            image_negative_z: 'dell-r310-rear.png'
          ,
            id: 6
            name: "Device six"
            start_u: 5
            _height_scale: 3
            image_positive_z: 'eaton_51xx_3u_ups.png'
            image_negative_z: 'ups_rear_3u.png'
          ,
            id: 7
            name: "Device seven"
            start_u: 9
            image_positive_z: 'dell-r310-front.png'
            image_negative_z: 'dell-r310-rear.png'
          ]
          racks.push rack

      scene.add rack.to3D() for rack in racks

    buildFloor: () ->
      # floor
      floor_material = new THREE.MeshLambertMaterial(map: THREE.ImageUtils.loadTexture(image_path+"dcpv.png"))
      floor = new THREE.Mesh(new THREE.PlaneGeometry(2000, 2000, 20, 20), floor_material)
      floor.position.y = -5
      floor.rotation.x = -Math.PI * 0.5
      floor.receiveShadow = true
      scene.add floor
    
    onWindowResize: (e) =>
      camera.aspect = window.innerWidth / window.innerHeight
    
      camera.updateProjectionMatrix()
      renderer.setSize window.innerWidth - leftPadding, window.innerHeight - topPadding

    render: () =>
      requestAnimationFrame @render
      controls.isOnObject false
     
      controls.update( Date.now() - time )

      renderer.render scene, camera

      time = Date.now()

     pointerBlock: () =>
       blocker = document.getElementById("blocker")
       instructions = document.getElementById("instructions")
       
       havePointerLock = "pointerLockElement" of document or "mozPointerLockElement" of document or "webkitPointerLockElement" of document
       if havePointerLock
         element = document.body
         pointerlockchange = (event) ->
           if document.pointerLockElement is element or document.mozPointerLockElement is element or document.webkitPointerLockElement is element
             controls.enabled = true
             blocker.style.display = "none"
           else
             controls.enabled = false
             blocker.style.display = "-webkit-box"
             blocker.style.display = "-moz-box"
             blocker.style.display = "box"
             instructions.style.display = ""
       
         pointerlockerror = (event) ->
           instructions.style.display = ""
       
         # Hook pointer lock state change events
         document.addEventListener "pointerlockchange", pointerlockchange, false
         document.addEventListener "mozpointerlockchange", pointerlockchange, false
         document.addEventListener "webkitpointerlockchange", pointerlockchange, false
         document.addEventListener "pointerlockerror", pointerlockerror, false
         document.addEventListener "mozpointerlockerror", pointerlockerror, false
         document.addEventListener "webkitpointerlockerror", pointerlockerror, false
         instructions.addEventListener "click", ((event) ->
           instructions.style.display = "none"
           
           # Ask the browser to lock the pointer
           element.requestPointerLock = element.requestPointerLock or element.mozRequestPointerLock or element.webkitRequestPointerLock
           if /Firefox/i.test(navigator.userAgent)
             fullscreenchange = (event) ->
               if document.fullscreenElement is element or document.mozFullscreenElement is element or document.mozFullScreenElement is element
                 document.removeEventListener "fullscreenchange", fullscreenchange
                 document.removeEventListener "mozfullscreenchange", fullscreenchange
                 element.requestPointerLock()
       
             document.addEventListener "fullscreenchange", fullscreenchange, false
             document.addEventListener "mozfullscreenchange", fullscreenchange, false
             element.requestFullscreen = element.requestFullscreen or element.mozRequestFullscreen or element.mozRequestFullScreen or element.webkitRequestFullscreen
             element.requestFullscreen()
           else
             element.requestPointerLock()
         ), false
       else
         instructions.innerHTML = "Your browser doesn't seem to support Pointer Lock API"

  # Execute!!
  $(window).load ->
    tdpv = new ThreeDeePlanView
    tdpv.render()
    
    #===== Easy tabs =====
    

    $('#tab-container').easytabs
      animationSpeed: 300
      collapsible: false
      tabActiveClass: "clicked"
      
    
    #===== Tabs =====

      
    $( ".tabs" ).tabs()

    tabs = $( ".tabs-sortable" ).tabs()
    tabs.find( ".ui-tabs-nav" ).sortable
      axis: "x",
      stop: () ->
        tabs.tabs( "refresh" )

    # default mode
    $("#progress1").anim_progressbar()
    
    # from second #5 till 15
    iNow = new Date().setTime(new Date().getTime() + 5 * 1000) # now plus 5 secs
    iEnd = new Date().setTime(new Date().getTime() + 15 * 1000) # now plus 15 secs
    $("#progress2").anim_progressbar # Type here!
      start: iNow
      finish: iEnd
      interval: 2

    #===== Select2 dropdowns =====//
    $(".select").select2()
    $(".selectMultiple").select2()
    $("#loadingdata").select2
      placeholder: "Enter at least 1 character"
      allowClear: true
      minimumInputLength: 1
      query: (query) ->
        data = results: []
        i = undefined
        j = undefined
        s = undefined
        i = 1
        while i < 5
          s = ""
          j = 0
          while j < i
            s = s + query.term
            j++
          data.results.push
            id: query.term + i
            text: s
    
          i++
        query.callback data
    
    $("#maxselect").select2 maximumSelectionSize: 3
    $("#minselect").select2
      minimumInputLength: 2
      width: "element"
    
    $("#minselect2").select2 minimumInputLength: 2
    $("#disableselect, #disableselect2").select2 "disable"
