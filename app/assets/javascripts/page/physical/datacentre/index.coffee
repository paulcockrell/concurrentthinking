require ['common', 'bootstrap', 'utils', 'dynamicTable', 'autotab', 'easytabs', 'jquery.jgrowl'], () ->
  console.log "Datacenter index JS loaded"
  #===== Easy tabs =====
  

  $('#tab-container').easytabs
    animationSpeed: 300
    collapsible: false
    tabActiveClass: "clicked"
    
  
  #===== Tabs =====

    
  $( ".tabs" ).tabs()

  tabs = $( ".tabs-sortable" ).tabs()
  tabs.find( ".ui-tabs-nav" ).sortable
    axis: "x",
    stop: () ->
      tabs.tabs( "refresh" )

  $ ->
    $("#dialog-remove").dialog
      dialogClass: "ui-dialog ui-widget ui-widget-content ui-corner-all ui-dialog-titlebar ui-dialog-buttonpane elfinder-dialog elfinder-dialog-error ui-dialog-content ui-dialog-buttons std42-dialog"
      resizable: false
      height: 'auto'
      width: 320
      modal: true
      autoOpen: false
      draggable: false
      hide: 250
      buttons:
        "Delete data centre": ->
          $(this).dialog "close"
          $.jGrowl('Data centre deleted', {theme: 'nNote nWarning'})
  
        Cancel: ->
          $(this).dialog "close"

  $(".remove").click ->
    $("#dialog-remove").dialog( "open" )
    
