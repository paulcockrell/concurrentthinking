#define ->
define ['page/physical/datacentre/objects/device', 'threejs'], (Device) ->
  class Rack
    position_x: 0
    position_y: 0  
    position_z: 0
    _facing: 0
    _default_colour: 0xC0C0C0
    _scale_multiplier: 25   # scale 1 = 1 * 100..
    _width_scale: 3          # 5 = full width, 2.5 = half width
    _depth_scale: 8.5          # 5 = full depth, 2.5 = half depth
    _height_scale: 1         # U height
    rotateBy: 0 
    id: undefined
    name: undefined
    entity: undefined
    height: 42    
    devices: []
 
    constructor: (parameters = {}) ->
      this[key] = value for own key, value of parameters
      @buildRack()

    dimensionX: () ->
      @_width_scale * @_scale_multiplier
  
    dimensionY: () ->
      @height * @_height_scale * @_scale_multiplier + (@panelThickness() * 2)
  
    dimensionZ: () ->
      @_depth_scale * @_scale_multiplier
    
    panelThickness: () ->
      @_scale_multiplier / 5

    buildGeometry: (x = @dimensionX(), y = @dimensionY(), z = @dimensionZ()) ->
      new THREE.CubeGeometry x, y, z
 
    generatePanel: (x, y, z) ->
      mesh = new THREE.Mesh @buildGeometry(x, y, z), new THREE.MeshLambertMaterial(color: @_default_colour)
      mesh.overdraw = true
      mesh.castShadow = true
      mesh.receiveShadow = false

      mesh

    buildRack: () =>
      @entity = new THREE.Object3D()
      @entity.name = @name
      
      # right rack panel
      #
      panel = @generatePanel(@panelThickness(), @dimensionY(), @dimensionZ())
      panel.position.set @position_x + (@dimensionX() / 2) + (@panelThickness() / 2), @position_y + (@dimensionY() / 2) - (@panelThickness() / 2), @position_z + 0
      @entity.add panel

      # left rack panel
      #
      panel = @generatePanel(@panelThickness(), @dimensionY(), @dimensionZ())
      panel.position.set -1 * (@dimensionX() / 2) - (@panelThickness() / 2) + @position_x, @position_y + (@dimensionY() / 2) - (@panelThickness() / 2), @position_z + 0
      @entity.add panel

      # top of rack
      # 
      panel = @generatePanel(@dimensionX(), @panelThickness(), @dimensionZ())
      panel.position.set @position_x + 0, @position_y + @dimensionY() - @panelThickness(), @position_z + 0
      @entity.add panel
     
      # bottom of rack
      #
      panel = @generatePanel(@dimensionX(), @panelThickness(), @dimensionZ())
      panel.position.set @position_x + 0, @position_y + 0, @position_z + 0
      @entity.add panel

    de2ra: (degree) ->
      degree * ( Math.PI / 180)

    addDevice: (opts) ->
      opts['position_x'] = @position_x
      opts['position_y'] = @position_y
      opts['position_z'] = @position_z
      opts['_width_scale'] = @_width_scale
      opts['_depth_scale'] = @_depth_scale
      opts['_height_scale'] = @_height_scale unless opts['_height_scale']? > 0
      device = new Device(opts).to3D()
      @devices.push device
      @entity.add device

    addDevices: (opts) ->
      for opt in opts
        @addDevice(opt)

    to3D: () ->
      @entity.rotateY @de2ra(@rotateBy)
      @entity
    
