define ->
  class Device
    position_x: 0   # this is calculated for us
    position_y: 0   # this is calculated for us  
    position_z: 0   # this is calculated for us
    _default_colour: 0x000000
    _image_path: "/assets/images/devices/"
    _image_format: ".png"
    
    id: undefined
    name: undefined
    association_object_class: undefined
    association_id: undefined
    entity: undefined        # this is the renderered object
    scale_multiplier: 25  # scale 1 = 1 * 100..
    _width_scale: 2        # 2 = full width, 1 = half width
    _depth_scale: 2        # 2 = full depth, 1 = half depth
    _height_scale: 1       # U height
    start_u: 1
    
    image_positive_x: undefined
    image_negative_x: undefined
    image_positive_y: undefined
    image_negative_y: undefined
    image_positive_z: undefined
    image_negative_z: undefined
   
 
    constructor: (parameters = {}) ->
      this[key] = value for own key, value of parameters
      @build()
   
    defaultImage: () -> 
      @_image_path + "default" + @_image_format

    dimensionX: () ->
      @_width_scale * @scale_multiplier
  
    dimensionY: () ->
      @_height_scale * @scale_multiplier
  
    dimensionZ: () ->
      @_depth_scale * @scale_multiplier
    
    addImage: (side, img) ->
      console.log "Adding new image to Device object"
      @images[side] = img
   
    addImages: (img_hsh) ->
      console.log "Adding new images to Device object"
  
    # images: () ->
    #   [(@_image_path + @image_positive_x) or @defaultImage(), 
    #    (@_image_path + @image_negative_x) or @defaultImage(), 
    #    (@_image_path + @image_positive_y) or @defaultImage(), 
    #    (@_image_path + @image_negative_y) or @defaultImage(), 
    #    (@_image_path + @image_positive_z) or @defaultImage(),
    #    (@_image_path + @image_negative_z) or @defaultImage()] 
    images: () ->
      pos_x = if @image_positive_x then (@_image_path + @image_positive_x) else @defaultImage()
      neg_x = if @image_negative_x then (@_image_path + @image_negative_x) else @defaultImage()
      pos_y = if @image_positive_y then (@_image_path + @image_positive_y) else @defaultImage()
      neg_y = if @image_negative_y then (@_image_path + @image_negative_y) else @defaultImage()
      pos_z = if @image_positive_z then (@_image_path + @image_positive_z) else @defaultImage()
      neg_z = if @image_negative_z then (@_image_path + @image_negative_z) else @defaultImage()

      [ pos_x
      , neg_x
      , pos_y
      , neg_y
      , pos_z
      , neg_z ]  
    zPosition: () ->
      @_z_position

    xPosition: () ->
      @_x_position
 
    yPosition: () ->
      # half the devices height + start_u * multiplier + half the height of the rack base panel thickness
      (@dimensionY() / 2) + (@start_u * @scale_multiplier) + 3 
 
    buildTexture: () ->
      materialArray = []
      i = 0

      while i < 6
        materialArray.push new THREE.MeshBasicMaterial(
          map: THREE.ImageUtils.loadTexture(@images()[i])
          doubleSided: false
          side: THREE.FrontSide
        )
        i++
      new THREE.MeshFaceMaterial(materialArray)
  
    buildGeometry: () ->
      new THREE.CubeGeometry @dimensionX(), @dimensionY(), @dimensionZ()
 
    build: () ->
      @entity = new THREE.Mesh @buildGeometry(), @buildTexture()
      @entity.overdraw = true
      console.log "building device at postion: "+@yPosition()+" with a height of "+@dimensionY()
      @entity.position.set @position_x + 0, @position_y + @yPosition(), @position_z + 0
      @entity.castShadow = true
      @entity.receiveShadow = false

    to3D: () ->
      @entity
    
