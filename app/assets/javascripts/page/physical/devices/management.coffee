require ['common', 'bootstrap', 'utils', 'dynamicTable', 'easytabs', 'jquery.select.2', 'progress', 'jquery.jgrowl'], () ->
  console.log "Device JS loaded"
  
  
  #===== Easy tabs =====
  

  $('#tab-container').easytabs
    animationSpeed: 300
    collapsible: false
    tabActiveClass: "clicked"

  #===== Tabs =====

    
  $( ".tabs" ).tabs()

  tabs = $( ".tabs-sortable" ).tabs()
  tabs.find( ".ui-tabs-nav" ).sortable
    axis: "x",
    stop: () ->
      tabs.tabs( "refresh" )

  #===== Select2 dropdowns =====//
  $(".select").select2()
  $(".selectMultiple").select2()
  $("#loadingdata").select2
    placeholder: "Enter at least 1 character"
    allowClear: true
    minimumInputLength: 1
    query: (query) ->
      data = results: []
      i = undefined
      j = undefined
      s = undefined
      i = 1
      while i < 5
        s = ""
        j = 0
        while j < i
          s = s + query.term
          j++
        data.results.push
          id: query.term + i
          text: s
  
        i++
      query.callback data


  # default mode
  $("#progress1").anim_progressbar()
  
  # from second #5 till 15
  iNow = new Date().setTime(new Date().getTime() + 5 * 1000) # now plus 5 secs
  iEnd = new Date().setTime(new Date().getTime() + 15 * 1000) # now plus 15 secs
  $("#progress2").anim_progressbar # Type here!
    start: iNow
    finish: iEnd
    interval: 2

  # show page warning on page load
  $( ()->
    $.jGrowl('Actions on this page may affect the devices state, and any other devices that depend on it.', {theme: 'nNote nWarning nIcon', life: 3000, header: '<span class="uNotice"><a href="#">Caution</a></span>'})
  )
