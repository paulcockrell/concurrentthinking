require ['common', 'bootstrap', 'utils', 'dynamicTable', 'easytabs'], () ->
  console.log "Device JS loaded"
  
  
  #===== Easy tabs =====
  

  $('#tab-container').easytabs
    animationSpeed: 300
    collapsible: false
    tabActiveClass: "clicked"

  #===== Tabs =====

    
  $( ".tabs" ).tabs()

  tabs = $( ".tabs-sortable" ).tabs()
  tabs.find( ".ui-tabs-nav" ).sortable
    axis: "x",
    stop: () ->
      tabs.tabs( "refresh" )
