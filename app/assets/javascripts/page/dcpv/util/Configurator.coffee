define ['dcpv/view/Hint', 'common/util/StaticGroupManager', 'common/widgets/Metric', 'dcpv/view/Breacher', 'dcpv/view/ContextMenu', 'dcpv/view/Highlight', 'common/util/PresetManager', 'dcpv/view/Chart', 'dcpv/view/DataCentre', 'dcpv/view/DataCentreObject', 'dcpv/view/Rack', 'common/widgets/ThumbNav', 'common/widgets/FilterBar', 'dcpv/ViewModel', 'common/util/Util', 'dcpv/view/LHNav', 'dcpv/view/Sensor', 'dcpv/view/NRAD'], (Hint, StaticGroupManager, Metric, Breacher, ContextMenu, Highlight, PresetManager, Chart, DataCentre, DataCentreObject, Rack, ThumbNav, FilterBar, ViewModel, Util, LHNav, Sensor, NRAD) ->

  class Configurator

    @setup: (config) ->
      document.poo = Util
      Util.SIG_FIG = config.DCPVUTIL.sigFig
      # parse colours into decimal ints
      obj.col = Configurator.parseColourString(obj.col) for obj in config.VIEWMODEL.colourScale
      Util.sortByProperty(config.VIEWMODEL.colourScale, 'pos', true)

      console.log(config.VIEWMODEL.colourScale)
      model_config                       = config.VIEWMODEL
      ViewModel.DEFAULT_MODE             = model_config.defaultMode
      ViewModel.DEFAULT_CHART_ORDER      = model_config.defaultChartOrder
      ViewModel.DEFAULT_SCALE_METRICS    = model_config.defaultScaleMetrics
      ViewModel.DEFAULT_SHOW_LABELS      = model_config.defaultShowLabels
      ViewModel.DEFAULT_SHOW_CHART       = model_config.defaultShowChart
      ViewModel.DEFAULT_SNAP_ENABLED     = model_config.defaultSnapEnabled
      ViewModel.DEFAULT_METRIC_STAT      = model_config.defaultMetricStat
      ViewModel.COLOUR_SCALE             = model_config.colourScale
      ViewModel.ORDER_ASCENDING          = model_config.orderAscending
      ViewModel.ORDER_DESCENDING         = model_config.orderDescending
      ViewModel.ORDER_ITEM_NAME          = model_config.orderItemName
      ViewModel.GROUP_LIST_PREFIXES      = model_config.groupListPrefixes
      ViewModel.GROUP_LIST_SEPARATOR     = model_config.groupListSeparator
      ViewModel.SELECT_THRESHOLD_CAPTION = model_config.selectThresholdCaption
      ViewModel.NO_THRESHOLDS_CAPTION    = model_config.noThresholdsCaption
      ViewModel.METRIC_STAT_MAP          = model_config.metricStatMap

      ctrl_config                      = config.CONTROLLER
      #DCPVController.ZOOM_KEY          = ctrl_config.zoomKey
      #DCPVController.METRIC_SELECTION  = ctrl_config.urls.metricSelection
      #DCPVController.METRIC_POLL_RATE  = ctrl_config.metricPollRate
      #DCPVController.DCPV_DEF_URL      = ctrl_config.urls.dCPVDef
      #DCPVController.METRIC_DEF_URL    = ctrl_config.urls.metricDef
      #DCPVController.METRIC_DATA_URL   = ctrl_config.urls.metricData
      #DCPVController.METRIC_GROUP      = ctrl_config.urls.metricGroup
      #DCPVController.METRIC_GROUP_JOIN = ctrl_config.urls.metricGroupJoin

      dc_config                             = ctrl_config.DATACENTRE
      DataCentre.FPS                        = dc_config.fps
      DataCentre.PADDING                    = dc_config.padding
      DataCentre.CANVAS_MAX_DIMENSION       = dc_config.canvasMaxDimension
      DataCentre.INFO_FADE_DURATION         = dc_config.infoFadeDuration
      DataCentre.ZOOM_DURATION              = dc_config.zoomDuration
      DataCentre.STEP_ZOOM_AMOUNT           = dc_config.stepZoomAmount
      DataCentre.NUDGE_AMOUNT               = dc_config.nudgeAmount
      DataCentre.SUPER_NUDGE_AMOUNT         = dc_config.superNudgeAmount
      DataCentre.SNAP_DISTANCE              = dc_config.snapDistance
      DataCentre.SELECTION_BOX_STROKE       = dc_config.selectionBox.stroke
      DataCentre.SELECTION_BOX_STROKE_WIDTH = dc_config.selectionBox.strokeWidth
      DataCentre.SELECTION_BOX_ALPHA        = dc_config.selectionBox.alpha
      DataCentre.IMAGE_UPDATE_THROTTLE      = dc_config.imageUpdateThrottle
      DataCentre.DEFAULT_RACK_WIDTH         = dc_config.defaultRackWidth
      DataCentre.DEFAULT_RACK_DEPTH         = dc_config.defaultRackDepth
      DataCentre.DEFAULT_CHASSIS_WIDTH      = dc_config.defaultChassisWidth
      DataCentre.DEFAULT_CHASSIS_DEPTH      = dc_config.defaultChassisDepth
      DataCentre.ERR_SAVE_FAILED            = dc_config.errSaveFailed

      dco_config                             = config.CONTROLLER.DATACENTRE.DATACENTREOBJECT
      DataCentreObject.LABEL_FONT            = dco_config.label.font
      DataCentreObject.LABEL_FILL            = dco_config.label.fill
      DataCentreObject.LABEL_PADDING         = dco_config.label.padding
      DataCentreObject.LABEL_HORIZONTAL_BIAS = dco_config.label.horizontalBias
      DataCentreObject.EXCLUSION_ALPHA       = dco_config.exclusionAlpha

      rack_config             = dco_config.RACK
      Rack.VIEW_STROKE        = rack_config.viewMode.stroke
      Rack.VIEW_STROKE_WIDTH  = rack_config.viewMode.strokeWidth
      Rack.VIEW_ALPHA         = rack_config.viewMode.alpha
      Rack.VIEW_FILL          = rack_config.viewMode.fill
      Rack.VIEW_FILL_ALPHA    = rack_config.viewMode.fillAlpha
      Rack.EDIT_STROKE        = rack_config.editMode.stroke
      Rack.EDIT_STROKE_WIDTH  = rack_config.editMode.strokeWidth
      Rack.EDIT_FILL          = rack_config.editMode.fill
      Rack.EDIT_ALPHA         = rack_config.editMode.alpha
      Rack.SELECTED_ANIM      = rack_config.selectedAnim

      breach_config          = config.CONTROLLER.BREACHER
      Breacher.STROKE_WIDTH  = breach_config.strokeWidth
      Breacher.STROKE        = breach_config.stroke
      Breacher.ALPHA_MAX     = breach_config.alphaMax
      Breacher.ALPHA_MIN     = breach_config.alphaMin
      Breacher.ANIM_DURATION = breach_config.animDuration

      nrad_config             = dco_config.NRAD
      NRAD.VIEW_STROKE        = nrad_config.viewMode.stroke
      NRAD.VIEW_STROKE_WIDTH  = nrad_config.viewMode.strokeWidth
      NRAD.VIEW_ALPHA         = nrad_config.viewMode.alpha
      NRAD.VIEW_FILL          = nrad_config.viewMode.fill
      NRAD.VIEW_FILL_ALPHA    = nrad_config.viewMode.fillAlpha
      NRAD.EDIT_STROKE        = nrad_config.editMode.stroke
      NRAD.EDIT_STROKE_WIDTH  = nrad_config.editMode.strokeWidth
      NRAD.EDIT_FILL          = nrad_config.editMode.fill
      NRAD.EDIT_ALPHA         = nrad_config.editMode.alpha
      NRAD.SELECTED_ANIM      = nrad_config.selectedAnim
 
      chart_config                   = config.CONTROLLER.CHART
      Chart.DEPTH_3D                 = chart_config.depth3D
      Chart.ANGLE                    = chart_config.angle
      Chart.MARGIN_TOP               = chart_config.margins.top
      Chart.MARGIN_BTM               = chart_config.margins.btm
      Chart.MARGIN_LEFT              = chart_config.margins.left
      Chart.MARGIN_RIGHT             = chart_config.margins.right
      Chart.GRAPH_TYPE               = chart_config.graph.type
      Chart.BG_FILL                  = chart_config.bgFill
      Chart.BG_ALPHA                 = chart_config.bgAlpha
      Chart.BORDER_COLOUR            = chart_config.borderColour
      Chart.BORDER_ALPHA             = chart_config.borderAlpha
      Chart.TEXT_COLOUR              = chart_config.text.colour
      Chart.TEXT_FONT                = chart_config.text.font
      Chart.TEXT_SIZE                = chart_config.text.size
      Chart.WIDTH                    = chart_config.width
      Chart.HEIGHT                   = chart_config.height
      Chart.GRAPH_LINE_ALPHA         = chart_config.graph.lineAlpha
      Chart.GRAPH_LINE_COLOUR        = chart_config.graph.lineColour
      Chart.GRAPH_LINE_THICKNESS     = chart_config.graph.lineThickness
      Chart.GRAPH_FILL_ALPHA         = chart_config.graph.fillAlpha
      Chart.BLN_BORDER_ALPHA         = chart_config.balloon.border.alpha
      Chart.BLN_BORDER_COLOUR        = chart_config.balloon.border.colour
      Chart.BLN_BORDER_THICKNESS     = chart_config.balloon.border.thickness
      Chart.BLN_H_PADDING            = chart_config.balloon.hPadding
      Chart.BLN_V_PADDING            = chart_config.balloon.vPadding
      Chart.BLN_CORNER_RADIUS        = chart_config.balloon.cornerRadius
      Chart.BLN_TEXT_COLOUR          = chart_config.balloon.text.colour
      Chart.BLN_TEXT_SIZE            = chart_config.balloon.text.size
      Chart.BLN_TEXT_ALIGN           = chart_config.balloon.text.align
      Chart.BLN_TEXT_SHADOW_COLOUR   = chart_config.balloon.text.shadowColour
      Chart.BLN_CAPTION              = chart_config.balloon.caption
      Chart.TITLE_CAPTION            = chart_config.title.caption
      Chart.TITLE_SIZE               = chart_config.title.size
      Chart.TITLE_COLOUR             = chart_config.title.colour
      Chart.TITLE_ALPHA              = chart_config.title.alpha
      Chart.TITLE_BOLD               = chart_config.title.bold
      Chart.POINTER_OFFSET_X         = chart_config.pointerOffsetX
      Chart.POINTER_OFFSET_Y         = chart_config.pointerOffsetY
      Chart.THRESHOLD_FILL_ALPHA     = chart_config.thresholdFillAlpha
      Chart.THRESHOLD_LINE_ALPHA     = chart_config.thresholdLineAlpha
      Chart.SELECT_BOX_STROKE        = chart_config.selectBox.stroke
      Chart.SELECT_BOX_STROKE_WIDTH  = chart_config.selectBox.strokeWidth
      Chart.SELECT_BOX_ALPHA         = chart_config.selectBox.alpha
      Chart.SELECTION_COUNT_OFFSET_X = chart_config.selectionCount.offsetX
      Chart.SELECTION_COUNT_OFFSET_Y = chart_config.selectionCount.offsetY
      Chart.SELECTION_COUNT_FILL     = chart_config.selectionCount.fill
      Chart.SELECTION_COUNT_BG_FILL  = chart_config.selectionCount.bgFill
      Chart.SELECTION_COUNT_BG_ALPHA = chart_config.selectionCount.bgAlpha
      Chart.SELECTION_COUNT_FONT     = chart_config.selectionCount.font
      Chart.SELECTION_COUNT_PADDING  = chart_config.selectionCount.padding
      Chart.SELECTION_COUNT_CAPTION  = chart_config.selectionCount.caption

      thumb_nav_config            = config.CONTROLLER.THUMBNAV
      ThumbNav.SHADE_FILL         = thumb_nav_config.shadeFill
      ThumbNav.SHADE_ALPHA        = thumb_nav_config.shadeFill
      ThumbNav.MASK_FILL          = thumb_nav_config.maskFill
      ThumbNav.MASK_FILL_ALPHA    = thumb_nav_config.maskFillAlpha
      ThumbNav.BREACH_FILL        = thumb_nav_config.breachFill
      ThumbNav.BREACH_ALPHA       = thumb_nav_config.breachAlpha
      ThumbNav.MODEL_DEPENDENCIES = thumb_nav_config.modelDependencies

      filter_bar_config                  = config.CONTROLLER.FILTERBAR
      FilterBar.THICKNESS                = filter_bar_config.thickness
      FilterBar.LENGTH                   = filter_bar_config.length
      FilterBar.PADDING                  = filter_bar_config.padding
      FilterBar.MODEL_UPDATE_DELAY       = filter_bar_config.slider.updateDelay
      FilterBar.DRAG_TAB_FILL            = filter_bar_config.slider.fill
      FilterBar.DRAG_TAB_SHAPE           = filter_bar_config.slider.shape
      FilterBar.DRAG_TAB_STROKE          = filter_bar_config.slider.stroke
      FilterBar.DRAG_TAB_STROKE_WIDTH    = filter_bar_config.slider.strokeWidth
      FilterBar.CUTOFF_LINE_STROKE       = filter_bar_config.cutoffLine.stroke
      FilterBar.CUTOFF_LINE_STROKE_WIDTH = filter_bar_config.cutoffLine.strokeWidth
      FilterBar.CUTOFF_LINE_ALPHA        = filter_bar_config.cutoffLine.alpha
      FilterBar.DRAG_TAB_DISABLED_ALPHA  = filter_bar_config.slider.disabledAlpha
      FilterBar.INPUT_WIDTH              = filter_bar_config.input.width
      FilterBar.INPUT_SPACING            = filter_bar_config.input.spacing
      FilterBar.INPUT_UPDATE_DELAY       = filter_bar_config.input.updateDelay
      FilterBar.FONT                     = filter_bar_config.font
      FilterBar.FONT_SIZE                = filter_bar_config.fontSize
      FilterBar.FONT_FILL                = filter_bar_config.fontFill
      FilterBar.DRAG_BOX_STROKE          = filter_bar_config.dragBox.stroke
      FilterBar.DRAG_BOX_STROKE_WIDTH    = filter_bar_config.dragBox.strokeWidth
      FilterBar.DRAG_BOX_ALPHA           = filter_bar_config.dragBox.alpha
      FilterBar.MODEL_DEPENDENCIES       = filter_bar_config.modelDependencies

      switch filter_bar_config.defaultAlign
            when 'top'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_TOP
            when 'bottom'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_BOTTOM
            when 'left'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_LEFT
            when 'right'
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_RIGHT
            else
                  FilterBar.DEFAULT_ALIGN = FilterBar.ALIGN_BOTTOM

      metric_config             = config.CONTROLLER.METRIC
      Metric.ALPHA              = metric_config.alpha
      Metric.ANIM_DURATION      = metric_config.animDuration
      Metric.FADE_DURATION      = metric_config.fadeDuration
      Metric.MODEL_DEPENDENCIES = metric_config.modelDependencies

      nav_config                   = config.CONTROLLER.LHNAV
      LHNav.EDIT_MODE_ELEMENTS     = nav_config.editModeElements
      LHNav.VIEW_MODE_ELEMENTS     = nav_config.viewModeElements
      LHNav.ITEM_EDIT_UPDATE_DELAY = nav_config.itemEditUpdateDelay

      sensor_config                  = dco_config.SENSOR
      Sensor.SOUND_GRAPHIC_DEF       = sensor_config.soundGraphic
      Sensor.HUMIDITY_GRAPHIC_DEF    = sensor_config.humidityGraphic
      Sensor.TEMPERATURE_GRAPHIC_DEF = sensor_config.temperatureGraphic
      Sensor.SELECTED_ANIM           = sensor_config.selectedAnim
      Sensor.DRAG_IMG_ALPHA          = sensor_config.dragImgAlpha

      preset_config                       = config.CONTROLLER.PRESETMANAGER
      PresetManager.PATH                  = preset_config.path
      PresetManager.GET                   = preset_config.get
      PresetManager.NEW                   = preset_config.new
      PresetManager.UPDATE                = preset_config.update
      PresetManager.VALUES                = preset_config.values
      PresetManager.ERR_CAPTION           = preset_config.errors.caption
      PresetManager.ERR_INVALID_NAME      = preset_config.errors.invalidName
      PresetManager.ERR_WHITE_NAME        = preset_config.errors.whiteName
      PresetManager.ERR_DUPLICATE_NAME    = preset_config.errors.duplicateName
      PresetManager.WARN_THRESHOLD        = preset_config.warnThreshold
      PresetManager.MESSAGE_HOLD_DURATION = preset_config.msgHoldDuration
      PresetManager.MODEL_DEPENDENCIES    = preset_config.modelDependencies
      PresetManager.DOM_DEPENDENCIES      = preset_config.domDependencies
      PresetManager.MSG_CONFIRM_UPDATE    = preset_config.msgConfirmUpdate

      group_config                          = config.CONTROLLER.STATICGROUPMANAGER
      StaticGroupManager.PATH               = group_config.path
      StaticGroupManager.GET                = group_config.get
      StaticGroupManager.NEW                = group_config.new
      StaticGroupManager.UPDATE             = group_config.update
      StaticGroupManager.MSG_CONFIRM_UPDATE = group_config.msgConfirmUpdate
      StaticGroupManager.MODEL_DEPENDENCIES = group_config.modelDependencies
      StaticGroupManager.DOM_DEPENDENCIES   = group_config.domDependencies
      StaticGroupManager.ERR_CAPTION        = group_config.errors.caption
      StaticGroupManager.ERR_DUPLICATE_NAME = group_config.errors.duplicateName
      StaticGroupManager.ERR_INVALID_NAME   = group_config.errors.invalidName
      StaticGroupManager.ERR_WHITE_NAME     = group_config.errors.whiteName

      hl_config               = config.CONTROLLER.HIGHLIGHT
      Highlight.FPS           = dc_config.fps
      Highlight.ANIM_DURATION = hl_config.animDuration
      Highlight.FILL          = hl_config.fill
      Highlight.STATE_A       = hl_config.stateA
      Highlight.STATE_B       = hl_config.stateB

      ctx_config                      = config.CONTROLLER.CONTEXTMENU
      ContextMenu.OPTIONS             = ctx_config.options
      ContextMenu.URL_INTERNAL_PREFIX = ctx_config.urlInternalPrefix
      ContextMenu.SPACER              = ctx_config.spacer

      hint_config          = config.CONTROLLER.HINT
      Hint.GROUP_CONTENT   = hint_config.groupContent
      Hint.OTHER_CONTENT   = hint_config.otherContent
      Hint.MORE_INFO_DELAY = hint_config.moreInfoDelay
 

    @parseColourString: (col_str) ->
      switch col_str.charAt(0)
        when '#'
          parseInt('0x' + col_str.substr(1))
        else
          parseInt(col_str)

