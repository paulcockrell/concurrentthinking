define [], () ->

  # static asset loader utility, grabs images and triggers a callback
  class AssetManager

    # overwritten by config
    @NUM_CONCURRENT_LOADS = 4

    # hard coded and run-time assigned statics
    @ASSET_LOAD_IN_PROGRESS = 'pending'
    @CACHE = {}
    @QUEUE = []
    @LOAD_COUNT = 0
    @CALLBACKS = {}
    @ASSET_READY_DELAY = 500


    @get: (path, on_success, on_error) ->
      if AssetManager.CACHE[path] is AssetManager.ASSET_LOAD_IN_PROGRESS
        # if the required asset is already being loaded append the callback to the existing list
        if on_success? or on_error?
          AssetManager.CALLBACKS[path] = [] unless AssetManager.CALLBACKS[path]?
          AssetManager.CALLBACKS[path].push({ onSuccess: on_success, onError: on_error })
      else if AssetManager.CACHE[path]?
        # if the asset has already been loaded then trigger callback immediately
        on_success(AssetManager.CACHE[path]) if on_success?
        return AssetManager.CACHE[path]
      else
        # otherwise queue it up and start load if available
        AssetManager.QUEUE.push(path)
        AssetManager.CACHE[path] = AssetManager.ASSET_LOAD_IN_PROGRESS
        if on_success? or on_error?
          AssetManager.CALLBACKS[path] = []
          AssetManager.CALLBACKS[path].push({ onSuccess: on_success, onError: on_error })

        if AssetManager.LOAD_COUNT < AssetManager.NUM_CONCURRENT_LOADS
          AssetManager.getAsset()


    @getAsset: ->
      ++AssetManager.LOAD_COUNT
      path = AssetManager.QUEUE.shift()
      console.log ">>> getAsset", path
      img         = new Image()
      img.onload  = ->
        console.log ">>>!! Image loaded!"
        AssetManager.assetReceived(img, path)
      img.onabort = ->
        AssetManager.assetFailed(path)
      img.onerror = ->
        AssetManager.assetFailed(path)
      img.src     = path


    @delay: (asset, path) =>
      setTimeout(=>
        @assetReceived(asset, path)
      , AssetManager.ASSET_READY_DELAY)


    @assetReceived: (asset, path) =>
      console.log "assetReceived!", asset, path
      if asset.width > 0
        AssetManager.CACHE[path] = asset
        --AssetManager.LOAD_COUNT

        if AssetManager.CALLBACKS[path]?
          for callback in AssetManager.CALLBACKS[path]
            callback.onSuccess(asset) if callback.onSuccess?
          AssetManager.CALLBACKS[path] = null

        if AssetManager.QUEUE.length > 0
          AssetManager.getAsset(AssetManager.QUEUE[0])
      else
        @delay(asset, path)


    @assetFailed: (path) =>
      --AssetManager.LOAD_COUNT
      if AssetManager.CALLBACKS[path]?
        for callback in AssetManager.CALLBACKS[path]
          callback.onError(path) if callback.onError?
        AssetManager.CALLBACKS[path] = null


