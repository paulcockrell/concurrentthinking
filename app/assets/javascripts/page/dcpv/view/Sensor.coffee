define ['dcpv/view/Breacher', 'common/widgets/Metric', 'dcpv/view/DataCentreObject', 'common/gfx/Easing', 'common/gfx/SimpleRenderer', 'dcpv/ViewModel'], (Breacher, Metric, DataCentreObject, Easing, SimpleRenderer, ViewModel) ->

  class Sensor extends DataCentreObject

    # statics overwritten by config
    @HUMIDITY_GRAPHIC_DEF    : {}
    @SOUND_GRAPHIC_DEF       : {}
    @TEMPERATURE_GRAPHIC_DEF : {}
    @SELECTED_ANIM           : {}
    @DRAG_IMG_ALPHA          : 0.1
    
    @IDENTIFIER_TYPE_SOUND       : 'sound'
    @IDENTIFIER_TYPE_TEMPERATURE : 'temperature'
    @IDENTIFIER_TYPE_HUMIDITY    : 'humidity'

    # constants and run-time assigned statics
    @TYPE_SOUND       : 0
    @TYPE_MOISTURE    : 1
    @TYPE_TEMPERATURE : 2

    @HUMIDITY_GRAPHIC    : null
    @SOUND_GRAPHIC       : null
    @TEMPERATURE_GRAPHIC : null


    @init: ->
      scaleGraphic = (raw) ->
        scale_x = raw.scaleX
        scale_y = raw.scaleY
        offset  = raw.offset
        ref_idx = raw.dimReferenceShapeIdx

        for shape, idx in raw.shapes
          switch shape.type
            when 'poly'
              shape.addFn = 'addPoly'
            when 'ellipse'
              shape.addFn = 'addEllipse'
            when 'rect'
              shape.addFn = 'addRect'
            when 'text'
              shape.addFn = 'addText'
            when 'img'
              shape.addFn = 'addImg'

          shape.x = 0 unless shape.x?
          shape.y = 0 unless shape.y?

          shape.x       = (offset.x + shape.x) * scale_x
          shape.y       = (offset.y + shape.y) * scale_y
          shape.width  *= scale_x if shape.width?
          shape.height *= scale_y if shape.height?

          if shape.coords?
            for point in shape.coords
              point.x = offset.x + point.x * scale_x
              point.y = offset.y + point.y * scale_y

          if idx is ref_idx
            if shape.width? and shape.height?
              raw.width  = Math.abs(shape.width)
              raw.height = Math.abs(shape.height)
            else
              asset_id   = DataCentreObject.HWARE_GFX[shape.addFn](shape)
              raw.width  = Math.abs(DataCentreObject.HWARE_GFX.getAttribute(asset_id, 'width'))
              raw.height = Math.abs(DataCentreObject.HWARE_GFX.getAttribute(asset_id, 'height'))
              DataCentreObject.HWARE_GFX.remove(asset_id)

        return raw.shapes

      Sensor.HUMIDITY_GRAPHIC    = scaleGraphic(Sensor.HUMIDITY_GRAPHIC_DEF)
      Sensor.SOUND_GRAPHIC       = scaleGraphic(Sensor.SOUND_GRAPHIC_DEF)
      Sensor.TEMPERATURE_GRAPHIC = scaleGraphic(Sensor.TEMPERATURE_GRAPHIC_DEF)


    constructor: (def) ->
      super(def)

      if @name.indexOf(Sensor.IDENTIFIER_TYPE_SOUND) isnt -1
        @type    = Sensor.TYPE_SOUND
        @graphic = Sensor.SOUND_GRAPHIC
        @width   = Sensor.SOUND_GRAPHIC_DEF.width
        @height  = Sensor.SOUND_GRAPHIC_DEF.height
        @dragImg = Sensor.SOUND_DRAG_IMG ? Sensor.SOUND_DRAG_IMG = @drawDragImg(Sensor.SOUND_GRAPHIC_DEF.dimReferenceShapeIdx)
      else if @name.indexOf(Sensor.IDENTIFIER_TYPE_HUMIDITY) isnt -1
        @type    = Sensor.TYPE_MOISTURE
        @graphic = Sensor.HUMIDITY_GRAPHIC
        @width   = Sensor.HUMIDITY_GRAPHIC_DEF.width
        @height  = Sensor.HUMIDITY_GRAPHIC_DEF.height
        @dragImg = Sensor.HUMIDITY_DRAG_IMG ? Sensor.HUMIDITY_DRAG_IMG = @drawDragImg(Sensor.HUMIDITY_GRAPHIC_DEF.dimReferenceShapeIdx)
      else if @name.indexOf(Sensor.IDENTIFIER_TYPE_TEMPERATURE) isnt -1
        @type    = Sensor.TYPE_TEMPERATURE
        @graphic = Sensor.TEMPERATURE_GRAPHIC
        @width   = Sensor.TEMPERATURE_GRAPHIC_DEF.width
        @height  = Sensor.TEMPERATURE_GRAPHIC_DEF.height
        @dragImg = Sensor.TEMPERATURE_DRAG_IMG ? Sensor.TEMPERATURE_DRAG_IMG = @drawDragImg(Sensor.TEMPERATURE_GRAPHIC_DEF.dimReferenceShapeIdx)

      @metric = new Metric(@group, @itemId, DataCentreObject.INFO_GFX, @x, @y, @width, @height, DataCentreObject.MODEL, Metric.TYPE_ELLIPTICAL)
      @metric.setActive(true)

      @subscriptions.push(DataCentreObject.MODEL.breaches.subscribe(@evSetBreaching))
      @subscriptions.push(DataCentreObject.MODEL.mode.subscribe(@evSwitchMode))

      # label is already set in super constructor, however we don't have an accurate
      # width/height until now so re-run
      @setLabel(DataCentreObject.MODEL.showLabels())
      @draw()


    destroy: ->
      super()
      console.log 'Sensor.destroy'
      DataCentreObject.HWARE_GFX.remove(asset_id) for asset_id in @assets


    drawDragImg: (dim_ref_idx) ->
      render = new SimpleRenderer(document.body, @width, @height, 1, 0)
      for shape, idx in @graphic
        asset_id = render[shape.addFn](shape)
        bounds = render.getBounds(asset_id) if idx is dim_ref_idx

      render.redraw()
      console.log(bounds)
      cvs             = document.createElement('canvas')
      cvs.width       = Math.abs(bounds.width)
      cvs.height      = Math.abs(bounds.height)
      ctx             = cvs[0].getContext('2d')
      ctx.globalAlpha = Sensor.DRAG_IMG_ALPHA

      ctx.drawImage(render.cvs, 0, 0)
      render.destroy()

      return cvs


    getDragImg: ->
      return @dragImg


    setCoords: (x, y, use_dims = false) ->
      super(x, y, use_dims)

      if @breaching
        @breacher.destroy()
        @breacher = new Breacher(Breacher.TYPE_RECTANGULAR, @group, @itemId, DataCentreObject.ALERT_GFX, @x, @y, @width, @height, DataCentreObject.MODEL)

      @metric.setCoords(x, y)


    draw: ->
      renew_selection = @selected
      @deselect() if renew_selection

      DataCentreObject.HWARE_GFX.remove(asset) for asset in @assets
      @assets = []

      fade_factor = if @included then 1 else DataCentreObject.EXCLUSION_ALPHA

      @metric.setActive(@included and DataCentreObject.MODEL.mode() is ViewModel.MODE_VIEW)

      for shape in @graphic
        old_x     = shape.x
        old_y     = shape.y
        old_alpha = shape.alpha ? 1

        shape.x     += @x
        shape.y     += @y
        shape.alpha *= fade_factor

        @assets.push(DataCentreObject.HWARE_GFX[shape.addFn](shape))

        shape.x     = old_x
        shape.y     = old_y
        shape.alpha = old_alpha

      @assets.push(DataCentreObject.HWARE_GFX.addImg(
        img   : @labelImg
        x     : @x + (@width - @labelImg.width) / 2
        y     : @y + (@height - @labelImg.height) / 2
        alpha : fade_factor
      ))

      @select() if renew_selection


    select: ->
      return if @selected
      @selected       = true
      @animRedrawList = @assets.slice(0)
      remove_idxs     = []

      for state in Sensor.SELECTED_ANIM.assetStates
        DataCentreObject.HWARE_GFX.setAttributes(@assets[state.assetIdx], state.targetState)
        remove_idxs.push(state.assetIdx)

      remove_idxs.sort()
      remove_idxs.reverse()
      @animRedrawList.slice(idx, 1) for idx in remove_idxs

      @evSelectFadedOut()


    evSelectFadedIn: =>
      DataCentreObject.HWARE_GFX.animate(@assets[state.assetIdx], state.targetState, Sensor.SELECTED_ANIM.duration, Easing.Cubic.easeOut, @evSelectFadedOut, null, @animRedrawList) for state in Sensor.SELECTED_ANIM.assetStates


    evSelectFadedOut: =>
      DataCentreObject.HWARE_GFX.animate(@assets[state.assetIdx], state.nativeState, Sensor.SELECTED_ANIM.duration, Easing.Cubic.easeIn, @evSelectFadedIn, null, @animRedrawList) for state in Sensor.SELECTED_ANIM.assetStates


    deselect: ->
      return unless @selected

      @selected = false
      DataCentreObject.HWARE_GFX.stopAnim(@assets[state.assetIdx]) for state in Sensor.SELECTED_ANIM
      @draw()


    setBreaching: (breaches, mode) ->
      if breaches[@group][@itemId] and mode is ViewModel.MODE_VIEW
        return if @breaching

        @breaching = true
        @breacher  = new Breacher(Breacher.TYPE_RECTANGULAR, @group, @itemId, DataCentreObject.ALERT_GFX, @x, @y, @width, @height, DataCentreObject.MODEL)
      else
        @breacher.destroy() if @breaching
        @breaching = false


    evSetBreaching: (breaches) =>
      @setBreaching(breaches, DataCentreObject.MODEL.mode())


    evSwitchMode: (mode) =>
      @metric.setActive(mode is ViewModel.MODE_VIEW)
      @setBreaching(DataCentreObject.MODEL.breaches(), mode)
      
