define ['common/util/Util', 'dcpv/ViewModel', 'common/gfx/SimpleRenderer', 'amcharts'], (Util, ViewModel, SimpleRenderer) ->


  class Chart

    @WIDTH                 : '100%'
    @HEIGHT                : '100%'
    @DEPTH_3D              : 0
    @ANGLE                 : 10
    @BG_FILL               : '#ffffff'
    @BG_ALPHA              : 0
    @BORDER_ALPHA          : 0
    @BORDER_COLOUR         : '#ff0000'
    @X_AXIS_LABEL_ROTATION : 0
    @THRESHOLD_LINE_ALPHA  : 0.5
    @THRESHOLD_FILL_ALPHA  : 0.5

    @MARGIN_TOP   : 10
    @MARGIN_BTM   : 10
    @MARGIN_LEFT  : 10
    @MARGIN_RIGHT : 10

    @TEXT_COLOUR : '#000000'
    @TEXT_FONT   : 'Karla'
    @TEXT_SIZE   : 11

    @GRAPH_TYPE           : 'column'
    @GRAPH_LINE_COLOUR    : '#000000'
    @GRAPH_LINE_ALPHA     : 1
    @GRAPH_LINE_THICKNESS : 1
    @GRAPH_FILL_ALPHA     : 1

    @BLN_BORDER_ALPHA       : 1
    @BLN_BORDER_COLOUR      : '#000000'
    @BLN_BORDER_THICKNESS   : 1
    @BLN_H_PADDING          : 5
    @BLN_V_PADDING          : 5
    @BLN_TEXT_COLOUR        : '#ffffff'
    @BLN_TEXT_SIZE          : 12
    @BLN_TEXT_ALIGN         : 'middle'
    @BLN_TEXT_SHADOW_COLOUR : '#000000'
    @BLN_CORNER_RADIUS      : 1
    @BLN_CAPTION            : '[[category]]: [[value]]'

    @TITLE_SIZE    : 14
    @TITLE_COLOUR  : '#000000'
    @TITLE_ALPHA   : .2
    @TITLE_BOLD    : true
    @TITLE_CAPTION : '[[metric_name]] [[num_metrics]] of [[total_metrics]] (max=[[max_val]], min=[[min_val]])'

    @POINTER_OFFSET_X : 10
    @POINTER_OFFSET_Y : 10

    @FPS : 60

    @SELECT_BOX_STROKE       : '#ff00ff'
    @SELECT_BOX_STROKE_WIDTH : 10
    @SELECT_BOX_ALPHA        : 1

    @SELECTION_COUNT_OFFSET_X : 10
    @SELECTION_COUNT_OFFSET_Y : 10
    @SELECTION_COUNT_FILL     : '#00ff00'
    @SELECTION_COUNT_BG_FILL  : '#00ffff'
    @SELECTION_COUNT_BG_ALPHA : 1
    @SELECTION_COUNT_FONT     : 'Karla'
    @SELECTION_COUNT_PADDING  : 20
    @SELECTION_COUNT_CAPTION  : 'Number of things: [[selection_count]]'

    @SENSOR_METRICS : [ 'ct.sensor.sound', 'ct.sensor.humidity', 'ct.sensor.temperature' ]


    constructor: (@containerEl, @model) ->
      @pointerEl = $('pointer')

      @chart                 = new AmCharts.AmSerialChart()
      @chart.categoryField   = 'device'
      @chart.marginTop       = Chart.MARGIN_TOP
      @chart.marginBottom    = Chart.MARGIN_BTM
      @chart.marginLeft      = Chart.MARGIN_LEFT
      @chart.marginRight     = Chart.MARGIN_RIGHT
      @chart.angle           = Chart.ANGLE
      @chart.depth3D         = Chart.DEPTH_3D
      @chart.backgroundColor = Chart.BG_FILL
      @chart.backgroundAlpha = Chart.BG_ALPHA
      @chart.borderColor     = Chart.BORDER_COLOUR
      @chart.borderAlpha     = Chart.BORDER_ALPHA
      @chart.color           = Chart.TEXT_COLOUR
      @chart.fontFamily      = Chart.TEXT_FONT
      @chart.fontSize        = Chart.TEXT_SIZE
      @chart.width           = Chart.WIDTH
      @chart.height          = Chart.HEIGHT

      @chart.addListener("rollOverGraphItem", =>
        @evRollOver(arguments[0].item.dataContext.instance))
      @chart.addListener('rollOutGraphItem', =>
        @evRollOut())

      balloon                   = @chart.balloon
      balloon.borderAlpha       = Chart.BLN_BORDER_ALPHA
      balloon.borderColor       = Chart.BLN_BORDER_COLOUR
      balloon.borderThickness   = Chart.BLN_BORDER_THICKNESS
      balloon.color             = Chart.BLN_TEXT_COLOUR
      balloon.fontSize          = Chart.BLN_TEXT_SIZE
      balloon.textAlign         = Chart.BLN_TEXT_ALIGN
      balloon.textShadowColor   = Chart.BLN_TEXT_SHADOW_COLOUR
      balloon.verticalPadding   = Chart.BLN_V_PADDING
      balloon.horizontalPadding = Chart.BLN_H_PADDING
      balloon.cornerRadius      = Chart.BLN_CORNER_RADIUS

      @graph               = new AmCharts.AmGraph()
      @graph.valueField    = 'metric'
      @graph.colorField    = 'colour'
      @graph.type          = Chart.GRAPH_TYPE
      @graph.lineColor     = Chart.GRAPH_LINE_COLOUR
      @graph.lineAlpha     = Chart.GRAPH_LINE_ALPHA
      @graph.lineThickness = Chart.GRAPH_LINE_THICKNESS
      @graph.fillAlphas    = Chart.GRAPH_FILL_ALPHA
      @graph.balloonText   = Chart.BLN_CAPTION

      @thresholds = []

      @chart.categoryAxis.labelsEnabled = false
      @chart.autoMargins = false
      @chart.marginBottom = 10
      @chart.categoryAxis.axisAlpha = 0
      @chart.categoryAxis.gridCount = 0

      @chart.addGraph(@graph)
      @chart.write(@containerEl.id)

      graph_parent = @containerEl.getElementsByTagName('svg')[0].parentElement ? @containerEl.getElementsByTagName('svg')[0].parentNode
      Util.setStyle(graph_parent, 'position', '')
      @model.showChart.subscribe(@setSubscriptions)
      @setSubscriptions(@model.showChart())


    update: =>
      data             = []
      metric_data      = @model.metricData()
      metric_templates = @model.metricTemplates()
      metric_template  = metric_templates[metric_data.metricId]
      selected_metric  = @model.selectedMetric()

      # ignore unrecognised metrics or redundant requests (when the metric data isn't
      # for the current metric)
      if not metric_template? or metric_data.metricId isnt selected_metric
        @clear()
        return

      device_lookup    = @model.deviceLookup()
      selected_devices = @model.selectedDevices()
      active_selection = @model.activeSelection()
      active_filter    = @model.activeFilter()
      filtered_devices = @model.filteredDevices()
      metric_stat      = @model.selectedMetricStat()

      groups    = @model.groups()
      @included = {}
      @included[group] = {} for group in groups

      groups_to_consider = []
      is_sensor_metric   = false
      for metric, idx in Chart.SENSOR_METRICS
        if selected_metric is metric
          is_sensor_metric = true
          break

      if is_sensor_metric
        groups_to_consider.push('sensors')
        selected_sensor = idx
      else
        groups_to_consider.push('racks', 'chassis')

      col_map  = @model.colourMaps()[metric_data.metricId]
      col_high = col_map.high
      col_low  = col_map.low
      range    = col_high - col_low

      # extract subset of all metrics according to selection
      metric_count = 0
      for group in groups_to_consider

        group_values = metric_data.values[group]
        for id of group_values
          continue if group is 'sensors' and device_lookup.sensors[id].instance.type isnt selected_sensor

          ++metric_count

          if (not active_selection or (active_selection and selected_devices[group][id])) and (not active_filter or (active_filter and filtered_devices[group][id]))
            @included[group][id] = true

            metric = group_values[id][metric_stat] ? group_values[id].value
            temp   = (metric - col_low) / range
            col    = @getColour(temp).toString(16)
            col    = '0' + col while col.length < 6
            device = device_lookup[group][id]

            data.push({ group: group, device: device.name, id: id, metric: Util.formatValue(metric), numMetric: Number(metric), colour: '#' + col, instance: device.instance }) if device?

      if data.length > 0
        sort = @model.chartSortOrder()

        switch sort
          when ViewModel.ORDER_ASCENDING
            data = Util.sortByProperty(data, 'numMetric', true)
            max  = data[data.length - 1]
            min  = data[0]
          when ViewModel.ORDER_DESCENDING
            data = Util.sortByProperty(data, 'numMetric', false)
            min  = data[data.length - 1]
            max  = data[0]
          when ViewModel.ORDER_ITEM_NAME
            data = Util.sortByProperty(data, 'device', true)
            max  = data[0]
            min  = data[0]
            for datum in data
              max = datum if datum.numMetric > max.numMetric
              min = datum if datum.numMetric < min.numMetric

        @graph.valueAxis.removeGuide(threshold) for threshold in @thresholds

        title = Chart.TITLE_CAPTION
        # swap in title variables
        title = Util.substitutePhrase(title, 'metric_name', metric_template.name)
        title = Util.substitutePhrase(title, 'num_metrics', data.length)
        title = Util.substitutePhrase(title, 'total_metrics', metric_count)
        title = Util.substitutePhrase(title, 'max_val', max.metric)
        title = Util.substitutePhrase(title, 'min_val', min.metric)
        title = Util.substitutePhrase(title, 'metric_units', metric_template.unit)
        title = Util.cleanUpSubstitutions(title)

        @chart.titles = []
        @chart.addTitle(unescape(title), Chart.TITLE_SIZE, Chart.TITLE_COLOUR, Chart.TITLE_ALPHA, Chart.TITLE_BOLD)
        @chart.dataProvider = data
        @chart.validateData()
        @chart.validateNow()
        @chart.invalidateSize()

        selected_threshold = @model.selectedThreshold()
        @updateThreshold(selected_threshold) if selected_threshold?
      else
        @clear()


    clear: =>
      @chart.titles       = []
      @chart.dataProvider = []
      @chart.validateData()
      @chart.validateNow()
      @chart.invalidateSize()


    setSubscriptions: (show_chart) =>
      if show_chart
        @subscriptions = []
        @subscriptions.push(@model.metricData.subscribe(@update))
        @subscriptions.push(@model.selectedMetric.subscribe(@clear))
        @subscriptions.push(@model.chartSortOrder.subscribe(@update))
        @subscriptions.push(@model.colourMaps.subscribe(@update))
        @subscriptions.push(@model.filteredDevices.subscribe(@update))
        @subscriptions.push(@model.highlighted.subscribe(@highlightDatum))
        @subscriptions.push(@model.filteredDevices.subscribe(@update))
        @subscriptions.push(@model.selectedDevices.subscribe(@update))
        @subscriptions.push(@model.selectedThreshold.subscribe(@updateThreshold))
        @subscriptions.push(@model.selectedMetricStat.subscribe(@update))

        setTimeout(@update, 500)
      else
        sub.dispose() for sub in @subscriptions


    getColour: (pos) ->
      colours = @model.colourScale()

      return colours[0].col if pos <= 0 or isNaN(pos)
      return colours[colours.length - 1].col if pos >= 1

      count = 0
      len   = colours.length
      ++count while pos > colours[count].pos
      low  = colours[count - 1]
      high = colours[count]
      Util.blendColour(low.col, high.col, (pos - low.pos) / (high.pos - low.pos))


    highlightDatum: (device) =>
      if not @over and device? and @included? and @included[device.group][device.itemId]
        # hacky route in to amcharts to find datum coordinates, consider revision
        coords = @chart.lookupTable[device.name].axes.valueAxis0.graphs.graph0
        console.log "highlightDatum"
        Util.setStyle(@pointerEl, 'left', coords.x + Chart.POINTER_OFFSET_X + 'px')
        Util.setStyle(@pointerEl, 'top', coords.y + Chart.POINTER_OFFSET_Y + 'px')
        Util.setStyle(@pointerEl, 'visibility', 'visible')
      else
        console.log "highlightDatum"
        Util.setStyle(@pointerEl, 'visibility', 'hidden')


    evRollOver: (device_instance) =>
      @over = true
      @model.highlighted(device_instance)


    evRollOut: =>
      @over = false
      @model.highlighted(null)


    startDrag: (x, y) ->
      # drag a selection box
      dims = Util.getElementDimensions(@containerEl)
      @gfx = new SimpleRenderer(@containerEl, dims.width, dims.height, 1, Chart.FPS)#@createFXLayer(@chartEl, 0, 0, dims.width, dims.height)
      console.log "startDrag"
      Util.setStyle(@gfx.cvs, 'position', 'absolute')
      Util.setStyle(@gfx.cvs, 'left', 0)
      Util.setStyle(@gfx.cvs, 'top', 0)

      @boxAnchor = { x: x, y: y }

      @box = @gfx.addRect(
        x           : x
        y           : y
        stroke      : Chart.SELECT_BOX_STROKE
        strokeWidth : Chart.SELECT_BOX_STROKE_WIDTH
        alpha       : Chart.SELECT_BOX_ALPHA
        width       : 1
        height      : 1)

      @selCount = @gfx.addText(
        x       : x + Chart.SELECTION_COUNT_OFFSET_X
        y       : y + Chart.SELECTION_COUNT_OFFSET_Y
        fill    : Chart.SELECTION_COUNT_FILL
        bgFill  : Chart.SELECTION_COUNT_BG_FILL
        bgAlpha : Chart.SELECTION_COUNT_BG_ALPHA
        font    : Chart.SELECTION_COUNT_FONT
        padding : Chart.SELECTION_COUNT_PADDING
        caption : '')


    drag: (x, y) ->
      @dragBox(x, y)
      box =
        x      : @gfx.attr(@box, 'x')
        y      : @gfx.attr(@box, 'y')
        width  : @gfx.attr(@box, 'width')
        height : @gfx.attr(@box, 'height')

      box.left   = box.x
      box.top    = box.y
      box.right  = box.left + box.width
      box.bottom = box.top + box.height

      # update selection count
      @gfx.setAttributes(@selCount,
        caption : Chart.SELECTION_COUNT_CAPTION.replace(/\[\[selection_count\]\]/g, @getSelection(box).count)
        x       : box.x + Chart.SELECTION_COUNT_OFFSET_X
        y       : box.y + Chart.SELECTION_COUNT_OFFSET_Y)


    stopDrag: (x, y) ->
      @gfx.destroy()

      box = {}
      if x > @boxAnchor.x
        box.x     = @boxAnchor.x
        box.width = x - @boxAnchor.x
      else
        box.x     = x
        box.width = @boxAnchor.x - x

      if y > @boxAnchor.y
        box.y      = @boxAnchor.y
        box.height = y - @boxAnchor.y
      else
        box.y      = y
        box.height = @boxAnchor.y - y

      box.left   = box.x
      box.right  = box.x + box.width
      box.top    = box.y
      box.bottom = box.y + box.height
      @selectWithinBox(box)


    getSelection: (box) ->
      groups           = @model.groups()
      selection        = {}
      selection[group] = {} for group in groups
      active_selection = false
      count            = 0
      # hacky route in to amcharts to find datum coordinates, consider revision
      for device of @chart.lookupTable
        datum = @chart.lookupTable[device]
        continue unless datum.axes?
        coords = datum.axes.valueAxis0.graphs.graph0

        if coords.x >= box.x and coords.x <= box.x + box.width
          group                = datum.axes.valueAxis0.graphs.graph0.dataContext.group
          id                   = datum.axes.valueAxis0.graphs.graph0.dataContext.id
          selection[group][id] = true
          active_selection     = true
          ++count

      { activeSelection: active_selection, selection: selection, count: count }


    selectWithinBox: (box) ->
      selected = @getSelection(box)
      @model.activeSelection(selected.activeSelection)
      @model.selectedDevices(selected.selection)


    dragBox: (x, y) ->
      attrs = {}

      if x > @boxAnchor.x
        attrs.x     = @boxAnchor.x
        attrs.width = x - @boxAnchor.x
      else
        attrs.x     = x
        attrs.width = @boxAnchor.x - x

      if y > @boxAnchor.y
        attrs.y      = @boxAnchor.y
        attrs.height = y - @boxAnchor.y
      else
        attrs.y      = y
        attrs.height = @boxAnchor.y - y

      @gfx.setAttributes(@box, attrs)


    updateThreshold: (threshold) =>
      @graph.valueAxis.removeGuide(thold) for thold in @thresholds
      @thresholds = []

      unless threshold?
        @chart.validateNow()
        return

      @thresholds = []
      count       = 0
      len         = threshold.colours.length
      if len is 1
        thold           = new AmCharts.Guide()
        thold.lineColor = threshold.colours[0]
        thold.lineAlpha = Chart.THRESHOLD_LINE_ALPHA
        thold.value     = threshold.values[0]
        @thresholds.push(thold)
        @graph.valueAxis.addGuide(thold)
      else
        bottom_val = @graph.valueAxis.min
        while count < len
          thold           = new AmCharts.Guide()
          thold.lineColor = threshold.colours[count]
          thold.lineAlpha = Chart.THRESHOLD_LINE_ALPHA
          thold.fillColor = threshold.colours[count]
          thold.fillAlpha = Chart.THRESHOLD_FILL_ALPHA
          thold.value     = bottom_val
          thold.toValue   = threshold.values[count] ? @graph.valueAxis.max
          bottom_val      = threshold.values[count]
          @thresholds.push(thold)
          @graph.valueAxis.addGuide(thold)
          ++count

      @chart.validateNow()
