define ['common/gfx/Easing'], (Easing) ->

  class Breacher

    # statics overwritten by config
    @STROKE_WIDTH  : 5
    @STROKE        : '#ff0000'
    @ALPHA_MAX     : 1
    @ALPHA_MIN     : .5
    @ANIM_DURATION : 900

    # constants and run-time assigned statics
    @TYPE_RECTANGULAR : 0
    @TYPE_ELLIPTICAL  : 1


    constructor: (type, @group, @id, @gfx, x, y, width, height, @model) ->

      if type is Breacher.TYPE_ELLIPTICAL
        @asset = @gfx.addEllipse(
          x           : x
          y           : y
          width       : width
          height      : height
          alpha       : Breacher.ALPHA_MIN
          stroke      : Breacher.STROKE
          strokeWidth : Breacher.STROKE_WIDTH)
      else
        @asset = @gfx.addRect(
          x           : x
          y           : y
          width       : width
          height      : height
          alpha       : Breacher.ALPHA_MIN
          stroke      : Breacher.STROKE
          strokeWidth : Breacher.STROKE_WIDTH)

      # delay animation start by a random amount to desynchronise breachers (looks cooler)
      @delay = setTimeout(@fadeToMax, Math.ceil(Math.random() * Breacher.ANIM_DURATION * 2))
      
      breaches              = @model.breachZones()
      breaches[@group][@id] = { x: x, y: y, width: width, height: height }
      @model.breachZones(breaches)


    fadeToMax: =>
      @gfx.animate(@asset, { alpha: Breacher.ALPHA_MAX }, Breacher.ANIM_DURATION, Easing.Cubic.easeOut, @fadeToMin)


    fadeToMin: =>
      @gfx.animate(@asset, { alpha: Breacher.ALPHA_MIN }, Breacher.ANIM_DURATION, Easing.Cubic.easeOut, @fadeToMax)


    destroy: ->
      @gfx.remove(@asset)
      clearTimeout(@delay)
      breaches = @model.breachZones()
      delete breaches[@group][@id]
      @model.breachZones(breaches)
      
