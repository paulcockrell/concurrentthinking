define ->

  class DataCentreObject

    # statics overwritten by config
    @LABEL_FONT            : 'Karla'
    @LABEL_FILL            : '#000000'
    @LABEL_PADDING         : 0.05
    @EXCLUSION_ALPHA       : 0.2
    @LABEL_HORIZONTAL_BIAS : 0.15

    # constants and run-time assigned statics
    @HWARE_GFX   : null
    @INFO_GFX    : null
    @ALERT_GFX   : null
    @MODEL       : null
    @SCALE_X     : null
    @SCALE_Y     : null
    @OFFSET_X    : null
    @OFFSET_Y    : null
    @BLANK_LABEL : null


    @init: (model, hware_gfx, info_gfx, alert_gfx, scale, px_dims, unit_dims, offset) ->
      DataCentreObject.MODEL     = model
      DataCentreObject.HWARE_GFX = hware_gfx
      DataCentreObject.INFO_GFX  = info_gfx
      DataCentreObject.ALERT_GFX = alert_gfx
      DataCentreObject.SCALE_X   = (px_dims.x / unit_dims.x) * scale
      DataCentreObject.SCALE_Y   = (px_dims.y / unit_dims.y) * scale
      DataCentreObject.OFFSET_X  = offset.x
      DataCentreObject.OFFSET_Y  = offset.y

      cvs        = document.createElement('canvas')
      cvs.width  = 1
      cvs.height = 1

      DataCentreObject.BLANK_LABEL = cvs


    @dimToPx: (axis, dim) ->
      return DataCentreObject['SCALE_' + axis.toUpperCase()] * dim


    @pxToDim: (axis, px) ->
      return px / DataCentreObject['SCALE_' + axis.toUpperCase()]


    constructor: (def) ->
      @def     = def
      @groupId = def.groupId
      @itemId  = def.itemId
      @mode    = DataCentreObject.DEFAULT_MODE
      @group   = def.group
      @name    = def.name

      @x      = DataCentreObject.dimToPx('x', def.position.x) + DataCentreObject.OFFSET_X
      @y      = DataCentreObject.dimToPx('y', def.position.y) + DataCentreObject.OFFSET_Y
      @width  = DataCentreObject.dimToPx('x', def.position.w)
      @height = DataCentreObject.dimToPx('y', def.position.d)

      # a list of SimpleRenderer ids
      @assets        = []
      @subscriptions = []
      @included      = true

      @setLabel(DataCentreObject.MODEL.showLabels())

      @lookup          = DataCentreObject.MODEL.deviceLookup()[def.group][@itemId]
      @lookup.instance = @

      @subscriptions.push(DataCentreObject.MODEL.showLabels.subscribe(@evToggleShowLabel))
      @subscriptions.push(DataCentreObject.MODEL.selectedDevices.subscribe(@evSetIncluded))
      @subscriptions.push(DataCentreObject.MODEL.filteredDevices.subscribe(@evSetIncluded))


    destroy: ->
      console.log 'DataCentreObject.destory'
      @lookup.instance = null
      @lookup.position = null

      sub.dispose() for sub in @subscriptions


    setCoords: (x, y, use_dims = false) ->
      # convert to pixel values if using dims
      if use_dims
        x = DataCentreObject.dimToPx('x', x)
        y = DataCentreObject.dimToPx('y', y)

      dx = x - @x
      dy = y - @y

      for asset, idx in @assets
        DataCentreObject.HWARE_GFX.setAttributes(asset,
         x: dx + DataCentreObject.HWARE_GFX.getAttribute(asset, 'x')
         y: dy + DataCentreObject.HWARE_GFX.getAttribute(asset, 'y'))

      @x = x
      @y = y


    setSize: (width, height, use_dims = false) ->
      # convert to pixel values if using dims
      if use_dims
        width  = DataCentreObject.dimToPx('x', width)
        height = DataCentreObject.dimToPx('y', height)

      @width  = width
      @height = height

      @draw()


    renderText: (caption, font, size, fill) ->
      cvs        = document.createElement('canvas')
      ctx        = cvs[0].getContext('2d')
      ctx.font   = size + 'px ' + font
      width      = ctx.measureText(caption).width
      cvs.width  = width
      cvs.height = size

      # re-assign font, this gets reset after setting the canvas size
      ctx.font = size + 'px ' + font
      ctx.fillStyle = fill
      ctx.fillText(caption, 0, size * 0.8)

      return cvs


    rotateImage: (image) ->
      cvs        = document.createElement('canvas')
      ctx        = cvs[0].getContext('2d')
      cvs.width  = image.height
      cvs.height = image.width

      #ctx.translate(2 * size, width)
      ctx.rotate(-Math.PI / 2)
      ctx.drawImage(image, -image.width, 0)

      return cvs


    setLabel: (visible) ->
      unless visible
        @labelImg = DataCentreObject.BLANK_LABEL
        return

      if @height / @width > 1 + DataCentreObject.LABEL_HORIZONTAL_BIAS
        horizontal = false
        padding    = @width * DataCentreObject.LABEL_PADDING
        label_w    = @height - (padding * 2)
        label_h    = @width - (padding * 2)
      else
        horizontal = true
        padding    = @height * DataCentreObject.LABEL_PADDING
        label_w    = @width - (padding * 2)
        label_h    = @height - (padding * 2)

      font_size  = label_h
      ctx        = DataCentreObject.HWARE_GFX.ctx
      ctx.font   = font_size + 'px ' + DataCentreObject.LABEL_FONT
      ratio      = ctx.measureText(@name).width / label_w
      font_size /= ratio if ratio > 1

      @labelImg = @renderText(@name, DataCentreObject.LABEL_FONT, font_size, DataCentreObject.LABEL_FILL)
      @labelImg = @rotateImage(@labelImg) unless horizontal


    evToggleShowLabel: (show) =>
      @setLabel(show)
      @draw()


    evSetIncluded: =>
      old_included = @included
      @included    = (not DataCentreObject.MODEL.activeSelection() or DataCentreObject.MODEL.selectedDevices()[@group][@itemId]) and (not DataCentreObject.MODEL.activeFilter() or DataCentreObject.MODEL.filteredDevices()[@group][@itemId])

      @draw() if @included isnt old_included
