# static class/singleton
define ['common/util/Util', 'knockout'], (Util, ko) ->

  class ViewModel
    # static overwritten by config
    @DEFAULT_MODE          : 0
    @DEFAULT_CHART_ORDER   : 'ascending'
    @DEFAULT_SCALE_METRICS : true
    @DEFAULT_SHOW_LABELS   : true
    @DEFAULT_SHOW_CHART    : true
    @DEFAULT_SNAP_ENABLED  : true
    @DEFAULT_METRIC_STAT   : 'Average'

    @COLOUR_SCALE        : [ { pos: 0, col: 0x00ff00 }, { pos: .3, col: 0xffff00 }, { pos: 1, col: 0xffffff } ]

    @ORDER_ASCENDING  : 'ascending'
    @ORDER_DESCENDING : 'descending'
    @ORDER_ITEM_NAME  : 'name'

    @GROUP_LIST_PREFIXES  : { racks : "Rack", sensors: "Sensor", chassis: "Chassis" }
    @GROUP_LIST_SEPARATOR : ' '

    @SELECT_THRESHOLD_CAPTION : 'Select a threshold to display'
    @NO_THRESHOLDS_CAPTION    : 'No thresholds available'

    @METRIC_STAT_MAP : { Mininun: 'min', Maximum: 'max', Average: 'mean', Total: 'sum' }

    # constants and run-time assigned statics
    @MODE_VIEW : 0
    @MODE_EDIT : 1


    constructor: ->
      @groups = ko.observable(['racks', 'sensors', 'chassis'])
      blank = {}
      groups = @groups()
      blank[group] = {} for group in groups

      @dataCentreDef = ko.observable()

      @groupMemberLookup = ko.observable({})

      @mode = ko.observable(ViewModel.DEFAULT_MODE)
      @scale = ko.observable()
      @scaleMetrics = ko.observable(ViewModel.DEFAULT_SCALE_METRICS)
      @metricTemplates = ko.observable([])
      @selectedMetric = ko.observable()
      @metricStatistics = ko.observable()
      @metricIds = ko.dependentObservable(->
        metrics = @metricTemplates()
        options = []
        options.push(metrics[metric_id].name) for metric_id of metrics
        options
      , @)

      stat_options = []
      stat_options.push(key) for key of ViewModel.METRIC_STAT_MAP
      @metricStatOptions = ko.observable(stat_options)
      @selectedMetricStatDisplayValue = ko.observable(ViewModel.DEFAULT_METRIC_STAT)
      @selectedMetricStat = ko.dependentObservable(->
        return ViewModel.METRIC_STAT_MAP[@selectedMetricStatDisplayValue()]
      , @)

      @metricData = ko.observable({ values: blank })
      @showChart = ko.observable(ViewModel.DEFAULT_SHOW_CHART)
      @colourScale = ko.observable(ViewModel.COLOUR_SCALE)
      @deviceLookup = ko.observable({})
      @chartSortOrders = ko.observable([ ViewModel.ORDER_ASCENDING, ViewModel.ORDER_DESCENDING, ViewModel.ORDER_ITEM_NAME ])
      @chartSortOrder = ko.observable(ViewModel.DEFAULT_CHART_ORDER)
      @dcImage = ko.observable()
      @breachZones = ko.observable(blank)
      @breaches = ko.observable(blank)
      @filters = ko.observable({})
      @filteredDevices = ko.observable(blank)
      @activeFilter = ko.observable(false)
      @activeSelection = ko.observable(false)
      @selectedDevices = ko.observable(blank)
      @colourMaps = ko.observable({})
      @deviceSnapping = ko.observable(ViewModel.DEFAULT_SNAP_ENABLED)
      @selection = ko.observable([])
      
      @unpositionedItems = ko.dependentObservable(->
        list   = []
        items  = @deviceLookup()
        groups = @groups()
        for group in groups
          prefix = ViewModel.GROUP_LIST_PREFIXES[group] + ViewModel.GROUP_LIST_SEPARATOR
          for id of items[group]
            item = items[group][id]
            list.push(prefix + item.name) unless item.position?

        list
      , @)

      @enablePositionItems = ko.dependentObservable(->
        @unpositionedItems().length > 0
      , @)

      @groupsById    = ko.observable([])
      @selectedGroup = ko.observable()
      @groupNames    = ko.dependentObservable(->
        presets      = @groupsById()
        preset_names = []
        preset_names.push(presets[i].name) for i of presets
        Util.sortCaseInsensitive(preset_names)
      , @)
      @enableGroupSelection = ko.dependentObservable(->
        groups = @groupNames()
        groups? and groups.length > 0
      , @)

      @showLabels = ko.observable(ViewModel.DEFAULT_SHOW_LABELS)
      @modified = ko.observable(false)
      @enableDelete = ko.observable(false)

      @presetsById    = ko.observable([])
      @selectedPreset = ko.observable()
      @presetNames    = ko.dependentObservable(->
        presets      = @presetsById()
        preset_names = []
        preset_names.push(presets[i].name) for i of presets
        Util.sortCaseInsensitive(preset_names)
      , @)
      @enablePresetSelection = ko.dependentObservable(->
        presets = @presetNames()
        presets? and presets.length > 0
      , @)

      @highlighted = ko.observable()

      # arrays of thresholds grouped by their associated metric. Metric id is the key
      @thresholdsByMetric = ko.observable()
      # object containing all thresholds using id as the key
      @thresholdsById = ko.observable({})
      @availableThresholds = ko.dependentObservable(->
        thresholds = []
        t_by_m     = @thresholdsByMetric()
        metric     = @selectedMetric()

        return [] unless t_by_m?

        list = t_by_m[metric]

        return [] unless list?

        thresholds.push(threshold.name) for threshold in list
        thresholds
      , @)

      @enableThresholdSelection = ko.dependentObservable(=>
        thresholds = @availableThresholds()
        thresholds? and thresholds.length > 0
      , @)

      @thresholdSelectCaption = ko.dependentObservable(=>
        if @enableThresholdSelection() then ViewModel.SELECT_THRESHOLD_CAPTION else ViewModel.NO_THRESHOLDS_CAPTION
      , @)

      # name of the selected threshold, set when clicked in the drop down
      @selectedThresholdName = ko.observable()
      @selectedThresholdId   = ko.dependentObservable(
        read: =>
          t_by_m = @thresholdsByMetric()
          metric = @selectedMetric()
          t_name = @selectedThresholdName()

          return unless t_by_m?

          list = t_by_m[metric]

          return unless list?

          for threshold in list
            return threshold.id if threshold.name is t_name

        write: (val) =>
          thold = @thresholdsById()[val]
          if thold?
            @selectedThresholdName(thold.name)
            thold.id
          else
            null
      , @)

      # holds the selected threshold definition
      @selectedThreshold = ko.dependentObservable(=>
        @thresholdsById()[@selectedThresholdId()]
      , @)

      @scaleMetrics = ko.observable(ViewModel.DEFAULT_SCALE_METRICS)

      ko.applyBindings(@)


    getItemFromSelect: (select_value) ->
      parts = select_value.split(ViewModel.GROUP_LIST_SEPARATOR)
      
      return null unless parts.length > 1

      group_prefix = parts.shift()
      device_name  = parts.join(ViewModel.GROUP_LIST_SEPARATOR)

      for group of ViewModel.GROUP_LIST_PREFIXES
        break if ViewModel.GROUP_LIST_PREFIXES[group] is group_prefix

      device_lookup = @deviceLookup()
      for id of device_lookup[group]
        return device_lookup[group][id] if device_lookup[group][id].name is device_name

      return null
