require ['common', 'bootstrap', 'uniform', 'hBar', 'pie', 'bar', 'chart', 'updating', 'iButton', 'collapsible', 'easytabs', 'utils'], () ->
  console.log "Index JS loaded"


  #===== Collapsible widget management =====
  #
  #
  $('.exp_button').click (el) ->
    exp_body = $(el.currentTarget).parent().parent().children('.exp_body')
    $(exp_body).toggle('slow')


  $('.exp_body').collapsible
  	defaultOpen: 'current'
  	cookieName: 'navAct'
  	cssOpen: 'subOpened'
  	cssClose: 'subClosed'
  	speed: 200
  

  $('.opened').collapsible
  	defaultOpen: 'opened,toggleOpened'
  	cssOpen: 'inactive'
  	cssClose: 'normal'
  	speed: 200
  
  
  $('.closed').collapsible
  	defaultOpen: ''
  	cssOpen: 'inactive'
  	cssClose: 'normal'
  	speed: 200
  

  #===== Easy tabs =====
  

  $('#tab-container').easytabs
    animationSpeed: 300
    collapsible: false
    tabActiveClass: "clicked"
    
  
  #===== Tabs =====

    
  $( ".tabs" ).tabs()

  tabs = $( ".tabs-sortable" ).tabs()
  tabs.find( ".ui-tabs-nav" ).sortable
    axis: "x",
    stop: () ->
      tabs.tabs( "refresh" )


  #===== Drag sort =====


  $(".column").sortable(
    connectWith: ".column"
    handle: "span.widget-move-handle"
    cursor: "move"
    placeholder: "placeholder"
    forcePlaceholderSize: true
    opacity: 0.4
    stop: (event, ui) ->
      $(ui.item).find("h6").click()
      sortorder = ""
      $(".column").each ->
        itemorder = $(this).sortable("toArray")
        columnId = $(this).attr("id")
        sortorder += columnId + "=" + itemorder.toString() + "&"
  ).disableSelection()


  #===== Connect the short cut buttons =====
  

  $("#middleNavR_bookmarks").on 'click', ->
    console.log "Fetching bookmarks snippet"
    $('.gridster').load("/dashboard/ajax/view")


  #===== Little function to make all divs with class column become the same height =====
  #
  $.fn.setAllToMaxHeight = ->
    @height Math.max.apply(this, $.map(this, (e) ->
      $(e).height()
    ))

  $("div.column").setAllToMaxHeight()

