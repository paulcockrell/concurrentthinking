require ['login'], () ->
  console.log "Login JS loaded"

  $(".loginPic").hover (->
    $(".logleft, .logback").animate
      left: 10
      opacity: 1
    , 200
    $(".logright").animate
      right: 10
      opacity: 1
    , 200
  ), ->
    $(".logleft, .logback").animate
      left: 0
      opacity: 0
    , 200
    $(".logright").animate
      right: 0
      opacity: 0
    , 200
