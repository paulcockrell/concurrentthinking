# --- !Ups

create table cluster (
  id        long         not null primary key AUTO_INCREMENT,
  name      varchar(255) not null,
  domain    varchar(255) not null
);

insert into cluster (name, domain) values ('FirstCluster', 'a.concurrent-thinking.com');
insert into cluster (name, domain) values ('SecondCluster', 'c.concurrent-thinking.com');
insert into cluster (name, domain) values ('ThirdCluster', 'b.concurrent-thinking.com');
insert into cluster (name, domain) values ('FourthCluster', 'd.concurrent-thinking.com');
insert into cluster (name, domain) values ('fifth', 'fifth.com');
insert into cluster (name, domain) values ('sixth', 'sixth.com');
insert into cluster (name, domain) values ('seventh', 'seventh.com');
insert into cluster (name, domain) values ('eighth', 'eighth.com');
insert into cluster (name, domain) values ('ninth', 'ninth.com');
insert into cluster (name, domain) values ('tenth', 'tenth.com');
insert into cluster (name, domain) values ('eleventh', 'eleventh.com');
insert into cluster (name, domain) values ('twelfth', 'twelfth.com');

# --- !Downs

drop table if exists cluster;

