# --- !Ups

create table device (
  id        	long         not null primary key AUTO_INCREMENT,
  name      	varchar(255) not null,
  description	varchar(255) not null,
  manufacturer	varchar(255) not null,
  model		varchar(255) not null,
  rack		varchar(255) not null
);

insert into device (name, description, manufacturer, model, rack) values ('comp001', 'Device name comp001 inside rack','Dell','WXA 2000','A1');
insert into device (name, description, manufacturer, model, rack) values ('comp002', 'Device name comp002 inside rack','Dell','WXA 2000','A2');
insert into device (name, description, manufacturer, model, rack) values ('comp003', 'Device name comp003 inside rack','Dell','WXA 2000','A3');
insert into device (name, description, manufacturer, model, rack) values ('comp004', 'Device name comp004 inside rack','Dell','WXA 2000','B1');
insert into device (name, description, manufacturer, model, rack) values ('comp005', 'Device name comp005 inside rack','Dell','WXA 2000','B2');
insert into device (name, description, manufacturer, model, rack) values ('comp006', 'Device name comp006 inside rack','Dell','WXA 2000','B3');
insert into device (name, description, manufacturer, model, rack) values ('comp007', 'Device name comp007 inside rack','Dell','WXA 2000','B4');
insert into device (name, description, manufacturer, model, rack) values ('comp008', 'Device name comp008 inside rack','Dell','WXA 2000','C1');
insert into device (name, description, manufacturer, model, rack) values ('comp009', 'Device name comp009 inside rack','Dell','WXA 2000','C2');
insert into device (name, description, manufacturer, model, rack) values ('comp010', 'Device name comp010 inside rack','Dell','WXA 2000','C3');
insert into device (name, description, manufacturer, model, rack) values ('comp011', 'Device name comp011 inside rack','Dell','WXA 2000','C4');
insert into device (name, description, manufacturer, model, rack) values ('comp012', 'Device name comp012 inside rack','Dell','WXA 2000','C5');

# --- !Downs

drop table if exists device;


