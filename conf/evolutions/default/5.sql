# --- !Ups

create table groups (
  id        		long         not null primary key AUTO_INCREMENT,
  name      		varchar(255) not null,
  description		varchar(255) not null,
  group_type		varchar(255) not null,
  feature_packs		varchar(255) not null
);

insert into groups (name, description, group_type, feature_packs) values ('A0', 'All devices in rack A0','RuleBaseGroup','feature_pack_001');
insert into groups (name, description, group_type, feature_packs) values ('A1', 'All devices in rack A1','RuleBaseGroup','feature_pack_002');
insert into groups (name, description, group_type, feature_packs) values ('A2', 'All devices in rack A2','RuleBaseGroup','feature_pack_003');
insert into groups (name, description, group_type, feature_packs) values ('B1', 'All devices in rack B1','RuleBaseGroup','feature_pack_004');
insert into groups (name, description, group_type, feature_packs) values ('B2', 'All devices in rack B2','RuleBaseGroup','feature_pack_005');

# --- !Downs

drop table if exists groups;
